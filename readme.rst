###################
Koperasi Projects
###################

Project ini adalah untuk perusahaan Koperasi

*******************
Server Requirements
*******************

PHP version 7.0 or newer is recommended.


*******
License
*******

Lisensi dan Hak Cipta ASTAMA Technologi Semarang`_.

*********
Author
*********

-  Abu Umar, S. Kom.

Report security issues to our `Some Known Bugs <mailto:abuumarsg@gmail.com>`_.
thank you.
