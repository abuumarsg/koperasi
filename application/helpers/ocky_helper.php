<?php

function decode_spesial($content){
  $ci = get_instance();
  $string = $content;
  $pattern = '#<script(.*?)>(.*?)</script>#is';
  $replacement = 'null';
  return preg_replace($pattern, $replacement, $string);
}

function rename_string_proses($string){
  if($string == 0){
    return "Belum di Tanggapi";
  }elseif($string == 1){
    return "Dalam Proses";
  }else{
    return "Sudah di Tanggapi";
  }
  
  
}

function cmb_dinamis_akses($name,$table,$field,$pk,$selected=null,$order=null){
  $ci = get_instance();
  $cmb = "<select name='$name' class='form-control'>";
  if($order){
      
      $ci->db->order_by($field,$order);
  }
  $data = $ci->db->where('is_programmer', '0');
  $data = $ci->db->get($table)->result();
  foreach ($data as $d){
      $cmb .="<option value='".$d->$pk."'";
      $cmb .= $selected==$d->$pk?" selected='selected'":'';
      $cmb .=">".  strtoupper($d->$field)."</option>";
  }
  $cmb .="</select>";
  return $cmb;
}


function batasi_kata($kalimat_lengkap, $jumlah_kata){
  $arr_str = explode(' ', $kalimat_lengkap);
  $arr_str = array_slice($arr_str, 0, $jumlah_kata );
  return implode(' ', $arr_str);
 }

function nama_kategori($id_kategori){
  $ci = get_instance();
  $query = "select nama_kategori_galeri from tbl_kategori_opendata where id = '$id_kategori'";
  $kat = $ci->db->query($query)->row();
  return $kat->nama_kategori_galeri;
}

?>
