<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="<?php echo $this->otherfunctions->companyProfile()['meta_description'];?>">
  <meta name="author" content="<?php echo $this->otherfunctions->companyProfile()['meta_author'];?>">
  <title><?php 
  echo $this->otherfunctions->titlePages($this->uri->segment(2));
  ?> Koperasi</title>
  <link rel="icon" href="<?php echo $this->otherfunctions->companyProfile()['fav_icon'];?>" type="image/png">
  <meta name="theme-color" content="#131c5b">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/bootstrap/dist/css/bootstrap.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/font-awesome/css/font-awesome.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/vendor/fontawesome5/css/all.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/Ionicons/css/ionicons.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/sweetalert2/dist/sweetalert2.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/dist/css/AdminLTE.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/plugins/iCheck/square/blue.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/plugins/viewerjs/dist/viewer.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/plugins/timepicker/bootstrap-timepicker.min.css');?>">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/morris.js/morris.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/jvectormap/jquery-jvectormap.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/bootstrap-daterangepicker/daterangepicker.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/datatables.net-bs/css/dataTables.bootstrap.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/dist/css/skins/_all-skins.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/select2/dist/css/select2.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/plugins/pace/pace.css');?>">
  <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
  <link href="<?php echo base_url('asset/vendor/overhang/dist/overhang.min.css');?>" rel="stylesheet" media="all">
  <link href="<?php echo base_url('asset/vendor/emoji/dist/emoji.min.css');?>" rel="stylesheet" media="all">
  <link href="<?php echo base_url('asset/vendor/toastr/toastr.min.css');?>" rel="stylesheet" media="all">
  <link href="<?php echo base_url('asset/vendor/iconpicker/dist/css/fontawesome-iconpicker.min.css');?>" rel="stylesheet">
  <link href="<?php echo base_url('asset/plugins/JsTree/dist/themes/default/style.min.css');?>" rel="stylesheet">
  <style type="text/css">
    .fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
  }
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.loader{
  margin-top: -20px;
  margin-bottom: 50px;
}
.loader span{
  width:30px;
  height:30px;
  border-radius:50%;
  display:inline-block;
  position:absolute;
  left:50%;
  margin-left:-10px;
  -webkit-animation:3s infinite linear;
  -moz-animation:3s infinite linear;
  -o-animation:3s infinite linear;
  
}
.toast-top-center {top: 15%;margin: 0 auto;}

.loader span:nth-child(2){
  background:#009dff;
  -webkit-animation:kiri 2s infinite linear;
  -moz-animation:kiri 2s infinite linear;
  -o-animation:kiri 2s infinite linear;
  
}
.loader span:nth-child(3){
  background:#F1C40F;
  z-index:100;
}
.loader span:nth-child(4){
  background:#2FCC71;
  -webkit-animation:kanan 2s infinite linear;
  -moz-animation:kanan 2s infinite linear;
  -o-animation:kanan 2s infinite linear;
}


@-webkit-keyframes kanan {
    0% {-webkit-transform:translateX(40px);
    }
   
  50%{-webkit-transform:translateX(-40px);
  }
  
  100%{-webkit-transform:translateX(40px);
  z-index:200;
  }
}
@-moz-keyframes kanan {
    0% {-moz-transform:translateX(40px);
    }
   
  50%{-moz-transform:translateX(-40px);
  }
  
  100%{-moz-transform:translateX(40px);
  z-index:200;
  }
}
@-o-keyframes kanan {
    0% {-o-transform:translateX(40px);
    }
   
  50%{-o-transform:translateX(-40px);
  }
  
  100%{-o-transform:translateX(40px);
  z-index:200;
  }
}




@-webkit-keyframes kiri {
     0% {-webkit-transform:translateX(-40px);
  z-index:200;
    }
  50%{-webkit-transform:translateX(40px);
  }
  100%{-webkit-transform:translateX(-40px);
  }
}

@-moz-keyframes kiri {
     0% {-moz-transform:translateX(-40px);
  z-index:200;
    }
  50%{-moz-transform:translateX(40px);
  }
  100%{-moz-transform:translateX(-40px);
  }
}
@-o-keyframes kiri {
     0% {-o-transform:translateX(-40px);
  z-index:200;
    }
  50%{-o-transform:translateX(40px);
  }
  100%{-o-transform:translateX(-40px);
  }
}
  </style>
</head>
<body class="hold-transition <?php echo $adm['skin'];?> sidebar-mini fixed" 
onload="set_interval()"
onmousemove="reset_interval()"
onclick="reset_interval()"
onkeypress="reset_interval()"
onscroll="reset_interval()">
  <div class="modal fade" id="swprog" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content text-center modal-danger">
        <div class="modal-header">
          <h4 class="modal-title">Progress Loading</h4>
        </div>
        <div class="modal-body">
          <div class="loader">
            <h1><label style="font-size: 25pt;" id="minutes">00</label><label style="font-size: 25pt;">:</label><label style="font-size: 25pt;" id="seconds">00</label></h1>
            <span></span>
            <span></span>
            <span></span>
          </div>
        <b style="color: yellow;">Jangan Refresh Halaman Ini</b>
      </div>
    </div>
  </div>
</div>
  <div class="wrapper">

    <header class="main-header">
      <a href="#" class="logo">
        <span class="logo-mini fnt"><b><img src="<?php echo $this->otherfunctions->companyProfile()['logo_only'];?>" width="40px"></b></span>
        <span class="logo-lg fnt"><img src="<?php echo $this->otherfunctions->companyProfile()['logo_only'];?>" width="40px">  <b><?php echo $this->otherfunctions->companyProfile()['title'];?></b></span>
      </a>
      <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li>
              <a class="tgl" id="date_time" style="font-size: 9pt"></a>
            </li>
        <?php
        if ($this->uri->segment(2) == "profile") {
          echo '<li class="dropdown user user-menu active">';
        }else{
          echo '<li class="dropdown user user-menu">';
        }
        ?>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <img src="<?php echo base_url($adm['foto']);?>" class="user-image" alt="User Image">
          <span class="hidden-xs"><?php echo $adm['nama'];?></span>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header">
            <img src="<?php echo base_url($adm['foto']);?>" class="img-circle" alt="User Image">

            <p>
              <?php echo $adm['nama'];?>
              <small>Terdaftar Sejak <?php echo date("d F Y", strtotime($adm['create']));?></small>
            </p>
          </li>
          <li class="user-body">
            <div class="row">
              <div class="col-xs-12 text-center">
                <a href="<?php echo base_url('auth/lock');?>" class="btn btn-info btn-block"><i class="fa fa-lock"></i> Lock Akun</a>
              </div>
            </div>
            <!-- /.row -->
          </li>
          <li class="user-footer">
            <div class="pull-left">
              <a href="<?php echo base_url('pages/profile');?>" class="btn  btn-success"><i class="fa fa-user"></i> Profile</a>
            </div>

            <div class="pull-right">
              <a href="<?php echo base_url('auth/logout');?>" class="btn  btn-danger">Log Out <i class="fas fa-sign-out-alt"></i></a>
            </div>
          </li>
        </ul>
      </li>
     
            </ul>
          </li>
      </li>
    </ul>
  </div>
</nav>
</header>
