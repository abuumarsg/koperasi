<footer id="footer-style-1">
	<div class="container">
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="widget">
				<div class="title">
					<h3><?=$data['nama']?></h3>
				</div>
				<p><?=$data['about']?></p>
				<div class="social-icons">
					<?php foreach ($data['kontak'] as $con){
						echo '<span><a data-toggle="tooltip" data-placement="bottom" title="'.$con->nama.'" href="'.$con->link.'"><i class="'.$con->icon.'"></i></a></span>';
					}
					?>

				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="widget">
				<div class="title">
					<h3>Twitter Feeds</h3>
				</div>
                  	<blockquote class="twitter-tweet">
					<p lang="in" dir="ltr">Opini ditulis oleh Paulus Mujiran, Ketua Pelaksana Yayasan Kesejahteraan Keluarga Soegijapranata Semarang 
					<a href="https://t.co/d8FRJqkkFC">https://t.co/d8FRJqkkFC</a></p>&mdash; tribunjateng.com (@tribunjateng) 
					<a href="https://twitter.com/tribunjateng/status/815753523082076160?ref_src=twsrc%5Etfw">January 2, 2017</a></blockquote> 
					<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="widget">
                <div class="title">
                      <h3>Jumlah Pengunjung</h3>
                </div>
                <div class="newsletter_widget">
					<img src="https://www.cutercounter.com/hits.php?id=gmqokck&nd=6&style=83" border="0" alt="website counter">
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="widget">
				<div class="title">
					<h3>Galeri</h3>
				</div>
				<ul class="footer_post">
					<?php
						$i=0;
						foreach($data['gambar_footer'] as $gf) {
							$i++;
							if($i==10) {
								break;
							}else{
								echo '<li><a href="'.base_url('publik/detail_galeri/'.$this->codegenerator->encryptChar($gf->kategori)).'"><img class="img-rounded" src="'.base_url($gf->gambar).'" height="50px" alt="'.$gf->judul.'"></a></li>';
							}
						}
					?>
				</ul>
			</div>
		</div>
	</div>
</footer>
<style>
    #id{
        background-color:deepskyblue;
    }
</style>
<div id="copyrights">
	<div class="container">
		<div class="col-lg-5 col-md-6 col-sm-12">
			<div class="copyright-text">
				<p style="color:black">Copyright © 2019 - <?=date("Y");?> | <?=$data['nama']?></p>
			</div>
		</div>
		<div class="col-lg-7 col-md-6 col-sm-12 clearfix">
			<div class="footer-menu">

			</div>
		</div>
	</div>
</div>
<div class="dmtop">Scroll to Top</div>

<!-- Main Scripts-->
<script src="<?=base_url()?>/asset/publik/js/jquery.js"></script>
<script src="<?=base_url()?>/asset/publik/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>/asset/publik/js/menu.js"></script>
<script src="<?=base_url()?>/asset/publik/js/owl.carousel.min.js"></script>
<script src="<?=base_url()?>/asset/publik/js/jquery.parallax-1.1.3.js"></script>
<script src="<?=base_url()?>/asset/publik/js/jquery.simple-text-rotator.js"></script>
<script src="<?=base_url()?>/asset/publik/js/wow.min.js"></script>
<script src="<?=base_url()?>/asset/publik/js/jquery.fitvids.js"></script>
<script src="<?=base_url()?>/asset/publik/js/custom.js"></script>
<script src="<?=base_url('asset/ajax.js');?>"></script>
<script src="<?=base_url('asset/vendor/overhang/dist/overhang.min.js');?>"></script>
<script src="<?=base_url('asset/plugins/pace/pace.min.js');?>"></script>

<script src="<?=base_url()?>/asset/publik/js/jquery.isotope.min.js"></script>
<script src="<?=base_url()?>/asset/publik/js/custom-portfolio.js"></script>
<script type="text/javascript" src="<?=base_url()?>/asset/publik/rs-plugin/js/jquery.themepunch.plugins.min.js">
</script>
<script type="text/javascript" src="<?=base_url()?>/asset/publik/rs-plugin/js/jquery.themepunch.revolution.min.js">
</script>
<script type="text/javascript">
	jQuery(document).ready(function () {
		jQuery('.fullwidthbanner').revolution({
			delay: 4000,
			startwidth: 1080,
			startheight: 500,
			hideThumbs: 10
		});
	});

</script>
<script src="<?=base_url()?>/asset/publik/royalslider/jquery.easing-1.3.js"></script>
<script src="<?=base_url()?>/asset/publik/royalslider/jquery.royalslider.min.js"></script>
<script src="<?=base_url()?>/asset/publik/js/jquery.prettyPhoto.js"></script>
<script>
	jQuery('a[data-gal]').each(function() {
		jQuery(this).attr('rel', jQuery(this).data('gal'));
	});  	
		jQuery("a[data-gal^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',slideshow:false,overlay_gallery: false,theme:'light_square',social_tools:false,deeplinking:false});
</script>
<script>
	jQuery(document).ready(function ($) {
		var rsi = $('#slider-in-laptop').royalSlider({
			autoHeight: false,
			arrowsNav: false,
			fadeinLoadedSlide: false,
			controlNavigationSpacing: 0,
			controlNavigation: 'bullets',
			imageScaleMode: 'fill',
			imageAlignCenter: true,
			loop: false,
			loopRewind: false,
			numImagesToPreload: 6,
			keyboardNavEnabled: true,
			autoScaleSlider: true,
			autoScaleSliderWidth: 486,
			autoScaleSliderHeight: 315,

			/* size of all images http://help.dimsemenov.com/kb/royalslider-jquery-plugin-faq/adding-width-and-height-properties-to-images */
			imgWidth: 792,
			imgHeight: 479

		}).data('royalSlider');
		$('#slider-next').click(function () {
			rsi.next();
		});
		$('#slider-prev').click(function () {
			rsi.prev();
		});
	});
</script>
<script>
	(function ($) {
		"use strict";
		$(document).ready(function () {
			$("body").fitVids();
		});
	})(jQuery);
</script>
</body>
</html>
