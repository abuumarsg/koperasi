<!DOCTYPE html>
<html lang="en">
<head>
  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
  <title><?php 
  echo $this->otherfunctions->titlePages($this->uri->segment(2));
  ?><?=$data['nama']?> </title>
  <link rel="icon" href="<?php echo $this->otherfunctions->companyClientProfile()['favicon'];?>" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Bootstrap Styles -->
  <link href="<?=base_url()?>/asset/publik/css/bootstrap.css" rel="stylesheet">
  
  <!-- Styles -->
  <link href="<?=base_url()?>/asset/publik/style.css" rel="stylesheet">
  
  <!-- Carousel Slider -->
  <link href="<?php echo base_url('asset/publik/css/owl-carousel.css');?>" rel="stylesheet">
  
  <!-- CSS Animations -->
  <link href="<?=base_url()?>/asset/publik/css/animate.min.css" rel="stylesheet">
  <link href="<?=base_url()?>/asset/publik/css/prettyPhoto.css" rel="stylesheet">
  
  <!-- Google Fonts -->
  <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Lato:400,300,400italic,300italic,700,700italic,900' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Exo:400,300,600,500,400italic,700italic,800,900' rel='stylesheet' type='text/css'>

  <!-- SLIDER ROYAL CSS SETTINGS -->
  <link href="<?=base_url()?>/asset/publik/royalslider/royalslider.css" rel="stylesheet">
  <link href="<?=base_url()?>/asset/publik/royalslider/skins/default-inverted/rs-default-inverted.css" rel="stylesheet">
  
  <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>/asset/publik/rs-plugin/css/settings.css" media="screen" />
  <link href="<?=base_url('asset/vendor/overhang/dist/overhang.min.css');?>" rel="stylesheet" media="all">
  <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/font-awesome/css/font-awesome.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('asset/vendor/fontawesome5/css/all.min.css');?>">
  
  <link rel="stylesheet" id="switcher-css" type="text/css" href="<?=base_url()?>asset/publik/switcher/css/blue.css" media="all" />
        
  <!-- Support for HTML5 -->
  <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <!-- Enable media queries on older bgeneral_rowsers -->
  <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>  <![endif]-->

</head>