<body>
	<div id="topbar" class="clearfix">
    	<div class="container">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="social-icons">
                    <?php foreach ($data['kontak'] as $con){
                        echo '<span><a href="'.$con->link.'" data-toggle="tooltip" data-placement="bottom" title="'.$con->nama.'" target="_blank"><i class="'.$con->icon.'"></i></a></span>';
                        }
                    ?>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            	<div class="callus">
                	<span class="topbar-email"><i class="fa fa-envelope"></i> <a href="mailto:<?=$data['email']?>"> <?=$data['email']?></a></span>
                    <span class="topbar-phone"><i class="fa fa-phone"></i> <?=$data['no_hp']?></span>
                </div>
            </div>
        </div>
    </div>
    <div id="wrapper" class="container">
    <header id="header-style-1">
		<div class="header-container">
			<nav class="navbar yamm navbar-default">
				<div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?=base_url()?>" class="navbar-brand">
                    <img widhth="70px" height="70px" src="<?=base_url($data['logo'])?>" alt="<?=$data['nama_kantor']?>" > <?=$data['nama']?>
                    </a>
        		</div>
				<div id="navbar-collapse-1" class="navbar-collapse collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <?php
                            $seg = $this->uri->segment(2);
                            echo $this->otherfunctions->getDrawMenuFront(0,$seg);
                        ?>
                    </ul>
				</div>		
            </nav>
		</div>
	</header>
</div>