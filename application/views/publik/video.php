    <section class="post-wrapper-top jt-shadow clearfix">
		<div class="container">
			<div class="col-lg-12">
				<h2>Galeri Video</h2>
                <ul class="breadcrumb pull-right">
                    <li><a href="index-2.html">Home</a></li>
                    <li>Video</li>
                </ul>
			</div>
		</div>
	</section>
	<section class="white-wrapper">
    	<div class="container">            
            <div id="boxed-portfolio" class="portfolio_wrapper padding-top">
            <?php
                foreach ($video as $v) {
                    $embed=$this->otherfunctions->getLinkYouTube()['embed'];
                    echo '<div class="col-md-4 logo">
                    <div class="blog-carousel" style="height:400px;">
                        <div class="portfolio_item">
                            <h3>'.$v->judul.'</h3>
                            <div class="entry">
                            <iframe width="560" height="315" src="'.$embed.$v->code_embed.'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <p>'.$v->keterangan.'</p>
                        </div>
                    </div>
                </div>';
                }
            ?>
            </div>
            <div class="clearfix"></div>
            <hr>
            <div class="pagination_wrapper">
                <div class="row">
                    <?php if(isset($paginasi) && $total > $limit) { ?>
                        <div class="paginasi col-md-12 text-center">
                            <?php  echo $paginasi; ?>
                            <div class="clearfix"></div>
                        </div>
                    <?php } ?>
                </div>
            </div>
		</div>
    </section>
  