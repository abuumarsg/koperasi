<section class="post-wrapper-top jt-shadow clearfix">
    <div class="container">
        <div class="col-lg-12">
            <h2>Visi Dan Misi</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="index-2.html">Home</a></li>
                <li>Visi Misi</li>
            </ul>
        </div>
    </div>
</section>
<section class="white-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="widget margin-top">
                    <div id="aboutslider" class="flexslider clearfix">
                            <img src="<?=base_url($comp['gambar_visi'])?>" class="img-responsive" alt="">
                            <div class="aboutslider-shadow">
                            <span class="s1"></span>
                            <h3>Moto</h3>
                            <?=$comp['moto'];?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="widget">
                    <h3>Visi</h3>
                    <?=$comp['visi'];?>
                    <h3>Misi</h3>
                    <?=$comp['misi'];?>
                    <div class="clearfix"></div>
                    <!-- <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 first">
                            <ul class="check">
                                <li>Custom Shortcodes</li>
                                <li>Visual Page Builder</li>
                                <li>Unlimited Shortcodes</li>
                                <li>Responsive Theme</li>
                                <li>Tons of Layouts</li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <ul class="check">
                                <li>Font Awesome Icons</li>
                                <li>Pre-Defined Colors</li>
                                <li>AJAX Transitions</li>
                                <li>High Quality Support</li>
                                <li>Unlimited Options</li>
                            </ul>    
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 last">
                            <ul class="check">
                                <li>WooCommerce Layouts</li>
                                <li>Pre-Defined Fonts</li>
                                <li>Style Changers</li>
                                <li>Footer Styles</li>
                                <li>Header Styles</li>
                            </ul>
                        </div> 
                    </div>    -->
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3>Sejarah</h3>
            </div>
        </div> -->
    </div>
</section>

	