	<section class="post-wrapper-top jt-shadow clearfix">
		<div class="container">
			<div class="col-lg-12">
				<h2>Kontak Kami</h2>
                <ul class="breadcrumb pull-right">
                    <li><a href="<?=base_url()?>">Home</a></li>
                    <li>Kontak Kami</li>
                </ul>
			</div>
		</div>
	</section>
    <section class="white-wrapper nopadding">
			<div id="content" class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<div id="map">
					<iframe src="<?=$this->otherfunctions->companyClientProfile()['maps'];?>" width="800" max-width="800" height="550" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
			<div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<div class="widget">
					<h3>Kontak Kami</h3>
					<ul class="nav nav-tabs nav-stacked">
					<?php
						foreach ($kontak as $kon) {
							echo '<li><a data-toggle="tooltip" target="BLANK_" data-placement="bottom" title="'.$kon->nama.'"  href="'.$kon->link.'"><i class="'.$kon->icon.'"></i>  '.$kon->nama.'</a></li>';
						}
					?>
					</ul>
				</div>
			</div>
        <div class="clearfix"></div>
        <div class="container">
        	<div class="contact_form">
				<div id="message"></div>
					<?php 
						if (!empty($this->session->flashdata('sukses'))){
							echo '<div class="alert alert-success">'.$this->session->flashdata('sukses').'</div>';
						}elseif(!empty($this->session->flashdata('gagal'))){
							echo '<div class="alert alert-danger">'.$this->session->flashdata('gagal').'</div>';
						}
             		?>
					 <h3>Silahkan mengisi Buku Tamu Berikut :</h3>
              		<?php echo form_open_multipart('publik/add_message',array('class'=>'form-horizontal'));?>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<input type="text" name="nama" id="contact_name" class="form-control" placeholder="Name"> 
							<input type="email" name="email" id="email_address" class="form-control" placeholder="Email Address"> 
							<input type="text" name="website" id="website" class="form-control" placeholder="Website"> 
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<input type="text" name="subject" id="subject" class="form-control" placeholder="Subject"> 
							<textarea class="form-control" name="pesan" id="comments" rows="6" placeholder="Message"></textarea>
							<button type="submit" class="btn btn-lg btn-primary pull-right">Isi Buku Tamu</button>
						</div>
           			<?php echo form_close(); ?>
				</div>
				<div class="clearfix"></div>
				<div class="row padding-top margin-top">
					<div class="contact_details">
						<div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
							<div class="text-center">
								<div class="wow swing">
									<div class="contact-icon">
										<a href="#" class=""> <i class="fa fa-map-marker fa-3x"></i> </a>
									</div>
									<p><?=$data['alamat']?></p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
							<div class="text-center">
								<div class="wow swing">
									<div class="contact-icon">
										<a href="#" class=""> <i class="fa fa-phone fa-3x"></i> </a>
									</div>
									<p><strong>Phone : </strong> <?=$data['no_hp']?></p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
							<div class="text-center">
								<div class="wow swing">
									<div class="contact-icon">
										<a href="#" class=""> <i class="fa fa-envelope fa-3x"></i> </a>
									</div>
									<p><strong>Email : </strong> <?=$data['email']?></p>
								</div>
							</div>
						</div>                 
					</div>
				</div>
			</div><br><br>
        </div>
    </section>



