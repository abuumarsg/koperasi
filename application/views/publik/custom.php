<section class="post-wrapper-top jt-shadow clearfix">
	<div class="container">
		<div class="col-lg-12">
			<h2><?=$nama?></h2>
			<ul class="breadcrumb pull-right">
				<li><a href="<?=base_url()?>">Home</a></li>
				<li><?=$nama?></li>
			</ul>
		</div>
	</div>
</section>

<section class="blog-wrapper">
	<div class="container">
		<div id="content" class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<div class="row">
				<div class="blog-masonry">
					<div class="col-lg-12 first">
						<div class="blog-carousel">
							<div class="entry">
							</div>
							<div class="blog-carousel-header">
								<h3><?=$nama?></h3>
								<div class="blog-carousel-meta">
								</div>
							</div>
							<div class="blog-carousel-desc">
								<?=html_entity_decode($isi);?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		</div>
	</div>
</section>
