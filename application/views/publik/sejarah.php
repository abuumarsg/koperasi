<section class="post-wrapper-top jt-shadow clearfix">
    <div class="container">
        <div class="col-lg-12">
            <h2>Sejarah</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="index-2.html">Home</a></li>
                <li>Sejarah</li>
            </ul>
        </div>
    </div>
</section>
<section class="white-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="widget margin-top">
                    <div id="aboutslider" class="flexslider clearfix">
                            <img src="<?=base_url($comp['gambar_sejarah'])?>" class="img-responsive" alt="">
                        <div class="aboutslider-shadow">
                            <span class="s1"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="widget">
                    <h3>Sejarah</h3>
                    <?=$comp['sejarah'];?>
                    <div class="clearfix"></div> 
                </div>
            </div>
        </div>
    </div>
</section>

	