    <section class="slider-wrapper fullwidthbanner-container">
        <div class="tp-banner-container">
            <div class="fullwidthbanner" >
                <ul>
                    <?php foreach ($banner as $bnr){?>
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                        <img src="<?=base_url($bnr->gambar)?>"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                        
                    </li>
                    <?php }?>
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
			<div class="slider-shadow"></div>
        </div>
    </section>
  	</div>
	<section class="grey-wrapper">
    	<div class="container">
        	<div class="general-title">
            	<h2>Berita Terbaru</h2>
                <hr>
                <p class="lead">We provide best solution for your business</p>
            </div><!-- end general title -->
            
            <div class="blog-masonry">
                <?php foreach ($berita as $brt){?>
                <div class="col-lg-4">
                    <div class="blog-carousel">
                        <div class="entry">
                            <img src="<?=base_url($brt->gambar)?>" alt="<?=$brt->judul?>" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="blog-single-sidebar.html"><i class="fa fa-link"></i></a>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            <div class="post-type">
                                <i class="fa fa-picture-o"></i>
                            </div><!-- end pull-right -->
                        </div><!-- end entry -->
                        <div class="blog-carousel-header">
                            <h3><a title="" href="blog-single-sidebar.html"><?=$brt->judul?></a></h3>
                            <div class="blog-carousel-meta">
                                <span><i class="fa fa-calendar"></i> <?=$brt->tgl_posting?></span>
                                <!-- <span><i class="fa fa-comment"></i> <a href="#">03 Comments</a></span> -->
                                <!-- <span><i class="fa fa-eye"></i> <a href="#">84 Views</a></span> -->
                            </div><!-- end blog-carousel-meta -->
                        </div><!-- end blog-carousel-header -->
                        <div class="blog-carousel-desc">
                            <p><?=$brt->isi?> </p>
                        </div><!-- end blog-carousel-desc -->
                    </div><!-- end blog-carousel -->
                </div><!-- end col-lg-4 -->
                <?php }?>
    
                <div class="col-lg-4">
                    <div class="blog-carousel">
                        <div class="entry">
                            <img src="<?=base_url()?>/asset/publik/demos/blog_02.png" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="blog-single-sidebar.html"><i class="fa fa-link"></i></a>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            <div class="post-type">
                                <i class="fa fa-picture-o"></i>
                            </div><!-- end pull-right -->
                        </div><!-- end entry -->
                        <div class="blog-carousel-header">
                            <h3><a title="" href="blog-single-sidebar.html">Did you see my new Mac?</a></h3>
                            <div class="blog-carousel-meta">
                                <span><i class="fa fa-calendar"></i> April 01, 2014</span>
                                <span><i class="fa fa-comment"></i> <a href="#">03 Comments</a></span>
                                <span><i class="fa fa-eye"></i> <a href="#">84 Views</a></span>
                            </div><!-- end blog-carousel-meta -->
                        </div><!-- end blog-carousel-header -->
                        <div class="blog-carousel-desc">
                            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel faucibus nunc, et venenatis magna. In hac habitasse platea dictumst. </p>
                        </div><!-- end blog-carousel-desc -->
                    </div><!-- end blog-carousel -->
                </div><!-- end col-lg-4 -->
                
                <div class="col-lg-4 last">
                    <div class="blog-carousel">
                        <div class="entry">
                            <img src="<?=base_url()?>/asset/publik/demos/blog_03.png" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="blog-single-sidebar.html"><i class="fa fa-link"></i></a>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            <div class="post-type">
                                <i class="fa fa-picture-o"></i>
                            </div><!-- end pull-right -->
                        </div><!-- end entry -->
                        <div class="blog-carousel-header">
                            <h3><a title="" href="blog-single-sidebar.html">Coke Mock-up Designs</a></h3>
                            <div class="blog-carousel-meta">
                                <span><i class="fa fa-calendar"></i> April 01, 2014</span>
                                <span><i class="fa fa-comment"></i> <a href="#">03 Comments</a></span>
                                <span><i class="fa fa-eye"></i> <a href="#">84 Views</a></span>
                            </div><!-- end blog-carousel-meta -->
                        </div><!-- end blog-carousel-header -->
                        <div class="blog-carousel-desc">
                            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel faucibus nunc, et venenatis magna. In hac habitasse platea dictumst. </p>
                        </div><!-- end blog-carousel-desc -->
                    </div><!-- end blog-carousel -->
                </div><!-- end col-lg-4 -->
    
                <div class="col-lg-4 first">
                    <div class="blog-carousel">
                        <div class="entry">
                            <img src="<?=base_url()?>/asset/publik/demos/blog_03.png" alt="" class="img-responsive">
                            <div class="post-type">
                                    <i class="fa fa-picture-o"></i>
                            </div><!-- end pull-right -->
                        </div><!-- end entry -->
                        <div class="blog-carousel-header">
                            <h3><a title="" href="blog-single-sidebar.html">A Video Post Type</a></h3>
                            <div class="blog-carousel-meta">
                                <span><i class="fa fa-calendar"></i> April 01, 2014</span>
                                <span><i class="fa fa-comment"></i> <a href="#">03 Comments</a></span>
                                <span><i class="fa fa-eye"></i> <a href="#">84 Views</a></span>
                            </div><!-- end blog-carousel-meta -->
                        </div><!-- end blog-carousel-header -->
                        <div class="blog-carousel-desc">
                            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel faucibus nunc, et venenatis magna. In hac habitasse platea dictumst. </p>
                        </div><!-- end blog-carousel-desc -->
                    </div><!-- end blog-carousel -->
                </div><!-- end col-lg-4 -->
    
                <div class="col-lg-4">
                    <div class="blog-carousel">
                        <div class="entry">
                                <img src="<?=base_url()?>/asset/publik/demos/blog_03.png" alt="" class="img-responsive">
							 <div class="post-type">
                                    <i class="fa fa-picture-o"></i>
                            </div><!-- end pull-right -->
                        </div><!-- end entry -->
                        <div class="blog-carousel-header">
                            <h3><a title="" href="blog-single-sidebar.html">Here We Go Again</a></h3>
                            <div class="blog-carousel-meta">
                                <span><i class="fa fa-calendar"></i> April 01, 2014</span>
                                <span><i class="fa fa-comment"></i> <a href="#">03 Comments</a></span>
                                <span><i class="fa fa-eye"></i> <a href="#">84 Views</a></span>
                            </div><!-- end blog-carousel-meta -->
                        </div><!-- end blog-carousel-header -->
                        <div class="blog-carousel-desc">
                            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel faucibus nunc, et venenatis magna. In hac habitasse platea dictumst. </p>
                        </div><!-- end blog-carousel-desc -->
                    </div><!-- end blog-carousel -->
                </div><!-- end col-lg-4 -->
                
                <div class="col-lg-4 last">
                    <div class="blog-carousel">
                        <div class="entry">
                            <img src="<?=base_url()?>/asset/publik/demos/blog_04.png" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="blog-single-sidebar.html"><i class="fa fa-link"></i></a>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            <div class="post-type">
                                <i class="fa fa-picture-o"></i>
                            </div><!-- end pull-right -->
                        </div><!-- end entry -->
                        <div class="blog-carousel-header">
                            <h3><a title="" href="blog-single-sidebar.html">Standard Blog Post With Image</a></h3>
                            <div class="blog-carousel-meta">
                                <span><i class="fa fa-calendar"></i> April 01, 2014</span>
                                <span><i class="fa fa-comment"></i> <a href="#">03 Comments</a></span>
                                <span><i class="fa fa-eye"></i> <a href="#">84 Views</a></span>
                            </div><!-- end blog-carousel-meta -->
                        </div><!-- end blog-carousel-header -->
                        <div class="blog-carousel-desc">
                            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel faucibus nunc, et venenatis magna. In hac habitasse platea dictumst. </p>
                        </div><!-- end blog-carousel-desc -->
                    </div><!-- end blog-carousel -->
                </div><!-- end col-lg-4 --> 
            </div><!-- end blog-masonry -->   
        </div><!-- end container -->
    </section><!-- end gray-wrapper -->
    
	<section class="white-wrapper jt-shadow">
    	<div class="container">
			<div class="calloutbox bggrey">
            	<div class="col-lg-10">
            	<h2>Silakan Mengisi Buku Tamu Kami</h2>
                <p></p>
                </div>
                <div class="col-lg-2">
                <a class="btn pull-right btn-dark btn-lg margin-top" href="#">Isi Buku Tamu</a>
                </div>
            </div><!-- end messagebox -->
		</div><!-- end container -->
    </section><!-- end white-wrapper -->
       
