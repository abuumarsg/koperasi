<section class="post-wrapper-top jt-shadow clearfix">
	<div class="container">
		<div class="col-lg-12">
			<h2>Daftar Berita</h2>
			<ul class="breadcrumb pull-right">
				<li><a href="<?=base_url()?>">Home</a></li>
				<li>Daftar Berita</li>
			</ul>
		</div>
	</div>
</section>
<section class="blog-wrapper">
  <div class="container">
      <div id="content" class="col-lg-10">
            <div class="row">
               <div class="blog-masonry">
                 <?php 
                 foreach ($berita as $brt){ ?>
                    <div class="col-lg-4 ">
                        <div class="blog-carousel" style="height:550px;">
                            <div class="entry">
                                <div class="flexslider">
                                    <ul class="slides">
                                      <img src="<?=base_url($brt->gambar)?>" alt="<?=$brt->judul?>" class="img-responsive">
                                </div>
                                <div class="post-type">
                                    <i class="fa fa-camera"></i>
                                </div>
                            </div>
                            <div class="blog-carousel-header">
                                <h3><a title="" href="<?php echo base_url('publik/read_berita/'.$this->codegenerator->encryptChar($brt->id_berita));?>"><?=decode_spesial($brt->judul)?></a></h3>
                                <div class="blog-carousel-meta">
                                    <span><i class="fa fa-calendar"></i> <?=$this->formatter->getDateMonthFormatUser($brt->tgl_posting)?></span>
                                    <span><i class="fa fa-eye"></i> <a href="#"><?=$brt->counting_reader?> Views</a></span>
                                </div>
                            </div>
                            <div class="blog-carousel-desc">
                                <p>
                                <?php
                                    echo batasi_kata(decode_spesial($brt->isi),40);
                                ?>
                                &nbsp;
                                <a href="<?php echo base_url('publik/read_berita/'.$this->codegenerator->encryptChar($brt->id_berita));?>"> selengkapnya...</a>
                              </p>
                            </div>
                        </div>
                    </div>
                  <?php } ?>
                </div>
                <div class="clearfix"></div>
                <hr>
                <div class="pagination_wrapper">
                    <div class="row">
                        <?php if(isset($paginasi) && $total > $limit) { ?>
                            <div class="paginasi col-md-12 text-center">
                                <?php  echo $paginasi; ?>
                                <div class="clearfix"></div>
                            </div>
                            <?php } ?>
                    </div>
                    <!-- Pagination Normal -->
                    <!-- <ul class="pagination">
                        <li><a href="#">«</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">»</a></li>
                    </ul> -->
                </div><!-- end pagination_wrapper -->

          </div><!-- end row -->
        </div><!-- end content -->


         <!-- sidebar mulai dari sini ya-->

         <div id="sidebar" class="col-lg-2">
           <div class="doc">
               <div class="widget">
                   <h3>Arsip Berita</h3>
                   <div id="accordion-first" class="clearfix">
                       <div class="accordion" id="accordion4">
                            <?php
                            foreach ($tgl_berita as $tanggal){
                                $tgl_val=$this->model_data->jumlahTanggalBerita($tanggal->bulan,$tanggal->tahun);
                                if($tgl_val->num_rows()>0){ ?>
                                <div class="accordion-group">
                                	<div class="accordion-heading">
                                		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4"
                                			href="#<?=$tanggal->bulan?>">
                                			<em class="fa fa-plus icon-fixed-width"></em><?=$tanggal->bulan?>
                                			<?=$tanggal->tahun?></a>
                                	</div>
                                	<div id="<?=$tanggal->bulan?>" class="accordion-body collapse">
                                		<div class="accordion-inner">
                                			<ul>
                                				<?php foreach ($tgl_val->result() as $berita) { ?>
                                				<li>
                                                    <a href="<?php echo base_url('publik/read_berita/'.$this->codegenerator->encryptChar($berita->id_berita));?>"> <?=batasi_kata($berita->judul,3)?></a>
                                				</li>
                                				<?php } ?>
                                			</ul>
                                		</div>
                                	</div>
                                </div>
                            <?php }} ?>
                       </div>
                   </div>
               </div>
           </div>
        </div>
    </div>
</section>
