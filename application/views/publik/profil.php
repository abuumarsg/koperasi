
	
	<section class="post-wrapper-top jt-shadow clearfix">
		<div class="container">
			<div class="col-lg-12">
				<h2>Profil</h2>
                <ul class="breadcrumb pull-right">
                    <li><a href="index-2.html">Home</a></li>
                    <li>Visi Misi</li>
                </ul>
			</div>
		</div>
	</section><!-- end post-wrapper-top -->

	<section class="white-wrapper">
		<div class="container">
        	<div class="row">
            	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="widget margin-top">
                        <div id="aboutslider" class="flexslider clearfix">
                            
                                <img src="<?=base_url()?>/asset/publik/demos/about_mini_01.jpg" class="img-responsive" alt="">
                              
                            
                            <div class="aboutslider-shadow">
                                <span class="s1"></span>
                            </div>
                        </div><!-- end slider -->
                    </div><!-- end widget -->
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="widget">
                        <h3>WHAT WE CAN OFFER YOU?</h3>
                        <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringill torquent per conubia nostra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                        
                        <p>Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringill torquent per conubia nostra.</p>
                        <div class="clearfix"></div>
                        <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 first">
                            <ul class="check">
                                <li>Custom Shortcodes</li>
                                <li>Visual Page Builder</li>
                                <li>Unlimited Shortcodes</li>
                                <li>Responsive Theme</li>
                                <li>Tons of Layouts</li>
                            </ul><!-- end check -->
                        </div><!-- end col-lg-4 -->
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <ul class="check">
                                <li>Font Awesome Icons</li>
                                <li>Pre-Defined Colors</li>
                                <li>AJAX Transitions</li>
                                <li>High Quality Support</li>
                                <li>Unlimited Options</li>
                            </ul><!-- end check -->    
                        </div><!-- end col-lg-4 -->
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 last">
                            <ul class="check">
                                <li>WooCommerce Layouts</li>
                                <li>Pre-Defined Fonts</li>
                                <li>Style Changers</li>
                                <li>Footer Styles</li>
                                <li>Header Styles</li>
                            </ul><!-- end check -->
                        </div><!-- end col-lg-4 --> 
                        </div><!-- end row -->      
                    </div><!-- end widget -->
                </div><!-- end col-lg-6 -->
			</div><!-- end row --><br>
		</div><!-- end container -->
	</section><!-- end postwrapper -->

	