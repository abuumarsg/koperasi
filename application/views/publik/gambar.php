    <section class="post-wrapper-top jt-shadow clearfix">
		<div class="container">
			<div class="col-lg-12">
				<h2>Galeri Gambar</h2>
                <ul class="breadcrumb pull-right">
                    <li><a href="index-2.html">Home</a></li>
                    <li>Gambar</li>
                </ul>
			</div>
		</div>
	</section>
	<section class="white-wrapper">
    	<div class="container">            
            <div id="boxed-portfolio" class="portfolio_wrapper padding-top">
            <?php foreach ($gambar as $g){
                echo '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 logo">
					<div class="portfolio_item">
                        <div class="entry">
                            <img src="'.base_url($g->gambar).'" alt="'.$g->judul.'" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st btn btn-default" rel="bookmark" href="'.base_url('publik/detail_galeri/'.$this->codegenerator->encryptChar($g->kategori)).'">View Detail</a>
                                    <h3>'.$g->nama_kategori.'</h3>
                                    <p>'.$g->judul.'</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
            }?>
            </div>
            <div class="clearfix"></div>
            <hr>
            <div class="pagination_wrapper">
                <div class="row">
                    <?php if(isset($paginasi) && $total > $limit) { ?>
                        <div class="paginasi col-md-12 text-center">
                            <?php  echo $paginasi; ?>
                            <div class="clearfix"></div>
                        </div>
                    <?php } ?>
                </div>
            </div>
		</div>
    </section>
  