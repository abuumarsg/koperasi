<section class="post-wrapper-top jt-shadow clearfix">
	<div class="container">
		<div class="col-lg-12">
			<h2>Unduhan</h2>
			<ul class="breadcrumb pull-right">
				<li><a href="index-2.html">Home</a></li>
				<li>Unduhan</li>
			</ul>
		</div>
	</div>
</section>
<section class="blog-wrapper">
	<div class="container">
		<div id="content" class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<div class="row">
				<div class="blog-masonry">
					<div class="col-lg-12">
						<div class="doc">
							<h3>Unduh Dokumen</h3>
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>Nama</th>
										<th>File</th>
										<th>Deskripsi</th>
										<th>Unduh</th>
									</tr>
								</thead>
								<tbody>
									<?php
                                    $no=1;
                                    foreach ($dokumen as $d){
                                    echo '<tr>
                                      <td>'.$no.'</td>
                                      <td>'.$d->nama.'</td>
                                      <td>'.str_replace('asset/upload/file/','',$d->file).'</td>
                                      <td>'.$d->deskripsi.'</td>
                                      <td><a href="'.base_url('global_control/file_download/'.$this->codegenerator->encryptChar($d->file)).'"><i class="fa fa-download"></i></a></td>
                                    </tr>';
                                    $no++;
                                    }
                                  ?>
								</tbody>
							</table>
						</div>
						<div class="clearfix"></div>
						<hr>
					</div>
				</div>
			</div>
		</div>
		<div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="widget">
				<h3>Kontak Kami</h3>
				<ul class="nav nav-tabs nav-stacked">
					<li><a href="#">Telepon : <?=$data['no_hp']?></a></li>
					<li><a href="#">Whatsapps : <?=$data['no_hp']?></a></li>
					<li><a href="#">Email : <?=$data['email']?></a></li>
				</ul>
			</div>
		</div>
	</div>
</section>
