    <section class="post-wrapper-top jt-shadow clearfix">
		<div class="container">
			<div class="col-lg-12">
				<h2>Galeri Gambar</h2>
                <ul class="breadcrumb pull-right">
                    <li><a href="index-2.html">Home</a></li>
                    <li>Gambar</li>
                </ul>
			</div>
		</div>
	</section>
	<section class="white-wrapper">
    	<div class="container">
            <div id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<span><h2>Detail Galeri Kategori <?=$kategori;?></h2></span>
                <div class="row">
                    <div class="blog-masonry">
                        <?php 
                        foreach ($gambar as $gb){ ?>
                            <div class="col-sm-4">
                                <div class="portfolio_item">
                                    <div class="entry">
                                        <img src="<?=base_url($gb->gambar)?>" alt="<?=$gb->judul?>" class="img-responsive">
                                        <div class="magnifier">
                                            <div class="buttons">
                                                <a href="<?=base_url($gb->gambar)?>" class="sf" title="" data-gal="prettyPhoto[product-gallery]"><span class="fa fa-search"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="blog-carousel-header">
                                        <h3><?=decode_spesial($gb->judul)?></h3>
                                        <div class="blog-carousel-meta">
                                            <span><i class="fa fa-calendar"></i> <?=$this->formatter->getDateMonthFormatUser($gb->tgl_posting)?></span>
                                            <!-- <span><i class="fa fa-eye"></i> <a href="#"><?=$gb->count_view?> Views</a></span> -->
                                        </div>
                                    </div>
                                    <div class="blog-carousel-desc">
                                        <p>
                                        <?php
                                            echo $gb->deskripsi;
                                        ?>
                                    </p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
            <hr>
                        <div class="pagination_wrapper">
                            <div class="row">
                                <?php if(isset($paginasi) && $total > $limit) { ?>
                                    <div class="paginasi col-md-12 text-center">
                                        <?php  echo $paginasi; ?>
                                        <div class="clearfix"></div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                </div>
            </div>
            <!-- <div class="clearfix"></div>
            <hr>
            <div class="pagination_wrapper">
                <div class="row">
                    <?php if(isset($paginasi) && $total > $limit) { ?>
                        <div class="paginasi col-md-12 text-center">
                            <?php  echo $paginasi; ?>
                            <div class="clearfix"></div>
                        </div>
                    <?php } ?>
                </div> -->
            </div>
		</div>
    </section>
  