<!DOCTYPE html>
<html>
<head>
  <title><?php 
  echo $this->otherfunctions->companyClientProfile()['name'];
  ?> - Company Profile </title>
  <link rel="icon" href="<?php echo $this->otherfunctions->companyClientProfile()['favicon'];?>" type="image/png">
<style>
body, html {
  height: 100%;
  margin: 0;
}

.bgimg {
  background-image: url('<?php echo base_url("asset/img/coming_soon.jpg");?>');
  height: 100%;
  background-position: center;
  background-size: cover;
  position: relative;
  color: white;
  font-family: "Courier New", Courier, monospace;
  font-size: 25px;
}

.toptop {
  position: absolute;
  height: 7px;
  top: 0;
  left: 50%;
  top: 10%;
  text-align: center;
  transform: translate(-50%, -50%);
}
.topleft {
  position: absolute;
  top: 0;
  left: 50%;
  top: 35%;
  text-align: center;
  transform: translate(-50%, -50%);
}

.bottomleft {
  position: absolute;
  bottom: 0;
  left: 16px;
}

.middle {
  position: absolute;
  top: 65%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}

hr {
  margin: auto;
  width: 40%;
}
</style>
</head>
<body>

<div class="bgimg">
  <div class="toptop">
    <img widhth="120px" height="120px" src="<?php echo $this->otherfunctions->companyClientProfile()['logo'];?>"></h2>
  </div>
  <div class="topleft">
    <h2><?php echo $this->otherfunctions->companyClientProfile()['name_office'];?></h2>
  </div>
  <div class="middle">
    <h1>COMING SOON</h1>
    <hr>
    <p>website under construction</p>
  </div>
  <div class="bottomleft">
    <p>By : <?php echo $this->otherfunctions->companyProfile()['name'];?></p>
  </div>
</div>

</body>
</html>