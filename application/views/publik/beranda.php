    <section class="slider-wrapper fullwidthbanner-container">
        <div class="tp-banner-container">
            <div class="fullwidthbanner" >
                <ul>
                    <?php foreach ($banner as $bnr){?>
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                        <img src="<?=base_url($bnr->gambar)?>"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                        
                    </li>
                    <?php }?>
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
			<div class="slider-shadow"></div>
        </div>
    </section>
  	</div>
	<section class="grey-wrapper">
    	<div class="container">
        	<div class="general-title">
            	<h2>Berita Terbaru</h2>
                <hr>
            </div>
            <div class="blog-masonry">
                <?php 
					$i=0;
                    foreach ($berita as $brt){
                        $i++;
						if($i==7) {
							break;
						}else{
                ?>
                <div class="col-lg-4">
                    <div class="blog-carousel" style="height:550px;">
                        <div class="entry">
                            <img src="<?=base_url($brt->gambar)?>" alt="<?=$brt->judul?>" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="<?=base_url('publik/read_berita/'.$this->codegenerator->encryptChar($brt->id_berita))?>"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                            <div class="post-type">
                                <i class="fa fa-picture-o"></i>
                            </div>
                        </div>
                        <div class="blog-carousel-header">
                            <h3><a title="" href="<?=base_url('publik/read_berita/'.$this->codegenerator->encryptChar($brt->id_berita))?>"><?=$brt->judul?></a></h3>
                            <div class="blog-carousel-meta">
                                <span><i class="fa fa-calendar"></i> <?=$this->formatter->getDateMonthFormatUser($brt->tgl_posting)?></span>
                                <!-- <span><i class="fa fa-comment"></i> <a href="#">03 Comments</a></span> -->
                                <span><i class="fa fa-eye"></i> <a href="#"><?=$brt->counting_reader?> Views</a></span>
                            </div>
                        </div>
                        <div class="blog-carousel-desc">
                            <p><?=batasi_kata(decode_spesial($brt->isi),30);?> </p>
                            &nbsp;
                            <a href="<?php echo base_url('publik/read_berita/'.$this->codegenerator->encryptChar($brt->id_berita));?>"> selengkapnya...</a>
                        </div>
                    </div>
                </div>
                <?php   }
                    }
                ?>
            </div>
        </div>
    </section>
	<section class="white-wrapper jt-shadow">
    	<div class="container">
			<div class="calloutbox bggrey">
            	<div class="col-lg-10">
            	<h2>Silakan Mengisi Buku Tamu Kami</h2>
                <p></p>
                </div>
                <div class="col-lg-2">
                <a class="btn pull-right btn-info btn-lg margin-top" href="<?=base_url('publik/kontak')?>">Isi Buku Tamu</a>
                </div>
            </div><!-- end messagebox -->
		</div><!-- end container -->
    </section><!-- end white-wrapper -->
       
