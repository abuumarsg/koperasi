<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fas fa-save"></i> Laporan
            <small>Simpanan</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('pages/dashboard');?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
            </li>
            <li class="active">Laporan Simpanan</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fas fa-save"></i> Laporan Simpanan</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" onclick="reload_table('table_data')" data-toggle="tooltip"
                                title="Refresh Table"><i class="fas fa-sync"></i></button>
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Tutup"><i
                                    class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <button id="btn_view_sisa" onclick="print()" class="btn btn-warning"><i
                                                    class="fa fa-print fa-fw"></i> Print</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Data Begin Here -->
                                <table id="table_data" class="table table-bordered table-striped table-responsive"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kode</th>
                                            <th>Nama</th>
                                            <?php
                                                foreach ($mSimpanan as $s) {
                                                    echo '<th>'.$s->nama.'</th>';
                                                }
                                            ?>
                                            <th>Pengurang</th>
                                            <th>Total</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="view" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Detail Data <b class="text-muted header_data" style="font-size: 12pt"></b></h2>
                <input type="hidden" name="data_id_view">
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Kode Anggota</label>
                            <div class="col-md-6" id="data_kode_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Nama Anggota</label>
                            <div class="col-md-6" id="data_nama_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Simpanan Pokok</label>
                            <div class="col-md-6" id="data_pokok_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Simpanan Wajib</label>
                            <div class="col-md-6" id="data_wajib_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Simpanan Sukarela</label>
                            <div class="col-md-6" id="data_sukarela_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Pengurang</label>
                            <div class="col-md-6" id="data_pengurang_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Total Simpanan</label>
                            <div class="col-md-6" id="data_total_view"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Status</label>
                            <div class="col-md-6" id="data_status_view">

                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Dibuat Tanggal</label>
                            <div class="col-md-6" id="data_create_date_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Diupdate Tanggal</label>
                            <div class="col-md-6" id="data_update_date_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Dibuat Oleh</label>
                            <div class="col-md-6" id="data_create_by_view">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Diupdate Oleh</label>
                            <div class="col-md-6" id="data_update_by_view">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>
<script type="text/javascript">
//wajib diisi
var table = "data_anggota";
var column = "id_anggota";
var jumcol = "<?php echo count($mSimpanan);?>";
$(document).ready(function() {
    $('#table_data').DataTable({
        ajax: {
            url: "<?php echo base_url('transaksi/laporan_simpanan/view_all/')?>",
            type: 'POST',
            data: {
                access: "<?php echo base64_encode(serialize($access));?>"
            }
        },
        scrollX: true,
        columnDefs: [{
                targets: 0,
                width: '5%',
                render: function(data, type, full, meta) {
                    return '<center>' + (meta.row + 1) + '.</center>';
                }
            },
            {
                targets: 2,
                width: '20%',
                render: function(data, type, full, meta) {
                    return data;
                }
            },
            {
                targets: 4 + jumcol,
                width: '10%',
                render: function(data, type, full, meta) {
                    return '<center>' + data + '</center>';
                }
            },
            {
                targets: 5 + jumcol,
                width: '5%',
                render: function(data, type, full, meta) {
                    return '<center>' + data + '</center>';
                }
            },
        ]
    });
});

function view_modal(id) {
    var data = {
        id_anggota: id
    };
    var callback = getAjaxData("<?php echo base_url('transaksi/laporan_simpanan/view_one')?>", data);
    $('#view').modal('show');
    $('.header_data').html(callback['nama']);
    $('#data_kode_view').html(callback['kode']);
    $('#data_nama_view').html(callback['nama']);
    $('#data_pokok_view').html(callback['pokok']);
    $('#data_wajib_view').html(callback['wajib']);
    $('#data_sukarela_view').html(callback['sukarela']);
    $('#data_pengurang_view').html(callback['pengurang']);
    $('#data_total_view').html(callback['total']);
    var status = callback['status'];
    if (status == 1) {
        var statusval = '<b class="text-success">Aktif</b>';
    } else {
        var statusval = '<b class="text-danger">Tidak Aktif</b>';
    }
    $('#data_status_view').html(statusval);
    $('#data_create_date_view').html(callback['create_date'] + ' WIB');
    $('#data_update_date_view').html(callback['update_date'] + ' WIB');
    $('input[name="data_id_view"]').val(callback['id']);
    $('#data_create_by_view').html(callback['nama_buat']);
    $('#data_update_by_view').html(callback['nama_update']);
}

function edit_modal() {

    var id = $('input[name="data_id_view"]').val();
    var data = {
        id_simpanan: id
    };
    var callback = getAjaxData("<?php echo base_url('master/master_simpanan/view_one')?>", data);
    $('#view').modal('toggle');
    setTimeout(function() {
        $('#edit').modal('show');
    }, 600);
    $('.header_data').html(callback['nama']);
    $('#data_id_edit').val(callback['id']);
    $('#kode_simpanan_edit').val(callback['kode']);
    $('#data_name_edit').val(callback['nama']);
    $('#data_besar_simpanan_edit').val(callback['besar_simpanan']);
    $('#data_keterangan_edit').val(callback['keterangan']);
}

function print() {
    $.redirect("<?php echo base_url('pages/print_laporan_simpanan'); ?>", {
        data_filter: $('#adv_form_filter').serialize()
    }, "POST", "_blank");
}
</script>