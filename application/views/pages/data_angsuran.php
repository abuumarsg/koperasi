<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fas fa-hand-holding-usd"></i> Pengelolaan Data
            <small>Angsuran</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('pages/dashboard');?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
            </li>
            <li><a href="<?php echo base_url('pages/data_pengajuan');?>"><i class="fas fa-hand-holding-usd"></i> Data
                    Pengajuan</a>
            </li>
            <li class="active"><i class="fas fa-hand-holding-usd"></i> Data Angsuran</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fas fa-hand-holding-usd"></i> Data Angsuran</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" onclick="reload_table('table_data')" data-toggle="tooltip"
                                title="Refresh Table"><i class="fas fa-sync"></i></button>
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Tutup"><i
                                    class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <?php 
                            $saldo = ($data_pinjam['angsuran']*$data_pinjam['lama_pinjam'])-($nominal_sudah_diangsur*$data_pinjam['angsuran']);
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <?php 
									if (in_array($access['l_ac']['add'], $access['access'])) {
										echo '<button class="btn btn-success btn-flat" type="button" id="btn_add" data-toggle="collapse" data-target="#add_acc"><i class="fa fa-plus"></i> Tambah Angsur Pinjaman</button>';
									}
									?>
                                </div>
                                <div class="pull-right" style="font-size: 8pt;">
                                    <h3>Kredit <?=$this->formatter->getFormatMoneyUser($saldo)?></h3>
                                </div>
                            </div>
                        </div>
                        <?php if(in_array($access['l_ac']['add'], $access['access'])){?>
                        <div class="collapse" id="add_acc">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <form id="form_add">
                                        <!-- <div class="col-md-2"></div> -->
                                        <input type="hidden" name="sisa_saldo" value="<?=$saldo?>">
                                        <div class="col-md-6">
                                            <div class="form-group clearfix">
                                                <label class="col-sm-3 control-label">Kode Angsuran</label>
                                                <div class="col-sm-9">
                                                    <input type="text" placeholder="Masukkan Kode Angsuran" name="kode"
                                                        id="kode_angsuran" class="form-control" required="required"
                                                        readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-sm-3 control-label">Nama Anggota</label>
                                                <div class="col-sm-9">
                                                    <input type="text" placeholder="Masukkan Kode Angsuran"
                                                        name="nama_anggota" id="nama_anggota" class="form-control"
                                                        required="required" readonly="readonly"
                                                        value="<?=$data_pinjam['nama_anggota']?>">
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-sm-3 control-label">Kode Pinjaman</label>
                                                <div class="col-sm-9">
                                                    <input type="text" placeholder="Masukkan Kode Angsuran"
                                                        name="kode_pinjaman" id="kode_pengajuan" class="form-control"
                                                        required="required" readonly="readonly"
                                                        value="<?=$data_pinjam['kode']?>">
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-sm-3 control-label">Tanggal</label>
                                                <div class="col-sm-9">
                                                    <div class="has-feedback">
                                                        <span class="fa fa-calendar form-control-feedback"></span>
                                                        <input type="text" name="tanggal"
                                                            class="form-control pull-right"
                                                            placeholder="Tanggal Pengajuan" readonly="readonly"
                                                            value="<?=$this->otherfunctions->getDateNow('d/m/Y');?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group clearfix">
                                                <label class="col-sm-3 control-label">Besar Angsuran</label>
                                                <div class="col-sm-9">
                                                    <input type="text" placeholder="Maksimal Pinjaman"
                                                        name="nominal_angsuran" id="nominal_angsuran"
                                                        class="form-control" required="required" readonly="readonly"
                                                        value="<?=$this->formatter->getFormatMoneyUser($data_pinjam['angsuran'])?>">
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-sm-3 control-label">Angsuran Ke</label>
                                                <div class="col-sm-9">
                                                    <input type="text" placeholder="Angsuran Ke" name="angsuran_ke"
                                                        id="angsuran_ke" class="form-control" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-sm-3 control-label">Denda</label>
                                                <div class="col-sm-9">
                                                    <input type="text" placeholder="Denda" name="denda"
                                                        class="form-control input-money" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Keterangan</label>
                                                <div class="col-sm-9">
                                                    <textarea name="keterangan" class="form-control"
                                                        placeholder="Catatan / Keterangan"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <button type="button" onclick="do_add()" id="btn_add"
                                            class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i>
                                            Simpan</button>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <?php } ?>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Data Begin Here -->
                                <table id="table_data" class="table table-bordered table-striped table-responsive"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kode</th>
                                            <th>Nama Anggota</th>
                                            <th>Kode Pinjamanan</th>
                                            <th>Besar Angsuran</th>
                                            <th>Angsuran Ke</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- view -->
<div id="view" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Detail Data <b class="text-muted header_data" style="font-size: 12pt"></b></h2>
                <input type="hidden" name="data_id_view">
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Kode</label>
                            <div class="col-md-6" id="data_kode_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Nama Anggota</label>
                            <div class="col-md-6" id="data_nama_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Kode Pinjaman</label>
                            <div class="col-md-6" id="data_kode_pinjaman_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Besar Angsuran</label>
                            <div class="col-md-6" id="data_nominal_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Angsuran Ke</label>
                            <div class="col-md-6" id="data_angsuran_ke_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Tanggal</label>
                            <div class="col-md-6" id="data_tanggal_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Denda</label>
                            <div class="col-md-6" id="data_denda_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Keterangan</label>
                            <div class="col-md-6" id="data_keterangan_view"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Status</label>
                            <div class="col-md-6" id="data_status_view">

                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Dibuat Tanggal</label>
                            <div class="col-md-6" id="data_create_date_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Diupdate Tanggal</label>
                            <div class="col-md-6" id="data_update_date_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Dibuat Oleh</label>
                            <div class="col-md-6" id="data_create_by_view">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Diupdate Oleh</label>
                            <div class="col-md-6" id="data_update_by_view">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="table_angsuran"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php if (in_array($access['l_ac']['edt'], $access['access'])) {
					echo '<button type="submit" class="btn btn-info" id="view_edit" onclick="edit_modal()" style="display:none;"><i class="fa fa-edit"></i> Edit</button>';
				}?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<!-- edit -->
<div id="edit" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Edit Data <b class="text-muted header_data" style="font-size: 12pt"></b>
                </h2>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="form_edit">
                        <input type="hidden" id="data_id_edit" name="id" value="">
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Kode Pengajuan</label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Masukkan Kode Pengajuan" name="kode"
                                        id="kode_pengajuan_edit" class="form-control" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Pilih Anggota</label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Masukkan Kode Pengajuan" name="kode"
                                        id="data_anggota_edit" class="form-control" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Tanggal</label>
                                <div class="col-sm-9">
                                    <div class="has-feedback">
                                        <span class="fa fa-calendar form-control-feedback"></span>
                                        <input type="text" name="tanggal" class="form-control pull-right date-picker"
                                            placeholder="Tanggal Pengajuan" readonly="readonly" id="tanggal_edit">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Besar Pengajuan</label>
                                <div class="col-sm-9">
                                    <input type="text" name="nominal" id="nominal_edit" class="form-control input-money"
                                        placeholder="Besar Pengajuan" required="required">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Jenis Pinjaman</label>
                                <div class="col-sm-9">
                                    <select class="form-control select2" name="jenis" required="required"
                                        id="data_jenis_edit" onchange="getJenisPinjamanEdit(this.value)"
                                        style="width: 100%;"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Lama Angsuran (Bulan)</label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Lama Angsuran (Bulan)" name="lama_angsur"
                                        id="lama_angsuran_edit" class="form-control" required="required"
                                        readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Maksimal Pinjaman</label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Maksimal Pinjaman" name="max_pinjam"
                                        id="maksimal_pinjaman_edit" class="form-control" required="required"
                                        readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Bunga (%)</label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Bunga (%)" name="bunga" id="bunga_pinjaman_edit"
                                        class="form-control" required="required" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Angsuran</label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Angsuran" name="angsuran" id="angsuran_edit"
                                        class="form-control" required="required" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Keterangan</label>
                                <div class="col-sm-9">
                                    <textarea name="keterangan" id="keterangan_edit" class="form-control"
                                        placeholder="Catatan / Keterangan"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="do_edit()" id="btn_edit" class="btn btn-success"><i
                        class="fa fa-floppy-o"></i> Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<div id="m_need" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm modal-default">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Validasi Pengajuan Pinjaman</h4>
            </div>
            <form id="form_need">
                <div class="modal-body text-center">
                    <input type="hidden" id="data_id_need" name="id">
                    <input type="hidden" id="data_idk_need" name="id_kar">
                    <input type="hidden" id="data_jenis_need" name="jenis">
                    <p>Mohon Validasi Pengajuan Pinjaman Anggota atas nama <b id="data_name_need"
                            class="header_data"></b>
                        berikut !!</p>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" onclick="do_validasi(2,0,'m_need')" class="btn btn-danger"><i
                        class="fa fa-times-circle"></i> Ditolak</button>
                <button type="button" onclick="do_validasi(2,1,'m_need')" class="btn btn-success"><i
                        class="fa fa-check-circle"></i> Diterima</button>
            </div>
        </div>
    </div>
</div>
<div id="m_yes" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm modal-default">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Validasi Pengajuan Pinjaman</h4>
            </div>
            <form id="form_yes">
                <div class="modal-body text-center">
                    <input type="hidden" id="data_id_yes" name="id">
                    <input type="hidden" id="data_idk_yes" name="id_kar">
                    <input type="hidden" id="data_jenis_yes" name="jenis">
                    <p>Apakah Anda yakin akan mengubah status Pengajuan Pinjaman dari <b class="text-green">Diterima</b>
                        menjadi <b class="text-red">Ditolak</b></b> atas nama Anggota <b id="data_name_yes"
                            class="header_data"></b> ??</p>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" onclick="do_validasi(1,0,'m_yes')" class="btn btn-danger" id="btn_yes_ditolak"><i
                        class="fa fa-times-circle"></i> Ditolak</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<div id="m_no" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm modal-default">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Validasi Pengajuan Pinjaman</h4>
            </div>
            <form id="form_no">
                <div class="modal-body text-center">
                    <input type="hidden" id="data_id_no" name="id">
                    <input type="hidden" id="data_idk_no" name="id_kar">
                    <input type="hidden" id="data_jenis_no" name="jenis">
                    <p>Apakah Anda yakin akan mengubah status Pengajuan Pinjaman dari <b class="text-red">Ditolak</b>
                        menjadi <b class="text-green">Diterima</b></b> atas nama Anggota <b id="data_name_no"
                            class="header_data"></b> ??</p>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" onclick="do_validasi(0,1,'m_no')" class="btn btn-success"><i
                        class="fa fa-check-circle"></i> Diterima</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>
<script type="text/javascript">
// var url_select = "<?php //echo base_url('global_control/select2_global');?>";
$(document).ready(function() {
    var sisa_pinjam = "<?php echo $data_pinjam['sisa_pinjam']?>";
    kode_generator("<?php echo base_url('transaksi/data_angsuran/kode');?>", 'kode_angsuran');
    $('#table_data').DataTable({
        ajax: {
            url: "<?php echo base_url('transaksi/data_angsuran/view_all/')?>",
            type: 'POST',
            data: {
                access: "<?php echo base64_encode(serialize($access));?>",
                kode_pinjaman: "<?php echo $data_pinjam['kode']?>",
            }
        },
        scrollX: true,
        columnDefs: [{
                targets: 0,
                width: '5%',
                render: function(data, type, full, meta) {
                    return '<center>' + (meta.row + 1) + '.</center>';
                }
            },
            {
                targets: 6,
                width: '10%',
                render: function(data, type, full, meta) {
                    return '<center>' + data + '</center>';
                }
            },
            //aksi
            {
                targets: 7,
                width: '12%',
                render: function(data, type, full, meta) {
                    return '<center>' + data + '</center>';
                }
            },
        ]
    });
    $('#nominal_add').keyup(function() {
        var kode = $('#data_jenis_add').val();
        getJenisPinjaman(kode);
    });
    $('#nominal_add').blur(function() {
        var kode = $('#data_jenis_add').val();
        getJenisPinjaman(kode);
    });
    if (sisa_pinjam == 0) {
        $('#btn_add').prop('disabled', true);
    }
    $('#btn_add').click(function() {
        var kode = $('#kode_pengajuan').val();
        var data = {
            kode: kode,
        };
        var callback = getAjaxData("<?php echo base_url('transaksi/data_angsuran/cekAngsuran')?>",
            data);
        $('#angsuran_ke').val(callback['angsuran_ke']);
    });
});

function do_add() {
    if ($("#form_add")[0].checkValidity()) {
        submitAjax("<?php echo base_url('transaksi/data_angsuran/do_add')?>", null, 'form_add', null, null);
        $('#table_data').DataTable().ajax.reload(function() {
            Pace.restart();
        });
        $('#form_add')[0].reset();
        resetTotal();
    } else {
        notValidParamx();
    }
}

function do_edit() {
    if ($("#form_edit")[0].checkValidity()) {
        submitAjax("<?php echo base_url('transaksi/data_angsuran/do_edit')?>", 'edit', 'form_edit', null, null);
        $('#table_data').DataTable().ajax.reload(function() {
            Pace.restart();
        });
        $('#form_edit')[0].reset();
    } else {
        notValidParamx();
    }
}

function view_modal(id) {
    var data = {
        id: id
    };
    var callback = getAjaxData("<?php echo base_url('transaksi/data_angsuran/view_one')?>", data);
    $('#view').modal('show');
    $('.header_data').html(callback['nama']);
    $('#data_kode_view').html(callback['kode']);
    $('#data_nama_view').html(callback['nama']);
    $('#data_kode_pinjaman_view').html(callback['kode_pinjaman']);
    $('#data_nominal_view').html(callback['nominal']);
    $('#data_angsuran_ke_view').html(callback['angsuran_ke']);
    $('#data_tanggal_view').html(callback['tanggal']);
    $('#data_denda_view').html(callback['denda']);
    $('#data_keterangan_view').html(callback['keterangan']);
    var status = callback['status'];
    if (status == 1) {
        var statusval = '<b class="text-success">Aktif</b>';
    } else {
        var statusval = '<b class="text-danger">Tidak Aktif</b>';
    }
    $('#data_status_view').html(statusval);
    $('#data_create_date_view').html(callback['create_date'] + ' WIB');
    $('#data_update_date_view').html(callback['update_date'] + ' WIB');
    $('input[name="data_id_view"]').val(callback['id']);
    $('#data_create_by_view').html(callback['nama_buat']);
    $('#data_update_by_view').html(callback['nama_update']);
    $('#table_angsuran').html(callback['tabel']);
}

function edit_modal(id) {
    select_data('data_jenis_edit', url_select, 'master_pinjaman', 'kode', 'nama', 'placeholder');
    var id = $('input[name="data_id_view"]').val();
    var data = {
        id: id
    };
    var callback = getAjaxData("<?php echo base_url('transaksi/data_angsuran/view_one')?>", data);
    $('#view').modal('toggle');
    setTimeout(function() {
        $('#edit').modal('show');
    }, 600);
    $('.header_data').html(callback['nama']);
    $('#data_id_edit').val(callback['id']);
    $('#kode_pengajuan_edit').val(callback['kode']);
    $('#data_anggota_edit').val(callback['nama']);
    $('#tanggal_edit').val(callback['tanggal']);
    $('#nominal_edit').val(callback['besar_pinjaman']);
    $('#data_jenis_edit').val(callback['jenis_e']).trigger('change');
    $('#lama_angsuran_edit').val(callback['lama_pinjam']);
    $('#maksimal_pinjaman_edit').val(callback['maksimal']);
    $('#bunga_pinjaman_edit').val(callback['bunga']);
    $('#angsuran_edit').val(callback['angsuran']);
    $('#keterangan_edit').val(callback['keterangan']);
}

// function modal_need(id) {
//     var data = {
//         id: id
//     };
//     $('#m_need').modal('toggle');
//     var callback = getAjaxData("<?php echo base_url('transaksi/data_angsuran/view_one')?>", data);
//     $('#m_need #data_id_need').val(callback['id']);
//     $('#m_need .header_data').html(callback['nama']);
// }

// function modal_yes(id) {
//     var data = {
//         id: id
//     };
//     $('#m_yes').modal('toggle');
//     var callback = getAjaxData("<?php echo base_url('transaksi/data_angsuran/view_one')?>", data);
//     $('#m_yes #data_id_yes').val(callback['id']);
//     $('#m_yes .header_data').html(callback['nama']);
//     var max_angsur = callback['banyakAngsuran'];
//     if (max_angsur > 0) {
//         $('#btn_yes_ditolak').prop('disabled', true);
//     } else {
//         $('#btn_yes_ditolak').prop('disabled', false);
//     }
// }

// function modal_no(id) {
//     var data = {
//         id: id
//     };
//     $('#m_no').modal('toggle');
//     var callback = getAjaxData("<?php echo base_url('transaksi/data_angsuran/view_one')?>", data);
//     $('#m_no #data_id_no').val(callback['id']);
//     $('#m_no .header_data').html(callback['nama']);
// }

// function do_validasi(data, val, form) {
//     if (data == 2) {
//         var id = $('#data_id_need').val();
//     } else if (data == 1) {
//         var id = $('#data_id_yes').val();
//     } else if (data == 0) {
//         var id = $('#data_id_no').val();
//     }
//     var datax = {
//         id: id,
//         validasi_db: data,
//         validasi: val,
//     };
//     submitAjax("<?php echo base_url('transaksi/data_angsuran/validasi_pengajuan')?>", form, datax, null, null,
//         'status');
//     $('#table_data').DataTable().ajax.reload(function() {
//         Pace.restart();
//     });
// }
</script>