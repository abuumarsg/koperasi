<div class="content-wrapper">
	<?php 
		if (!empty($this->session->flashdata('sukses'))){
			echo '<div class="alert alert-success">'.$this->session->flashdata('sukses').'</div>';
		}elseif(!empty($this->session->flashdata('gagal'))){
			echo '<div class="alert alert-danger">'.$this->session->flashdata('gagal').'</div>';
		}
	?>
	<section class="content-header">
		<h1>
			<i class="fas fa-university"></i> Company Profile
			<small>Kelola Company Profile</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('pages/dashboard');?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
			</li>
			<li class="active">Company Profile</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fas fa-university"></i> Company Profile <?=$data['name']?></h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" onclick="reload_table('table_data')" data-toggle="tooltip" title="Refresh Table"><i class="fas fa-sync"></i></button>
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
							<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Tutup"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<form id="form_data">
							<div class="row">
								<div class="col-md-4">
									<p class="text-center text-blue" style="font-size: 9pt;"><i class="fa fa-info-circle"></i> Foto Ideal 3x3</p>
									<div id="frame_foto">
										<img class="profile-user-img img-responsive img-circle" src="<?php echo base_url($data['logo']);?>" alt="Foto" />
									</div><br>
									<div class="form-group clearfix">
										<div class="input-group">
											<label class="input-group-btn">
												<span class="btn btn-primary"> 
													<i class="fas fa-folder-open"></i> Pilih File <input type="file" id="BSbtnsuccess" name="file" style="display: none;">
												</span>
											</label>
											<input type="text" class="form-control" readonly="readonly" value="<?=$data['logo']?>">
										</div>
									</div>
								</div>
								<input type="hidden" name="id" value="<?=$data['id'];?>">
								<div class="col-md-8">
									<div class="form-group clearfix">
										<label class="col-sm-2 control-label">Nama</label>
										<div class="col-sm-10">
											<input type="text" name="nama" class="form-control" placeholder="Nama" value="<?=$data['name'];?>">
										</div>
									</div>
									<div class="form-group clearfix">
										<label class="col-sm-2 control-label">Nama Kantor</label>
										<div class="col-sm-10">
											<input type="text" name="nama_kantor" class="form-control" placeholder="Nama Kantor" value="<?=$data['name_office'];?>">
										</div>
									</div>
									<div class="form-group clearfix">
										<label class="col-sm-2 control-label">Alamat</label>
										<div class="col-sm-10">
											<textarea name="alamat" class="form-control" placeholder="Alamat"><?=$data['alamat'];?></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
										<div class="form-group clearfix">
											<label class="col-sm-2 control-label">Email</label>
											<div class="col-sm-10">
												<input type="text" name="email" class="form-control" placeholder="Email" value="<?=$data['email'];?>">
											</div>
										</div>
										<div class="form-group clearfix">
											<label class="col-sm-2 control-label">No Handphone</label>
											<div class="col-sm-10">
												<input type="text" name="no_hp" maxlength="15" class="form-control" placeholder="No Handphone" value="<?=$data['no_hp'];?>">
											</div>
										</div>
									<div class="form-group clearfix">
										<label class="col-sm-2 control-label">Google Maps</label>
										<div class="col-sm-10">
											<textarea name="maps" class="form-control" placeholder="Google Maps"><?=$data['maps'];?></textarea>
										</div>
									</div>
									<div class="form-group clearfix">
										<label class="col-sm-2 control-label">Tentang Instansi</label>
										<div class="col-sm-10">
											<textarea name="about" class="form-control tinymce" placeholder="Tentang Instansi"><?=$data['about'];?></textarea>
										</div>
									</div>
									<div class="form-group clearfix">
										<label class="col-sm-2 control-label">Visi</label>
										<div class="col-sm-10">
											<textarea name="visi" class="form-control tinymce" height="80" placeholder="Visi"><?=$data['visi'];?></textarea>
										</div>
									</div>
									<div class="form-group clearfix">
										<label class="col-sm-2 control-label">Misi</label>
										<div class="col-sm-10">
											<textarea name="misi" class="form-control tinymce" height="80" placeholder="Misi"><?=$data['misi'];?></textarea>
										</div>
									</div>
									<div class="form-group clearfix">
										<label class="col-sm-2 control-label">Moto</label>
										<div class="col-sm-10">
											<textarea name="moto" class="form-control tinymce" height="80" placeholder="Moto"><?=$data['moto'];?></textarea>
										</div>
									</div>
									<div class="form-group clearfix">
										<label class="col-sm-2 control-label">Sejarah</label>
										<div class="col-sm-10">
											<textarea name="sejarah" class="form-control tinymce" height="80" placeholder="Sejarah"><?=$data['sejarah'];?></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-sm-12">
											<button type="button" id="btn_add" onclick="do_add()" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Simpan</button>
											<button type="submit" id="btn_addx" style="display: none;"></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-success">
					<div class="panel-heading bg-green"><h4>Ubah Foto Sejarah</h4></div>
					<div class="panel-body">
						<form id="form_sejarah">
							<input type="hidden" name="id" value="<?=$data['id'];?>">
							<div class="row">
								<div class="col-md-12">
									<p class="text-center text-blue" style="font-size: 9pt;"><i class="fa fa-info-circle"></i> Foto Ideal 4x3</p>
									<div id="frame_foto">
										<img class="img-responsive img-box" src="<?php echo base_url($data['gambar_sejarah']);?>" alt="Foto" />
									</div><br>
									<div class="form-group clearfix">
										<div class="input-group">
											<label class="input-group-btn">
												<span class="btn btn-primary"> 
													<i class="fas fa-folder-open"></i> Pilih File <input type="file" id="BSbtnsuccess" name="file" style="display: none;">
												</span>
											</label>
											<input type="text" class="form-control" readonly="readonly" value="<?=$data['gambar_sejarah']?>">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-sm-12">
											<button type="button" id="btn_sjrh" onclick="do_sjrh()" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Simpan</button>
											<button type="submit" id="btn_sjrhx" style="display: none;"></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-success">
					<div class="panel-heading bg-red"><h4>Ubah Foto Visi Misi</h4></div>
					<div class="panel-body">
						<form id="form_visi">
							<input type="hidden" name="id" value="<?=$data['id'];?>">
							<div class="row">
								<div class="col-md-12">
									<p class="text-center text-blue" style="font-size: 9pt;"><i class="fa fa-info-circle"></i> Foto Ideal 4x3</p>
									<div id="frame_foto">
										<img class="img-responsive img-box" src="<?php echo base_url($data['gambar_visi']);?>" alt="Foto" />
									</div><br>
									<div class="form-group clearfix">
										<div class="input-group">
											<label class="input-group-btn">
												<span class="btn btn-primary"> 
													<i class="fas fa-folder-open"></i> Pilih File <input type="file" id="BSbtnsuccess" name="file" style="display: none;">
												</span>
											</label>
											<input type="text" class="form-control" readonly="readonly" value="<?=$data['gambar_visi']?>">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-sm-12">
											<button type="button" id="btn_visi" onclick="do_visi()" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Simpan</button>
											<button type="submit" id="btn_visix" style="display: none;"></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>
<script type="text/javascript">
	var url_select="<?php echo base_url('global_control/select2_global');?>";
	$(document).ready(function(){
		$('#form_data').submit(function(e){
			e.preventDefault();
			var data = new FormData(this);
			var urlx = "<?php echo base_url('transaksi/update_company'); ?>";
			submitAjaxFile(urlx,data,null,null,null);
		});
		$('#form_sejarah').submit(function(e){
			e.preventDefault();
			var data = new FormData(this);
			var urlx = "<?php echo base_url('transaksi/update_foto_sejarah'); ?>";
			submitAjaxFile(urlx,data,null,null,null);
		});
		$('#form_visi').submit(function(e){
			e.preventDefault();
			var data = new FormData(this);
			var urlx = "<?php echo base_url('transaksi/update_foto_visi'); ?>";
			submitAjaxFile(urlx,data,null,null,null);
		});
	});
	function do_add(){
		if($("#form_data")[0].checkValidity()) {
			tinyMCE.triggerSave();
			$('#btn_addx').click();
		}else{
			notValidParamx();
		}
	}
	function do_sjrh(){
		if($("#form_sejarah")[0].checkValidity()) {
			$('#btn_sjrhx').click();
		}else{
			notValidParamx();
		}
	}
	function do_visi(){
		if($("#form_visi")[0].checkValidity()) {
			$('#btn_visix').click();
		}else{
			notValidParamx();
		}
	}
	$(function() {
		$(document).on('change', ':file', function() {
			var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
		});
		$(document).ready( function() {
			$(':file').on('fileselect', function(event, numFiles, label) {
				var input = $(this).parents('.input-group').find(':text'),
				log = numFiles > 1 ? numFiles + ' files selected' : label;
				if( input.length ) {
					input.val(log);
				} else {
					if( log ) alert(log);
				}
			});
		});
	});
</script>