<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fas fa-save"></i> Laporan
            <small>Perbulan</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('pages/dashboard');?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
            </li>
            <li class="active">Laporan Perbulan</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fas fa-save"></i> Laporan Perbulan</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" onclick="reload_table('table_data')" data-toggle="tooltip"
                                title="Refresh Table"><i class="fas fa-sync"></i></button>
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Tutup"><i
                                    class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div style="padding-top: 10px;">
                        <form id="form_filter">
                            <div class="box-body">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Pilih Bulan</label>
                                        <select class="form-control select2" id="bulan" name="bulan"
                                            style="width: 100%;">
                                            <option></option>
                                            <?php
                                            for ($i=1; $i <= 12; $i++) { 
                                                echo '<option value="'.$this->formatter->zeroPadding($i).'">'.$this->formatter->getNameOfMonth($i).'</option>'; } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Pilih Tahun</label>
                                        <select class="form-control select2" id="tahun" name="tahun"
                                            style="width: 100%;">
                                            <option></option>
                                            <?php
                                            $year = $this->formatter->getYearAll();
                                            foreach ($year as $yk => $yv) {
                                                echo '<option value="'.$yk.'">'.$yv.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Jenis</label>
                                        <select class="form-control select2" id="jenis" name="jenis"
                                            style="width: 100%;">
                                            <option></option>
                                            <option value="pinjaman">Pinjaman</option>
                                            <option value="simpanan">Simpanan</option>
                                            <option value="angsuran">Angsuran</option>
                                            <option value="ambil">Pengambilan Uang</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="box-footer">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <button onclick="print()" class="btn btn-warning"><i class="fa fa-print fa-fw"></i>
                                        Print</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>
<script type="text/javascript">
var table = "data_anggota";
var column = "id_anggota";
$(document).ready(function() {

});

function print() {
    $.redirect("<?php echo base_url('pages/print_laporan_bulanan'); ?>", {
            data_filter: $('#form_filter').serialize()
        },
        "POST", "_blank");
}
</script>