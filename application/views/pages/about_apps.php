  <style type="text/css">
  .data_detail{
    display: none;
    border-style: solid;
    border-width: 1px;
    border-radius: 3px;
    padding: 8px;
    border-color: #7F7F7F;
  }
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fas fa-info-circle"></i> Tentang Aplikasi
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('pages/dashboard');?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a></li>
      <li class="active">Tentang Aplikasi</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fas fa-info-circle"></i> Tentang Aplikasi</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Tutup"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
                <div class="col-md-12 text-center">
                  <img class="img-responsive img profile-user-img" style="width: 200px"  src="<?php echo $this->otherfunctions->companyProfile()['logo']; ?>" alt="">
                  <p class="text-muted">Software Version <?php echo $this->otherfunctions->companyProfile()['version']; ?></p>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 text-center">
                  <h2><b><?php echo $this->otherfunctions->companyProfile()['name']; ?></b></h2>
                  <p>
                    <?php echo $this->otherfunctions->companyProfile()['description']; ?>
                    <br>
                    <br>
                    <i class="fas fa-map-marker-alt"></i> <?php echo $this->otherfunctions->companyProfile()['address']; ?> <br> <i class="fas fa-phone"></i> <?php echo $this->otherfunctions->companyProfile()['call']; ?> <i class="fas fa-at"></i> <a href="mailto:<?php echo $this->otherfunctions->companyProfile()['email']; ?>"><?php echo $this->otherfunctions->companyProfile()['email']; ?></a> <i class="fab fa-internet-explorer"></i> <a href="<?php echo $this->otherfunctions->companyProfile()['website']; ?>"><?php echo $this->otherfunctions->companyProfile()['website']; ?></a>
                  </p>
                  <div class="row">
                    <div class="col-md-12">
                      <?php echo $this->otherfunctions->companyProfile()['maps']; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>