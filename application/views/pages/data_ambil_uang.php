<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<i class="fas fa-donate"></i> Pengelolaan Data
			<small>Pengambilan Uang</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('pages/dashboard');?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
			</li>
			<li><a href="<?php echo base_url('pages/data_tabungan');?>"><i class="far fa-credit-card"></i> Data Tabungan</a></li>
			<li class="active"><i class="fas fa-donate"></i> Data Pengambilan Uang <?=$tabungan['nama_anggota']?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fas fa-donate"></i> Data Pengambilan Uang <?=$tabungan['nama_anggota']?></h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" onclick="reload_table('table_data')" data-toggle="tooltip" title="Refresh Table"><i class="fas fa-sync"></i></button>
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
							<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Tutup"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-left">
											<?php 
											if (in_array($access['l_ac']['add'], $access['access'])) {
												echo '<button class="btn btn-success btn-flat" type="button" data-toggle="collapse" data-target="#add_acc"><i class="fas fa-donate"></i> Ambil Uang</button>';
											}
											?>
										</div>
									</div>
								</div>
								<?php if(in_array($access['l_ac']['add'], $access['access'])){?>
								<div class="collapse" id="add_acc">
									<form id="form_add">
                  						<div class="box-body">
											<div class="col-md-2"></div>
											<div class="col-md-8">
											<input type="hidden" name="kode_tabungan" value="<?=$tabungan['kode_tabungan']?>">
												<div class="form-group clearfix">
													<label class="col-sm-3 control-label">Jumlah Tabungan</label>
													<div class="col-sm-9">
														<input type="text" name="total" id="total" class="form-control input-money" placeholder="Jumlah Tabungan" readonly="readonly" value="<?=$this->formatter->getFormatMoneyUser($tabungan['besar_tabungan'])?>">
													</div>
												</div>
												<div class="form-group clearfix">
													<label class="col-sm-3 control-label">Pengambilan</label>
													<div class="col-sm-9">
														<input type="text" name="pengambilan" id="pengambilan" class="form-control input-money" placeholder="Nominal Pengambilan">
													</div>
												</div>
												<div class="form-group clearfix">
													<label class="col-sm-3 control-label">Sisa Tabungan</label>
													<div class="col-sm-9">
														<input type="text" name="sisa" id="sisa_tabungan" class="form-control input-money" placeholder="Simpanan Pokok" readonly="readonly">
													</div>
												</div>
											</div>
										</div>
										<div class="box-footer">
											<div class="pull-right">
												<button type="button" onclick="do_add()" id="btn_add" class="btn btn-danger"><i class="fas fa-donate"></i> OK, Ambil</button>
											</div>
										</div>
									</form>
								</div>
								<?php } ?>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-12">
								<!-- Data Begin Here -->
								<table id="table_data" class="table table-bordered table-striped table-responsive" width="100%">
									<thead>
										<tr>
											<th>No.</th>
											<th>Kode Transaksi</th>
											<th>Kode Tabungan</th>
											<th>Kode Anggota</th>
											<th>Nama Anggota</th>
											<th>Tanggal</th>
											<th>Nominal Ambil</th>
											<th>Tanggal Input<br>Tanggal Update</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>
<script type="text/javascript">
	//wajib diisi
	var table = "data_tabungan";
	var column = "id_tabungan";
	var kode_tabungan = "<?php echo $tabungan['kode_tabungan']?>";
	$(document).ready(function () {
		$('#table_data').DataTable({
			ajax: {
				url: "<?php echo base_url('transaksi/ambil_uang/view_all')?>",
				type: 'POST',
				data: {
					access: "<?php echo base64_encode(serialize($access));?>",
					kode_tabungan:kode_tabungan,
				}
			},
			scrollX: true,
			columnDefs: [{
					targets: 0,
					width: '5%',
					render: function (data, type, full, meta) {
						return '<center>' + (meta.row + 1) + '.</center>';
					}
				},
				{
					targets: 6,
					width: '10%',
					render: function (data, type, full, meta) {
						return '<center>' + data + '</center>';
					}
				},
				//aksi
				{
					targets: 7,
					width: '5%',
					render: function (data, type, full, meta) {
						return '<center>' + data + '</center>';
					}
				},
			]
		});
		$('#pengambilan').keyup(function(){
			hitungTotal();
		});
		$('#pengambilan').blur(function(){
			hitungTotal();
		});
	});
	function hitungTotal() {
		var ambil = $('#pengambilan').val();
		var total = $('#total').val();
		var nominal=getAjaxData("<?php echo base_url('transaksi/ambil_uang/cek_total')?>",{ambil:ambil,total:total});
		$('#sisa_tabungan').val(nominal);
	}
	function resetTotal(){
		var nominal=getAjaxData("<?php echo base_url('transaksi/ambil_uang/cek_total_update')?>",{kode_tabungan:kode_tabungan});
		$('#total').val(nominal);
	}
	function do_add(){
		if($("#form_add")[0].checkValidity()) {
			submitAjax("<?php echo base_url('transaksi/ambil_uang/do_add')?>",null,'form_add',null,null);
			$('#table_data').DataTable().ajax.reload(function(){
				Pace.restart();
			});
			$('#form_add')[0].reset();
			resetTotal();
		}else{
			notValidParamx();
		} 
	}
</script>