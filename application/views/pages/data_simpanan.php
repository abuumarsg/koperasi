<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fas fa-donate"></i> Transaksi
            <small>Data Simpanan</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('pages/dashboard');?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
            </li>
            <li><a href="<?php echo base_url('pages/transaksi');?>"><i class="far fa-credit-card"></i> Data
                    Transaksi</a></li>
            <li class="active"><i class="fas fa-donate"></i> Data Data Simpanan <?=$tabungan['nama_anggota']?></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fas fa-donate"></i> Data Data Simpanan
                            <?=$tabungan['nama_anggota']?></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" onclick="reload_table('table_data')" data-toggle="tooltip"
                                title="Refresh Table"><i class="fas fa-sync"></i></button>
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Tutup"><i
                                    class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <?php 
											if (in_array($access['l_ac']['add'], $access['access'])) {
												echo '<button class="btn btn-success btn-flat" type="button" data-toggle="collapse" data-target="#add_acc"><i class="fas fa-donate"></i> Tambah Simpanan</button>';
											}
											?>
                                            <button id="btn_view_sisa" onclick="print()" class="btn btn-warning"><i
                                                    class="fa fa-print fa-fw"></i> Print</button>
                                        </div>
                                        <div class="pull-right" style="font-size: 8pt;">
                                            <h3>Total Simpanan <b id="besar_simpanan_view"></b></h3>
                                        </div>
                                    </div>
                                </div>
                                <?php if(in_array($access['l_ac']['add'], $access['access'])){?>
                                <div class="collapse" id="add_acc">
                                    <form id="form_add">
                                        <div class="box-body">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <div class="form-group clearfix">
                                                    <label class="col-sm-3 control-label">Kode Tabungan</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="kode_tabungan" class="form-control"
                                                            placeholder="" readonly="readonly"
                                                            value="<?=$tabungan['kode_tabungan']?>">
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix">
                                                    <label class="col-sm-3 control-label">Jenis Simpanan</label>
                                                    <div class="col-sm-9">
                                                        <select class="form-control select2" name="jenis"
                                                            required="required" id="data_jenis_add"
                                                            onchange="getJenisSimpanan(this.value)"
                                                            style="width: 100%;"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix">
                                                    <label class="col-sm-3 control-label">Besar Simpanan</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="besar_simpanan" id="besar_simpanan"
                                                            class="form-control input-money"
                                                            placeholder="Besar Simpanan">
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix">
                                                    <label class="col-sm-3 control-label">Tanggal</label>
                                                    <div class="col-sm-9">
                                                        <div class="has-feedback">
                                                            <span class="fa fa-calendar form-control-feedback"></span>
                                                            <input type="text" name="tanggal"
                                                                class="form-control pull-right date-picker"
                                                                placeholder="Tanggal" required="required">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix">
                                                    <label class="col-sm-3 control-label">Keterangan</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" name="keterangan"
                                                            placeholder="Keterangan"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <div class="pull-right">
                                                <button type="button" onclick="do_add()" id="btn_add"
                                                    class="btn btn-success"><i class="fas fa-donate"></i>
                                                    Simpan</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="table_data" class="table table-bordered table-striped table-responsive"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kode</th>
                                            <th>Tanggal Simpan</th>
                                            <th>Jenis Simpan</th>
                                            <th>( + / - )</th>
                                            <th>Besar Simpan</th>
                                            <th>Tanggal Input<br>Tanggal Update</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- view -->
<div id="view" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Detail Data <b class="text-muted header_data" style="font-size: 12pt"></b></h2>
                <input type="hidden" name="data_id_view">
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Kode</label>
                            <div class="col-md-6" id="data_kode_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Nama Anggota</label>
                            <div class="col-md-6" id="data_nama_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Kode Simpanan</label>
                            <div class="col-md-6" id="data_kode_simpanan_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Jenis Simpanan</label>
                            <div class="col-md-6" id="data_jenis_simpanan_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Besar Simpanan</label>
                            <div class="col-md-6" id="data_nominal_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Tanggal</label>
                            <div class="col-md-6" id="data_tanggal_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">(+ / -)</label>
                            <div class="col-md-6" id="data_plus_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Keterangan</label>
                            <div class="col-md-6" id="data_keterangan_view"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Status</label>
                            <div class="col-md-6" id="data_status_view">

                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Dibuat Tanggal</label>
                            <div class="col-md-6" id="data_create_date_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Diupdate Tanggal</label>
                            <div class="col-md-6" id="data_update_date_view"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Dibuat Oleh</label>
                            <div class="col-md-6" id="data_create_by_view">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-6 control-label">Diupdate Oleh</label>
                            <div class="col-md-6" id="data_update_by_view">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="table_angsuran"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php if (in_array($access['l_ac']['edt'], $access['access'])) {
					echo '<button type="submit" class="btn btn-info" id="btn_view_edit" onclick="edit_modal()" style="display:none"><i class="fa fa-edit"></i> Edit</button>';
				}?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<!-- edit -->
<div id="edit" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Edit Data <b class="text-muted header_data" style="font-size: 12pt"></b>
                </h2>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="form_edit">
                        <input type="hidden" id="data_id_edit" name="id" value="">
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Kode Pengajuan</label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Masukkan Kode Pengajuan" name="kode"
                                        id="kode_pengajuan_edit" class="form-control" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Pilih Anggota</label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Masukkan Kode Pengajuan" name="kode"
                                        id="data_anggota_edit" class="form-control" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Tanggal</label>
                                <div class="col-sm-9">
                                    <div class="has-feedback">
                                        <span class="fa fa-calendar form-control-feedback"></span>
                                        <input type="text" name="tanggal" class="form-control pull-right date-picker"
                                            placeholder="Tanggal Pengajuan" readonly="readonly" id="tanggal_edit">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Besar Simpanan</label>
                                <div class="col-sm-9">
                                    <input type="text" name="nominal" id="nominal_edit" class="form-control input-money"
                                        placeholder="Besar Pengajuan" required="required">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Jenis Simpanan</label>
                                <div class="col-sm-9">
                                    <select class="form-control select2" name="jenis" required="required"
                                        id="data_jenis_edit" onchange="getJenisSimpananEdit(this.value)"
                                        style="width: 100%;"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group clearfix">
                                <label class="col-sm-3 control-label">Besar Simpanan</label>
                                <div class="col-sm-9">
                                    <input type="text" name="besar_simpanan" id="besar_simpanan_edit"
                                        class="form-control input-money" placeholder="Besar Simpanan"
                                        readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Keterangan</label>
                                <div class="col-sm-9">
                                    <textarea name="keterangan" id="keterangan_edit" class="form-control"
                                        placeholder="Catatan / Keterangan"></textarea>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="total_simpanan" id="total_simpanan_edit" readonly="readonly">
                        <input type="hidden" name="nominal_old" id="nominal_old_edit" readonly="readonly">
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="do_edit()" id="btn_edit" class="btn btn-success"><i
                        class="fa fa-floppy-o"></i> Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_delete_partial"></div>
<script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>
<script type="text/javascript">
//wajib diisi
var table = "transaksi_tabungan";
var column = "id";
var kode_tabungan = "<?php echo $tabungan['kode_tabungan']?>";
var url_select = "<?php echo base_url('global_control/select2_global');?>";
var kode_tab = "<?=$tabungan['kode_tabungan']?>";
$(document).ready(function() {
    getTotalSimpanan(kode_tab);
    select_data('data_jenis_add', url_select, 'master_simpanan', 'kode', 'nama', 'placeholder');
    $('#table_data').DataTable({
        ajax: {
            url: "<?php echo base_url('transaksi/data_transaksi/simpanan_all')?>",
            type: 'POST',
            data: {
                access: "<?php echo base64_encode(serialize($access));?>",
                kode_tabungan: kode_tabungan,
            }
        },
        scrollX: true,
        columnDefs: [{
                targets: 0,
                width: '5%',
                render: function(data, type, full, meta) {
                    return '<center>' + (meta.row + 1) + '.</center>';
                }
            },
            {
                targets: 3,
                width: '10%',
                render: function(data, type, full, meta) {
                    return '<center>' + data + '</center>';
                }
            },
            //aksi
            {
                targets: 7,
                width: '5%',
                render: function(data, type, full, meta) {
                    return '<center>' + data + '</center>';
                }
            },
        ]
    });
});

function getJenisSimpanan(kode) {
    var data = {
        kode: kode,
    };
    var callback = getAjaxData("<?php echo base_url('transaksi/data_transaksi/view_select')?>", data);
    $('#besar_simpanan').val(callback['besar_simpanan']);
}

function getJenisSimpananEdit(kode) {
    var data = {
        kode: kode,
    };
    var callback = getAjaxData("<?php echo base_url('transaksi/data_transaksi/view_select')?>", data);
    $('#besar_simpanan_edit').val(callback['besar_simpanan']);
}

function getTotalSimpanan(kode) {
    var data = {
        kode: kode,
    };
    var callback = getAjaxData("<?php echo base_url('transaksi/data_transaksi/view_total_simpanan')?>", data);
    $('#besar_simpanan_view').html(callback['besar_simpanan']);
}

function do_add() {
    if ($("#form_add")[0].checkValidity()) {
        submitAjax("<?php echo base_url('transaksi/data_transaksi/do_add')?>", null, 'form_add', null, null);
        $('#table_data').DataTable().ajax.reload(function() {
            Pace.restart();
        });
        $('#form_add')[0].reset();
        getTotalSimpanan(kode_tab);
    } else {
        notValidParamx();
    }
}

function do_edit() {
    if ($("#form_edit")[0].checkValidity()) {
        submitAjax("<?php echo base_url('transaksi/data_transaksi/do_edit')?>", null, 'form_edit', null, null);
        $('#table_data').DataTable().ajax.reload(function() {
            Pace.restart();
        });
        $('#form_edit')[0].reset();
        getTotalSimpanan(kode_tab);
    } else {
        notValidParamx();
    }
}

function view_modal(id) {
    var data = {
        id: id
    };
    var callback = getAjaxData("<?php echo base_url('transaksi/data_transaksi/simpanan_one')?>", data);
    $('#view').modal('show');
    $('.header_data').html(callback['nama']);
    $('#data_kode_view').html(callback['kode']);
    $('#data_nama_view').html(callback['nama']);
    $('#data_kode_simpanan_view').html(callback['kode_simpanan']);
    $('#data_jenis_simpanan_view').html(callback['jenis_simpanan']);
    $('#data_nominal_view').html(callback['nominal']);
    $('#data_tanggal_view').html(callback['tanggal']);
    $('#data_plus_view').html(callback['plus']);
    $('#data_keterangan_view').html(callback['keterangan']);
    var status = callback['status'];
    if (status == 1) {
        var statusval = '<b class="text-success">Aktif</b>';
    } else {
        var statusval = '<b class="text-danger">Tidak Aktif</b>';
    }
    var flag = callback['flag'];
    if (flag == 'penambah') {
        $('#btn_view_edit').show();
    } else {
        $('#btn_view_edit').hide();
    }
    $('#data_status_view').html(statusval);
    $('#data_create_date_view').html(callback['create_date'] + ' WIB');
    $('#data_update_date_view').html(callback['update_date'] + ' WIB');
    $('input[name="data_id_view"]').val(callback['id']);
    $('#data_create_by_view').html(callback['nama_buat']);
    $('#data_update_by_view').html(callback['nama_update']);
}

function edit_modal(id) {
    select_data('data_jenis_edit', url_select, 'master_simpanan', 'kode', 'nama', 'placeholder');
    var id = $('input[name="data_id_view"]').val();
    var data = {
        id: id
    };
    var callback = getAjaxData("<?php echo base_url('transaksi/data_transaksi/simpanan_one')?>", data);
    $('#view').modal('toggle');
    setTimeout(function() {
        $('#edit').modal('show');
    }, 600);
    $('.header_data').html(callback['nama']);
    $('#data_id_edit').val(callback['id']);
    $('#kode_pengajuan_edit').val(callback['e_kode']);
    $('#data_anggota_edit').val(callback['e_nama']);
    $('#tanggal_edit').val(callback['e_tanggal']);
    $('#nominal_edit').val(callback['e_nominal']);
    $('#nominal_old_edit').val(callback['e_nominal']);
    $('#data_jenis_edit').val(callback['e_jenis_simpanan']).trigger('change');
    $('#keterangan_edit').val(callback['keterangan']);
    $('#total_simpanan_edit').val(callback['total_simpanan']);
}

function delete_modal(id) {
    var data = {
        id_simpanan: id
    };
    var callback = getAjaxData("<?php echo base_url('master/master_simpanan/view_one')?>", data);
    var datax = {
        table: table,
        column: column,
        id: id,
        nama: callback['nama']
    };
    loadModalAjax("<?php echo base_url('pages/load_modal_delete')?>", 'modal_delete_partial', datax, 'delete');
}

function print() {
    $.redirect("<?php echo base_url('pages/print_data_simpanan'); ?>", {
        kode_tabungan: kode_tabungan,
    }, "POST", "_blank");
}
</script>