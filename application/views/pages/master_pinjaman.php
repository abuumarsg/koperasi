<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fas fa-database"></i> Master Data
      <small>Pinjaman</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('pages/dashboard');?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a></li>
      <li class="active">Pinjaman</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fas fa-database"></i> Master Pinjaman</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" onclick="reload_table('table_data')" data-toggle="tooltip" title="Refresh Table"><i class="fas fa-sync"></i></button>
              <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Tutup"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-12">
                    <div class="pull-left">
                      <?php 
                      if (in_array($access['l_ac']['add'], $access['access'])) {
                        echo '<button class="btn btn-success btn-flat" type="button" data-toggle="collapse" data-target="#add_acc"><i class="fa fa-plus"></i> Tambah Master</button>';
                      }
                      ?>
                    </div>
                    <div class="pull-right" style="font-size: 8pt;">
                      <i class="fa fa-toggle-on stat scc"></i> Aktif<br>
                      <i class="fa fa-toggle-off stat err"></i> Tidak Aktif
                    </div>
                  </div>
                </div>
                <?php if(in_array($access['l_ac']['add'], $access['access'])){?>
                  <div class="collapse" id="add_acc">
                    <br>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                      <form id="form_add">
                        <div class="form-group">
                          <label>Kode Pinjaman</label>
                            <input type="text" placeholder="Masukkan Kode Pinjaman" name="kode" id="kode_pinjaman" class="form-control" required="required" readonly="readonly">
                          <label>Nama Pinjaman</label>
                            <input type="text" placeholder="Masukkan Nama Pinjaman" name="nama" class="form-control" required="required">
                          <label>Lama Angsur (bulan)</label>
                            <input type="text" placeholder="Lama Pinjaman" name="lama_pinjam" class="form-control" required="required">
                          <label>Maksimal Pinjaman</label>
                            <input type="text" placeholder="Maksimal Pinjaman" name="maksimal" class="form-control input-money" required="required">
                          <label>Bunga (%)</label>
                            <input type="text" placeholder="Bunga Pinjaman" name="bunga" class="form-control" required="required">
                          <label>Keterangan</label>
										        <textarea class="form-control" name="keterangan" placeholder="Keterangan"></textarea>
                          <br>
                          <div class="form-group">
                            <button type="button" onclick="do_add()" id="btn_add" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
                          </div>
                        </form>
                      </div>
                    </div>
                    </div><?php } ?>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <!-- Data Begin Here -->
                    <table id="table_data" class="table table-bordered table-striped table-responsive" width="100%">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>Kode Pinjaman</th>
                          <th>Nama Pinjaman</th>
                          <th>Lama Angsur (bulan)</th>
                          <th>Maksimal Pinjaman</th>
                          <th>Bunga (%)</th>
                          <th>Status</th>
                          <th>Tanggal</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> 
        <!-- view -->
        <div id="view" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Detail Data <b class="text-muted header_data" style="font-size: 12pt"></b></h2>
                <input type="hidden" name="data_id_view">
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group col-md-12">
                      <label class="col-md-6 control-label">Kode Master Pinjaman</label>
                      <div class="col-md-6" id="data_kode_view"></div>
                    </div>
                    <div class="form-group col-md-12">
                      <label class="col-md-6 control-label">Nama Pinjaman</label>
                      <div class="col-md-6" id="data_nama_view"></div>
                    </div>
                    <div class="form-group col-md-12">
                      <label class="col-md-6 control-label">Lama Angsur (Bulan)</label>
                      <div class="col-md-6" id="data_lama_pinjam_view"></div>
                    </div>
                    <div class="form-group col-md-12">
                      <label class="col-md-6 control-label">Masksimal Pinjaman</label>
                      <div class="col-md-6" id="data_maksimal_view"></div>
                    </div>
                    <div class="form-group col-md-12">
                      <label class="col-md-6 control-label">Bunga (%)</label>
                      <div class="col-md-6" id="data_bunga_view"></div>
                    </div>
                    <div class="form-group col-md-12">
                      <label class="col-md-6 control-label">Keterangan</label>
                      <div class="col-md-6" id="data_keterangan_view"></div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group col-md-12">
                      <label class="col-md-6 control-label">Status</label>
                      <div class="col-md-6" id="data_status_view">

                      </div>
                    </div>
                    <div class="form-group col-md-12">
                      <label class="col-md-6 control-label">Dibuat Tanggal</label>
                      <div class="col-md-6" id="data_create_date_view"></div>
                    </div>
                    <div class="form-group col-md-12">
                      <label class="col-md-6 control-label">Diupdate Tanggal</label>
                      <div class="col-md-6" id="data_update_date_view"></div>
                    </div>
                    <div class="form-group col-md-12">
                      <label class="col-md-6 control-label">Dibuat Oleh</label>
                      <div class="col-md-6" id="data_create_by_view">
                      </div>
                    </div>
                    <div class="form-group col-md-12">
                      <label class="col-md-6 control-label">Diupdate Oleh</label>
                      <div class="col-md-6" id="data_update_by_view">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <?php if (in_array($access['l_ac']['edt'], $access['access'])) {
                  echo '<button type="submit" class="btn btn-info" onclick="edit_modal()"><i class="fa fa-edit"></i> Edit</button>';
                }?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
              </div>
            </div>
          </div>
        </div>

        <!-- edit -->
        <div id="edit" class="modal fade" role="dialog">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Edit Data <b class="text-muted header_data" style="font-size: 12pt"></b></h2>
              </div>
              <div class="modal-body">
                <form id="form_edit">
                  <input type="hidden" id="data_id_edit" name="id" value="">
                  <input type="hidden" id="data_kode_edit_old" name="kode_old" value="">
                  <div class="form-group">
                    <label>Kode Pinjaman</label>
                      <input type="text" placeholder="Masukkan Kode Pinjaman" name="kode" id="kode_pinjaman_edit" class="form-control" readonly="readonly">
                  </div>
                  <div class="form-group">
                    <label>Nama Pinjaman</label>
                      <input type="text" placeholder="Masukkan Nama Pinjaman" id="data_name_edit" name="nama" class="form-control" required="required">
                  </div>
                  <div class="form-group">
                    <label>Lama Angsur (bulan)</label>
                      <input type="text" placeholder="Lama Pinjaman" id="data_lama_pinjam_edit" name="lama_pinjam" class="form-control" required="required">
                  </div>
                  <div class="form-group">
                    <label>Maksimal Pinjaman</label>
                      <input type="text" placeholder="Maksimal Pinjaman" id="data_maksimal_edit" name="maksimal" class="form-control input-money" required="required">
                  </div>
                  <div class="form-group">
                      <label>Bunga (%)</label>
                        <input type="text" placeholder="Bunga Pinjaman" id="data_bunga_edit" name="bunga" class="form-control" required="required">
                  <div class="form-group">
                    <label>Keterangan</label>
										<textarea class="form-control" name="keterangan" id="data_keterangan_edit" placeholder="Keterangan" required="required"></textarea>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" onclick="do_edit()" id="btn_edit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                </div>
              </form>
            </div>

          </div>
        </div>
        <!-- delete -->
        <div id="modal_delete_partial"></div>

      </section>
    </div>
    <script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>
    <script type="text/javascript">
  //wajib diisi
  var table="master_pinjaman";
  var column="id_pinjaman";
  $(document).ready(function(){ 
    kode_generator("<?php echo base_url('master/master_pinjaman/kode');?>",'kode_pinjaman');
    $('#table_data').DataTable( {
      ajax: {
        url: "<?php echo base_url('master/master_pinjaman/view_all/')?>",
        type: 'POST',
        data:{access:"<?php echo base64_encode(serialize($access));?>"}
      },
      scrollX: true,
      columnDefs: [
      {   targets: 0, 
        width: '5%',
        render: function ( data, type, full, meta ) {
          return '<center>'+(meta.row+1)+'.</center>';
        }
      },
      {   targets: 7,
        width: '10%',
        render: function ( data, type, full, meta ) {
          return '<center>'+data+'</center>';
        }
      },
      //aksi
      {   targets: 8, 
        width: '5%',
        render: function ( data, type, full, meta ) {
          return '<center>'+data+'</center>';
        }
      },
      ]
    });
  });
  function view_modal(id) {
    var data={id_pinjaman:id};
    var callback=getAjaxData("<?php echo base_url('master/master_pinjaman/view_one')?>",data);  
    $('#view').modal('show');
    $('.header_data').html(callback['nama']);
    $('#data_kode_view').html(callback['kode']);
    $('#data_nama_view').html(callback['nama']);
    $('#data_lama_pinjam_view').html(callback['lama_pinjam']);
    $('#data_maksimal_view').html(callback['maksimal']);
    $('#data_bunga_view').html(callback['bunga']);
    $('#data_keterangan_view').html(callback['keterangan']);
    var status = callback['status'];
    if(status==1){
      var statusval = '<b class="text-success">Aktif</b>';
    }else{
      var statusval = '<b class="text-danger">Tidak Aktif</b>';
    }
    $('#data_status_view').html(statusval);
    $('#data_create_date_view').html(callback['create_date']+' WIB');
    $('#data_update_date_view').html(callback['update_date']+' WIB');
    $('input[name="data_id_view"]').val(callback['id']);
    $('#data_create_by_view').html(callback['nama_buat']);
    $('#data_update_by_view').html(callback['nama_update']);
  }
  function edit_modal() {

    var id = $('input[name="data_id_view"]').val();
    var data={id_pinjaman:id};
    var callback=getAjaxData("<?php echo base_url('master/master_pinjaman/view_one')?>",data); 
    $('#view').modal('toggle');
    setTimeout(function () {
     $('#edit').modal('show');
   },600); 
    $('.header_data').html(callback['nama']);
    $('#data_id_edit').val(callback['id']);
    $('#kode_pinjaman_edit').val(callback['kode']);
    $('#data_name_edit').val(callback['nama']);
    $('#data_lama_pinjam_edit').val(callback['lama_pinjam']);
    $('#data_maksimal_edit').val(callback['maksimal']);
    $('#data_bunga_edit').val(callback['bunga']);
    $('#data_keterangan_edit').val(callback['keterangan']);
  }
  function delete_modal(id) {
    var data={id_pinjaman:id};
    var callback=getAjaxData("<?php echo base_url('master/master_pinjaman/view_one')?>",data);
    var datax={table:table,column:column,id:id,nama:callback['nama']};
    loadModalAjax("<?php echo base_url('pages/load_modal_delete')?>",'modal_delete_partial',datax,'delete');
  }
  //doing db transaction
  function do_status(id,data) {
    var data_table={status:data};
    var where={id_pinjaman:id};
    var datax={table:table,where:where,data:data_table};
    submitAjax("<?php echo base_url('global_control/change_status')?>",null,datax,null,null,'status');
    $('#table_data').DataTable().ajax.reload(function (){
      Pace.restart();
    });
  }
  function do_edit(){
    if($("#form_edit")[0].checkValidity()) {
      submitAjax("<?php echo base_url('master/edit_master_pinjaman')?>",'edit','form_edit',null,null);
      $('#table_data').DataTable().ajax.reload(function (){
        Pace.restart();
      });
    }else{
      notValidParamx();
    } 
  }
  function do_add(){
    if($("#form_add")[0].checkValidity()) {
      submitAjax("<?php echo base_url('master/add_master_pinjaman')?>",null,'form_add',null,null);
      $('#table_data').DataTable().ajax.reload(function (){
        Pace.restart();
      });
      $('#form_add')[0].reset();

    }else{
      notValidParamx();
    } 
  }
</script>