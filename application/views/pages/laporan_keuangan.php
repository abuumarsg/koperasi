  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <section class="content-header">
          <h1>
              Dashboard
              <small>Laporan Keuangan</small>
          </h1>
          <ol class="breadcrumb">
              <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
              <li class="active">Laporan Keuangan</li>
          </ol>
      </section>
      <section class="content">
          <div class="row">
              <div class="col-lg-4 col-xs-6">
                  <div class="small-box bg-green">
                      <div class="inner">
                          <h3><?php echo $pendapatan;?><sup style="font-size: 20px"></sup></h3>
                          <p style="font-size: 20px">Pendapatan</p>
                      </div>
                      <!-- <a href="<?php echo base_url('pages/berita');?>" class="small-box-footer">Detail <i
                              class="fa fa-arrow-circle-right"></i></a> -->
                  </div>
              </div>
              <div class="col-lg-4 col-xs-6">
                  <div class="small-box bg-red">
                      <div class="inner">
                          <h3><?php echo $pengeluaran;?></h3>
                          <p style="font-size: 20px">Pengeluaran</p>
                      </div>
                      <!-- <a href="<?php echo base_url('pages/video');?>" class="small-box-footer">Detail <i
                              class="fa fa-arrow-circle-right"></i></a> -->
                  </div>
              </div>
              <div class="col-lg-4 col-xs-6">
                  <div class="small-box bg-yellow">
                      <div class="inner">
                          <h3><?php echo $aktiva;?></h3>
                          <p style="font-size: 20px">Aktiva</p>
                      </div>
                      <!-- <a href="<?php echo base_url('pages/gambar');?>" class="small-box-footer">Detail <i
                              class="fa fa-arrow-circle-right"></i></a> -->
                  </div>
              </div>
          </div>
      </section>
  </div>
  <script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>