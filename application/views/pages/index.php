  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <section class="content-header">
          <h1>
              Dashboard
              <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
              <li><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
              <li class="active">Dashboard</li>
          </ol>
      </section>
      <section class="content">
          <div class="row">
              <div class="col-lg-3 col-xs-6">
                  <div class="small-box bg-aqua">
                      <div class="inner">
                          <h3><?php echo $nPinjaman;?></h3>
                          <p style="font-size: 20px">Peminjaman</p>
                      </div>
                      <!-- <div class="icon">
                          <i class="fas fa-money-bill"></i>
                      </div> -->
                      <!-- <a href="<?php echo base_url('pages/inbox');?>" class="small-box-footer">Detail <i
                              class="fa fa-arrow-circle-right"></i></a> -->
                  </div>
              </div>
              <div class="col-lg-3 col-xs-6">
                  <div class="small-box bg-green">
                      <div class="inner">
                          <h3><?php echo $nSimpanan;?><sup style="font-size: 20px"></sup></h3>
                          <p style="font-size: 20px">Simpanan</p>
                      </div>
                      <!-- <div class="icon">
                          <i class="far fa-newspaper"></i>
                      </div> -->
                      <!-- <a href="<?php echo base_url('pages/berita');?>" class="small-box-footer">Detail <i
                              class="fa fa-arrow-circle-right"></i></a> -->
                  </div>
              </div>
              <div class="col-lg-3 col-xs-6">
                  <div class="small-box bg-yellow">
                      <div class="inner">
                          <h3><?php echo $nAngsuran;?></h3>
                          <p style="font-size: 20px">Angsuran</p>
                      </div>
                      <!-- <div class="icon">
                          <i class="far fa-images"></i>
                      </div> -->
                      <!-- <a href="<?php echo base_url('pages/gambar');?>" class="small-box-footer">Detail <i
                              class="fa fa-arrow-circle-right"></i></a> -->
                  </div>
              </div>
              <div class="col-lg-3 col-xs-6">
                  <div class="small-box bg-red">
                      <div class="inner">
                          <h3><?php echo $nAmbil;?></h3>
                          <p style="font-size: 20px">Pengambilan Uang</p>
                      </div>
                      <!-- <div class="icon">
                          <i class="fas fa-video"></i>
                      </div> -->
                      <!-- <a href="<?php echo base_url('pages/video');?>" class="small-box-footer">Detail <i
                              class="fa fa-arrow-circle-right"></i></a> -->
                  </div>
              </div>
          </div>
          <div class="row">
              <section class="col-lg-7 connectedSortable">
                  <div class="col-md-12">
                      <div class="callout callout-info">
                          <label style="font-size: 12pt;"><i class="fa fa-smile-o"></i> Hallo,
                              <?php echo $adm['nama'];?>.</label><br>
                          Website Company Profile Pada
                          <?php echo $this->otherfunctions->companyClientProfile()['name_office'] ?> merupakan suatu
                          perangkat lunak dengan <code>platform web</code> yang bertujuan untuk mempermudah penyampaian
                          informasi pada <?php echo $this->otherfunctions->companyClientProfile()['name_office'] ?>.
                          Untuk memastikan perangkat lunak ini berjalan secara baik, Anda bisa menggunakan browser yang
                          kami rekomendasikan seperti <code>Google Chrome</code> dan <code>Mozilla Firefox</code> dengan
                          versi terbaru, nikmati kemudahan untuk mendapatkan dan berbagi informasi antar sesama.
                          <blockquote class="blockquote-reverse">
                              <p class="mb-0" style="font-size: 9pt;"><?php echo $quote['quote'];?></p>
                              <footer class="blockquote-footer" style="font-size: 8pt;"><cite
                                      title="Penulis <?php echo $quote['person'];?>"><?php echo $quote['person'];?></cite>
                              </footer>
                          </blockquote>
                      </div>
                  </div>
              </section>
              <section class="col-lg-5 connectedSortable">
                  <div class="box box-solid bg-green-gradient">
                      <div class="box-header">
                          <i class="fa fa-calendar"></i>
                          <h3 class="box-title">Calendar</h3>
                          <div class="pull-right box-tools">
                              <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i
                                      class="fa fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i
                                      class="fa fa-times"></i>
                              </button>
                          </div>
                      </div>
                      <div class="box-body no-padding">
                          <div id="calendar" style="width: 100%"></div>
                      </div>
                  </div>
              </section>
          </div>
      </section>
  </div>
  <script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>