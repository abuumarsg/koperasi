<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="far fa-credit-card"></i> Pengelolaan Data
            <small>Tabungan</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('pages/dashboard');?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
            </li>
            <li class="active"><i class="far fa-credit-card"></i> Data Tabungan</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="far fa-credit-card"></i> Data Tabungan</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" onclick="reload_table('table_data')" data-toggle="tooltip"
                                title="Refresh Table"><i class="fas fa-sync"></i></button>
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Tutup"><i
                                    class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <button id="btn_view_sisa" onclick="print()" class="btn btn-warning"><i
                                        class="fa fa-print fa-fw"></i> Print</button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Data Begin Here -->
                                <table id="table_data" class="table table-bordered table-striped table-responsive"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kode Tabungan</th>
                                            <th>Kode Anggota</th>
                                            <th>Nama Anggota</th>
                                            <th>Tanggal</th>
                                            <th>Jumlah Saldo</th>
                                            <th>Tanggal Input<br>Tanggal Update</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    kode_generator("<?php echo base_url('transaksi/data_tabungan/kode');?>", 'kode');
    $('#table_data').DataTable({
        ajax: {
            url: "<?php echo base_url('transaksi/data_tabungan/view_all/')?>",
            type: 'POST',
            data: {
                access: "<?php echo base64_encode(serialize($access));?>"
            }
        },
        scrollX: true,
        columnDefs: [{
                targets: 0,
                width: '5%',
                render: function(data, type, full, meta) {
                    return '<center>' + (meta.row + 1) + '.</center>';
                }
            },
            {
                targets: 6,
                width: '10%',
                render: function(data, type, full, meta) {
                    return '<center>' + data + '</center>';
                }
            },
            //aksi
            {
                targets: 7,
                width: '5%',
                render: function(data, type, full, meta) {
                    return '<center>' + data + '</center>';
                }
            },
        ]
    });
});

function print() {
    $.redirect("<?php echo base_url('pages/print_data_tabungan'); ?>", {
        data_filter: $('#adv_form_filter').serialize()
    }, "POST", "_blank");
}
</script>