<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<i class="fa fa-users"></i> Pengelolaan Data
			<small>Anggota</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('pages/dashboard');?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
			</li>
			<li class="active">Data Anggota</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-users"></i> Data Anggota</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" onclick="reload_table('table_data')" data-toggle="tooltip"
								title="Refresh Table"><i class="fas fa-sync"></i></button>
							<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
									class="fa fa-minus"></i></button>
							<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Tutup"><i
									class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-left">
											<?php 
											if (in_array($access['l_ac']['add'], $access['access'])) {
												echo '<button class="btn btn-success btn-flat" type="button" data-toggle="collapse" data-target="#add_acc"><i class="fa fa-plus"></i> Tambah Anggota</button>';
											}
											?>
										</div>
										<div class="pull-right" style="font-size: 8pt;">
											<i class="fa fa-toggle-on stat scc"></i> Aktif<br>
											<i class="fa fa-toggle-off stat err"></i> Tidak Aktif
										</div>
									</div>
								</div>
								<?php if(in_array($access['l_ac']['add'], $access['access'])){?>
								<div class="collapse" id="add_acc">
									<form id="form_add">
                  						<div class="box-body">
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-3">
														<p class="text-center text-blue" style="font-size: 9pt;"><i class="fa fa-info-circle"></i> Foto Ideal 3x3</p>
														<div id="frame_foto">
															<img id="fotok" class="profile-user-img img-responsive img-circle" src="<?php echo base_url('asset/img/user-photo/user.png');?>" alt="Foto"/>
														</div>
														<div class="form-group clearfix">
															<span class="input-group-btn"> 
																<div class="fileUpload btn btn-warning btn-flat btn-xs btn-block">
																	<span><i class="fa fa-folder-open"></i> Pilih Foto</span>
																	<input id="uploadBtn1" type="file" onchange="checkFile('#uploadBtn1',null,null,event)" class="upload" name="foto"/>
																</div>
																<button id="resf" type="button" class="btn btn-block btn-xs btn-danger" onclick="rest_foto()"><i class="fa fa-camera"></i> Foto Default</button>
															</span>
														</div>
													</div>
													<div class="col-md-9">
														<div class="form-group clearfix">
															<label class="col-sm-3 control-label">Kode Anggota</label>
															<div class="col-sm-9">
																<input type="text" name="kode" id="kode" class="form-control" placeholder="Kode Anggota" readonly="readonly" data-toggle="tooltip">
															</div>
														</div>
														<div class="form-group clearfix">
															<label class="col-sm-3 control-label">Nama Anggota</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="nama" placeholder="Nama Lengkap Anggota" required="required">
															</div>
														</div>
														<div class="form-group clearfix">
															<label class="col-sm-3 control-label">NO KTP</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="no_ktp" placeholder="Nomor KTP" required="required">
															</div>
														</div>
														<div class="form-group clearfix">
															<label class="col-sm-3 control-label">Tempat, Tanggal Lahir</label>
															<div class="col-sm-9">
																<input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir" required="required">
																<div class="has-feedback">
																	<span class="fa fa-calendar form-control-feedback"></span>
																	<input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control pull-right date-picker" placeholder="Tanggal Lahir" required="required">
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group clearfix">
															<label for="nama" class="col-sm-3 control-label">Jenis Kelamin</label>
															<div class="col-sm-9">
																<select class="form-control select2" id="kelamin" name="kelamin" required="required" style="width:100%">
																	<option></option>
																	<?php
																	$gender = $this->otherfunctions->getGenderList();
																	foreach ($gender as $g => $vg) { ?>
																		<option value="<?php echo $g; ?>"><?php echo $vg; ?></option>
																	<?php } ?>
																</select>
															</div>
														</div>
														<div class="form-group clearfix">
															<label class="col-sm-3 control-label">Handphone</label>
															<div class="col-sm-9">
																<input type="text" pattern="\d*" maxlength="15" class="form-control" name="no_hp" placeholder="Nomor HP Anggota">
															</div>
														</div>
													</div>
												</div>
												<div class="row">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group clearfix">
													<label class="col-sm-3 control-label">Simpanan Pokok</label>
													<div class="col-sm-9">
														<input type="text" name="simpanan_pokok" id="simpanan_pokok" class="form-control input-money" placeholder="Simpanan Pokok">
													</div>
												</div>
												<div class="form-group clearfix">
													<label class="col-sm-3 control-label">Tanggal Masuk</label>
													<div class="col-sm-9">
														<div class="has-feedback">
															<span class="fa fa-calendar form-control-feedback"></span>
															<input type="text" name="tgl_masuk" class="form-control pull-right date-picker" placeholder="Tanggal Masuk" required="required">
														</div>
													</div>
												</div>
												<div class="form-group clearfix">
													<label class="col-sm-3 control-label">Alamat</label>
													<div class="col-sm-9">
										        		<textarea class="form-control" name="alamat" placeholder="Alamat"></textarea>
													</div>
												</div>
												<div class="form-group clearfix">
													<label class="col-sm-3 control-label">Pekerjaan</label>
													<div class="col-sm-9">
										        		<textarea class="form-control" name="pekerjaan" placeholder="Pekerjaan"></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="box-footer">
											<div class="pull-right">
												<button type="submit" id="btn_add" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
											</div>
										</div>
									</form>
								</div>
								<?php } ?>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-12">
								<!-- Data Begin Here -->
								<table id="table_data" class="table table-bordered table-striped table-responsive" width="100%">
									<thead>
										<tr>
											<th>No.</th>
											<th>Kode</th>
											<th>Nama</th>
											<th>Tempat, Tanggal Lahir</th>
											<th>Pekerjaan</th>
											<th>Tanggal Masuk</th>
											<th>Tanggal Input<br>Tanggal Update</th>
											<th>Status</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
</div>
<!-- view -->
<div id="view" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h2 class="modal-title">Detail Data <b class="text-muted header_data" style="font-size: 12pt"></b></h2>
				<input type="hidden" name="data_id_view">
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
                  		<div id="data_foto_view"></div>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">Kode</label>
							<div class="col-md-6" id="data_kode_view"></div>
						</div>
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">Nama Anggota</label>
							<div class="col-md-6" id="data_nama_view"></div>
						</div>
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">Tempat, Tanggal Lahir</label>
							<div class="col-md-6" id="data_ttl_view"></div>
						</div>
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">NO KTP</label>
							<div class="col-md-6" id="data_ktp_view"></div>
						</div>
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">NO HP</label>
							<div class="col-md-6" id="data_no_hp_view"></div>
						</div>
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">Jenis Kelamin</label>
							<div class="col-md-6" id="data_kelamin_view"></div>
						</div>
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">Pekerjaan</label>
							<div class="col-md-6" id="data_pekerjaan_view"></div>
						</div>
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">Alamat</label>
							<div class="col-md-6" id="data_alamat_view"></div>
						</div>
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">Tanggal Masuk</label>
							<div class="col-md-6" id="data_tanggal_masuk_view"></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">Status</label>
							<div class="col-md-6" id="data_status_view">

							</div>
						</div>
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">Dibuat Tanggal</label>
							<div class="col-md-6" id="data_create_date_view"></div>
						</div>
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">Diupdate Tanggal</label>
							<div class="col-md-6" id="data_update_date_view"></div>
						</div>
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">Dibuat Oleh</label>
							<div class="col-md-6" id="data_create_by_view">
							</div>
						</div>
						<div class="form-group col-md-12">
							<label class="col-md-6 control-label">Diupdate Oleh</label>
							<div class="col-md-6" id="data_update_by_view">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<?php if (in_array($access['l_ac']['edt'], $access['access'])) {
                  echo '<button type="submit" class="btn btn-info" onclick="edit_modal()"><i class="fa fa-edit"></i> Edit</button>';
                }?>
				<button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
			</div>
		</div>
	</div>
</div>

<!-- edit -->
<div id="edit" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h2 class="modal-title">Edit Data <b class="text-muted header_data" style="font-size: 12pt"></b></h2>
			</div>
			<form id="form_edit">
				<div class="modal-body">
					<input type="hidden" id="data_id_edit" name="id" value="">
					<input type="hidden" id="data_kode_edit_old" name="kode_old" value="">
					<input type="hidden" id="foto_old" name="foto_old" value="">
					<div class="col-md-12">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-3">
									<p class="text-center text-blue" style="font-size: 9pt;"><i class="fa fa-info-circle"></i> Foto Ideal 3x3</p>
									<div id="frame_foto">
										<img id="fotok_edit" class="profile-user-img img-responsive img-circle" src="<?php echo base_url('asset/img/user-photo/user.png');?>" alt="Foto"/>
									</div>
									<div class="form-group clearfix">
										<span class="input-group-btn"> 
											<div class="fileUpload btn btn-warning btn-flat btn-xs btn-block">
												<span><i class="fa fa-folder-open"></i> Pilih Foto</span>
												<input id="uploadBtn2" type="file" onchange="checkFile_e('#uploadBtn2',null,null,event)" class="upload" name="foto"/>
											</div>
											<button id="resf2" type="button" class="btn btn-block btn-xs btn-danger" onclick="rest_foto_e()"><i class="fa fa-camera"></i> Foto Default</button>
										</span>
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group clearfix">
										<label>Kode Anggota</label>
										<input type="text" name="kode" id="kode_edit" class="form-control" placeholder="Kode Anggota" readonly="readonly" data-toggle="tooltip">
									</div>
									<div class="form-group clearfix">
										<label>Nama Anggota</label>
										<input type="text" class="form-control" name="nama" id="nama_edit" placeholder="Nama Lengkap Anggota" required="required">
									</div>
									<div class="form-group clearfix">
										<label>NO KTP</label>
										<input type="text" class="form-control" name="no_ktp" id="ktp_edit" placeholder="Nomor KTP" required="required">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group clearfix">
										<label>Tempat, Tanggal Lahir</label>
										<input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir_edit" placeholder="Tempat Lahir" required="required">
										<div class="has-feedback">
											<span class="fa fa-calendar form-control-feedback"></span>
											<input type="text" name="tgl_lahir" id="tgl_lahir_edit" class="form-control pull-right date-picker" placeholder="Tanggal Lahir" required="required">
										</div>
									</div>
									<div class="form-group clearfix">
										<label for="nama">Jenis Kelamin</label>
										<select class="form-control select2" id="kelamin_edit" name="kelamin" required="required" style="width:100%">
											<option></option>
											<?php
											$gender = $this->otherfunctions->getGenderList();
											foreach ($gender as $g => $vg) { ?>
												<option value="<?php echo $g; ?>"><?php echo $vg; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group clearfix">
										<label>Handphone</label>
										<input type="text" pattern="\d*" maxlength="15" class="form-control" name="no_hp" id="no_hp_edit" placeholder="Nomor HP Anggota">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group clearfix">
								<label>Simpanan Pokok</label>
								<input type="text" name="simpanan_pokok" id="simpanan_pokok_edit" class="form-control input-money" placeholder="Simpanan Pokok" readonly="readonly">
							</div>
							<div class="form-group clearfix">
								<label>Tanggal Masuk</label>
								<div class="has-feedback">
									<span class="fa fa-calendar form-control-feedback"></span>
									<input type="text" name="tgl_masuk" id="tgl_masuk_edit" class="form-control pull-right date-picker" placeholder="Tanggal Masuk" required="required">
								</div>
							</div>
							<div class="form-group clearfix">
								<label>Alamat</label>
								<textarea class="form-control" name="alamat" id="alamat_edit" placeholder="Alamat"></textarea>
							</div>
							<div class="form-group clearfix">
								<label>Pekerjaan</label>
								<textarea class="form-control" name="pekerjaan" id="pekerjaan_edit" placeholder="Pekerjaan"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="row">
						<div class="col-md-12">
							<button type="submit" id="btn_edit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- delete -->
<div id="modal_delete_partial"></div>

</section>
</div>
<script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>
<script type="text/javascript">
	//wajib diisi
	var table = "data_anggota";
	var column = "id_anggota";
	$(document).ready(function () {
		kode_generator("<?php echo base_url('transaksi/data_anggota/kode');?>", 'kode');
		$('#table_data').DataTable({
			ajax: {
				url: "<?php echo base_url('transaksi/data_anggota/view_all/')?>",
				type: 'POST',
				data: {
					access: "<?php echo base64_encode(serialize($access));?>"
				}
			},
			scrollX: true,
			columnDefs: [{
					targets: 0,
					width: '5%',
					render: function (data, type, full, meta) {
						return '<center>' + (meta.row + 1) + '.</center>';
					}
				},
				{
					targets: 2,
					width: '30%',
					render: function (data, type, full, meta) {
               			return '<div class="contain-img-profile"><img src="<?=base_url()?>'+full[9]+'" class="profile-user-img img-responsive img-circle view_photo pull-left" style="width:50px;height:50px" title="Foto '+data+'"> <span class="title-img-profile">'+data+'</span></div>';
					}
				},
				{
					targets: 5,
					width: '10%',
					render: function (data, type, full, meta) {
						return '<center>' + data + '</center>';
					}
				},
				//aksi
				{
					targets: 6,
					width: '5%',
					render: function (data, type, full, meta) {
						return '<center>' + data + '</center>';
					}
				},
			]
		});
      $('#form_add').validator().on('submit', function (e) {
         if (e.isDefaultPrevented()) {
            notValidParamx();
         } else {
            e.preventDefault(); 
            var data_add = new FormData(this);
            var urladd = "<?php echo base_url('transaksi/add_anggota');?>";
            submitAjaxFile(urladd,data_add,null,null,null);
            $('#form_add')[0].reset();
            // refreshCode();
            // resetselectAdd();
         }
      })
      $('#form_edit').validator().on('submit', function (e) {
         if (e.isDefaultPrevented()) {
            notValidParamx();
         } else {
            e.preventDefault(); 
            var data_edit = new FormData(this);
            var urledit = "<?php echo base_url('transaksi/edit_anggota');?>";
            submitAjaxFile(urledit,data_edit,'#edit',null,null);
            $('#form_edit')[0].reset();
         }
      })
	});
   function checkFile(idf,idt,btnx,event) {
      var fext = ['jpg', 'png', 'jpeg', 'gif'];
      var output = document.getElementById('fotok');
      output.src = URL.createObjectURL(event.target.files[0]);
      pathFile(idf,idt,fext,btnx);
      var valfoto = $(idf).val();
      if(valfoto==''){
         output.src = "<?php echo base_url('asset/img/user-photo/user.png');?>";
      }
   }
   function rest_foto() {
      var output = document.getElementById('fotok');
      output.src = "<?php echo base_url('asset/img/user-photo/user.png');?>";
      $('#uploadBtn1').val('');
   }
   function checkFile_e(idf,idt,btnx,event) {
      var fext = ['jpg', 'png', 'jpeg', 'gif'];
      var output = document.getElementById('fotok_edit');
      output.src = URL.createObjectURL(event.target.files[0]);
      pathFile(idf,idt,fext,btnx);
      var valfoto = $(idf).val();
      if(valfoto==''){
         output.src = "<?php echo base_url('asset/img/user-photo/user.png');?>";
      }
   }
   function rest_foto_e() {
      var output = document.getElementById('fotok_edit');
      output.src = "<?php echo base_url('asset/img/user-photo/user.png');?>";
      $('#uploadBtn2').val('');
   }

	function view_modal(id) {
		var data = {id_anggota: id};
		var callback = getAjaxData("<?php echo base_url('transaksi/data_anggota/view_one')?>", data);
		$('#view').modal('show');
		$('.header_data').html(callback['nama']);
      	$('#data_foto_view').html('<img class="profile-user-img img-responsive img-circle view_photo" data-source-photo="'+callback['foto']+'" src="'+callback['foto']+'" alt="User profile picture">');
		$('#data_kode_view').html(callback['kode']);
		$('#data_nama_view').html(callback['nama']);
		$('#data_ttl_view').html(callback['ttl']);
		$('#data_ktp_view').html(callback['ktp']);
		$('#data_no_hp_view').html(callback['no_hp']);
		$('#data_kelamin_view').html(callback['kelamin']);
		$('#data_pekerjaan_view').html(callback['pekerjaan']);
		$('#data_alamat_view').html(callback['alamat']);
		$('#data_tanggal_masuk_view').html(callback['tanggal_masuk']);
		var status = callback['status'];
		if (status == 1) {
			var statusval = '<b class="text-success">Aktif</b>';
		} else {
			var statusval = '<b class="text-danger">Tidak Aktif</b>';
		}
		$('#data_status_view').html(statusval);
		$('#data_create_date_view').html(callback['create_date'] + ' WIB');
		$('#data_update_date_view').html(callback['update_date'] + ' WIB');
		$('input[name="data_id_view"]').val(callback['id']);
		$('#data_create_by_view').html(callback['nama_buat']);
		$('#data_update_by_view').html(callback['nama_update']);
	}

	function edit_modal() {
		var id = $('input[name="data_id_view"]').val();
		var data = {
			id_anggota: id
		};
		var callback = getAjaxData("<?php echo base_url('transaksi/data_anggota/view_one')?>", data);
		$('#view').modal('toggle');
		setTimeout(function () {
			$('#edit').modal('show');
		}, 600);
		var output = document.getElementById('fotok_edit');
		if(callback['foto_db'] == ''){
			output.src = "<?php echo base_url('asset/img/user-photo/user.png');?>";
		}else{
			var files = "<?php echo base_url();?>"+callback['foto_db'];
			output.src = files;
		}
		$('.header_data').html(callback['nama']);
		$('#data_id_edit').val(callback['id']);
		$('#kode_edit').val(callback['kode']);
		$('#nama_edit').val(callback['nama']);
		$('#ktp_edit').val(callback['ktp']);
		$('#tempat_lahir_edit').val(callback['tempat_lahir']);
		$('#tgl_lahir_edit').val(callback['tanggal_lahir']);
		$('#kelamin_edit').val(callback['kelamin_edit']);
		$('#no_hp_edit').val(callback['no_hp']);
		$('#simpanan_pokok_edit').val(callback['simpanan_pokok']);
		$('#tgl_masuk_edit').val(callback['tanggal_masuk_edit']);
		$('#alamat_edit').val(callback['alamat']);
		$('#pekerjaan_edit').val(callback['pekerjaan']);
		$('#foto_old').val(callback['foto_db']);
	}

	function delete_modal(id) {
		var data = {
			id_anggota: id
		};
		var callback = getAjaxData("<?php echo base_url('transaksi/data_anggota/view_one')?>", data);
		var datax = {
			table: table,
			column: column,
			id: id,
			nama: callback['nama']
		};
		loadModalAjax("<?php echo base_url('pages/load_modal_delete')?>", 'modal_delete_partial', datax, 'delete');
	}
	//doing db transaction
	function do_status(id, data) {
		var data_table = {
			status: data
		};
		var where = {
			id_anggota: id
		};
		var datax = {
			table: table,
			where: where,
			data: data_table
		};
		submitAjax("<?php echo base_url('global_control/change_status')?>", null, datax, null, null, 'status');
		$('#table_data').DataTable().ajax.reload(function () {
			Pace.restart();
		});
	}
</script>