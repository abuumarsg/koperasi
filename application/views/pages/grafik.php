<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="far fa-chart-bar"></i> Grafik
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fas fa-tachometer-alt"></i> Dashboard</a></li>
            <li class="active"><i class="far fa-chart-bar"></i> Grafik</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <section class="col-lg-6 connectedSortable">
                <div class="box box-success">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-right">
                            <li class="pull-left header"><i class="far fa-chart-bar"></i> Grafik Peminjaman</li>
                        </ul>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <form id="form_peminjaman">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Tahun</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="tahun_pinjam" placeholder="Tahun"
                                                    class="form-control tahun search_peminjaman"
                                                    data-date-format="yyyy">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="col-md-12" id="peminjamanChart_cols"><canvas id="peminjamanChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-lg-6 connectedSortable">
                <div class="box box-success">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-right">
                            <li class="pull-left header"><i class="far fa-chart-bar"></i> Grafik Simpanan</li>
                        </ul>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <form id="form_simpanan">
                                    <!-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Bulan</label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2 search_simpanan" id="bulan_pinjam"
                                                    name="bulan_pinjam" style="width: 100%;">
                                                    <option></option>
                                                    <?php
                                                    // for ($i=1; $i <= 12; $i++) { 
                                                    //     echo '<option value="'.$this->formatter->zeroPadding($i).'">'.$this->formatter->getNameOfMonth($i).'</option>'; 
                                                    // } 
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Tahun</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="tahun_pinjam" placeholder="Tahun"
                                                    class="form-control tahun search_simpanan" data-date-format="yyyy">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="col-md-12" id="simpananChart_cols"><canvas id="simpananChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="row">
            <section class="col-lg-6 connectedSortable">
                <div class="box box-success">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-right">
                            <li class="pull-left header"><i class="far fa-chart-bar"></i> Grafik Angsuran</li>
                        </ul>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <form id="form_angsuran">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Tahun</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="tahun_pinjam" placeholder="Tahun"
                                                    class="form-control tahun search_angsuran" data-date-format="yyyy">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="col-md-12" id="angsuranChart_cols"><canvas id="angsuranChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-lg-6 connectedSortable">
                <div class="box box-success">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-right">
                            <li class="pull-left header"><i class="far fa-chart-bar"></i> Grafik Pengambilan Uang</li>
                        </ul>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <form id="form_ambil">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Tahun</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="tahun_pinjam" placeholder="Tahun"
                                                    class="form-control tahun search_ambil" data-date-format="yyyy">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="col-md-12" id="ambilChart_cols"><canvas id="ambilChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
<script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>
<script type="text/javascript">
var url_select = "<?php echo base_url('global_control/select2_global');?>";
$(document).ready(function() {
    var search_peminjaman = $('#form_peminjaman').serialize();
    drawChart("<?php echo base_url('chart/chart_peminjaman');?>", "line", "peminjamanChart", true, {
        param: search_peminjaman
    });
    $('.search_peminjaman').change(function() {
        peminjaman();
    });
    var search_simpanan = $('#form_simpanan').serialize();
    drawChart("<?php echo base_url('chart/chart_simpanan');?>", "bar", "simpananChart", true, {
        param: search_simpanan
    });
    $('.search_simpanan').change(function() {
        simpanan();
    });
    var search_angsuran = $('#form_angsuran').serialize();
    drawChart("<?php echo base_url('chart/chart_angsuran');?>", "horizontalBar", "angsuranChart", true, {
        param: search_angsuran
    });
    $('.search_angsuran').change(function() {
        angsuran();
    });
    var search_ambil = $('#form_ambil').serialize();
    drawChart("<?php echo base_url('chart/chart_ambil');?>", "line", "ambilChart", true, {
        param: search_ambil
    });
    $('.search_ambil').change(function() {
        ambil();
    });
});

// function refreshChart(id) {
//     var urlx;
//     var range;
//     var type;
//     if (id == 'peminjamanChart') {
//         var search = $('#form_peminjaman').serialize();
//         urlx = "<?php echo base_url('chart/chart_peminjaman');?>";
//         range = false;
//         type = 'line';
//     } else if (id == 'statusKarChart') {
//         var search = $('#form_status').serialize();
//         urlx = "<?php echo base_url('chart/chart_status_kar');?>";
//         range = false;
//         type = 'pie';
//     } else if (id == 'bagianChart') {
//         var search = $('#form_bagian').serialize();
//         urlx = "<?php echo base_url('chart/chart_bagian');?>";
//         range = false;
//         type = 'horizontalBar';
//     } else if (id == 'pendidikanChart') {
//         var search = $('#form_pendidikan').serialize();
//         urlx = "<?php echo base_url('chart/chart_pendidikan');?>";
//         range = false;
//         type = 'bar';
//     }
//     drawChart(urlx, type, id, range, {
//         param: search
//     });
// }

function peminjaman() {
    var search = $('#form_peminjaman').serialize();
    drawChart("<?php echo base_url('chart/chart_peminjaman');?>", "line", "peminjamanChart", false, {
        param: search
    });
}

function simpanan() {
    var search = $('#form_simpanan').serialize();
    drawChart("<?php echo base_url('chart/chart_simpanan');?>", "bar", "simpananChart", false, {
        param: search
    });
}

function angsuran() {
    var search = $('#form_angsuran').serialize();
    drawChart("<?php echo base_url('chart/chart_angsuran');?>", "horizontalBar", "angsuranChart", false, {
        param: search
    });
}

function ambil() {
    var search = $('#form_ambil').serialize();
    drawChart("<?php echo base_url('chart/chart_ambil');?>", "line", "ambilChart", false, {
        param: search
    });
}
</script>