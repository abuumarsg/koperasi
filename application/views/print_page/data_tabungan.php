<div style="padding: 10px;">
    <style type="text/css">
    th {
        padding: 5px;
        text-align: center;
        white-space: pre;
    }

    td {
        padding: 5px;
        white-space: pre;
    }

    @media print,
    screen {
        @page {
            size: Legal landscape;
            margin: 10mm;
        }

        .box-id {
            /* page-break-inside: avoid; */
            /* transform: scale(0.9, 0.9); */
            page-break-after: always;
        }

        .font {
            font-size: 8pt;
        }

        .border-buttom {
            border-bottom-style: solid;
            border-width: 3px;
        }

        .photo {
            top: 7px;
            position: center;
            max-width: 90px;
            /* max-height:113.38px; */
            max-height: 90px;
            object-fit: cover;
        }

        .col-sm-1,
        .col-sm-2,
        .col-sm-3,
        .col-sm-4,
        .col-sm-5,
        .col-sm-6,
        .col-sm-7,
        .col-sm-8,
        .col-sm-9,
        .col-sm-10,
        .col-sm-11,
        .col-sm-12 {
            float: left;
        }

        .col-sm-12 {
            width: 100%;
        }

        .col-sm-11 {
            width: 91.66666667%;
        }

        .col-sm-10 {
            width: 83.33333333%;
        }

        .col-sm-9 {
            width: 75%;
        }

        .col-sm-8 {
            width: 66.66666667%;
        }

        .col-sm-7 {
            width: 58.33333333%;
        }

        .col-sm-6 {
            width: 50%;
        }

        .col-sm-5 {
            width: 41.66666667%;
        }

        .col-sm-4 {
            width: 33.33333333%;
        }

        .col-sm-3 {
            width: 25%;
        }

        .col-sm-2 {
            width: 16.66666667%;
        }

        .col-sm-1 {
            width: 8.33333333%;
        }
    }
    </style>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-2">
                <div style="font-weight: bold;text-align: left;">
                    <img class="photo" src="<?php echo base_url($com['logo']);?>" alt="Foto" />
                </div>
            </div>
            <div class="col-sm-10">
                <h5><?=$com['name'];?></h5>
                <h5><?=$com['alamat'];?></h5>
                <h5><?=$com['no_hp'];?></h5>
            </div>
        </div>
    </div>
    <div class="row">
        <br>
        <div class="border-buttom"></div>
        <div style="font-weight: bold;text-align: center;">LAPORAN SELURUH TABUNGAN</div>
        <br>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div style="font-weight: bold;text-align: left;">Dicetak Pada :
                <?php $datetime = $this->otherfunctions->getDateNow();
                echo $this->formatter->getDayDateFormatUserIdTime($datetime);?>
            </div>
        </div>
    </div>
    <table class="border" width="100%" border="3">
        <tr>
            <th class="font">No.</th>
            <th class="font">Kode Tabungan</th>
            <th class="font">Kode Anggota</th>
            <th class="font">Nama Anggota</th>
            <th class="font">Jumlah Saldo</th>
        </tr>
        <?php
		$no = 1;
		$besar_tabungan=0;
		foreach ($datax as $d) { ?>
        <tr>
            <td class="font"><?php echo $no; ?></td>
            <td class="font"><?php echo $d->kode_tabungan;?></td>
            <td class="font"><?php echo $d->kode_anggota;?></td>
            <td class="font"><?php echo $d->nama_anggota;?></td>
            <td class="font"><?=$this->formatter->getFormatMoneyUser($d->besar_tabungan)?></td>
        </tr>
        <?php
			$no++;
			$besar_tabungan+=$d->besar_tabungan;
		}
	?>
        <tr>
            <td></td>
            <td><b>TOTAL</b></td>
            <td></td>
            <td></td>
            <td><b><?=$this->formatter->getFormatMoneyUser($besar_tabungan)?></b></td>
        </tr>
    </table>
    <!-- <table width="100%" style="margin-top: 20px;">
	<tr>
		<td colspan="2" style="text-align: right;padding-right: 10px;">Karangjati, <?php echo date('d').' '.$this->formatter->getMonth()[date('m')].' '.date('Y'); ?></td>
	</tr>
	<tr width="50%">
		<td style="text-align: center;">Menyetujui</td>
		<td style="text-align: center;">Dibuat Oleh</td>
	</tr>
	<tr width="50%">
		<td style="padding-top: 50px;"></td>
		<td style="padding-top: 50px;"></td>
	</tr>
	<tr width="50%">
		<td style="text-align: center;"><?=$menyetujui?></td>
		<td style="text-align: center;"><?=$nama_buat?></td>
	</tr>
</table> -->
</div>