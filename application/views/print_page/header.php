<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Koperasi">
    <meta name="author" content="PT Intech Indonesia">
    <title><?php 
    echo $this->otherfunctions->titlePages($this->uri->segment(2));
    ?> Koperasi</title>
    <meta name="theme-color" content="#131c5b">
    <link rel="icon" href="<?php echo base_url();?>asset/img/favicon.png" type="image/png">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/bootstrap/dist/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/font-awesome/css/font-awesome.min.css');?>"
        media="print">
    <link rel="stylesheet" href="<?php echo base_url('asset/bower_components/Ionicons/css/ionicons.min.css');?>"
        media="print">
    <link rel="stylesheet" href="<?php echo base_url('asset/dist/css/AdminLTE.css');?>" media="print">
    <link rel="stylesheet" href="<?php echo base_url('asset/dist/css/skins/_all-skins.min.css');?>" media="print">
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" media="print">
    <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet" media="print">
    <link href="https://fonts.googleapis.com/css?family=Kaushan Script" rel="stylesheet" media="print">
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"
        media="print">
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'
        media="print">
    <style type="text/css" media="print">
    .text_red {
        color: red;
    }

    @media print,
    screen {
        @page {
            size: Legal landscape;
            margin: 10mm;
        }

        .box-id {
            /* page-break-inside: avoid; */
            /* transform: scale(0.9, 0.9); */
            page-break-after: always;
        }

        .font {
            font-size: 8pt;
        }

        .border-buttom {
            border-bottom-style: solid;
            border-width: 3px;
        }

        .photo {
            top: 7px;
            position: center;
            max-width: 50px;
            /* max-height:113.38px; */
            max-height: 50px;
            object-fit: cover;
        }

        .col-sm-1,
        .col-sm-2,
        .col-sm-3,
        .col-sm-4,
        .col-sm-5,
        .col-sm-6,
        .col-sm-7,
        .col-sm-8,
        .col-sm-9,
        .col-sm-10,
        .col-sm-11,
        .col-sm-12 {
            float: left;
        }

        .col-sm-12 {
            width: 100%;
        }

        .col-sm-11 {
            width: 91.66666667%;
        }

        .col-sm-10 {
            width: 83.33333333%;
        }

        .col-sm-9 {
            width: 75%;
        }

        .col-sm-8 {
            width: 66.66666667%;
        }

        .col-sm-7 {
            width: 58.33333333%;
        }

        .col-sm-6 {
            width: 50%;
        }

        .col-sm-5 {
            width: 41.66666667%;
        }

        .col-sm-4 {
            width: 33.33333333%;
        }

        .col-sm-3 {
            width: 25%;
        }

        .col-sm-2 {
            width: 16.66666667%;
        }

        .col-sm-1 {
            width: 8.33333333%;
        }
    }
    </style>
</head>

<body class="hold-transition login-page" onload="window.print()">