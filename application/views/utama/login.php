 <hr> <div class="login-box-body">
    <p class="login-box-msg">Login <b class="fnt">Administrator</b></p>
    <form id="form_first">
      <div class="form-group has-feedback">
        <?php echo form_input('username','',array('class'=>'form-control','placeholder'=>'Username','required'=>'required','id'=>'username'));?>
        <!-- <span class="form-control-feedback fa fa-at"></span> -->
      </div>
      <div class="form-group has-feedback">
        <?php echo form_password('password','',array('class'=>'form-control','placeholder'=>'Password','required'=>'required','id'=>'password'));?>
        <!-- <span class="fa fa-lock form-control-feedback"></span> -->
      </div>
      <div class="row">
        <div class="col-xs-8">
          <!-- <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div> -->
        </div>
        <div class="col-xs-4">
          <button type="button" onclick="do_auth()" id="action_btn" class="btn btn-success btn-block "><i class="fa fa-sign-in"></i> Login</button>
        </div>
      </div>
    </form>
    <!-- <div id="forget_pass"></div> -->

  </div>
</div>
<script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function () {
    form_key('form_first','action_btn');
});
function do_auth() {
  submitAjax("<?php echo base_url('auth/do_login')?>",null,'form_first',null,null,'auth');
}
</script>