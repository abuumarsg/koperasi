  <div class="login-box-body">
    <p class="login-box-msg">Lupa Password</p> 
    <div class="alert alert-info text-center" id="alert-success"><h4><i class="fa fa-info-circle"></i> Petunjuk</h4> Anda akan mendapatkan panduan reset password dari email anda, cek spam dan kotak masuk!</div>
    <form id="form_first">
    <div class="form-group has-feedback">
      <input type="email" name="email" class="form-control" placeholder="Masukkan Email Anda" required="required">
      <span class="form-control-feedback fa fa-envelope"></span>
    </div>
    </form>
    <div class="row">
      <div class="col-xs-4 pull-left">
        <a class="btn btn-primary" href="<?php echo base_url('auth');?>"><i class="fa fa-chevron-circle-left"></i> Kembali ke Login</a>
      </div>
      <div class="col-xs-4 pull-right">
        <button type="button" onclick="do_send_email()" class="btn btn-success btn-block "><i class="fa fa-paper-plane"></i> Kirim</button>
      </div>
    </div>

  </div>
</div>
<script src="<?php echo base_url('asset/bower_components/jquery/dist/jquery.min.js');?>"></script>
<script type="text/javascript">
function do_send_email() {
  submitAjax("<?php echo base_url('auth/send_email_forget')?>",null,'form_first',null,null,'auth');
}
</script>