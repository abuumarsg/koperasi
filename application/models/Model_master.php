<?php

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Model_master
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_master extends CI_Model
{
	function __construct()
    {
        parent::__construct();
        $this->date = $this->otherfunctions->getDateNow();
	}
	
	//--------------------------------------------------------------------------------------------------------------//
	//Setting Hak Akses
	public function getListAccess()
	{
		$this->db->order_by('create_date','DESC');
		$query=$this->db->get('master_access')->result();
		return $query;
	}
	public function getAccess($id){
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('master_access AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where('a.id_access',$id); 
		$query=$this->db->get()->result();
		return $query;
	}
	
	public function checkAccessCode($code)
	{
		return $this->model_global->checkCode($code,'master_access','kode_access');
	}

	//--------------------------------------------------------------------------------------------------------------//
	//Setting User Group
	public function getListUserGroup($level_admin)
	{
		$this->db->select('a.*');
		$this->db->from('master_user_group a');
		$this->db->order_by('create_date','DESC');
		if($level_admin!=0){
			$this->db->where('a.id_group !=',1); 
		}
		$query=$this->db->get()->result();
		return $query;
	}
	public function getUserGroupOne($id)
	{
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('master_user_group AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where('a.id_group',$id); 
		$query=$this->db->get()->result();
		return $query;
	}
	public function getUserGroup($id){
		return $this->db->get_where('master_user_group',array('id_group'=>$id))->row_array();
	}
	//--------------------------------------------------------------------------------------------------------------//
	//Setting Menu Management 
	public function getListMenu()
	{
		$where=['a.id_menu !='=>0];
		$this->db->select('a.*,b.nama as parent_name,');
		$this->db->from('master_menu AS a');
		$this->db->join('master_menu AS b', 'b.id_menu = a.parent', 'left'); 
		$this->db->order_by('a.create_date','DESC');
		$this->db->where($where);
		$query=$this->db->get()->result();
		return $query;
	}
	public function getMenu($id){
		return $this->db->get_where('master_menu',array('id_menu'=>$id,'status'=>1,'id_menu !='=>0))->row_array();
	}
	public function getAllMenubyId($id){
		$where=['a.id_menu'=>$id];
		$this->db->select('a.*,b.nama as parent_name,c.nama as nama_buat, d.nama as nama_update');
		$this->db->from('master_menu AS a');
		$this->db->join('master_menu AS b', 'b.id_menu = a.parent', 'inner'); 
		$this->db->join('admin AS c', 'c.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS d', 'd.id_admin = a.update_by', 'left'); 
		$this->db->where($where);
		$query=$this->db->get()->result();
		return $query;
	}
	public function getListMenuActive()
	{
		$this->db->order_by('sequence','ASC');
		$query=$this->db->get_where('master_menu',['status'=>1,'id_menu !='=>0])->result();
		return $query;
	}
	//Setting Menu User Admin Management 
	public function getListMenuUser()
	{
		$where=['a.id_menu !='=>0];
		$this->db->select('a.*,b.nama as parent_name,');
		$this->db->from('master_menu_user AS a');
		$this->db->join('master_menu_user AS b', 'b.id_menu = a.parent', 'left'); 
		$this->db->order_by('a.create_date','DESC');
		$this->db->where($where);
		$query=$this->db->get()->result();
		return $query;
	}
	public function getMenuUser($id){
		return $this->db->get_where('master_menu_user',array('id_menu'=>$id,'status'=>1,'id_menu !='=>0))->row_array();
	}
	public function getAllMenuUserbyId($id){
		$where=['a.id_menu'=>$id];
		$this->db->select('a.*,b.nama as parent_name,c.nama as nama_buat, d.nama as nama_update');
		$this->db->from('master_menu_user AS a');
		$this->db->join('master_menu_user AS b', 'b.id_menu = a.parent', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS d', 'd.id_admin = a.update_by', 'left'); 
		$this->db->where($where);
		$query=$this->db->get()->result();
		return $query;
	}
	public function getListMenuUserActive()
	{
		$this->db->order_by('sequence','ASC');
		$query=$this->db->get_where('master_menu_user',['status'=>1,'id_menu !='=>0])->result();
		return $query;
	}
	public function getListUserMenuActive()
	{
		$this->db->order_by('sequence','ASC');
		$query=$this->db->get_where('master_menu_user',['status'=>1,'id_menu !='=>0])->result();
		return $query;
	}
	function list_menu(){
		return $this->db->query("SELECT * FROM master_menu ORDER BY sequence ASC")->result();
	}
	function list_access(){
		return $this->db->get('master_access')->result();
	}
	function list_user_group(){
		return $this->db->get('master_user_group')->result();
	}
	function user_group_avl(){
		return $this->db->get_where('master_user_group',array('status'=>'aktif'))->result();
	}
	function loker_avl(){
		return $this->db->get_where('master_loker',array('status'=>'aktif'))->result();
	}
	function bidang_avl(){
		return $this->db->get_where('master_bidang',array('status'=>'aktif'))->result();
	}
	function access_avl(){
		return $this->db->get_where('master_access',array('status'=>'aktif'))->result();
	}
	function menu_avl(){
		return $this->db->query("SELECT * FROM master_menu WHERE status = 'aktif' ORDER BY sequence ASC")->result();
	}
	//--------------------------------------------------------------------------------------------------------------//
	//Master Simpanan
	public function getListMasterSimpanan()
	{
		$this->db->order_by('create_date','DESC');
		$query=$this->db->get('master_simpanan')->result();
		return $query;
	}
	public function getListMasterSimpananNew()
	{
		$this->db->order_by('id_simpanan','ASC');
		$query=$this->db->get('master_simpanan')->result();
		return $query;
	}
	public function getMasterSimpanan($id){
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('master_simpanan AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where('a.id_simpanan',$id); 
		$query=$this->db->get()->result();
		return $query;
	}
	//Master Pinjaman
	public function getListMasterPinjaman()
	{
		$this->db->order_by('create_date','DESC');
		$query=$this->db->get('master_pinjaman')->result();
		return $query;
	}
	public function getMasterPinjaman($id){
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('master_pinjaman AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where('a.id_pinjaman',$id); 
		$query=$this->db->get()->result();
		return $query;
	}
	public function getMasterPinjamanKode($kode){
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('master_pinjaman AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where('a.kode',$kode); 
		$query=$this->db->get()->row_array();
		return $query;
	}
	public function getMasterSimpananKode($kode){
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('master_simpanan AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where('a.kode',$kode); 
		$query=$this->db->get()->row_array();
		return $query;
	}
	// Custom Menu
	public function customMenuUrl($url){
		$this->db->select('*');
		$this->db->from('master_menu_user'); 
		$this->db->where('url',$url);
		$query=$this->db->get()->row_array();
		return $query;
	}
}