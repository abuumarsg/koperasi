<?php
/**
* 
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_karyawan extends CI_Model
{
	protected $CI;
	function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
    }
	function list_karyawan(){
		return $this->db->get('karyawan')->result();
	}
	function count_emp(){
		return $this->db->get('karyawan')->num_rows();
	}
	function emp($id){
		return $this->db->get_where('karyawan',array('id_karyawan'=>$id))->row_array();
	}
	function forget_email($em){
		return $this->db->get_where('karyawan',array('email'=>$em))->row_array();
	}
	function res($t){
		return $this->db->get_where('karyawan',array('reset_token'=>$t))->row_array();
	}
	function emp_cek($un,$pass){
		return $this->db->get_where('karyawan',array('nik'=>$un,'password'=>$pass))->row_array();
	}
	function emp_cek_root($un,$pass){
		return $this->db->get_where('karyawan',array('nik'=>$un,'root_password'=>$pass))->row_array();
	}
	function log_login($id){
		return $this->db->get_where('log_login_karyawan',array('id_karyawan'=>$id))->result();
	}
	function emp_nik($nik){
		return $this->db->get_where('karyawan',array('nik'=>$nik))->row_array();
	}
	//new function
	public function getEmployeeId($id)
	{
		$this->db->select('emp.*,loker.nama as nama_loker,jbt.nama as nama_jabatan,bag.nama as bagian,b.nama as nama_buat, c.nama as nama_update, grd.nama as nama_grade, d.nama as nama_loker_grade');
		$this->db->from('karyawan AS emp');
		$this->db->join('master_jabatan AS jbt', 'emp.jabatan = jbt.kode_jabatan', 'left');
		$this->db->join('master_loker AS loker', 'emp.loker = loker.kode_loker', 'left');
		$this->db->join('master_bagian AS bag', 'jbt.kode_bagian = bag.kode_bagian', 'left');
		$this->db->join('admin AS b', 'b.id_admin = emp.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = emp.update_by', 'left'); 
		$this->db->join('master_grade AS grd', 'emp.grade = grd.kode_grade', 'left');
		$this->db->join('master_loker AS d', 'grd.kode_loker = d.kode_loker', 'left');
		$this->db->where('emp.id_karyawan',$id);
		$query=$this->db->get()->row_array();
		return $query;
	}
	public function getEmployeeAllActive()
	{
		$this->db->select('emp.*,loker.nama as nama_loker,jbt.nama as nama_jabatan,bag.nama as bagian, grd.nama as nama_grade,d.nama as nama_loker_grade');
		$this->db->from('karyawan AS emp');
		$this->db->join('master_jabatan AS jbt', 'emp.jabatan = jbt.kode_jabatan', 'left');
		$this->db->join('master_loker AS loker', 'emp.loker = loker.kode_loker', 'left');
		$this->db->join('master_bagian AS bag', 'jbt.kode_bagian = bag.kode_bagian', 'left');
		$this->db->join('master_grade AS grd', 'emp.grade = grd.kode_grade', 'left');
		$this->db->join('master_loker AS d', 'grd.kode_loker = d.kode_loker', 'left');
		$this->db->where('status_emp',1);
		$query=$this->db->get()->result();
		return $query;
	}
	public function checkNik($nik)
	{
		return $this->model_global->checkCode($nik,'karyawan','nik');
	}
	public function getEmployeeForSelect2()
	{
		$data=$this->getEmployeeAllActive();
		$pack=[];
		foreach ($data as $d) {
			$pack[$d->id_karyawan]=$d->nama.((!empty($d->nama_jabatan)) ? ' ('.$d->nama_jabatan.')' : '');
		}
		return $pack;
	}
	public function getListEmployeeActive()
	{
		return $this->model_global->listUserActiveRecord('karyawan','id_karyawan','nama','fo');
	}
	public function getEmployeeByEmail($email)
	{
		return $this->db->get_where('karyawan',array('email'=>$email))->row_array();
	}
	public function getEmployeeByToken($token)
	{
		return $this->db->get_where('karyawan',array('reset_token'=>$token))->row_array();
	}
	public function emp_jbt($j){
		return $this->db->get_where('karyawan',array('jabatan'=>$j))->result();
	}
	public function getBawahan($j)
	{
		$CI =& get_instance();
		if (empty($j)) 
			return null;
		$bag=[];
		$j_bwh=$CI->model_master->getBawahan($j);
		if (count($j_bwh) == 0) {
			return null;
		}else{
			foreach ($j_bwh as $jb) {
				$kar=$this->emp_jbt($jb->kode_jabatan);
				if (count($kar) > 0) {
					foreach ($kar as $k) {
						array_push($bag, $k->id_karyawan);
					}
				}
			}
		}
		if (count($bag)>0) {
			return $bag;
		}else{
			return null;
		}
	}
	public function getConvertNiktoId($nik)
	{
		if (empty($nik)) 
			return null;
		$kar=$this->emp_nik($nik);
		if (isset($kar)) {
			return $kar['id_karyawan'];
		}else{
			return null;
		}
	}
	public function getConvertIdtoNik($id)
	{
		if (empty($id)) 
			return null;
		$kar=$this->emp_nik($id);
		if (isset($kar)) {
			return $kar['nik'];
		}else{
			return null;
		}
	}
	//for chart
	public function getEmployeeDateInChart()
	{
		$data=$this->db->query('SELECT YEAR(STR_TO_DATE(tgl_masuk, "%Y-%m-%d")) as tahun,COUNT(*) as jumlah  FROM karyawan GROUP BY tahun')->result();
		$pack=[];
		$pack['label']='Data Karyawan Masuk';
		foreach ($data as $d) {
			$pack['tahun'][]=$d->tahun;
			$pack['jumlah'][]=$d->jumlah;
		}
		return $pack;
	}
	public function getEmployeeGenderChart()
	{
		$data=$this->db->query('SELECT kelamin,count(*) as jumlah FROM karyawan GROUP BY kelamin')->result();
		$pack=[];
		$pack['label']='Data Karyawan Kelamin';
		foreach ($data as $d) {
			$pack['kelamin'][]=$this->otherfunctions->getGender($d->kelamin);
			$pack['jumlah'][]=$d->jumlah;
		}
		return $pack;
	}
	//Log Karyawan
	function log_kar($idkar){
		return $this->db->get_where('log_login_karyawan', array('id_karyawan'=>$idkar))->result();
	}
	//Grade Karyawan
	function emp_grade($kode){
		$this->db->select('grd.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('master_grade AS grd');
		$this->db->join('admin AS b', 'b.id_admin = grd.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = grd.update_by', 'left'); 
		$this->db->where('grd.kode_grade',$kode);
		$query=$this->db->get()->row_array();
		return $query;
	}
//              ____  ____    __  __                ||
//             / _  |/ _  \  / / / /                ||
//=======     / /_/ / /_) / / / / /      ===========||
//           / __  / /__) \/ /_/ /                  ||
//          /_/ / /_______/\____/                   ||

	function getEmployeeNik($nik){
		// return $this->db->get_where('karyawan',array('nik'=>$nik))->row_array();
		$this->db->select('emp.*,sts.nama as nama_status,b.nama as nama_buat, c.nama as nama_update,d.nama as nama_jabatan,e.nama as nama_grade,f.nama as nama_loker,g.nama as nama_level_jabatan,h.nama as nama_peringatan,i.nama as nama_bagian,j.nama as nama_loker_grade');
		$this->db->from('karyawan AS emp');
		$this->db->join('master_status_karyawan AS sts', 'emp.status_karyawan = sts.kode_status', 'left');
		$this->db->join('admin AS b', 'b.id_admin = emp.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = emp.update_by', 'left'); 
		$this->db->join('master_jabatan AS d', 'd.kode_jabatan = emp.jabatan', 'left'); 
		$this->db->join('master_grade AS e', 'e.kode_grade = emp.grade', 'left'); 
		$this->db->join('master_loker AS f', 'f.kode_loker = emp.loker', 'left'); 
		$this->db->join('master_level_jabatan AS g', 'g.kode_level_jabatan = d.kode_level', 'left'); 
		$this->db->join('master_surat_peringatan AS h', 'h.kode_sp = emp.status_disiplin', 'left'); 
		$this->db->join('master_bagian AS i', 'i.kode_bagian = d.kode_bagian', 'left'); 
		$this->db->join('master_loker AS j', 'j.kode_loker = e.kode_loker', 'left'); 
		$this->db->where('emp.nik',$nik);
		$query=$this->db->get()->row_array();
		return $query;
	}
	function getEmployeeOneNik($nik){
		// return $this->db->get_where('karyawan',array('nik'=>$nik))->row_array();
		$this->db->select('emp.*,sts.nama as nama_status,b.nama as nama_buat, c.nama as nama_update,d.nama as nama_jabatan,e.nama as nama_grade,f.nama as nama_loker,g.nama as nama_level_jabatan,h.nama as nama_peringatan,i.nama as nama_bagian,j.nama as nama_loker_grade');
		$this->db->from('karyawan AS emp');
		$this->db->join('master_status_karyawan AS sts', 'emp.status_karyawan = sts.kode_status', 'left');
		$this->db->join('admin AS b', 'b.id_admin = emp.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = emp.update_by', 'left'); 
		$this->db->join('master_jabatan AS d', 'd.kode_jabatan = emp.jabatan', 'left'); 
		$this->db->join('master_grade AS e', 'e.kode_grade = emp.grade', 'left'); 
		$this->db->join('master_loker AS f', 'f.kode_loker = emp.loker', 'left'); 
		$this->db->join('master_level_jabatan AS g', 'g.kode_level_jabatan = d.kode_level', 'left'); 
		$this->db->join('master_surat_peringatan AS h', 'h.kode_sp = emp.status_disiplin', 'left'); 
		$this->db->join('master_bagian AS i', 'i.kode_bagian = d.kode_bagian', 'left'); 
		$this->db->join('master_loker AS j', 'j.kode_loker = e.kode_loker', 'left'); 
		$this->db->where('emp.nik',$nik);
		$query=$this->db->get()->result();
		return $query;
	}
	function getEmpID($id_karyawan){
		$this->db->select('emp.*,sts.nama as nama_status,b.nama as nama_buat, c.nama as nama_update,d.nama as nama_jabatan,e.nama as nama_grade,f.nama as nama_loker,g.nama as nama_level_jabatan');
		$this->db->from('karyawan AS emp');
		$this->db->join('master_status_karyawan AS sts', 'emp.status_karyawan = sts.kode_status', 'left');
		$this->db->join('admin AS b', 'b.id_admin = emp.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = emp.update_by', 'left'); 
		$this->db->join('master_jabatan AS d', 'd.kode_jabatan = emp.jabatan', 'left'); 
		$this->db->join('master_grade AS e', 'e.kode_grade = emp.grade', 'left'); 
		$this->db->join('master_loker AS f', 'f.kode_loker = emp.loker', 'left'); 
		$this->db->join('master_level_jabatan AS g', 'g.kode_level_jabatan = d.kode_level', 'left'); 
		$this->db->where('emp.id_karyawan',$id_karyawan);
		$query=$this->db->get()->row_array();
		return $query;
	}
	// Karyawan Anak
	function getListAnak($nik)	{
		return $this->db->get_where('karyawan_anak',array('nik'=>$nik))->result();	
	}
	function getAnak($id,$nik){
		$where = array(
						'id_anak' => $id,
						'nik'=> $nik
					);
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('karyawan_anak AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where($where); 
		$query=$this->db->get()->result();
		return $query;
	}
	//Saudara
	function saudara($nik){
		return $this->db->get_where('karyawan_saudara', array('nik'=>$nik))->result();
	}
	function getSaudara($id,$nik){	
		$where = array(
						'id_saudara' => $id,
						'nik'=> $nik
					);
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('karyawan_saudara AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where($where); 
		$query=$this->db->get()->result();
		return $query;
	}
	//Pendidikan
	function pendidikan($nik){
		return $this->db->get_where('karyawan_pendidikan', array('nik'=>$nik))->result();
	}
	function pendidikan_max($nik){
		$sql=("SELECT * FROM karyawan_pendidikan WHERE id_k_pendidikan = (SELECT max(id_k_pendidikan) FROM karyawan_pendidikan WHERE nik='$nik')");
		return $this->db->query($sql)->row_array();
	}
	function countPendidikan($nik){
		return $this->db->get_where('karyawan_pendidikan', array('nik'=>$nik))->num_rows();
	}
	function getPendidikan($id,$nik){
		$where = array(
						'id_k_pendidikan' => $id,
						'nik'=> $nik
					);
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('karyawan_pendidikan AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where($where); 
		$query=$this->db->get()->result();
		return $query;
	}
	//Pendidikan Non Formal
	function pnf($nik){
		return $this->db->get_where('karyawan_pnf', array('nik'=>$nik))->result();
	}
	function getPnf($id,$nik){
		$where = array(
						'id_k_pnf' => $id,
						'nik'=> $nik
					);
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('karyawan_pnf AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where($where); 
		$query=$this->db->get()->result();
		return $query;
	}
	//Organisasi
	function organisasi($nik){
		$this->db->order_by('update_date','DESC');
		return $this->db->get_where('karyawan_organisasi', array('nik'=>$nik))->result();
	}
	function getOrganisasi($id,$nik){
		$where = array(
						'id_k_organisasi' => $id,
						'nik'=> $nik
					);
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('karyawan_organisasi AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where($where); 
		$query=$this->db->get()->result();
		return $query;
	}
	//Penghargaan
	function penghargaan($nik){
		$this->db->order_by('update_date','DESC');
		return $this->db->get_where('karyawan_penghargaan', array('nik'=>$nik))->result();
	}
	function getPenghargaan($id,$nik){
		$where = array(
						'id_k_penghargaan' => $id,
						'nik'=> $nik
					);
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('karyawan_penghargaan AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where($where); 
		$query=$this->db->get()->result();
		return $query;
	}
	//Bahasa
	function bahasa($nik){
		$this->db->order_by('update_date','DESC');
		return $this->db->get_where('karyawan_bahasa', array('nik'=>$nik))->result();
	}
	function getBahasa($id,$nik){
		$where = array(
						'id_k_bahasa' => $id,
						'nik'=> $nik
					);
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('karyawan_bahasa AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where($where); 
		$query=$this->db->get()->result();
		return $query;
	}
//MUTASI
	public function getListMutasi()
	{
		$sc="SELECT a.*,(select count(*) from mutasi_jabatan cnt where cnt.id_karyawan = a.id_karyawan) as jum,
		b.nama as nama_jabatan,
		c.nama as nama_loker,
		d.nama as nama_jabatan_baru,
		e.nama as nama_loker_baru,
		f.nama as nama_karyawan,
		f.nik as nik_karyawan,
		g.nama as nama_status
		FROM mutasi_jabatan a
		LEFT JOIN master_jabatan b ON b.kode_jabatan = a.jabatan_asal
		LEFT JOIN master_loker c ON c.kode_loker = a.lokasi_asal
		LEFT JOIN master_jabatan d ON d.kode_jabatan = a.jabatan_baru
		LEFT JOIN master_loker e ON e.kode_loker = a.lokasi_baru
		LEFT JOIN karyawan f ON f.id_karyawan = a.id_karyawan
		LEFT JOIN master_mutasi g ON g.kode_mutasi = a.status_mutasi
		WHERE id_mutasi = (SELECT max(id_mutasi) FROM mutasi_jabatan x WHERE x.id_karyawan = a.id_karyawan) ORDER BY create_date DESC";
		$query=$this->db->query($sc)->result();
		return $query;
	}
	public function getPilihKaryawanMutasi()
	{
		$this->db->select('emp.id_karyawan as id_karyawan,emp.nama as nama, emp.jabatan as jabatan, emp.loker as loker, emp.nik as nik, loker.nama as nama_loker, jbt.nama as nama_jabatan');
		$this->db->from('karyawan AS emp');
		$this->db->join('master_jabatan AS jbt', 'emp.jabatan = jbt.kode_jabatan', 'left');
		$this->db->join('master_loker AS loker', 'emp.loker = loker.kode_loker', 'left');
		$this->db->where('emp.status',1);
		$this->db->where('emp.status_emp',1);
		// $this->db->order_by('create_date','DESC');
		$query=$this->db->get()->result();
		return $query;
	}
	public function getMutasi($id)
	{
		$this->db->select('a.*,b.nama as nama_buat,c.nama as nama_update,
		d.nama as nama_jabatan,
		e.nama as nama_loker,
		f.nama as nama_jabatan_baru,
		g.nama as nama_loker_baru,
		h.nama as nama_karyawan,
		h.nik as nik_karyawan,
		i.nama as nama_status,
		j.nama as nama_mengetahui,
		k.nama as jbt_mengetahui,
		l.nama as nama_jabatan_baru,
		m.nama as nama_loker_baru,
		n.nama as nama_menyetujui,
		o.nama as jbt_menyetujui');
		$this->db->from('mutasi_jabatan AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left');
		$this->db->join('master_jabatan AS d', 'd.kode_jabatan = a.jabatan_asal', 'left');
		$this->db->join('master_loker AS e', 'e.kode_loker = a.lokasi_asal', 'left');
		$this->db->join('master_jabatan AS f', 'f.kode_jabatan = a.jabatan_baru', 'left');
		$this->db->join('master_loker AS g', 'g.kode_loker = a.lokasi_baru', 'left');
		$this->db->join('karyawan AS h', 'h.id_karyawan = a.id_karyawan', 'left');
		$this->db->join('master_mutasi AS i', 'i.kode_mutasi = a.status_mutasi', 'left');
		$this->db->join('karyawan AS j', 'j.id_karyawan = a.mengetahui', 'left');
		$this->db->join('master_jabatan AS k', 'k.kode_jabatan = j.jabatan', 'left');
		$this->db->join('master_jabatan AS l', 'l.kode_jabatan = h.jabatan', 'left');
		$this->db->join('master_loker AS m', 'm.kode_loker = h.loker', 'left');
		$this->db->join('karyawan AS n', 'n.id_karyawan = a.menyetujui', 'left');
		$this->db->join('master_jabatan AS o', 'o.kode_jabatan = n.jabatan', 'left');
		$this->db->where('id_mutasi',$id); 
		$query=$this->db->get()->result();
		return $query;
	}
	public function getListMutasiNik($nik)
	{
		$where=['h.nik'=>$nik];
		$this->db->select('a.*,
		d.nama as nama_jabatan,
		e.nama as nama_loker,
		f.nama as nama_jabatan_baru,
		g.nama as nama_loker_baru,
		h.nik as nik,
		h.nama as nama_karyawan,
		i.nama as nama_status,
		j.nama as nama_jabatan,
		k.nama as nama_loker');
		$this->db->from('mutasi_jabatan AS a');
		$this->db->join('master_jabatan AS d', 'd.kode_jabatan = a.jabatan_asal', 'left');
		$this->db->join('master_loker AS e', 'e.kode_loker = a.lokasi_asal', 'left');
		$this->db->join('master_jabatan AS f', 'f.kode_jabatan = a.jabatan_baru', 'left');
		$this->db->join('master_loker AS g', 'g.kode_loker = a.lokasi_baru', 'left');
		$this->db->join('karyawan AS h', 'h.id_karyawan = a.id_karyawan', 'left');
		$this->db->join('master_mutasi AS i', 'i.kode_mutasi = a.status_mutasi', 'left');
		$this->db->join('master_jabatan AS j', 'j.kode_jabatan = h.jabatan', 'left'); 
		$this->db->join('master_loker AS k', 'k.kode_loker = h.loker', 'left'); 
		$this->db->where($where); 
		$this->db->order_by('create_date','ASC');
		$query=$this->db->get()->result();
		return $query;
	}
	public function checkMutasiCode($code)
	{
		return $this->model_global->checkCode($code,'mutasi_jabatan','no_sk');
	}
//PERJANJIAN KERJA
	public function getListPerjanjianKerja()
	{
		$sc="SELECT a.*,(select count(*) from perjanjian_kerja cnt where cnt.nik = a.nik) as jum,
		b.nama as nama_status_lama,
		c.nama as nama_status_baru,
		d.nama as nama_karyawan
		FROM perjanjian_kerja a
		LEFT JOIN master_surat_perjanjian b ON b.kode_perjanjian = a.status_lama
		LEFT JOIN master_surat_perjanjian c ON c.kode_perjanjian = a.status_baru
		LEFT JOIN karyawan d ON d.nik = a.nik
		WHERE id_p_kerja = (SELECT max(id_p_kerja) FROM perjanjian_kerja x WHERE x.nik = a.nik) ORDER BY create_date DESC";
		$query=$this->db->query($sc)->result();
		return $query;
	}
	function listPerjanjianNik($nik){
		$sql="SELECT * FROM perjanjian_kerja WHERE id_p_kerja=(SELECT max(id_p_kerja) FROM perjanjian_kerja WHERE nik = '$nik')";
		return $this->db->query($sql)->row_array();
	}
	public function getPilihKaryawanPerjanjian()
	{
		$this->db->select('emp.id_karyawan as id_karyawan,emp.nama as nama,emp.nik as knik,c.nama as nama_status,b.*');
		$this->db->from('karyawan AS emp');
		//$this->db->join('master_surat_perjanjian AS a', 'emp.status_perjanjian = a.kode_perjanjian', 'left');
		$this->db->join('perjanjian_kerja AS b', 'b.no_sk_baru = emp.status_perjanjian', 'left');
		$this->db->join('master_surat_perjanjian AS c', 'c.kode_perjanjian = b.status_baru', 'left');
		$this->db->where('emp.status',1);
		$this->db->where('emp.status_emp',1);
		//$this->db->where('b.id_p_kerja','b.id_p_kerja(SELECT max(id_p_kerja))');
		$query=$this->db->get()->result();
		return $query;
	}
	public function getPilihKaryawanPerjanjianNIK($nik)
	{
		$this->db->select('emp.nama as nama,emp.nik as knik,c.nama as nama_status,b.*');
		$this->db->from('karyawan AS emp');
		$this->db->join('perjanjian_kerja AS b', 'b.no_sk_baru = emp.status_perjanjian', 'left');
		$this->db->join('master_surat_perjanjian AS c', 'c.kode_perjanjian = b.status_baru', 'left');
		$this->db->where('b.nik',$nik);
		$query=$this->db->get()->result();
		return $query;
	}
	public function getPerjanjianKerja($id)
	{
		$this->db->select('a.*,b.nama as nama_buat,c.nama as nama_update,
		d.nama as nama_status_lama,
		e.nama as nama_status_baru,
		h.nama as nama_karyawan,
		j.nama as nama_mengetahui,
		k.nama as jbt_mengetahui,
		l.nama as nama_jabatan,
		m.nama as nama_loker,
		n.nama as nama_menyetujui,
		o.nama as jbt_menyetujui');
		$this->db->from('perjanjian_kerja AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left');
		$this->db->join('master_surat_perjanjian AS d', 'd.kode_perjanjian = a.status_lama', 'left');
		$this->db->join('master_surat_perjanjian AS e', 'e.kode_perjanjian = a.status_baru', 'left');
		$this->db->join('karyawan AS h', 'h.nik = a.nik', 'left');
		$this->db->join('karyawan AS j', 'j.id_karyawan = a.mengetahui', 'left');
		$this->db->join('master_jabatan AS k', 'k.kode_jabatan = j.jabatan', 'left');
		$this->db->join('master_jabatan AS l', 'l.kode_jabatan = h.jabatan', 'left');
		$this->db->join('master_loker AS m', 'm.kode_loker = h.loker', 'left');
		$this->db->join('karyawan AS n', 'n.id_karyawan = a.menyetujui', 'left');
		$this->db->join('master_jabatan AS o', 'o.kode_jabatan = n.jabatan', 'left');
		$this->db->where('id_p_kerja',$id); 
		$query=$this->db->get()->result();
		return $query;
	}
	public function getListPerjanjianKerjaNik($nik)
	{
		$where=['a.nik'=>$nik];
		$this->db->select('a.*,
		d.nama as nama_status_lama,
		e.nama as nama_status_baru,
		h.nama as nama_karyawan,
		j.nama as nama_mengetahui,
		k.nama as jbt_mengetahui,
		l.nama as nama_menyetujui,
		m.nama as jbt_menyetujui');
		$this->db->from('perjanjian_kerja AS a');
		$this->db->join('master_surat_perjanjian AS d', 'd.kode_perjanjian = a.status_lama', 'left');
		$this->db->join('master_surat_perjanjian AS e', 'e.kode_perjanjian = a.status_baru', 'left');
		$this->db->join('karyawan AS h', 'h.nik = a.nik', 'left');
		$this->db->join('karyawan AS j', 'j.nik = a.mengetahui', 'left');
		$this->db->join('master_jabatan AS k', 'k.kode_jabatan = j.jabatan', 'left');
		$this->db->join('karyawan AS l', 'l.nik = a.menyetujui', 'left');
		$this->db->join('master_jabatan AS m', 'm.kode_jabatan = l.jabatan', 'left');
		$this->db->where($where); 
		$this->db->order_by('create_date','ASC');
		$query=$this->db->get()->result();
		return $query;
	}
	public function checkPerjanjianCode($code)
	{
		return $this->model_global->checkCode($code,'perjanjian_kerja','no_sk_baru');
	}
//PERINGATAN KERJA
	public function getListPeringatan()
	{
		$sc="SELECT a.*,(select count(*) from peringatan_karyawan cnt where cnt.id_karyawan = a.id_karyawan) as jum,
		b.nama as nama_status_lama,
		c.nama as nama_status_baru,
		d.nik as nik_karyawan,
		d.nama as nama_karyawan
		FROM peringatan_karyawan a
		LEFT JOIN master_surat_peringatan b ON b.kode_sp = a.status_asal
		LEFT JOIN master_surat_peringatan c ON c.kode_sp = a.status_baru
		LEFT JOIN karyawan d ON d.id_karyawan = a.id_karyawan
		WHERE id_peringatan = (SELECT max(id_peringatan) FROM peringatan_karyawan x WHERE x.id_karyawan = a.id_karyawan) ORDER BY create_date DESC";
		$query=$this->db->query($sc)->result();
		return $query;
	}
	public function getPilihKaryawanPeringatan()
	{
		$this->db->select('emp.id_karyawan as id_kar,emp.nama as nama,emp.nik as nik,emp.status_disiplin as kode_disiplin,d.nama as nama_disiplin,b.*');
		$this->db->from('karyawan AS emp');
		$this->db->join('peringatan_karyawan AS b', 'b.status_baru = emp.status_disiplin', 'left');
		$this->db->join('master_surat_peringatan AS c', 'c.kode_sp = b.status_baru', 'left');
		$this->db->join('master_surat_peringatan AS d', 'emp.status_disiplin = d.kode_sp', 'left');
		$this->db->where('emp.status',1);
		$this->db->where('emp.status_emp',1);
		$query=$this->db->get()->result();
		return $query;
	}
	public function getPeringatanKerja($id)
	{
		$this->db->select('a.*,b.nama as nama_buat,c.nama as nama_update,
		d.nama as nama_status_lama,
		e.nama as nama_status_baru,
		h.nama as nama_karyawan,
		h.nik as nik_karyawan,
		j.nama as nama_mengetahui,
		k.nama as jbt_mengetahui,
		l.nama as nama_jabatan,
		m.nama as nama_loker,
		n.nama as nama_menyetujui,
		o.nama as jbt_menyetujui');
		$this->db->from('peringatan_karyawan AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left');
		$this->db->join('master_surat_peringatan AS d', 'd.kode_sp = a.status_asal', 'left');
		$this->db->join('master_surat_peringatan AS e', 'e.kode_sp = a.status_baru', 'left');
		$this->db->join('karyawan AS h', 'h.id_karyawan = a.id_karyawan', 'left');
		$this->db->join('karyawan AS j', 'j.id_karyawan = a.mengetahui', 'left');
		$this->db->join('master_jabatan AS k', 'k.kode_jabatan = j.jabatan', 'left');
		$this->db->join('master_jabatan AS l', 'l.kode_jabatan = h.jabatan', 'left');
		$this->db->join('master_loker AS m', 'm.kode_loker = h.loker', 'left');
		$this->db->join('karyawan AS n', 'n.id_karyawan = a.menyetujui', 'left');
		$this->db->join('master_jabatan AS o', 'o.kode_jabatan = n.jabatan', 'left');
		$this->db->where('id_peringatan',$id); 
		$query=$this->db->get()->result();
		return $query;
	}
	public function getListPeringatanNik($nik)
	{
		$where=['h.nik'=>$nik];
		$this->db->select('a.*,
		d.nama as nama_status_lama,
		e.nama as nama_status_baru,
		h.nama as nama_karyawan,
		h.nik as nik_karyawan,
		j.nama as nama_mengetahui,
		k.nama as jbt_mengetahui,
		l.nama as nama_menyetujui,
		m.nama as jbt_menyetujui');
		$this->db->from('peringatan_karyawan AS a');
		$this->db->join('master_surat_peringatan AS d', 'd.kode_sp = a.status_asal', 'left');
		$this->db->join('master_surat_peringatan AS e', 'e.kode_sp = a.status_baru', 'left');
		$this->db->join('karyawan AS h', 'h.id_karyawan = a.id_karyawan', 'left');
		$this->db->join('karyawan AS j', 'j.id_karyawan = a.mengetahui', 'left');
		$this->db->join('master_jabatan AS k', 'k.kode_jabatan = j.jabatan', 'left');
		$this->db->join('karyawan AS l', 'l.id_karyawan = a.menyetujui', 'left');
		$this->db->join('master_jabatan AS m', 'm.kode_jabatan = l.jabatan', 'left');
		$this->db->where($where); 
		$this->db->order_by('create_date','ASC');
		$query=$this->db->get()->result();
		return $query;
	}
	public function checkPeringatanCode($code)
	{
		return $this->model_global->checkCode($code,'peringatan_karyawan','no_sk');
	}
//GRADE KARYAWAN
	public function getListGrade()
	{
		$sc="SELECT a.*,(select count(*) from grade_karyawan cnt where cnt.id_karyawan = a.id_karyawan) as jum,
		b.nama as nama_grade_lama,
		c.nama as nama_grade_baru,
		d.nik as nik_karyawan,
		d.nama as nama_karyawan,
		e.nama as nama_loker_grade
		FROM grade_karyawan a
		LEFT JOIN master_grade b ON b.kode_grade = a.grade_asal
		LEFT JOIN master_grade c ON c.kode_grade = a.grade_baru
		LEFT JOIN karyawan d ON d.id_karyawan = a.id_karyawan
		LEFT JOIN master_loker e ON e.kode_loker = c.kode_loker
		WHERE a.id_grade = (SELECT max(id_grade) FROM grade_karyawan x WHERE x.id_karyawan = a.id_karyawan) ORDER BY create_date DESC";
		$query=$this->db->query($sc)->result();
		return $query;
	}
	public function getPilihKaryawanGrade()
	{
		$this->db->select('emp.id_karyawan as id_kar,emp.nama as nama,emp.nik as nik,emp.grade as kode_grade,d.nama as nama_grade,e.nama as nama_loker,b.*');
		$this->db->from('karyawan AS emp');
		$this->db->join('grade_karyawan AS b', 'b.grade_baru = emp.grade', 'left');
		$this->db->join('master_grade AS c', 'c.kode_grade = b.grade_baru', 'left');
		$this->db->join('master_grade AS d', 'emp.grade = d.kode_grade', 'left');
		$this->db->join('master_loker AS e', 'e.kode_loker = d.kode_loker', 'left');
		$this->db->where('emp.status',1);
		$this->db->where('emp.status_emp',1);
		$query=$this->db->get()->result();
		return $query;
	}
	public function getGradeKaryawan($id)
	{
		$this->db->select('a.*,b.nama as nama_buat,c.nama as nama_update,
		d.nama as nama_grade_lama,
		e.nama as nama_grade_baru,
		h.nama as nama_karyawan,
		h.nik as nik_karyawan,
		j.nama as nama_mengetahui,
		k.nama as jbt_mengetahui,
		l.nama as nama_jabatan,
		m.nama as nama_loker,
		n.nama as nama_menyetujui,
		o.nama as jbt_menyetujui,
		p.nama as nama_loker_grade,
		q.nama as nama_loker_grade_lama');
		$this->db->from('grade_karyawan AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left');
		$this->db->join('master_grade AS d', 'd.kode_grade = a.grade_asal', 'left');
		$this->db->join('master_grade AS e', 'e.kode_grade = a.grade_baru', 'left');
		$this->db->join('karyawan AS h', 'h.id_karyawan = a.id_karyawan', 'left');
		$this->db->join('karyawan AS j', 'j.id_karyawan = a.mengetahui', 'left');
		$this->db->join('master_jabatan AS k', 'k.kode_jabatan = j.jabatan', 'left');
		$this->db->join('master_jabatan AS l', 'l.kode_jabatan = h.jabatan', 'left');
		$this->db->join('master_loker AS m', 'm.kode_loker = h.loker', 'left');
		$this->db->join('karyawan AS n', 'n.id_karyawan = a.menyetujui', 'left');
		$this->db->join('master_jabatan AS o', 'o.kode_jabatan = n.jabatan', 'left');
		$this->db->join('master_loker AS p', 'p.kode_loker = e.kode_loker', 'left');
		$this->db->join('master_loker AS q', 'q.kode_loker = d.kode_loker', 'left');
		$this->db->where('a.id_grade',$id); 
		$query=$this->db->get()->result();
		return $query;
	}
	public function getListGradeNik($nik)
	{
		$where=['h.nik'=>$nik];
		$this->db->select('a.*,
		d.nama as nama_grade_lama,
		e.nama as nama_grade_baru,
		h.nama as nama_karyawan,
		h.nik as nik_karyawan,
		j.nama as nama_mengetahui,
		k.nama as jbt_mengetahui,
		l.nama as nama_menyetujui,
		m.nama as jbt_menyetujui,
		n.nama as nama_loker_grade');
		$this->db->from('grade_karyawan AS a');
		$this->db->join('master_grade AS d', 'd.kode_grade = a.grade_asal', 'left');
		$this->db->join('master_grade AS e', 'e.kode_grade = a.grade_baru', 'left');
		$this->db->join('karyawan AS h', 'h.id_karyawan = a.id_karyawan', 'left');
		$this->db->join('karyawan AS j', 'j.id_karyawan = a.mengetahui', 'left');
		$this->db->join('master_jabatan AS k', 'k.kode_jabatan = j.jabatan', 'left');
		$this->db->join('karyawan AS l', 'l.id_karyawan = a.menyetujui', 'left');
		$this->db->join('master_jabatan AS m', 'm.kode_jabatan = l.jabatan', 'left');
		$this->db->join('master_loker AS n', 'n.kode_loker = e.kode_loker', 'left');
		$this->db->where($where); 
		$this->db->order_by('create_date','ASC');
		$query=$this->db->get()->result();
		return $query;
	}
	public function checkGradeCode($code)
	{
		return $this->model_global->checkCode($code,'peringatan_karyawan','no_sk');
	}
//KECELAKAAN KERJA
	public function getListKecelakaanKerja()
	{
		$sc="SELECT a.*,(select count(*) from kecelakaan_kerja cnt where cnt.id_karyawan = a.id_karyawan) as jum,
		b.nama as nama_kategori_kecelakaan,
		d.nik as nik_karyawan,
		d.nama as nama_karyawan
		FROM kecelakaan_kerja a
		LEFT JOIN master_kategori_kecelakaan b ON b.kode_kategori_kecelakaan = a.kode_kategori_kecelakaan
		LEFT JOIN karyawan d ON d.id_karyawan = a.id_karyawan
		WHERE id_kecelakaan = (SELECT max(id_kecelakaan) FROM kecelakaan_kerja x WHERE x.id_karyawan = a.id_karyawan)";
		$query=$this->db->query($sc)->result();
		return $query;
	}
	public function getPilihKaryawanKecelakaanKerja()
	{
		$this->db->select('emp.id_karyawan as id_kar,emp.nama as nama,emp.nik as nik,emp.jabatan as kode_jabatan,d.nama as nama_jabatan');
		$this->db->from('karyawan AS emp');
		$this->db->join('master_jabatan AS d', 'emp.jabatan = d.kode_jabatan', 'left');
		$this->db->where('emp.status',1);
		$this->db->where('emp.status_emp',1);
		$query=$this->db->get()->result();
		return $query;
	}
	public function getKecelakaanKerjaKaryawan($id)
	{
		$this->db->select('a.*,b.nama as nama_buat,c.nama as nama_update,
		d.nama as nama_kategori_kecelakaan,
		e.nama as nama_rs,
		f.nama as nama_loker_kejadian,
		h.nama as nama_karyawan,
		h.nik as nik_karyawan,
		l.nama as nama_jabatan,
		m.nama as nama_loker,
		j.nama as nama_mengetahui,
		k.nama as jbt_mengetahui,
		n.nama as nama_menyatakan,
		o.nama as jbt_menyatakan,
		p.nama as nama_saksi_1,
		q.nama as jbt_saksi_1,
		r.nama as nama_saksi_2,
		s.nama as jbt_saksi_2,
		t.nama as nama_penanggungjawab,
		u.nama as jbt_penanggungjawab');
		$this->db->from('kecelakaan_kerja AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left');
		$this->db->join('master_kategori_kecelakaan AS d', 'd.kode_kategori_kecelakaan = a.kode_kategori_kecelakaan', 'left');
		$this->db->join('master_daftar_rs AS e', 'e.kode_master_rs = a.kode_master_rs', 'left');
		$this->db->join('master_loker AS f', 'f.kode_loker = a.kode_loker', 'left');
		$this->db->join('karyawan AS h', 'h.id_karyawan = a.id_karyawan', 'left');
		$this->db->join('master_jabatan AS l', 'l.kode_jabatan = h.jabatan', 'left');
		$this->db->join('master_loker AS m', 'm.kode_loker = h.loker', 'left');
		$this->db->join('karyawan AS j', 'j.id_karyawan = a.mengetahui', 'left');
		$this->db->join('master_jabatan AS k', 'k.kode_jabatan = j.jabatan', 'left');
		$this->db->join('karyawan AS n', 'n.id_karyawan = a.menyatakan', 'left');
		$this->db->join('master_jabatan AS o', 'o.kode_jabatan = n.jabatan', 'left');
		$this->db->join('karyawan AS p', 'p.id_karyawan = a.saksi_1', 'left');
		$this->db->join('master_jabatan AS q', 'q.kode_jabatan = p.jabatan', 'left');
		$this->db->join('karyawan AS r', 'r.id_karyawan = a.saksi_2', 'left');
		$this->db->join('master_jabatan AS s', 's.kode_jabatan = r.jabatan', 'left');
		$this->db->join('karyawan AS t', 't.id_karyawan = a.penanggungjawab', 'left');
		$this->db->join('master_jabatan AS u', 'u.kode_jabatan = t.jabatan', 'left');
		$this->db->where('id_kecelakaan',$id); 
		$query=$this->db->get()->result();
		return $query;
	}
	public function getListKecelakaanKerjaNik($nik)
	{
		$where=['h.nik'=>$nik];
		$this->db->select('a.*,
		d.nama as nama_kategori_kecelakaan,
		e.nama as nama_rs,
		f.nama as nama_loker,
		h.nama as nama_karyawan,
		h.nik as nik_karyawan,
		j.nama as nama_mengetahui,
		k.nama as jbt_mengetahui');
		$this->db->from('kecelakaan_kerja AS a');
		$this->db->join('master_kategori_kecelakaan AS d', 'd.kode_kategori_kecelakaan = a.kode_kategori_kecelakaan', 'left');
		$this->db->join('master_daftar_rs AS e', 'e.kode_master_rs = a.kode_master_rs', 'left');
		$this->db->join('master_loker AS f', 'f.kode_loker = a.kode_loker', 'left');
		$this->db->join('karyawan AS h', 'h.id_karyawan = a.id_karyawan', 'left');
		$this->db->join('karyawan AS j', 'j.id_karyawan = a.mengetahui', 'left');
		$this->db->join('master_jabatan AS k', 'k.kode_jabatan = j.jabatan', 'left');
		$this->db->where($where); 
		$this->db->order_by('create_date','ASC');
		$query=$this->db->get()->result();
		return $query;
	}
	public function checkKecelakaanKerjaCode($code)
	{
		return $this->model_global->checkCode($code,'peringatan_karyawan','no_sk');
	}
//KARYAWAN TIDAK AKTIF
	public function getListKaryawanNonAktif()
	{
		$sc="SELECT a.*,
		b.nama as nama_jabatan,
		c.nama as nama_loker,
		f.nama as nama_karyawan,
		f.nik as nik_karyawan,
		f.tgl_masuk as tgl_masuk
		FROM karyawan_tidak_aktif a
		LEFT JOIN karyawan f ON f.id_karyawan = a.id_karyawan
		LEFT JOIN master_jabatan b ON b.kode_jabatan = f.jabatan
		LEFT JOIN master_loker c ON c.kode_loker = f.loker
		WHERE id_kta = 1";
		$query=$this->db->query($sc)->result();
		return $query;
	}
	public function getPilihKaryawanNonAktif()
	{
		$this->db->select('emp.id_karyawan as id_karyawan,emp.nama as nama, emp.jabatan as jabatan, emp.loker as loker, emp.nik as nik, emp.tgl_masuk as tgl_masuk, loker.nama as nama_loker, jbt.nama as nama_jabatan');
		$this->db->from('karyawan AS emp');
		$this->db->join('master_jabatan AS jbt', 'emp.jabatan = jbt.kode_jabatan', 'left');
		$this->db->join('master_loker AS loker', 'emp.loker = loker.kode_loker', 'left');
		$this->db->where('emp.status',1);
		$this->db->where('emp.status_emp',1);
		// $this->db->order_by('create_date','DESC');
		$query=$this->db->get()->result();
		return $query;
	}
	public function getKaryawanNonAktif($id)
	{
		$this->db->select('a.*,b.nama as nama_buat,c.nama as nama_update,
		d.nama as nama_jabatan,
		e.nama as nama_loker,
		h.id_karyawan as id_karyawan,
		h.nik as nik,
		h.nama as nama_karyawan,
		h.tgl_masuk as tgl_masuk,
		j.nama as nama_mengetahui,
		k.nama as jbt_mengetahui,
		l.nama as nama_menyetujui,
		m.nama as jbt_menyetujui');
		$this->db->from('karyawan_tidak_aktif AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left');
		$this->db->join('karyawan AS h', 'h.id_karyawan = a.id_karyawan', 'left');
		$this->db->join('master_jabatan AS d', 'd.kode_jabatan = h.jabatan', 'left');
		$this->db->join('master_loker AS e', 'e.kode_loker = h.loker', 'left');
		$this->db->join('karyawan AS j', 'j.id_karyawan = a.mengetahui', 'left');
		$this->db->join('master_jabatan AS k', 'k.kode_jabatan = j.jabatan', 'left');
		$this->db->join('karyawan AS l', 'l.id_karyawan = a.menyetujui', 'left');
		$this->db->join('master_jabatan AS m', 'm.kode_jabatan = l.jabatan', 'left');
		$this->db->where('id_kta',$id); 
		$this->db->order_by('create_date','ASC');
		$query=$this->db->get()->result();
		return $query;
	}
	public function getListKaryawanNonAktifNik($nik)
	{
		$where=['h.nik'=>$nik];
		$this->db->select('a.*,
		d.nama as nama_jabatan,
		e.nama as nama_loker,
		f.nama as nama_jabatan_baru,
		g.nama as nama_loker_baru,
		h.nik as nik,
		h.nama as nama_karyawan,
		i.nama as nama_status,
		j.nama as nama_jabatan,
		k.nama as nama_loker');
		$this->db->from('mutasi_jabatan AS a');
		$this->db->join('master_jabatan AS d', 'd.kode_jabatan = a.jabatan_asal', 'left');
		$this->db->join('master_loker AS e', 'e.kode_loker = a.lokasi_asal', 'left');
		$this->db->join('master_jabatan AS f', 'f.kode_jabatan = a.jabatan_baru', 'left');
		$this->db->join('master_loker AS g', 'g.kode_loker = a.lokasi_baru', 'left');
		$this->db->join('karyawan AS h', 'h.id_karyawan = a.id_karyawan', 'left');
		$this->db->join('master_mutasi AS i', 'i.kode_mutasi = a.status_mutasi', 'left');
		$this->db->join('master_jabatan AS j', 'j.kode_jabatan = h.jabatan', 'left'); 
		$this->db->join('master_loker AS k', 'k.kode_loker = h.loker', 'left'); 
		$this->db->where($where); 
		$this->db->order_by('create_date','ASC');
		$query=$this->db->get()->result();
		return $query;
	}
	public function checkKaryawanNonAktifCode($code)
	{
		return $this->model_global->checkCode($code,'mutasi_jabatan','no_sk');
	}
//================================================  ABSENSI KARYAWAN ====================================================
//_________________IZIN DAN CUTI_________________________
	public function getLisIzinCuti()
	{
		$sc="SELECT a.*,(select count(*) from izin_cuti_karyawan cnt where cnt.id_karyawan = a.id_karyawan) as jum,
		c.nama as nama_jabatan,
		d.nama as nama_loker,
		b.nama as nama_karyawan,
		b.nik as nik_karyawan,
		e.nama as nama_bagian
		FROM izin_cuti_karyawan a
		LEFT JOIN karyawan b ON b.id_karyawan = a.id_karyawan
		LEFT JOIN master_jabatan c ON c.kode_jabatan = b.jabatan
		LEFT JOIN master_loker d ON d.kode_loker = b.loker
		LEFT JOIN master_bagian e ON e.kode_bagian = c.kode_bagian
		WHERE id_izin_cuti = (SELECT max(id_izin_cuti) FROM izin_cuti_karyawan x WHERE x.id_karyawan = a.id_karyawan) ORDER BY create_date DESC";
		$query=$this->db->query($sc)->result();
		return $query;
	}
	public function getIzinCuti($id)
	{
		$this->db->select('a.*,b.nama as nama_buat,c.nama as nama_update,
		d.nama as nama_jabatan,
		e.nama as nama_loker,
		f.nama as nama_jabatan_baru,
		g.nama as nama_loker_baru,
		h.nama as nama_karyawan,
		h.nik as nik_karyawan,
		i.nama as nama_status,
		j.nama as nama_mengetahui,
		k.nama as jbt_mengetahui,
		l.nama as nama_jabatan_baru,
		m.nama as nama_loker_baru,
		n.nama as nama_menyetujui,
		o.nama as jbt_menyetujui');
		$this->db->from('izin_cuti_karyawan AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left');
		$this->db->join('master_jabatan AS d', 'd.kode_jabatan = a.jabatan_asal', 'left');
		$this->db->join('master_loker AS e', 'e.kode_loker = a.lokasi_asal', 'left');
		$this->db->join('master_jabatan AS f', 'f.kode_jabatan = a.jabatan_baru', 'left');
		$this->db->join('master_loker AS g', 'g.kode_loker = a.lokasi_baru', 'left');
		$this->db->join('karyawan AS h', 'h.id_karyawan = a.id_karyawan', 'left');
		$this->db->join('master_mutasi AS i', 'i.kode_mutasi = a.status_mutasi', 'left');
		$this->db->join('karyawan AS j', 'j.id_karyawan = a.mengetahui', 'left');
		$this->db->join('master_jabatan AS k', 'k.kode_jabatan = j.jabatan', 'left');
		$this->db->join('master_jabatan AS l', 'l.kode_jabatan = h.jabatan', 'left');
		$this->db->join('master_loker AS m', 'm.kode_loker = h.loker', 'left');
		$this->db->join('karyawan AS n', 'n.id_karyawan = a.menyetujui', 'left');
		$this->db->join('master_jabatan AS o', 'o.kode_jabatan = n.jabatan', 'left');
		$this->db->where('id_izin_cuti',$id); 
		$query=$this->db->get()->result();
		return $query;
	}
	public function getListIzinCutiNik($nik)
	{
		$where=['h.nik'=>$nik];
		$this->db->select('a.*,
		d.nama as nama_jabatan,
		e.nama as nama_loker,
		f.nama as nama_jabatan_baru,
		g.nama as nama_loker_baru,
		h.nik as nik,
		h.nama as nama_karyawan,
		i.nama as nama_status,
		j.nama as nama_jabatan,
		k.nama as nama_loker');
		$this->db->from('mutasi_jabatan AS a');
		$this->db->join('master_jabatan AS d', 'd.kode_jabatan = a.jabatan_asal', 'left');
		$this->db->join('master_loker AS e', 'e.kode_loker = a.lokasi_asal', 'left');
		$this->db->join('master_jabatan AS f', 'f.kode_jabatan = a.jabatan_baru', 'left');
		$this->db->join('master_loker AS g', 'g.kode_loker = a.lokasi_baru', 'left');
		$this->db->join('karyawan AS h', 'h.id_karyawan = a.id_karyawan', 'left');
		$this->db->join('master_mutasi AS i', 'i.kode_mutasi = a.status_mutasi', 'left');
		$this->db->join('master_jabatan AS j', 'j.kode_jabatan = h.jabatan', 'left'); 
		$this->db->join('master_loker AS k', 'k.kode_loker = h.loker', 'left'); 
		$this->db->where($where); 
		$this->db->order_by('create_date','ASC');
		$query=$this->db->get()->result();
		return $query;
	}
	public function checkIzinCutiCode($code)
	{
		return $this->model_global->checkCode($code,'mutasi_jabatan','no_sk');
	}
}