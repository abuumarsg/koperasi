<?php

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Model_data
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_data extends CI_Model
{
	function __construct()
    {
        parent::__construct();
        $this->date = $this->otherfunctions->getDateNow();
    }
//KONTAK
	public function getListKontak()
	{
		$this->db->order_by('create_date','DESC');
		$query=$this->db->get('data_kontak')->result();
		return $query;
	}
	public function getKontak($id){
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('data_kontak AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left'); 
		$this->db->where('a.id_kontak',$id); 
		$query=$this->db->get()->result();
		return $query;
	}
	public function getListKontakFO()
	{
		$this->db->order_by('create_date','DESC');
		$this->db->where('status',1);
		$query=$this->db->get('data_kontak')->result();
		return $query;
	}
//Company profile
	public function getCompanyProfile(){
		$this->db->select('*');
		$this->db->from('data_company');
		$this->db->where('id',1); 
		$query=$this->db->get()->row_array();
		return $query;
	}
	public function getCompanyProfileBelajar(){
		$this->db->select('*');
		$this->db->from('z_data_company');
		$this->db->where('id',1); 
		$query=$this->db->get()->row_array();
		return $query;
	}
// DATA ANGGOTA
	public function getListDataAnggota($where=null){
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from('data_anggota AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left');
		if(!empty($where)){
			$this->db->where($where);
		}
		$query=$this->db->get()->result();
		return $query;
	}
//DATA TABUNGAN
	public function getListDataTabungan($where=null, $row = false){
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update, d.nama as nama_anggota');
		$this->db->from('data_tabungan AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left');
		$this->db->join('data_anggota AS d', 'd.kode = a.kode_anggota', 'left');
		if(!empty($where)){
			$this->db->where($where);
		}
		if($row){
			$query=$this->db->get()->row_array();
		}else{
			$query=$this->db->get()->result();
		}
		return $query;
	}
//TRANSAKSI TABUNGAN
	public function getListTransaksiTabungan($where=null, $row = false){
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update, d.kode_anggota, e.nama as nama_anggota, f.nama as nama_jenis, d.besar_tabungan');
		$this->db->from('transaksi_tabungan AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left');
		$this->db->join('data_tabungan AS d', 'd.kode_tabungan = a.kode_tabungan', 'left');
		$this->db->join('data_anggota AS e', 'e.kode = d.kode_anggota', 'left');
		$this->db->join('master_simpanan AS f', 'f.kode = a.jenis', 'left');
		if(!empty($where)){
			$this->db->where($where);
		}
		if($row){
			$query=$this->db->get()->row_array();
		}else{
			$query=$this->db->get()->result();
		}
		return $query;
	}
//DATA PENGAJUAN
	public function getListDataPengajuan($where=null, $row = false){
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update, d.nama as nama_anggota, e.nama as nama_pinjaman,e.lama_pinjam, e.maksimal,e.bunga');
		$this->db->from('data_pengajuan AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left');
		$this->db->join('data_anggota AS d', 'd.kode = a.kode_anggota', 'left');
		$this->db->join('master_pinjaman AS e', 'e.kode = a.jenis_pinjam', 'left');
		if(!empty($where)){
			$this->db->where($where);
		}
		if($row){
			$query=$this->db->get()->row_array();
		}else{
			$query=$this->db->get()->result();
		}
		return $query;
	}
	public function getListTransaksiAngsuran($where=null, $row = false){
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update, e.nama as nama_anggota, f.nama as nama_pinjaman,f.lama_pinjam, f.maksimal,f.bunga,d.status_pinjaman');
		$this->db->from('transaksi_angsuran AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'left'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'left');
		$this->db->join('data_pengajuan AS d', 'd.kode = a.kode_pengajuan', 'left');
		$this->db->join('data_anggota AS e', 'e.kode = d.kode_anggota', 'left');
		$this->db->join('master_pinjaman AS f', 'f.kode = d.jenis_pinjam', 'left');
		if(!empty($where)){
			$this->db->where($where);
		}
		if($row){
			$query=$this->db->get()->row_array();
		}else{
			$query=$this->db->get()->result();
		}
		return $query;
	}
}