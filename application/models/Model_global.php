<?php

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Model_global
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_global extends CI_Model
{
	function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }
    public function authSecure($uname,$pass)
    {
        if (empty($uname) || empty($pass)) 
            return $this->messages->unfillForm();
        $data=$this->db->get_where('admin',['username'=>$uname,'password'=>$pass])->row_array();
        $url_now=$this->session->userdata('data_lock')['url'];
        if (empty($url_now)) {
            $url_adm=base_url('pages');
        }else{
            $url_adm=$url_now;
        }
        if (!isset($data)) {
            return $this->messages->wrongPass();
        }else{
            $status=array('last_login'=>$this->otherfunctions->getDateNow(),'status'=>1);
            $data_log=array('id_admin'=>$data['id_admin'],'tgl_login'=>$this->otherfunctions->getDateNow());
            $this->db->insert('log_login_admin',$data_log);
            $this->db->where('id_admin',$data['id_admin']);
            $this->db->update('admin',$status);
            $this->session->set_userdata('adm', ['id'=>$data['id_admin']]);
            $url['linkx']=$url_adm;
            $datax=array_merge($url,$this->messages->youIn($data['nama']));
            return $datax;
        }
    }
    public function checkCode($code,$table,$column)
    {
        if (empty($code) || empty($table) || empty($column)) 
            return false;
        $data=$this->db->get_where($table,[$column=>$code])->num_rows();
        if ($data > 0) {
            return true;
        }else{
            return false;
        }
    }
    public function listActiveRecord($table,$key,$val)
    {
        if (empty($table) || empty($key) || empty($val)) 
            return null;
        $pack=[];
        $data=$this->db->get_where($table,['status'=>1])->result();
        foreach ($data as $d) {
            $pack[$d->$key]=$d->$val;
        }
        return $pack;
    }
    public function listActiveRecord_2($table,$key,$val)
    {
        if (empty($table) || empty($key) || empty($val)) 
            return null;
        $pack=[];
        $data=$this->db->get_where($table,['status'=>1,'delete_at'=>null])->result();
        foreach ($data as $d) {
            $pack[$d->$key]=$d->$val;
        }
        return $pack;
    }
    public function listUserActiveRecord($table,$key,$val,$usage)
    {
        if (empty($table) || empty($key) || empty($val) || empty($usage)) 
            return null;
        $pack=[];
        if ($usage == 'admin') {
            $st='status_adm';
        }else{
            $st='status_emp';
        }
        $data=$this->db->get_where($table,[$st=>1])->result();
        foreach ($data as $d) {
            $pack[$d->$key]=$d->$val;
        }
        return $pack;
    }

    //query
    public function insertQuery($data,$table)
    {
        if (empty($data) || empty($table)) 
            return $this->messages->allFailure();
        $this->db->trans_begin();
        $this->db->insert($table,$data);
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $msg=$this->messages->allFailure();
        }else{
            $this->db->trans_commit();
            $msg=$this->messages->allGood();
        }
        return $msg;
    }
    public function insertQueryCC($data,$table,$cc)
    {
        //$cc is check code [true/false]
        if (empty($data) || empty($table)) 
            return $this->messages->allFailure();
        if (!$cc) {
            $this->db->trans_begin();
            $in=$this->db->insert($table,$data);
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $msg=$this->messages->allFailure();
            }else{
                $this->db->trans_commit();
                $msg=$this->messages->allGood();
            }
        }else{
            $msg=$this->messages->sameCode();
        }
        return $msg;
    }
    public function insertQueryNoMsg($data,$table)
    {
        if (empty($data) || empty($table)) 
            return false;
        $this->db->trans_begin();
        $in=$this->db->insert($table,$data);
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $msg=false;
        }else{
            $this->db->trans_commit();
            $msg=true;
        }
        return $msg;
    }
    public function insertQueryCCNoMsg($data,$table,$cc)
    {
        //$cc is check code [true/false]
        if (empty($data) || empty($table)) 
            return $this->messages->allFailure();
        if (!$cc) {
            $this->db->trans_begin();
            $in=$this->db->insert($table,$data);
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $msg=false;
            }else{
                $this->db->trans_commit();
                $msg=true;
            }
        }else{
            $msg=false;
        }
        return $msg;
    }
    public function updateQuery($data,$table,$where)
    {
        //where is array
        if (empty($data) || empty($table) || empty($where)) 
            return $this->messages->allFailure();
        $this->db->trans_begin();
        $this->db->where($where);
        $this->db->update($table,$data);
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $msg=$this->messages->allFailure();
        }else{
            $this->db->trans_commit();
            $msg=$this->messages->allGood();
        }
        return $msg;
    }
    public function updateQueryCC($data,$table,$where,$cc)
    {
        //where is array, $cc is check code [true/false]
        if (empty($data) || empty($table) || empty($where)) 
            return $this->messages->allFailure();
        if (!$cc) {
            $this->db->trans_begin();
            $this->db->where($where);
            $this->db->update($table,$data);
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $msg=$this->messages->allFailure();
            }else{
                $this->db->trans_commit();
                $msg=$this->messages->allGood();
            }
        }else{
            $msg=$this->messages->sameCode();
        }
        return $msg;
    }
    public function updateQueryNoMsg($data,$table,$where)
    {
        //where is array
        if (empty($data) || empty($table) || empty($where)) 
            return $this->messages->allFailure();
        $this->db->trans_begin();
        $this->db->where($where);
        $this->db->update($table,$data);
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $msg=false;
        }else{
            $this->db->trans_commit();
            $msg=true;
        }
        return $msg;
    }
    public function updateQueryCCNoMsg($data,$table,$where,$cc)
    {
        //where is array, $cc is check code [true/false]
        if (empty($data) || empty($table) || empty($where)) 
            return $this->messages->allFailure();
        if (!$cc) {
            $this->db->trans_begin();
            $this->db->where($where);
            $this->db->update($table,$data);
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $msg=false;
            }else{
                $this->db->trans_commit();
                $msg=true;
            }
        }else{
            $msg=false;
        }
        return $msg;
    }
    public function deleteQuery($table,$where = null)
    {
        //where is array
        if (empty($table)) 
            return $this->messages->delFailure();
        $this->db->trans_begin();
        if (empty($where)) {
            $this->db->delete($table);
        }else{
           $this->db->where($where);
           $this->db->delete($table); 
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $msg=$this->messages->allFailure();
        }else{
            $this->db->trans_commit();
            $msg=$this->messages->allGood();
        }
        return $msg;
    }
    public function deleteQueryNoMsg($table,$where = null)
    {
        //where is array
        if (empty($table)) 
            return $this->messages->delFailure();
        $this->db->trans_begin();
        if (empty($where)) {
            $this->db->delete($table);
        }else{
           $this->db->where($where);
           $this->db->delete($table); 
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $msg=false;
        }else{
            $this->db->trans_commit();
            $msg=true;
        }
        return $msg;
    }
    public function createTable($name,$cols,$pk)
    {
        if(empty($name) || empty($cols) || empty($pk)) 
            return false; 
        $this->db->trans_begin();
        $this->dbforge->add_field($cols);
        $this->dbforge->add_key($pk, TRUE);
        $this->dbforge->create_table($name);
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $msg=false;
        }else{
            $this->db->trans_commit();
            $msg=true;
        }
        return $msg;
    }
    public function dropTable($table)
    {
        if(empty($table)) 
            return false;
        $this->db->trans_begin();
        $this->dbforge->drop_table($table,TRUE);
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $msg=false;
        }else{
            $this->db->trans_commit();
            $msg=true;
        }
        return $msg;
    }

    //others
    public function getCreateProperties($id)
    {
    	if (empty($id)) 
    		return null;
    	$new_val=[
    		'create_date'=>$this->otherfunctions->getDateNow(),
    		'update_date'=>$this->otherfunctions->getDateNow(),
    		'update_by'=>$id,
    		'create_by'=>$id,
    		'status'=>1,
    	];
    	return $new_val;
    }
    public function getUpdateProperties($id)
    {
    	if (empty($id)) 
    		return null;
    	$new_val=[
    		'update_date'=>$this->otherfunctions->getDateNow(),
    		'update_by'=>$id,
    	];
    	return $new_val;
    }
}