<?php

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Model_admin
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_belajar extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
	public function getListAdmin()
	{
		$where=['a.id_admin !='=>1];
		$this->db->select('a.*,b.nama as nama_group');
		$this->db->from('admin AS a');
		$this->db->join('master_user_group AS b', 'b.id_group = a.id_group', 'left'); 
		$this->db->order_by('update_date','DESC');
		$this->db->where($where); 
		$query=$this->db->get()->result();
		return $query;
	}
	public function getAdmin($id)
	{
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update,d.nama as nama_group');
		$this->db->from('admin AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'inner'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'inner'); 
		$this->db->join('master_user_group AS d', 'd.id_group = a.id_group', 'left');
		$this->db->where('a.id_admin',$id); 
		$query=$this->db->get()->result();
		return $query;
	}
	public function getLogLogin($id){
		if (empty($id)) 
			return null;
		return $this->db->get_where('log_login_admin',array('id_admin'=>$id))->result();
	}
	public function usernameAdmin($username){
		return $this->db->get_where('admin',array('username'=>$username))->result();
	}
	public function getDataWhere($tabel,$where=null,$row=false, $join='left')
	{
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update');
		$this->db->from($tabel.' AS a');
		$this->db->join('z_guru AS b', 'b.id = a.create_by', $join); 
		$this->db->join('z_guru AS c', 'c.id = a.update_by', $join);
		$this->db->where('a.delete_at IS NULL');
		if(!empty($where)){
			$this->db->where($where);
		}
		if($row){
			$query=$this->db->get()->row_array();
		}else{
			$query=$this->db->get()->result();
		}
		return $query;
	}
	public function getDataPdf($where=null,$row = false)
	{
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update, d.nama as nama_mapel');
		$this->db->from('z_materi_pdf AS a');
		$this->db->join('z_guru AS b', 'b.id = a.create_by', 'left'); 
		$this->db->join('z_guru AS c', 'c.id = a.update_by', 'left');
		$this->db->join('z_mata_pelajaran AS d', 'd.id = a.id_mata_pelajaran', 'left');
		$this->db->where('a.delete_at IS NULL');
		if(!empty($where)){
			$this->db->where($where);
		}
		if($row){
			$query=$this->db->get()->row_array();
		}else{
			$query=$this->db->get()->result();
		}
		return $query;
	}
	public function getDataVideo($where=null,$row = false)
	{
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update, d.nama as nama_mapel');
		$this->db->from('z_materi_video AS a');
		$this->db->join('z_guru AS b', 'b.id = a.create_by', 'left'); 
		$this->db->join('z_guru AS c', 'c.id = a.update_by', 'left');
		$this->db->join('z_mata_pelajaran AS d', 'd.id = a.id_mata_pelajaran', 'left');
		$this->db->where('a.delete_at IS NULL');
		if(!empty($where)){
			$this->db->where($where);
		}
		if($row){
			$query=$this->db->get()->row_array();
		}else{
			$query=$this->db->get()->result();
		}
		return $query;
	}
	public function getDataLatihanSoal($where=null,$row = false)
	{
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update, d.nama as nama_mapel, e.nama as nama_tipe_soal');
		$this->db->from('z_latihan_soal AS a');
		$this->db->join('z_guru AS b', 'b.id = a.create_by', 'left'); 
		$this->db->join('z_guru AS c', 'c.id = a.update_by', 'left');
		$this->db->join('z_mata_pelajaran AS d', 'd.id = a.id_mata_pelajaran', 'left');
		$this->db->join('z_tipe_soal AS e', 'e.id = a.id_tipe_soal', 'left');
		$this->db->where('a.delete_at IS NULL');
		if(!empty($where)){
			$this->db->where($where);
		}
		if($row){
			$query=$this->db->get()->row_array();
		}else{
			$query=$this->db->get()->result();
		}
		return $query;
	}
	public function getDataSoalSoal($where=null,$row = false)
	{
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update, d.nama as nama_mapel, e.nama as nama_tipe_soal, ls.judul as judul_latihan');
		$this->db->from('z_soal_soal AS a');
		$this->db->join('z_guru AS b', 'b.id = a.create_by', 'left'); 
		$this->db->join('z_guru AS c', 'c.id = a.update_by', 'left');
		$this->db->join('z_latihan_soal AS ls', 'ls.id = a.id_latihan_soal', 'left');
		$this->db->join('z_mata_pelajaran AS d', 'd.id = ls.id_mata_pelajaran', 'left');
		$this->db->join('z_tipe_soal AS e', 'e.id = ls.id_tipe_soal', 'left');
		$this->db->where('a.delete_at IS NULL');
		if(!empty($where)){
			$this->db->where($where);
		}
		if($row){
			$query=$this->db->get()->row_array();
		}else{
			$query=$this->db->get()->result();
		}
		return $query;
	}
}