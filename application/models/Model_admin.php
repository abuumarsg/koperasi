<?php

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Model_admin
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_admin extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
    function list_admin(){
    	return $this->db->query("SELECT * FROM admin WHERE id_admin != '1'")->result();
    }
	function adm_cek($un,$pass){
		return $this->db->get_where('admin',array('username'=>$un,'password'=>$pass))->row_array();
	}
	function avl($un,$em){
		return $this->db->query('SELECT id_admin FROM admin WHERE username = ? OR email = ?',array($un,$em))->row_array();
	}
	function adm($id){
		return $this->db->get_where('admin',array('id_admin'=>$id))->row_array();
	}
	function forget_email($em){
		return $this->db->get_where('admin',array('email'=>$em))->row_array();
	}
	function log_login($id){
		return $this->db->get_where('log_login_admin',array('id_admin'=>$id))->result();
	}
	function list_adm($id){
		return $this->db->query('SELECT * FROM admin WHERE id_admin != ?', array($id))->result();
	}
	function ver($id,$tok){
		return $this->db->query('SELECT * FROM admin WHERE id_admin = ? AND email_token = ?',array($id,$tok))->row_array();
	}
	function res($t){
		return $this->db->get_where('admin',array('reset_token'=>$t))->row_array();
	}
	public function getAdminName($id)
	{
		$data=$this->db->get_where('admin',array('id_admin'=>$id))->row_array();
		if (isset($data)) {
			return $data['nama'];
		}else{
			return null;
		}
	}
	public function getAdminById($id)
	{
		return $this->db->get_where('admin',array('id_admin'=>$id))->row_array();
	}
	public function getAdminByEmail($email)
	{
		return $this->db->get_where('admin',array('email'=>$email))->row_array();
	}
	public function getAdminByToken($token)
	{
		return $this->db->get_where('admin',array('reset_token'=>$token))->row_array();
	}
	public function getListAdminActive()
	{
		return $this->model_global->listUserActiveRecord('admin','id_admin','nama','admin');
	}
	public function getListAdmin()
	{
		$where=['a.id_admin !='=>1];
		$this->db->select('a.*,b.nama as nama_group');
		$this->db->from('admin AS a');
		$this->db->join('master_user_group AS b', 'b.id_group = a.id_group', 'left'); 
		$this->db->order_by('update_date','DESC');
		$this->db->where($where); 
		$query=$this->db->get()->result();
		return $query;
	}
	public function getAdmin($id)
	{
		$this->db->select('a.*,b.nama as nama_buat, c.nama as nama_update,d.nama as nama_group');
		$this->db->from('admin AS a');
		$this->db->join('admin AS b', 'b.id_admin = a.create_by', 'inner'); 
		$this->db->join('admin AS c', 'c.id_admin = a.update_by', 'inner'); 
		$this->db->join('master_user_group AS d', 'd.id_group = a.id_group', 'left');
		$this->db->where('a.id_admin',$id); 
		$query=$this->db->get()->result();
		return $query;
	}
	public function getLogLogin($id){
		if (empty($id)) 
			return null;
		return $this->db->get_where('log_login_admin',array('id_admin'=>$id))->result();
	}
	public function usernameAdmin($username){
		return $this->db->get_where('admin',array('username'=>$username))->result();
	}
}