<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Master
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

class Master extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->date = $this->otherfunctions->getDateNow();
		if ($this->session->has_userdata('adm')) {
			$this->admin = $this->session->userdata('adm')['id'];	 
		}else{ 
			redirect('auth');
		}	
		$dtroot['admin']=$this->model_admin->adm($this->admin);
		$nm=explode(" ", $dtroot['admin']['nama']);
		$datax['adm'] = [
				'nama'=>$nm[0],
				'namax'=>$dtroot['admin']['nama'],
				'email'=>$dtroot['admin']['email'],
				'kelamin'=>$dtroot['admin']['kelamin'],
				'foto'=>$dtroot['admin']['foto'],
				'create'=>$dtroot['admin']['create_date'],
				'update'=>$dtroot['admin']['update_date'],
				'login'=>$dtroot['admin']['last_login'],
				'level'=>$dtroot['admin']['level'],
		];
		$this->dtroot=$datax;
	}
	public function index(){
		redirect('pages/dashboard');
	}
	//==SETTINGS BEGIN==//

	//------------------------------------------------------------------------------------------------------//
	//Setting Hak Akses
	public function master_access()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_master->getListAccess();
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_access,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];

					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_access,
						$d->kode_access,
						$d->nama,
						$properties['tanggal'],
						$properties['status'],
						$properties['aksi']
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_access');
				$data=$this->model_master->getAccess($id);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_access,
						'kode_access'=>$d->kode_access,
						'nama'=>$d->nama,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update)
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function add_access(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$kode=$this->input->post('kode');
		if ($kode != "") {
			$data=[
				'kode_access'=>strtoupper($kode),
				'nama'=>ucwords($this->input->post('nama')),
			];
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			$datax = $this->model_global->insertQueryCC($data,'master_access',$this->model_master->checkAccessCode($kode));
		}else{
			$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
	function edt_access(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		if ($id != "") {
			$data=[
				'kode_access'=>strtoupper($this->input->post('kode')),
				'nama'=>ucwords($this->input->post('nama')),
			];
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			//cek data
			$old=$this->input->post('kode_old');
			if ($old != $data['kode_access']) {
				$cek=$this->model_master->checkAccessCode($data['kode_access']);
			}else{
				$cek=false;
			}
			$datax = $this->model_global->updateQueryCC($data,'master_access',['id_access'=>$id],$cek);
		}else{
        	$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}	

	//------------------------------------------------------------------------------------------------------//
	//Setting User Group
	public function master_user_group()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_master->getListUserGroup($this->dtroot['adm']['level']);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_group,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];

					$properties=$this->otherfunctions->getPropertiesTable($var);

					$idm=array_filter(explode(';', $d->list_id_menu));
					$amenu = count($idm).' Menu';
					$amenux ='';
					foreach ($idm as $menul) {
						$mnuu=$this->db->get_where('master_menu',array('id_menu'=>$menul,'id_menu !='=>1))->row_array();
						$amenux .= '<i class="'.$mnuu['icon'].'"></i> '.$mnuu['nama'].'<br>';
					}

					$hak=array_filter(explode(';', $d->list_access));
					$ahak = count($hak).' Hak Akses';
					$ahakx ='';
					foreach ($hak as $hakl) {
						$haku=$this->db->get_where('master_access',array('id_access'=>$hakl))->row_array();
						$ahakx .= '<i class="fa fa-link"></i> '.$haku['nama'].'<br>';
					}
					// $level_admin=$this->model_admin->adm($this->admin)['level'];
					// if ($level_admin==0) {
						$delete = '<button type="button" class="btn btn-danger btn-sm"  href="javascript:void(0)" onclick=delete_modal('.$d->id_group.')><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i></button> ';
					// }else{
					// 	$delete = null;
					// }
					$info = '<button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick=view_modal('.$d->id_group.')><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button> ';
					$datax['data'][]=[
						$d->id_group,
						$d->nama,
						$amenu,
						$ahak,
						$properties['status'],
						$properties['tanggal'],
						$info.$delete,
						$amenux,
						$ahakx

					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_group');
				$data=$this->model_master->getUserGroupOne($id);
				foreach ($data as $d) {

					$idm=explode(';', $d->list_id_menu);
						$amenu = count($idm).' Menu';
					$amenux ='';
					foreach ($idm as $menul) {
						$mnuu=$this->db->get_where('master_menu',array('id_menu'=>$menul,'id_menu !='=>1))->row_array();
						$amenux .= '<i class="'.$mnuu['icon'].'"></i> '.$mnuu['nama'].'<br>';
					}
					
					$aks=explode(';', $d->list_access);
						$ahak = count($aks).' Hak Akses';
					$ahakx ='';
					foreach ($aks as $hakl) {
						$haku=$this->db->get_where('master_access',array('id_access'=>$hakl))->row_array();
						$ahakx .= '<i class="fa fa-link"></i> '.$haku['nama'].'<br>';
					}
					
					$datax=[
						'id'=>$d->id_group,
						'nama'=>$d->nama,
						'menu'=>$amenu,
						'akses'=>$ahak,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'checked_menu'=>$idm,
						'checked_akses'=>$aks,
						'detail_menu'=>$amenux,
						'detail_akses'=>$ahakx
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function add_user_group(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$nama=ucwords($this->input->post('nama'));
		$menu_add=$this->input->post('menu_add');
		$akses_add=$this->input->post('akses_add');
		if ($nama != "") {
			$data=array(
				'nama'=>$nama,
				'list_id_menu'=>str_replace(",",";",$menu_add),
				'list_access'=>str_replace(",",";",$akses_add),
			);
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			$datax = $this->model_global->insertQueryCC($data,'master_user_group',null);
		}else{
			$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax); 
	}
	function edt_user_group(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$menu_edit=$this->input->post('menu_edit');
		$akses_edit=$this->input->post('akses_edit');
		if ($id != "") {
			$data=array(
				'nama'=>ucwords($this->input->post('nama')),
				'list_id_menu'=>str_replace(",",";",$menu_edit),
				'list_access'=>str_replace(",",";",$akses_edit),
			);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQueryCC($data,'master_user_group',['id_group'=>$id],null);
		}else{
			$datax = $this->messages->notValidParam(); 
		}
	   	echo json_encode($datax);
	}

	//------------------------------------------------------------------------------------------------------//
	//Setting Manajemen Menu
	public function master_menu(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_master->getListMenu();
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_menu,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
		      		$sb=$this->otherfunctions->getParseOneLevelVar($d->sub_url);
					$res=null;
					if (count($sb) > 0) {
						$res='<ol>';	
						foreach ($sb as $sbb) {
							$res.='<li>'.$sbb.'</li>';
						}
						$res.='</ol>';
					}
					$datax['data'][]=[
						$d->id_menu,
						'<i class="fa '.$d->icon.'"></i> '.$d->nama,
						($d->parent!=0)?$d->parent_name:'Parent',
						$d->sequence,
						$d->url,
						$res,
						$properties['tanggal'],
						$properties['status'],
						$properties['aksi']
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_menu');
				$data=$this->model_master->getAllMenubyId($id);
				foreach ($data as $d) {
					$sb=$this->otherfunctions->getParseOneLevelVar($d->sub_url);
					$res=null;
					if (count($sb) > 0) {
						$res='<ol>';	
						foreach ($sb as $sbb) {
							$res.='<li>'.$sbb.'</li>';
						}
						$res.='</ol>';
					}
					$datax=[
						'id'=>$d->id_menu,
						'nama'=>$d->nama,
						'parent'=>$d->parent_name,
						'parent_val'=>$d->parent,
						'url'=>$d->url,
						'sub_url'=>$res,
						'sub_url_val'=>$d->sub_url,
						'icon'=>$d->icon,
						'sequence'=>$d->sequence,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update)
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function add_menu(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$nama=$this->input->post('nama');
		$seq=$this->input->post('sequence');
		$parent=$this->input->post('parent');
		$sub_in=$this->input->post('sub_url');
		if ($nama == "" || $seq == ""){
			echo json_encode($this->messages->notValidParam());
		}else{
			$icon =$this->input->post('icon');
			if ($icon == '') {
				$icon = 'fas fa-chevron-circle-right';
			}
			$url=strtolower($this->input->post('url'));
			if ($parent != 0) {
				$par=$this->model_master->getAllMenubyId($parent);
				foreach ($par as $px) {
					$sub_url=$this->otherfunctions->addValueToArrayDb($px->sub_url,$url,';');
				}
				$this->model_global->updateQuery(['sub_url'=>$sub_url],'master_menu',['id_menu'=>$parent]);
			}
			$data=[
				'nama'=>ucwords($nama),
				'parent'=>$parent,
				'sequence'=>$seq,
				'icon'=>$icon,
				'url'=>$url,
				'sub_url'=>$sub_in,
			]; 
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			echo json_encode($this->model_global->insertQuery($data,'master_menu'));
		}
	}
	function edt_menu(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$nama=$this->input->post('nama');
		$seq=$this->input->post('sequence');
		$parent=$this->input->post('parent');
		$parent_old=$this->input->post('parent_old');
		$sub_in=$this->input->post('sub_url');
		if ($id == "" || $parent_old == "" || $nama == "" || $seq == "" || $parent == ""){
			echo json_encode($this->messages->unfillForm());
		}else{
			$icon =$this->input->post('icon');
			if ($icon == '') {
				$icon = 'fas fa-chevron-circle-right';
			}
			$url=strtolower($this->input->post('url'));
			$par=$this->model_master->getAllMenubyId($parent);
			if ($parent_old == $parent) {
				foreach ($par as $px) {
					$sub_url_add=$this->otherfunctions->addValueToArrayDb($px->sub_url,$url,';');
				}
				if ($parent != 0) {
					$this->model_global->updateQuery(['sub_url'=>$sub_url_add],'master_menu',['id_menu'=>$parent]);
				}
			}else{
				$par_old=$this->model_master->getAllMenubyId($parent_old);
				foreach ($par_old as $px_old) {
					$sub_url_del=$this->otherfunctions->removeValueToArrayDb($px_old->sub_url,$url,';');
				}
				foreach ($par as $px) {
					$sub_url_add=$this->otherfunctions->addValueToArrayDb($px->sub_url,$url,';');
				}
				if ($parent_old != 0 && $parent != 0) {
					$this->model_global->updateQuery(['sub_url'=>$sub_url_add],'master_menu',['id_menu'=>$parent]);
					$this->model_global->updateQuery(['sub_url'=>$sub_url_del],'master_menu',['id_menu'=>$parent_old]);
				}
			}
			$data=[
				'nama'=>ucwords($nama),
				'parent'=>$parent,
				'sequence'=>$seq,
				'icon'=>$icon,
				'url'=>$url,
				'sub_url'=>$sub_in,
			]; 
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			echo json_encode($this->model_global->updateQuery($data,'master_menu',['id_menu'=>$id]));
		}
	}
	//Setting Manajemen Menu User Admin
	public function master_menu_user(){
		if (!$this->input->is_ajax_request()) 
		redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
			echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_master->getListMenuUser();
				$access=$this->codegenerator->decryptChar($this->input->post('access'));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_menu,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$properties['aksi']=str_replace('view_modal', 'view_modal_u', $properties['aksi']);
					$properties['status']=str_replace('do_status', 'do_status_u', $properties['status']);
					$properties['aksi']=str_replace('delete_modal', 'delete_modal_u', $properties['aksi']);
					$sb=$this->otherfunctions->getParseOneLevelVar($d->sub_url);
					$res=null;
					if (count($sb) > 0) {
						$res='<ol>';	
						foreach ($sb as $sbb) {
							$res.='<li>'.$sbb.'</li>';
						}
						$res.='</ol>';
					}
					$datax['data'][]=[
						$d->id_menu,
						'<i class="fa '.$d->icon.'"></i> '.$d->nama,
						$d->parent_name,
						$d->sequence,
						$d->url,
						$res,
						$properties['tanggal'],
						$properties['status'],
						$properties['aksi']
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_menu');
				$data=$this->model_master->getAllMenuUserbyId($id);
				foreach ($data as $d) {
					$sb=$this->otherfunctions->getParseOneLevelVar($d->sub_url);
					$res=null;
					if (count($sb) > 0) {
						$res='<ol>';	
						foreach ($sb as $sbb) {
							$res.='<li>'.$sbb.'</li>';
						}
						$res.='</ol>';
					}
					$datax=[
						'id'=>$d->id_menu,
						'nama'=>$d->nama,
						'parent'=>$d->parent_name,
						'parent_val'=>$d->parent,
						'url'=>$d->url,
						'sub_url'=>$res,
						'sub_url_val'=>$d->sub_url,
						'icon'=>$d->icon,
						'sequence'=>$d->sequence,
						'custom'=>$d->custom,
						'isi'=>$d->isi,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update)
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function add_user_menu(){
		if (!$this->input->is_ajax_request()) 
		redirect('not_found');
		$nama=$this->input->post('nama');
		$seq=$this->input->post('sequence');
		$parent=$this->input->post('parent');
		$sub_in=$this->input->post('sub_url');
		$parent=(($parent=='' || $parent==null) ? 0 : $parent);
		if ($nama == "" || $seq == ""){
			echo json_encode($this->messages->notValidParam());
		}else{
			$icon =$this->input->post('icon');
			if ($icon == '') {
				$icon = 'fas fa-chevron-circle-right';
			}
			$url=strtolower($this->input->post('url'));
			$par=$this->model_master->getAllMenuUserbyId($parent);
			$sub_url='';
			foreach ($par as $px) {
				$sub_url=$this->otherfunctions->addValueToArrayDb($px->sub_url,$url,';');
			}
			if ($parent != 0) {
				$this->model_global->updateQuery(['sub_url'=>$sub_url],'master_menu_user',['id_menu'=>$parent]);
			}
			$data=[
				'nama'=>ucwords($nama),
				'parent'=>$parent,
				'sequence'=>$seq,
				'icon'=>$icon,
				'url'=>$url,
				'sub_url'=>$sub_in,
				'custom'=>$this->input->post('custom'),
				'isi'=>$this->input->post('isi'),
			]; 
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			echo json_encode($this->model_global->insertQuery($data,'master_menu_user'));
		}
	}
	function edt_user_menu(){
		if (!$this->input->is_ajax_request()) 
		redirect('not_found');
		$id=$this->input->post('id');
		$nama=$this->input->post('nama');
		$seq=$this->input->post('sequence');
		$parent=$this->input->post('parent');
		$parent_old=$this->input->post('parent_old');
		$sub_in=$this->input->post('sub_url');
		// if ($id == "" || $parent_old == "" || $nama == "" || $seq == "" || $parent == ""){
		if ($id == "" || $nama == "" || $seq == ""){
			echo json_encode($this->messages->unfillForm());
		}else{
			$icon =$this->input->post('icon');
			if ($icon == '') {
				$icon = 'fas fa-chevron-circle-right';
			}
			$url=strtolower($this->input->post('url'));
			$par=$this->model_master->getAllMenuUserbyId($parent);
			$sub_url_add='';
			if ($parent_old == $parent) {
				foreach ($par as $px) {
					$sub_url_add=$this->otherfunctions->addValueToArrayDb($px->sub_url,$url,';');
				}
				if ($parent != 0) {
					$this->model_global->updateQuery(['sub_url'=>$sub_url_add],'master_menu_user',['id_menu'=>$parent]);
				}
			}else{
				$par_old=$this->model_master->getAllMenuUserbyId($parent_old);
				foreach ($par_old as $px_old) {
					$sub_url_del=$this->otherfunctions->removeValueToArrayDb($px_old->sub_url,$url,';');
				}
				foreach ($par as $px) {
					$sub_url_add=$this->otherfunctions->addValueToArrayDb($px->sub_url,$url,';');
				}
				if ($parent_old != 0 && $parent != 0) {
					$this->model_global->updateQuery(['sub_url'=>$sub_url_add],'master_menu_user',['id_menu'=>$parent]);
					$this->model_global->updateQuery(['sub_url'=>$sub_url_del],'master_menu_user',['id_menu'=>$parent_old]);
				}
			}
			$data=[
				'nama'=>ucwords($nama),
				'parent'=>$parent,
				'sequence'=>$seq,
				'icon'=>$icon,
				'url'=>$url,
				'sub_url'=>$sub_in,
				'custom'=>$this->input->post('custom_edit'),
				'isi'=>$this->input->post('isi_edit'),
			]; 
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			echo json_encode($this->model_global->updateQuery($data,'master_menu_user',['id_menu'=>$id]));
		}
	}
	//========================================================== MASTER ================================================================
	public function master_simpanan()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_master->getListMasterSimpanan();
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_simpanan,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					if (isset($access['l_ac']['del'])) {
						$delete = (in_array($access['l_ac']['del'], $access['access'])&&$d->kode!='SIM202103090001'&&$d->kode!='SIM202103090002'&&$d->kode!='SIM202104100001') ? '<button type="button" class="btn btn-danger btn-sm"  href="javascript:void(0)" onclick=delete_modal('.$d->id_simpanan.')><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i></button> ' : null;
					}else{
						$delete = null;
					}
					$info = '<button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick=view_modal('.$d->id_simpanan.')><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button> ';

					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_simpanan,
						$d->kode,
						$d->nama,
						$this->formatter->getFormatMoneyUser($d->besar_simpanan),
						$properties['tanggal'],
						$properties['status'],
						$info.$delete,
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_simpanan');
				$data=$this->model_master->getMasterSimpanan($id);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_simpanan,
						'kode'=>$d->kode,
						'nama'=>$d->nama,
						'besar_simpanan'=>$this->formatter->getFormatMoneyUser($d->besar_simpanan),
						'keterangan'=>$d->keterangan,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update)
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'kode') {
				$data = $this->codegenerator->kodeMasterSimpan();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function add_master_simpanan(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$nama=$this->input->post('nama');
		if ($nama != "") {
			$data=[
				'kode'=>ucwords($this->input->post('kode')),
				'nama'=>$this->input->post('nama'),
				'besar_simpanan'=>$this->formatter->getFormatMoneyDb($this->input->post('besar_simpanan')),
				'keterangan'=>$this->input->post('keterangan'),
			];
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			$datax = $this->model_global->insertQuery($data,'master_simpanan');
		}else{
			$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
	function edit_master_simpanan(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$nama=$this->input->post('nama');
		if ($nama != "") {
			$data=[
				'kode'=>ucwords($this->input->post('kode')),
				'nama'=>$this->input->post('nama'),
				'besar_simpanan'=>$this->formatter->getFormatMoneyDb($this->input->post('besar_simpanan')),
				'keterangan'=>$this->input->post('keterangan'),
			];
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'master_simpanan',['id_simpanan'=>$id]);
		}else{
        	$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
	//===================== PINJAMAN ===================================
	public function master_pinjaman()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_master->getListMasterPinjaman();
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_pinjaman,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];

					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_pinjaman,
						$d->kode,
						$d->nama,
						$d->lama_pinjam.' Bulan',
						$this->formatter->getFormatMoneyUser($d->maksimal),
						$d->bunga,	
						$properties['tanggal'],
						$properties['status'],
						$properties['aksi']
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_pinjaman');
				$data=$this->model_master->getMasterPinjaman($id);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_pinjaman,
						'kode'=>$d->kode,
						'nama'=>$d->nama,
						'lama_pinjam'=>$d->lama_pinjam,
						'maksimal'=>$this->formatter->getFormatMoneyUser($d->maksimal),
						'bunga'=>$d->bunga,
						'keterangan'=>$d->keterangan,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update)
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'kode') {
				$data = $this->codegenerator->kodeMasterPinjam();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function add_master_pinjaman(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$nama=$this->input->post('nama');
		if ($nama != "") {
			$data=[
				'kode'=>ucwords($this->input->post('kode')),
				'nama'=>$this->input->post('nama'),
				'lama_pinjam'=>$this->input->post('lama_pinjam'),
				'maksimal'=>$this->formatter->getFormatMoneyDb($this->input->post('maksimal')),
				'bunga'=>$this->input->post('bunga'),
				'keterangan'=>$this->input->post('keterangan'),
			];
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			$datax = $this->model_global->insertQuery($data,'master_pinjaman');
		}else{
			$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
	function edit_master_pinjaman(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$nama=$this->input->post('nama');
		if ($nama != "") {
			$data=[
				'kode'=>ucwords($this->input->post('kode')),
				'nama'=>$this->input->post('nama'),
				'lama_pinjam'=>$this->input->post('lama_pinjam'),
				'maksimal'=>$this->formatter->getFormatMoneyDb($this->input->post('maksimal')),
				'bunga'=>$this->input->post('bunga'),
				'keterangan'=>$this->input->post('keterangan'),
			];
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'master_pinjaman',['id_pinjaman'=>$id]);
		}else{
        	$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
}	