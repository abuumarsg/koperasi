<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Pages
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

class Pages extends CI_Controller 
{
	public function __construct() 
	{  
		parent::__construct(); 
		$this->date = $this->otherfunctions->getDateNow();
		if (isset($_SESSION['adm'])) {
			$this->admin = $_SESSION['adm']['id'];	
		}else{
			redirect('auth');  
		}
	    $this->rando = $this->codegenerator->getPin(6,'number');
		$dtroot['admin']=$this->model_admin->adm($this->admin);
		$l_acc=$this->otherfunctions->getYourAccess($this->admin);
		$l_ac=$this->otherfunctions->getAllAccess();
		if (isset($l_ac['stt'])) {
			if (in_array($l_ac['stt'], $l_acc)) {
		      $attr='type="submit"';
		    }else{
		      $attr='type="button" data-toggle="tooltip" title="Tidak Diizinkan"';
		    }
		    if (!in_array($l_ac['edt'], $l_acc) && !in_array($l_ac['del'], $l_acc)) {
		      $not_allow='<label class="label label-danger">Tidak Diizinkan</label>';
		    }else{
		      $not_allow=NULL;
		    }
		}else{
			$not_allow=null;
			$attr=null;
		}
		
		$this->access=array('access'=>$l_acc,'l_ac'=>$l_ac,'b_stt'=>$attr,'n_all'=>$not_allow);
		$this->link=$this->otherfunctions->getYourMenu($this->admin);
		$nm=explode(" ", $dtroot['admin']['nama']);
		$datax['adm'] = array( 
				'nama'=>$nm[0],
				'email'=>$dtroot['admin']['email'],
				'kelamin'=>$dtroot['admin']['kelamin'],
				'foto'=>$dtroot['admin']['foto'],
				'create'=>$dtroot['admin']['create_date'],
				'update'=>$dtroot['admin']['update_date'],
				'login'=>$dtroot['admin']['last_login'],
				'level'=>$dtroot['admin']['level'],
				'skin'=>$dtroot['admin']['skin'],
				'menu'=>$this->model_master->getListMenuActive(),
				'your_menu'=>$this->otherfunctions->getYourMenuId($this->admin),

			);
		$this->dtroot=$datax;
	}
	public function coba(){
		echo '<pre>';
		$data['lama_pinjam'] = 9;
		$tanggal = '21/09/2021';
		if($data['lama_pinjam'] <= 12){
			$bulan = $data['lama_pinjam'];
		}else{

		}
		$tgl = $this->formatter->getDateFormatDb($tanggal);
		$tglx = explode('-', $tgl);
		$tgl = mktime(0, 0, 0, date($tglx[1])+$bulan, date($tglx[2]), date($tglx[0]));
		$data = date("d/m/Y", $tgl);
		print_r($data);
	}
	public function index(){
		redirect('pages/dashboard');
	}
	//=============PAGE SETTINGS BEGIN=============//
	public function setting_update(){
		$data=['access'=>$this->access];
		$this->load->view('admin_tem/headerx',$this->dtroot); 
		$this->load->view('admin_tem/sidebarx',$this->dtroot);
		$this->load->view('pages/setting_update',$data);
		$this->load->view('admin_tem/footerx');
	}
	public function setting_access(){
		$nama_menu="setting_access";
		if (in_array($nama_menu, $this->link)) {
			if ($this->dtroot['adm']['level'] == 0) {
				$data=array(
					'level_adm'=>$this->dtroot['adm']['level'],
					'access'=>$this->access,
				);
				$this->load->view('admin_tem/headerx',$this->dtroot);
				$this->load->view('admin_tem/sidebarx',$this->dtroot);
				$this->load->view('pages/setting_access',$data);
				$this->load->view('admin_tem/footerx');
			}else{
				redirect('not_found');
			}
		}else{
			redirect('pages/not_found');
		}
	}
	public function setting_user_group(){
		$nama_menu="setting_user_group";
		if (in_array($nama_menu, $this->link)) {
			$data=array(
				'hak_access'=>$this->model_master->getListAccess(),
				'access'=>$this->access,
				'level_admin'=>$this->model_admin->adm($this->admin)['level'],
			);
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/setting_user_group',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}
	public function setting_menu(){
		$nama_menu="setting_menu";
		if (in_array($nama_menu, $this->link)) {
			$data=array(
				'access'=>$this->access,
				'level_admin'=>$this->model_admin->adm($this->admin)['level'],
			);
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/setting_menu',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}
	public function setting_admin(){
		$nama_menu="setting_admin";
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'access'=>$this->access,
				'level_admin'=>$this->model_admin->adm($this->admin)['level'],
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/setting_admin',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function load_modal_delete()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('table');
		if (!empty($id)) {
			$data['modal']=$this->load->view('_partial/_delete_modal_confirm','',TRUE);
			echo json_encode($data);
		}else{
			echo json_encode($this->messages->sessNotValidParam());
		}
	}
	public function load_modal_delete_2()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('table');
		if (!empty($id)) {
			$data['modal']=$this->load->view('_partial/_delete_modal_confirm_2','',TRUE);
			echo json_encode($data);
		}else{
			echo json_encode($this->messages->sessNotValidParam());
		}
	}
	function not_found(){
		$this->load->view('admin_tem/header');
		$this->load->view('not_found');
		$this->load->view('admin_tem/footer');
	}
	//========================================================= main pages =======================================================
	public function dashboard(){
		$nama_menu="dashboard";
		$qt=$this->db->get('quote')->result();
		$idq=rand(1,count($qt));
		$qt1=$this->db->get_where('quote',array('id'=>$idq))->row_array();
		$nPinjaman=0;
		$pinjaman = $this->model_data->getListDataPengajuan(['a.status_pinjaman'=>'0']);
		foreach ($pinjaman as $d) {
			$nPinjaman+=$d->besar_pinjam;
		}
		$nSimpanan=0;
		$Simpanan = $this->model_data->getListTransaksiTabungan(['a.flag'=>'penambah']);
		foreach ($Simpanan as $e) {
			$nSimpanan+=$e->nominal;
		}
		$nAngsuran=0;
		$Angsuran = $this->model_data->getListTransaksiAngsuran(['d.status_pinjaman'=>'0']);
		foreach ($Angsuran as $f) {
			$nAngsuran+=$f->nominal;
		}
		$nAmbil=0;
		$Ambil = $this->model_data->getListTransaksiTabungan(['a.flag'=>'pengurang']);
		foreach ($Ambil as $g) {
			$nAmbil+=$g->nominal;
		}
		if (in_array($nama_menu, $this->link)) {
			$data=array(
				'access'=>$this->access,
				'quote'=>$qt1,
				'nPinjaman'=>$this->formatter->getFormatMoneyUser($nPinjaman),
				'nSimpanan'=>$this->formatter->getFormatMoneyUser($nSimpanan),
				'nAngsuran'=>$this->formatter->getFormatMoneyUser($nAngsuran),
				'nAmbil'=>$this->formatter->getFormatMoneyUser($nAmbil),
				'berita'=>0,
				'gambar'=>0,
				'video'=>0,
			);
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/index',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}
	public function profile(){
		$pro=$this->model_admin->adm($this->admin);
		$log=$this->model_admin->log_login($this->admin);
		$dtp['profile'] = array(
			'id'=>$this->admin,
			'nama'=>$pro['nama'],
			'alamat'=>$pro['alamat'],
			'email'=>$pro['email'],
			'kelamin'=>$pro['kelamin'],
			'username'=>$pro['username'],
			'foto'=>$pro['foto'],
			'ev'=>$pro['email_verified'],
			'level'=>$pro['level'],
			'create'=>date("d F Y",strtotime($pro['create_date'])),
			'update'=>date("l, d F Y", strtotime($pro['update_date'])).' <i style="color:red;" class="fa fa-clock-o"></i> '.date("H:i:s", strtotime($pro['update_date'])),
			'login'=>date("l, d F Y", strtotime($pro['last_login'])).' <i style="color:red;" class="fa fa-clock-o"></i> '.date("H:i:s", strtotime($pro['last_login'])),
			'log'=>$log,
		);
		$this->load->view('admin_tem/headerx',$this->dtroot);
		$this->load->view('admin_tem/sidebarx',$this->dtroot);
		$this->load->view('pages/profile',$dtp);
		$this->load->view('admin_tem/footerx');
	}
	public function about_apps(){
		$dtp['profile'] = array(
			'id'=>$this->admin,
		);
		$this->load->view('admin_tem/headerx',$this->dtroot);
		$this->load->view('admin_tem/sidebarx',$this->dtroot);
		$this->load->view('pages/about_apps',$dtp);
		$this->load->view('admin_tem/footerx');
	}
	public function kontak(){
		$nama_menu="kontak";
		if (in_array($nama_menu, $this->link)) {
			$data=array(
				'access'=>$this->access,
			);
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/kontak',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}
	public function company_profile(){
		$nama_menu="company_profile";
		if (in_array($nama_menu, $this->link)) {
			$data=array(
				'access'=>$this->access,
				'data'=>$this->model_data->getCompanyProfile(),
			);
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/company_profile',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}
	//====================================================PAGE MASTER BEGIN====================================================//
	public function master_pinjaman(){
		$nama_menu="master_pinjaman";
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'access'=>$this->access,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/master_pinjaman',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function master_simpanan(){
		$nama_menu="master_simpanan";
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'access'=>$this->access,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/master_simpanan',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	//====================================================PAGE PENGELOLAAN BEGIN====================================================//
	public function data_anggota(){
		$nama_menu="data_anggota";
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'access'=>$this->access,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/data_anggota',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function data_tabungan(){
		$nama_menu="data_tabungan";
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'access'=>$this->access,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/data_tabungan',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function data_ambil_uang(){
		$nama_menu="data_ambil_uang";
		if (in_array($nama_menu, $this->link)) {
			$kodetab=$this->uri->segment(3);
			$tabungan = $this->model_data->getListDataTabungan(['a.kode_tabungan'=>$kodetab],true);
			$data=[
				'access'=>$this->access,
				'tabungan'=>$tabungan,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/data_ambil_uang',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function data_pengajuan(){
		$nama_menu="data_pengajuan";
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'access'=>$this->access,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/data_pengajuan',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function data_angsuran(){
		$nama_menu="data_pengajuan";
		$id_ang = $this->uri->segment(3);
		$pinjam =$this->model_data->getListDataPengajuan(['a.id'=>$id_ang],true);
		$banyakAngsuran=$this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$pinjam['kode']]);
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'access'=>$this->access,
				'id_pinjaman'=>$id_ang,
				'data_pinjam'=>$pinjam,
				'nominal_sudah_diangsur'=>count($banyakAngsuran),
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/data_angsuran',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function transaksi(){
		$nama_menu="transaksi";
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'access'=>$this->access,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/data_transaksi',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function data_simpanan(){
		$nama_menu="data_simpanan";
		if (in_array($nama_menu, $this->link)) {
			$kodeAng=$this->uri->segment(3);
			$tabungan = $this->model_data->getListDataTabungan(['a.kode_anggota'=>$kodeAng],true);
			$data=[
				'access'=>$this->access,
				'tabungan'=>$tabungan,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/data_simpanan',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function data_pinjaman(){
		$nama_menu="data_pinjaman";
		if (in_array($nama_menu, $this->link)) {
			$kodeAng=$this->uri->segment(3);
			$pinjaman = $this->model_data->getListDataPengajuan(['a.kode_anggota'=>$kodeAng],true);
			$data=[
				'access'=>$this->access,
				'pinjaman'=>$pinjaman,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/data_pinjaman',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function data_angsur(){
		$nama_menu="data_pinjaman";
		$id_ang = $this->uri->segment(3);
		$pinjam =$this->model_data->getListDataPengajuan(['a.id'=>$id_ang],true);
		$banyakAngsuran=$this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$pinjam['kode']]);
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'before'=>'data_pinjaman',
				'access'=>$this->access,
				'id_pinjaman'=>$id_ang,
				'data_pinjam'=>$pinjam,
				'nominal_sudah_diangsur'=>count($banyakAngsuran),
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/data_angsur',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	//==================================================== LAPORAN =====================================================================
	
	public function laporan_anggota(){
		$nama_menu="laporan_anggota";
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'access'=>$this->access,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/laporan_anggota',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function laporan_simpanan(){
		$nama_menu="laporan_simpanan";
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'mSimpanan'=>$this->model_master->getListMasterSimpananNew(),
				'access'=>$this->access,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/laporan_simpanan',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function laporan_pinjaman(){
		$nama_menu="laporan_pinjaman";
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'access'=>$this->access,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/laporan_pinjaman',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function laporan_perbulan(){
		$nama_menu="laporan_perbulan";
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'access'=>$this->access,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/laporan_perbulan',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	public function laporan_keuangan(){
		$nPinjaman=0;
		$pinjaman = $this->model_data->getListDataPengajuan(['a.status_pinjaman'=>'0']);
		foreach ($pinjaman as $d) {
			$nPinjaman+=$d->besar_pinjam;
		}
		$nSimpanan=0;
		$Simpanan = $this->model_data->getListTransaksiTabungan(['a.flag'=>'penambah']);
		foreach ($Simpanan as $e) {
			$nSimpanan+=$e->nominal;
		}
		$nAngsuran=0;
		$Angsuran = $this->model_data->getListTransaksiAngsuran(['d.status_pinjaman'=>'0']);
		foreach ($Angsuran as $f) {
			$nAngsuran+=$f->nominal;
		}
		$nAmbil=0;
		$Ambil = $this->model_data->getListTransaksiTabungan(['a.flag'=>'pengurang']);
		foreach ($Ambil as $g) {
			$nAmbil+=$g->nominal;
		}
		$pendapatan = $nSimpanan+$nAngsuran;
		$pengeluaran = $nPinjaman+$nAmbil;
		$aktiva = $pendapatan-$pengeluaran;
		$nama_menu="laporan_keuangan";
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'access'=>$this->access,
				'pendapatan'=>$this->formatter->getFormatMoneyUser($pendapatan),
				'pengeluaran'=>$this->formatter->getFormatMoneyUser($pengeluaran),
				'aktiva'=>$this->formatter->getFormatMoneyUser($aktiva),
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/laporan_keuangan',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
	//==================================================== PRINT =======================================================================	
	public function print_data_tabungan()
	{
		$getData=$this->model_data->getListDataTabungan();
		$data =[
			'datax'=>$getData,
			'com'=>$this->model_data->getCompanyProfile(),
		];
		$this->load->view('print_page/header');
		$this->load->view('print_page/data_tabungan',$data);
		$this->load->view('print_page/footer');
	}	
	public function print_data_simpanan()
	{
		$kode = $this->input->post('kode_tabungan');
		// echo $kode;
		$getData=$this->model_data->getListTransaksiTabungan(['a.kode_tabungan'=>$kode]);
		$tab = $this->model_data->getListDataTabungan(['a.kode_tabungan'=>$kode],true);
		$data =[
			'datax'=>$getData,
			'com'=>$this->model_data->getCompanyProfile(),
			'nama_anggota'=>$this->model_data->getListTransaksiTabungan(['a.kode_tabungan'=>$kode],true)['nama_anggota'],
			'besar_simpanan'=>$this->formatter->getFormatMoneyUser($tab['besar_tabungan']),
		];
		$this->load->view('print_page/header');
		$this->load->view('print_page/data_simpanan',$data);
		$this->load->view('print_page/footer');
	}
	public function print_data_angsuran()
	{
		$kodex = $this->uri->segment('3');
		$kode = $this->formatter->decryptChar($kodex);
		$getData=$this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$kode]);
		$data =[
			'datax'=>$getData,
			'com'=>$this->model_data->getCompanyProfile(),
			'nama_anggota'=>$this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$kode],true)['nama_anggota'],
		];
		$this->load->view('print_page/header');
		$this->load->view('print_page/lap_angsuran',$data);
		$this->load->view('print_page/footer');
	}
	public function print_laporan_anggota()
	{
		$getData=$this->model_data->getListDataAnggota();
		$data =[
			'datax'=>$getData,
			'com'=>$this->model_data->getCompanyProfile(),
		];
		$this->load->view('print_page/header');
		$this->load->view('print_page/laporan_anggota',$data);
		$this->load->view('print_page/footer');
	}	
	public function print_laporan_simpanan()
	{
		$getData=$this->model_data->getListDataAnggota();
		$data =[
			'datax'=>$getData,
			'com'=>$this->model_data->getCompanyProfile(),
			'mSimpanan'=>$this->model_master->getListMasterSimpananNew(),
		];
		$this->load->view('print_page/header');
		$this->load->view('print_page/laporan_simpanan',$data);
		$this->load->view('print_page/footer');
	}	
	public function print_laporan_pinjaman()
	{
		$getData=$this->model_data->getListDataPengajuan();
		$data =[
			'datax'=>$getData,
			'com'=>$this->model_data->getCompanyProfile(),
		];
		$this->load->view('print_page/header');
		$this->load->view('print_page/laporan_pinjaman',$data);
		$this->load->view('print_page/footer');
	}	
	public function print_laporan_bulanan()
	{
		parse_str($this->input->post('data_filter'), $data_filter);
		$bulan = $data_filter['bulan'];
		$tahun = $data_filter['tahun'];
		if($data_filter['jenis'] == 'pinjaman'){
			$where = 'MONTH(a.tgl_pengajuan)="'.$bulan.'" AND YEAR(a.tgl_pengajuan)="'.$tahun.'" AND a.status_pinjaman="0"';
			$getData=$this->model_data->getListDataPengajuan($where);
			$data =[
				'bulan'=>$this->formatter->getNameOfMonth($bulan),
				'tahun'=>$tahun,
				'datax'=>$getData,
				'com'=>$this->model_data->getCompanyProfile(),
			];
			$this->load->view('print_page/header');
			$this->load->view('print_page/laporan_pinjaman',$data);
			$this->load->view('print_page/footer');	
		}elseif($data_filter['jenis'] == 'simpanan'){
			$where = 'MONTH(a.tanggal)="'.$bulan.'" AND YEAR(a.tanggal)="'.$tahun.'" AND a.flag = "penambah"';
			$getData=$this->model_data->getListTransaksiTabungan($where);
			$data =[
				'bulan'=>$this->formatter->getNameOfMonth($bulan),
				'tahun'=>$tahun,
				'datax'=>$getData,
				'com'=>$this->model_data->getCompanyProfile(),
				'mSimpanan'=>$this->model_master->getListMasterSimpananNew(),
			];
			$this->load->view('print_page/header');
			$this->load->view('print_page/laporan_tabungan_bulanan',$data);
			$this->load->view('print_page/footer');		
		}elseif($data_filter['jenis'] == 'angsuran'){
			$where = 'MONTH(a.tanggal)="'.$bulan.'" AND YEAR(a.tanggal)="'.$tahun.'"';
			$getData=$this->model_data->getListTransaksiAngsuran($where);
			$data =[
				'bulan'=>$this->formatter->getNameOfMonth($bulan),
				'tahun'=>$tahun,
				'datax'=>$getData,
				'com'=>$this->model_data->getCompanyProfile(),
				'nama_anggota'=>null,
			];
			$this->load->view('print_page/header');
			$this->load->view('print_page/lap_angsuran',$data);
			$this->load->view('print_page/footer');
		}elseif($data_filter['jenis'] == 'ambil'){
			$where = 'MONTH(a.tanggal)="'.$bulan.'" AND YEAR(a.tanggal)="'.$tahun.'" AND a.flag = "pengurang"';
			$getData = $this->model_data->getListTransaksiTabungan($where);
			$data =[
				'bulan'=>$this->formatter->getNameOfMonth($bulan),
				'tahun'=>$tahun,
				'datax'=>$getData,
				'com'=>$this->model_data->getCompanyProfile(),
			];
			$this->load->view('print_page/header');
			$this->load->view('print_page/laporan_ambil_uang',$data);
			$this->load->view('print_page/footer');
		}
	}
	//=========================================================== GRAFIK ================================================================

	public function grafik(){
		$nama_menu="grafik";
		if (in_array($nama_menu, $this->link)) {
			$data=[
				'access'=>$this->access,
			];
			$this->load->view('admin_tem/headerx',$this->dtroot);
			$this->load->view('admin_tem/sidebarx',$this->dtroot);
			$this->load->view('pages/grafik',$data);
			$this->load->view('admin_tem/footerx');
		}else{
			redirect('pages/not_found');
		}
	}	
}