<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
     * Code From GFEACORP.
     * Web Developer
     * @author      Galeh Fatma Eko Ardiansa
     * @package     Otherfunctions
     * @copyright   Copyright (c) 2018 GFEACORP
     * @version     1.0, 1 September 2018
     * Email        galeh.fatma@gmail.com
     * Phone        (+62) 85852924304
     */

class Employee extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->date = gmdate("Y-m-d H:i:s", time() + 3600*(7));
		if (isset($_SESSION['adm'])) {
			$this->admin = $_SESSION['adm']['id'];	
		}else{
			redirect('auth');
		}
		$ha = '0123456789';
	    $panjang = strlen($ha);
	    $rand = '';
	    for ($i = 0; $i < 6; $i++) {
	        $rand .= $ha[rand(0, $panjang - 1)];
	    }
	    $this->rando = $rand;		
		$dtroot['admin']=$this->model_admin->adm($this->admin);
		$nm=explode(" ", $dtroot['admin']['nama']);
		$datax['adm'] = array(
				'nama'=>$nm[0],
				'email'=>$dtroot['admin']['email'],
				'kelamin'=>$dtroot['admin']['kelamin'],
				'foto'=>$dtroot['admin']['foto'],
				'create'=>$dtroot['admin']['create_date'],
				'update'=>$dtroot['admin']['update_date'],
				'login'=>$dtroot['admin']['last_login'],
			);
		$this->dtroot=$datax;
		$l_acc=$this->otherfunctions->getYourAccess($this->admin);
		$l_ac=$this->otherfunctions->getAllAccess();
		if (isset($l_ac['stt'])) {
			if (in_array($l_ac['stt'], $l_acc)) {
		      $attr='type="submit"';
		    }else{
		      $attr='type="button" data-toggle="tooltip" title="Tidak Diizinkan"';
		    }
		    if (!in_array($l_ac['edt'], $l_acc) && !in_array($l_ac['del'], $l_acc)) {
		      $not_allow='<label class="label label-danger">Tidak Diizinkan</label>';
		    }else{
		      $not_allow=NULL;
		    }
		}else{
			$not_allow=null;
			$attr=null;
		}
		
		$this->access=array('access'=>$l_acc,'l_ac'=>$l_ac,'b_stt'=>$attr,'n_all'=>$not_allow);
	}
	public function index(){
		redirect('pages/dashboard');
	}
	// public function employee()
	// {
	// 	if (!$this->input->is_ajax_request()) 
	// 	   redirect('not_found');
	// 	$usage=$this->uri->segment(3);
	// 	if ($usage == null) {
	// 	   echo json_encode($this->messages->notValidParam());
	// 	}else{
	// 		if ($usage == 'view_all') {
	// 			$data=$this->model_karyawan->getEmployeeAllActive();
	// 			$access=unserialize(base64_decode($this->input->post('access')));
	// 			$no=1;
	// 			$datax['data']=[];
	// 			foreach ($data as $d) {
	// 				unset($access['l_ac']['stt']);
	// 				$var=[
	// 					'id'=>$d->id_karyawan,
	// 					'create'=>$d->create_date,
	// 					'update'=>$d->update_date,
	// 					'access'=>$access,
	// 					'status'=>$d->status,
	// 				];
	// 				$properties=$this->otherfunctions->getPropertiesTable($var);
	// 				$datax['data'][]=[
	// 					$d->id_karyawan,
	// 					$d->nik,
	// 					$d->nama,
	// 					($d->nama_jabatan != null) ? $d->nama_jabatan : $this->otherfunctions->getMark(null),
	// 					($d->nama_loker != null) ? $d->nama_loker : $this->otherfunctions->getMark(null),
	// 					($d->grade != null) ? $d->grade : $this->otherfunctions->getMark(null),
	// 					($d->email != null) ? $d->email : $this->otherfunctions->getMark(null),
	// 					($d->tgl_masuk != null) ? $this->formatter->getDateMonthFormatUser($d->tgl_masuk) : $this->otherfunctions->getMark(null),
	// 					$properties['tanggal'],
	// 					($d->status != null && $d->status == 1) ? '<i class="fa fa-circle scc" title="Online"></i>' : '<i class="fa fa-circle text-muted" title="Offline"></i>',
	// 					$properties['aksi'],
	// 				];
	// 				$no++;
	// 			}
	// 			echo json_encode($datax);
	// 		}elseif ($usage == 'view_one') {
	// 			$id = $this->input->post('id_karyawan');
	// 			$d=$this->model_karyawan->getEmployeeId($id);
	// 			$datax=[
	// 				'id'=>$d['id_karyawan'],
	// 				'nama'=>$d['nama'],
	// 				'status'=>$d['status'],
	// 				'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d['create_date']),
	// 				'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d['update_date']),
	// 				'create_by'=>$d['create_by'],
	// 				'update_by'=>$d['update_by'],
	// 				'nama_buat'=>(!empty($d['nama_buat'])) ? $d->nama_buat:$this->otherfunctions->getMark($d['nama_buat']),
	// 				'nama_update'=>(!empty($d['nama_update']))?$d->nama_update:$this->otherfunctions->getMark($d['nama_update'])
	// 			];
	// 			echo json_encode($datax);
	// 		}else{
	// 			echo json_encode($this->messages->notValidParam());
	// 		}
	// 	}
	// } 
	function edt_jabatan(){
		$id=$this->input->post('id_karyawan');
		$nik=$this->input->post('nik');
		if ($id == "" || $nik == ""){
			$this->messages->notValidParam();  
			redirect('pages/employee');
		}else{
			if ($this->input->post('jabatan_pa') == "" || $this->input->post('jabatan_pa') == NULL) {
				$jpa=NULL;
			}else{
				$jpa=$this->input->post('jabatan_pa');
			}
			if ($this->input->post('loker_pa') == "" || $this->input->post('loker_pa') == NULL) {
				$lpa=NULL;
			}else{
				$lpa=$this->input->post('loker_pa');
			}
			if ($this->input->post('pilih') == '0') {
				$jpa=NULL;
				$lpa=NULL;
			}
			if ($this->input->post('pilih_sub') == '0') {
				$kode_sub=NULL;
				$jbt=$this->input->post('jabatan');
				$lok=$this->input->post('loker');
			}else{
				$sub=$this->input->post('sub_jbt');
				$subd=$this->model_master->k_s_jabatan($sub);
				$jbt=$subd['kode_jabatan'];				
				$lok=$subd['kode_loker'];
				$kode_sub=$sub;				

			}

			$data=array(
				'jabatan'=>$jbt,
				'unit'=>$lok,
				'jabatan_pa'=>$jpa,
				'loker_pa'=>$lpa,
				'kode_sub'=>$kode_sub,
			);
			$this->db->where('id_karyawan',$id);
			$in=$this->db->update('karyawan',$data);
			if ($in) {
				$this->messages->allGood(); 
			}else{
				$this->messages->allFailure(); 
			}
	   		redirect('pages/view_employee/'.$nik);
		}
	}
	function up_foto(){
		$id=$this->input->post('nik');
		if ($id == ""){
			$this->messages->notValidParam();  
			redirect('pages/employee');
		}else{
			$config['upload_path']          = './asset/img/user-photo';
			$config['allowed_types']        = 'gif|jpg|png|jpeg|JPEG';
			$config['max_size']             = 1000;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('image')){
				$data = array('df' => $this->upload->data());
				if ($data['df']['is_image'] == "") {
					$this->messages->customFailure('Type File Foto harus *.gif, *.jpg, *.jpeg, *.png');
				}elseif ($data['df']['file_size'] > $config['max_size'] ) {
					$this->messages->customFailure('Ukuran Foto harus berukuran KURANG DARI 1 MB');
				}else{
					$this->messages->customFailure('Foto tidak bisa diupload, silahkan ganti file foto yang lain');
				}
			}else{
				$data = array('df' => $this->upload->data());
				$file='asset/img/user-photo/'.$data['df']['file_name'];
				$dt=array('foto'=>$file,'update_date'=>$this->date);
				$this->db->where('nik',$id);
				$this->db->update('karyawan',$dt);
				$this->messages->allGood();
			}
			redirect('pages/view_employee/'.$id);
		}
	}
	function add_new(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		// $karyawan=$this->model_karyawan->list_karyawan();
		// $finger_db='';
		// foreach ($karyawan as $karya) {
		// 	$finger_db .= $karya->id_finger;
		// }
		// $id_finger=$this->input->post('id_finger');
		// if($finger_db == $id_finger){
		// 	$this->messages->customFailure('ID Finger '.$id_finger.' sudah ada di database, Silahkan ganti nik atau ID Finger');
		// }else{
			$nama=$this->input->post('nama');
			if ($nama != "") {
				$data = array(	//'df' 						=> $this->upload->data(),
					//'foto'						=> $file,
					//'update_date'				=> $this->date,
					'nik'						=> $this->input->post('nik'),
					'no_ktp'					=> $this->input->post('no_id'),
					'id_finger'					=> $this->input->post('id_finger'),
					'nama'						=> strtoupper($nama),
					'tempat_lahir'				=> $this->input->post('tempat_lahir'),
					'tgl_lahir'					=> $this->formatter->getDateFormatDb($this->input->post('tgl_lahir')),
					'no_hp'						=> $this->input->post('no_hp'),
					'agama'						=> $this->input->post('agama'),
					'kelamin'					=> $this->input->post('kelamin'),
					'jabatan'					=> $this->input->post('jabatan'),
					'loker'						=> $this->input->post('loker'),
					'grade'						=> $this->input->post('grade'),
					'status_perjanjian'			=> $this->input->post('no_sk_baru'),
					'status_karyawan'			=> $this->input->post('status_karyawan'),
					'tgl_masuk'					=> $this->formatter->getDateFormatDb($this->input->post('tgl_masuk')),
					'npwp'						=> $this->input->post('no_npwp'),
					'gol_darah'					=> $this->input->post('darah'),
					'bpjskes'					=> $this->input->post('no_bpjskes'),
					'bpjstk'					=> $this->input->post('no_bpjstk'),
					'berat_badan'				=> $this->input->post('berat_badan'),
					'tinggi_badan'				=> $this->input->post('tinggi_badan'),
					'rekening'					=> $this->input->post('rekening'),
					'email'						=> $this->input->post('email'),
					'status_pajak'				=> $this->input->post('status_pajak'),
					'status_nikah'				=> $this->input->post('nikah'),
					'alamat_asal_jalan'			=> $this->input->post('alamat_asal_jalan'),
					'alamat_asal_desa'			=> $this->input->post('alamat_asal_desa'),
					'alamat_asal_kecamatan'		=> $this->input->post('alamat_asal_kecamatan'),
					'alamat_asal_kabupaten'		=> $this->input->post('alamat_asal_kabupaten'),
					'alamat_asal_provinsi'		=> $this->input->post('alamat_asal_provinsi'),
					'alamat_asal_pos'			=> $this->input->post('alamat_asal_pos'),
					'alamat_sekarang_jalan'		=> $this->input->post('alamat_sekarang_jalan'),
					'alamat_sekarang_desa'		=> $this->input->post('alamat_sekarang_desa'),
					'alamat_sekarang_kecamatan'	=> $this->input->post('alamat_sekarang_kecamatan'),
					'alamat_sekarang_kabupaten'	=> $this->input->post('alamat_sekarang_kabupaten'),
					'alamat_sekarang_provinsi'	=> $this->input->post('alamat_sekarang_provinsi'),
					'alamat_sekarang_pos'		=> $this->input->post('alamat_sekarang_pos'),
					'nama_ayah'					=> $this->input->post('nama_ayah'),
					'tempat_lahir_ayah'			=> $this->input->post('tempat_lahir_ayah'),
					'tanggal_lahir_ayah'		=> $this->formatter->getDateFormatDb($this->input->post('tanggal_lahir_ayah')),
					'pendidikan_terakhir_ayah'	=> $this->input->post('pendidikan_terakhir_ayah'),
					'no_hp_ayah'				=> $this->input->post('no_telp_ayah'),
					'alamat_ayah'				=> $this->input->post('alamat_ayah'),
					'desa_ayah'					=> $this->input->post('desa_ayah'),
					'kecamatan_ayah'			=> $this->input->post('kecamatan_ayah'),
					'kabupaten_ayah'			=> $this->input->post('kabupaten_ayah'),
					'provinsi_ayah'				=> $this->input->post('provinsi_ayah'),
					'kode_pos_ayah'				=> $this->input->post('kode_pos_ayah'),
					'nama_ibu'					=> $this->input->post('nama_ibu'),
					'tempat_lahir_ibu'			=> $this->input->post('tempat_lahir_ibu'),
					'tanggal_lahir_ibu'			=> $this->formatter->getDateFormatDb($this->input->post('tanggal_lahir_ibu')),
					'pendidikan_terakhir_ibu'	=> $this->input->post('pendidikan_terakhir_ibu'),
					'no_hp_ibu'					=> $this->input->post('no_telp_ibu'),
					'alamat_ibu'				=> $this->input->post('alamat_ibu'),
					'desa_ibu'					=> $this->input->post('desa_ibu'),
					'kecamatan_ibu'				=> $this->input->post('kecamatan_ibu'),
					'kabupaten_ibu'				=> $this->input->post('kabupaten_ibu'),
					'provinsi_ibu'				=> $this->input->post('provinsi_ibu'),
					'kode_pos_ibu'				=> $this->input->post('kode_pos_ibu'),
					'nama_pasangan'				=> $this->input->post('nama_pasangan'),
					'tempat_lahir_pasangan'		=> $this->input->post('tempat_lahir_pasangan'),
					'tanggal_lahir_pasangan'	=> $this->formatter->getDateFormatDb($this->input->post('tanggal_lahir_pasangan')),
					'pendidikan_terakhir_pasangan'	=> $this->input->post('pendidikan_terakhir_pasangan'),
					'no_hp_pasangan'			=> $this->input->post('no_telp_pasangan'),
					'alamat_pasangan'			=> $this->input->post('alamat_pasangan'),
					'desa_pasangan'				=> $this->input->post('desa_pasangan'),
					'kecamatan_pasangan'		=> $this->input->post('kecamatan_pasangan'),
					'kabupaten_pasangan'		=> $this->input->post('kabupaten_pasangan'),
					'provinsi_pasangan'			=> $this->input->post('provinsi_pasangan'),
					'kode_pos_pasangan'			=> $this->input->post('kode_pos_pasangan'),
					'password'					=> hash('sha512', '123456'),
				//	'create_date'				=> $this->date,
					);
						$id_anak			=$this->input->post('id_anak');
						$nama_anak			=$this->input->post('nama_anak');
						$kelamin_anak		=$this->input->post('kelamin_anak');
						$tempat_lahir_anak	=$this->input->post('tempat_lahir_anak');
						$tanggal_lahir_anak	=$this->input->post('tanggal_lahir_anak');
						$no_hp_anak	= $this->input->post('nohp_anak');
						$pendidikan_anak	=$this->input->post('pendidikan_anak');
						if ($nama_anak != NULL) {
							for ($i = 1; $i <=count($nama_anak) ; $i++) {
								$data_anak=array(
									'nama_anak'			=>$nama_anak[$i],
									'kelamin_anak'		=>$kelamin_anak[$i],
									'tempat_lahir_anak'	=>$tempat_lahir_anak[$i],
									'tanggal_lahir_anak'=>$this->formatter->getDateFormatDb($tanggal_lahir_anak[$i]),
									'pendidikan_anak'	=>$pendidikan_anak[$i],
									'no_telp'			=>$no_hp_anak[$i],
									'nik'				=>$data['nik']
									);
								$data_anak=array_merge($data_anak,$this->model_global->getCreateProperties($this->admin));
								$this->model_global->insertQuery($data_anak,'karyawan_anak');
							}
						}
						if ($nama_anak>1) {
							$data['jumlah_anak']=$i;
						}else{
							$data['jumlah_anak']=null;
						}
						$nama_saudara				= $this->input->post('nama_saudara');
						$jenis_kelamin_saudara		= $this->input->post('jenis_kelamin_saudara');
						$tempat_lahir_saudara		= $this->input->post('tempat_lahir_saudara');
						$tanggal_lahir_saudara		= $this->input->post('tanggal_lahir_saudara');
						$pendidikan_saudara			= $this->input->post('pendidikan_saudara');
						$no_telp_saudara			= $this->input->post('no_telp_saudara');
						if ($nama_saudara != NULL){
							for ($j = 1; $j <=count($nama_saudara); $j++) {
							$data_saudara = array(
								'nama_saudara'			=> $nama_saudara[$j],
								'jenis_kelamin_saudara'	=> $jenis_kelamin_saudara[$j],
								'tempat_lahir_saudara'	=> $tempat_lahir_saudara[$j],
								'tanggal_lahir_saudara'	=> $this->formatter->getDateFormatDb($tanggal_lahir_saudara[$j]),
								'pendidikan_saudara'	=> $pendidikan_saudara[$j],
								'no_telp_saudara'		=> $no_telp_saudara[$j],
								'nik'					=> $data['nik']
								);
							$data_saudara=array_merge($data_saudara,$this->model_global->getCreateProperties($this->admin));
							$this->model_global->insertQuery($data_saudara,'karyawan_saudara');
							}
						}
						$jenjang_pendidikan 	= $this->input->post('jenjang_pendidikan');
						$nama_sekolah			= $this->input->post('nama_sekolah');
						$jurusan				= $this->input->post('jurusan');
						$fakultas				= $this->input->post('fakultas');
						$tahun_masuk			= $this->input->post('tahun_masuk');
						$tahun_keluar			= $this->input->post('tahun_keluar');
						$alamat_sekolah			= $this->input->post('alamat_sekolah');
						if ($nama_sekolah != NULL){
							for ($k = 1; $k <=count($nama_sekolah) ; $k++) {
							$data_sekolah = array ( 
								'jenjang_pendidikan'	=>$jenjang_pendidikan[$k],
								'nama_sekolah'	=> $nama_sekolah[$k],
								'jurusan'		=> $jurusan[$k],
								'fakultas'		=> $fakultas[$k],
								'tahun_masuk'	=> $this->formatter->getDateFormatDb($tahun_masuk[$k]),
								'tahun_keluar'	=> $this->formatter->getDateFormatDb($tahun_keluar[$k]),
								'alamat_sekolah'=> $alamat_sekolah[$k],
								'nik'			=> $data['nik']
							);
							$data_sekolah=array_merge($data_sekolah,$this->model_global->getCreateProperties($this->admin));
							$this->model_global->insertQuery($data_sekolah,'karyawan_pendidikan');
							}
						}
						$nama_pnf				= $this->input->post('nama_pnf');
						$tanggal_masuk_pnf		= $this->input->post('tanggal_masuk_pnf');
						$sertifikat_pnf			= $this->input->post('sertifikat_pnf');
						$nama_lembaga_pnf		= $this->input->post('nama_lembaga_pnf');
						$alamat_pnf				= $this->input->post('alamat_pnf');
						$keterangan_pnf			= $this->input->post('keterangan_pnf');
						if ($nama_pnf != NULL){
							for ($l = 1; $l <=count($nama_pnf) ; $l++) {
							$data_pnf = array (
								'nama_pnf'			=> $nama_pnf[$l],
								'tanggal_masuk_pnf'	=> $this->formatter->getDateFormatDb($tanggal_masuk_pnf[$l]),
								'sertifikat_pnf'	=> $sertifikat_pnf[$l],
								'nama_lembaga_pnf'	=> $nama_lembaga_pnf[$l],
								'alamat_pnf'		=> $alamat_pnf[$l],
								'keterangan_pnf'	=> $keterangan_pnf[$l],
								'nik'				=> $data['nik']
							);
							$data_pnf=array_merge($data_pnf,$this->model_global->getCreateProperties($this->admin));
							$this->model_global->insertQuery($data_pnf,'karyawan_pnf');
							}
						}
						$nama_penghargaan 		= $this->input->post('nama_penghargaan');
						$tahun 					= $this->input->post('tahun');
						$peringkat				= $this->input->post('peringkat');
						$yg_menetapkan			= $this->input->post('yg_menetapkan');
						$penyelenggara			= $this->input->post('penyelenggara');
						$keterangan 			= $this->input->post('keterangan');
						if ($nama_penghargaan !=NULL){
							for ($m = 1; $m <=count($nama_penghargaan) ; $m++) {
							$data_penghargaan = array (
								'nama_penghargaan'	=> $nama_penghargaan[$m],
								'tahun'			=> $this->formatter->getDateFormatDb($tahun[$m]),
								'peringkat'			=> $peringkat[$m],
								'yg_menetapkan'		=> $yg_menetapkan[$m],
								'penyelenggara'		=> $penyelenggara[$m],
								'keterangan'		=> $keterangan[$m],
								'nik'				=> $data['nik']  
							);
							$data_penghargaan=array_merge($data_penghargaan,$this->model_global->getCreateProperties($this->admin));
							$this->model_global->insertQuery($data_penghargaan,'karyawan_penghargaan');
							}
						}
						$nama_organisasi		= $this->input->post('nama_organisasi');
						$tahun_masuk 			= $this->input->post('tahun_masuk');
						$tahun_keluar 			= $this->input->post('tahun_keluar');
						$jabatan 				= $this->input->post('jabatan_org');
						if ($nama_organisasi !=NULL){
							for ($n = 1; $n <=count($nama_organisasi) ; $n++) {
							$data_organisasi = array (
								'nama_organisasi'	=> $nama_organisasi[$n],
								'tahun_masuk'		=> $this->formatter->getDateFormatDb($tahun_masuk[$n]),
								'tahun_keluar'		=> $this->formatter->getDateFormatDb($tahun_keluar[$n]),
								'jabatan_org'		=> $jabatan[$n],
								'nik'				=> $data['nik']
							);
							$data_organisasi=array_merge($data_organisasi,$this->model_global->getCreateProperties($this->admin));
							$this->model_global->insertQuery($data_organisasi,'karyawan_organisasi');
							}
						}
						$bahasa 		= $this->input->post('bahasa');
						$membaca		= $this->input->post('membaca');
						$menulis		= $this->input->post('menulis');
						$berbicara		= $this->input->post('berbicara');
						$mendengar		= $this->input->post('mendengar');
						if  ($bahasa != NULL){
							for ($o = 1; $o <=count($bahasa) ; $o++) {
							$data_bahasa=array(
								'bahasa'	=> $bahasa[$o],
								'membaca'	=> $membaca[$o],
								'menulis'	=> $menulis[$o],
								'berbicara'	=> $berbicara[$o],
								'mendengar'	=> $mendengar[$o],
								'nik'		=> $data['nik']
							);
							$data_bahasa=array_merge($data_bahasa,$this->model_global->getCreateProperties($this->admin));
							$this->model_global->insertQuery($data_bahasa,'karyawan_bahasa');
							}
						}
						$datas1 = array( 'nik'=> $this->input->post('nik'),
									'no_sk_baru'=>$this->input->post('no_sk_baru'),
									'tgl_sk_baru'=>$data['tgl_masuk'],
									'tgl_berlaku_baru'=>$data['tgl_masuk'],
									'berlaku_sampai_baru'=>$this->formatter->getDateFormatDb($this->input->post('berlaku_sampai')),
									'status_baru'=>$this->input->post('status_perjanjian'),
										);
						$datas1=array_merge($datas1,$this->model_global->getCreateProperties($this->admin));
						$this->model_global->insertQueryCC($datas1,'perjanjian_kerja',$this->model_karyawan->checkPeringatanCode($datas1['no_sk_baru']));
					$pendidikan_max = $this->model_karyawan->pendidikan_max($data['nik']);
					$data['pendidikan']=$pendidikan_max['jenjang_pendidikan'];
					$data['universitas']=$pendidikan_max['nama_sekolah'];
					$data['jurusan']=$pendidikan_max['jurusan'];
				$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
				$datax = $this->model_global->insertQuery($data,'karyawan');
			}else{
				$datax=$this->messages->notValidParam();
			}
		echo json_encode($datax);
	}
	function generate_nik(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$tgl_masuk = $this->input->post('tgl_masuk');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$nik=$this->codegenerator->nikJkb($tgl_masuk,$tgl_lahir);
		$cek=$this->model_karyawan->checkNik($nik);
		if ($cek) {
			$msg='<i class="fa fa-times"></i> NIK sudah dipakai';
		}else{
			$msg='<i class="fa fa-check"></i> NIK Tersedia';
		}
		$data=['msg'=>$msg,'nik'=>$nik,'status_data'=>$cek];
		echo json_encode($data);
	}
	function export_employee(){
		$ex=1;
		if ($ex == "") {
			$this->messages->notValidParam();  
			redirect('pages/employee');
		}else{ 
	            $objPHPExcel = new PHPExcel();
				// Set document properties
				$objPHPExcel->getProperties()->setCreator("Galeh Fatma Eko A")
											 ->setLastModifiedBy("Galeh Fatma Eko A")
											 ->setTitle("Template Data Karyawan")
											 ->setSubject("Template Data Karyawan")
											 ->setDescription("Template Data Karyawan")
											 ->setKeywords("Template Data Karyawan")
											 ->setCategory("Template Data Karyawan");
				// Add some data
	            $tri=1;
				for ($chrf='A'; $chrf!="AAA"; $chrf++){
				 	$huruf[$tri]=$chrf;
				 	$tri++;
				}
				$styleArray = array(
				      'borders' => array(
				          'allborders' => array(
				              'style' => PHPExcel_Style_Border::BORDER_THIN
				          )
				      )
				  );
				$objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('A1:BA1')->applyFromArray(
		                array(
		                    'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array('rgb' => '15ff00')
		                    ),
		                    'font' => array(
		                        'color' => array('rgb' => '000000')
		                    )
		                )
		            );	
				for ($i=1; $i <=53 ; $i++) { 
					$objPHPExcel->getActiveSheet(0)
							->getColumnDimension($huruf[$i])
							->setAutoSize(true);
				}
		        			
				$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A1', 'NIK')
							->setCellValue('B1', 'Nama Karyawan')
							->setCellValue('C1', 'Tempat Lahir')
							->setCellValue('D1', 'Tanggal Lahir (yyyy-mm-dd)')
							->setCellValue('E1', 'Nomor Ponsel')
							->setCellValue('F1', 'Status Pajak')
							->setCellValue('G1', 'Agama')
							->setCellValue('H1', 'Jenis Kelamin (Pria / Wanita)')
							->setCellValue('I1', 'Kode Jabatan (Lihat Master Jabatan)')
							->setCellValue('J1', 'Kode Lokasi Kerja (Lihat Master Lokasi Kerja)')
							->setCellValue('K1', 'Kode Grade (Lihat Master Grade)')
							->setCellValue('L1', 'Nomor NPWP')
							->setCellValue('M1', 'Email')
							->setCellValue('N1', 'Golongan Darah')
							->setCellValue('O1', 'Nomor Rekening')
							->setCellValue('P1', 'Nomor BPJS-KES')
							->setCellValue('Q1', 'Nomor BPJS-TK')
							->setCellValue('R1', 'Nomor KTP')
							->setCellValue('S1', 'Berat Badan')
							->setCellValue('T1', 'Tinggi Badan')
							->setCellValue('U1', 'Pendidikan Terakhir')
							->setCellValue('V1', 'Universitas')
							->setCellValue('W1', 'Jurusan')
							->setCellValue('X1', 'Kode Status Karyawan (Master Status Karyawan)')
							->setCellValue('Y1', 'Tanggal Masuk (yyyy-mm-dd)')
							->setCellValue('Z1', 'Alamat Asal (Jalan)')
							->setCellValue('AA1', 'Alamat Asal (Desa)')
							->setCellValue('AB1', 'Alamat Asal (Kecamatan)')
							->setCellValue('AC1', 'Alamat Asal (Kabupaten/Kota)')
							->setCellValue('AD1', 'Alamat Asal (Provinsi)')
							->setCellValue('AE1', 'Alamat Asal (Kode Pos)')
							->setCellValue('AF1', 'Alamat Sekarang (Jalan)')
							->setCellValue('AG1', 'Alamat Sekarang (Desa)')
							->setCellValue('AH1', 'Alamat Sekarang (Kecamatan)')
							->setCellValue('AI1', 'Alamat Sekarang (Kabupaten/Kota)')
							->setCellValue('AJ1', 'Alamat Sekarang (Provinsi)')
							->setCellValue('AK1', 'Alamat Sekarang (Kode Pos)')
							->setCellValue('AL1', 'Nama Ibu Kandung')
							->setCellValue('AM1', 'Nama Ayah Kandung')
							->setCellValue('AN1', 'Nomor Ponsel Ibu')
							->setCellValue('AO1', 'Nomor Ponsel Ayah')
							->setCellValue('AP1', 'Status Nikah')
							->setCellValue('AQ1', 'Nama Pasangan')
							->setCellValue('AR1', 'Tanggal Lahir Pasangan (yyyy-mm-dd)')
							->setCellValue('AS1', 'Nomor Ponsel Pasangan')
							->setCellValue('AT1', 'Jumlah Anak')
							->setCellValue('AU1', 'Nama Anak Pertama')
							->setCellValue('AV1', 'Tanggal Lahir Anak Pertama (yyyy-mm-dd)')
							->setCellValue('AW1', 'Nama Anak Kedua')
							->setCellValue('AX1', 'Tanggal Lahir Anak Kedua (yyyy-mm-dd)')
							->setCellValue('AY1', 'Nama Anak Ketiga')
							->setCellValue('AZ1', 'Tanggal Lahir Anak Ketiga (yyyy-mm-dd)')
							->setCellValue('BA1', 'Fingerprint Code');
				$objPHPExcel->getActiveSheet()->setTitle('Data Template Karyawan');
				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);
				// Redirect output to a client’s web browser (Excel5)
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="Template Karyawan.xls"');
				header('Cache-Control: max-age=0');
				// If you're serving to IE 9, then the following may be needed
				header('Cache-Control: max-age=1');
				// If you're serving to IE over SSL, then the following may be needed
				header ('Expires: '.date('D, d M Y H:i:s',strtotime($this->date)).' GMT'); // Date in the past
				header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
				header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
				header ('Pragma: public'); // HTTP/1.0
				$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save('php://output');
				exit;
			redirect('pages/employee');	
		}
	}
	function import(){
		$fileName = $this->input->post('file', TRUE);

		$config['upload_path'] = './asset/upload-exel/'; 
		$config['file_name'] = $fileName;
		$config['allowed_types'] = 'xls|xlsx|csv|ods|ots';

		$this->load->library('upload', $config);
		$this->upload->initialize($config); 
		
		if (!$this->upload->do_upload('file')) {
			$this->messages->customFailure($this->upload->display_errors());
			redirect('pages/employee'); 
		} else {
			$media = $this->upload->data();
			$inputFileName = './asset/upload-exel/'.$media['file_name'];
			
			try {
				$inputFileType = IOFactory::identify($inputFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch(Exception $e) {
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = ($sheet->getHighestRow());
			$highestColumn = $sheet->getHighestColumn();
			for ($row = 2; $row <= $highestRow; $row++){  
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
					NULL,
					TRUE,
					FALSE);
				$cek=$this->model_karyawan->emp_nik($rowData[0][0]);
				if ($rowData[0][0] != "") {
					$data = array(
						"nik"=> $rowData[0][0],
						"nama"=> $rowData[0][1],
						"tempat_lahir"=> $rowData[0][2],
						"tgl_lahir"=> PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][3],  'YYYY-MM-DD'),
						"no_hp"=> $rowData[0][4],
						"status_pajak"=> $rowData[0][5],
						"agama"=> $rowData[0][6],
						"kelamin"=> $rowData[0][7],
						"jabatan"=> $rowData[0][8],
						"unit"=> $rowData[0][9],
						"grade"=> $rowData[0][10],
						"npwp"=> $rowData[0][11],
						"email"=> $rowData[0][12],
						"gol_darah"=> $rowData[0][13],
						"rekening"=> $rowData[0][14],
						"bpjskes"=> $rowData[0][15],
						"bpjstk"=> $rowData[0][16],
						"no_ktp"=> $rowData[0][17],
						"berat_badan"=> $rowData[0][18], 
						"tinggi_badan"=> $rowData[0][19],
						"pendidikan"=> $rowData[0][20],
						"universitas"=> $rowData[0][21],
						"jurusan"=> $rowData[0][22],
						"status_karyawan"=> $rowData[0][23],
						"tgl_masuk"=> PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][24],  'YYYY-MM-DD'),
						"alamat_asal_jalan"=> $rowData[0][25],
						"alamat_asal_desa"=> $rowData[0][26],
						"alamat_asal_kecamatan"=> $rowData[0][27],
						"alamat_asal_kabupaten"=> $rowData[0][28],
						"alamat_asal_provinsi"=> $rowData[0][29],
						"alamat_asal_pos"=> $rowData[0][30],
						"alamat_sekarang_jalan"=> $rowData[0][31],
						"alamat_sekarang_desa"=> $rowData[0][32],
						"alamat_sekarang_kecamatan"=> $rowData[0][33],
						"alamat_sekarang_kabupaten"=> $rowData[0][34],
						"alamat_sekarang_provinsi"=> $rowData[0][35],
						"alamat_sekarang_pos"=> $rowData[0][36],
						"ibu_kandung"=> $rowData[0][37],
						"ayah_kandung"=> $rowData[0][38],
						"no_hp_ibu"=> $rowData[0][39],
						"no_hp_ayah"=> $rowData[0][40],
						"status_nikah"=> $rowData[0][41],
						"nama_pasangan"=> $rowData[0][42],
						"ttl_pasangan"=> PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][43],  'YYYY-MM-DD'),
						"no_hp_pasangan"=> $rowData[0][44],
						"jumlah_anak"=> $rowData[0][45],
						"anak_1"=> $rowData[0][46],
						"ttl_anak_1"=> PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][47],  'YYYY-MM-DD'),
						"anak_2"=> $rowData[0][48],
						"ttl_anak_2"=> PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][49],  'YYYY-MM-DD'),
						"anak_3"=> $rowData[0][50],
						"ttl_anak_3"=> PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][51],  'YYYY-MM-DD'),
						"finger_code"=> $rowData[0][52],
						"foto"=> 'asset/img/user-photo/user.png',
						"create_date"=> $this->date,
						"update_date"=> $this->date,
						"password"=> hash("sha512", "123456"),
					);


					if (count($cek) == 0) {
						$this->db->insert("karyawan",$data);
						$ls1[]=$rowData[0][0];
					}else{
						$ls[]=$rowData[0][0];
					}
				}
				
			} 
			if (isset($ls)) {
				if (count($ls) > 0 && isset($ls1)) {
					$this->messages->customWarning('NIK '.implode(', ', $ls).' sudah ada di database, beberapa data karyawan sudah masuk');
				}else{
					$this->messages->customFailure('NIK '.implode(', ', $ls).' sudah ada di database');
				}
			}else {
				$this->messages->allGood();
			}
			redirect('pages/employee');
		} 
	}
	function res_foto(){
		$id=$this->input->post('nik');
		if ($id == ""){
			$this->messages->notValidParam();  
			redirect('pages/employee');
		}else{
			$kl=$this->input->post('kelamin');
			if ($kl == "l") {
				$data=array('foto'=>'asset/img/user-photo/user.png','update_date'=>$this->date);
			}else{
				$data=array('foto'=>'asset/img/user-photo/userf.png','update_date'=>$this->date);
			}
			$this->db->where('nik',$id);
			$in=$this->db->update('karyawan',$data);
			if ($in) {
				$this->session->set_flashdata('msgres_fs','<label><i class="fa fa-check-circle"></i> Reset Foto Berhasil</label><hr class="message-inner-separator">Foto Profile berhasil direset ke foto default'); 
			}else{
				$this->session->set_flashdata('msgres_fe','<label><i class="fa fa-times-circle"></i> Reset Foto Gagal</label><hr class="message-inner-separator">Foto Profile gagal direset ke foto default'); 
			}
	   		redirect('pages/view_employee/'.$id);
		}
	}
	function up_pass(){
		$nik=$this->input->post('nik');
		if ($nik == "") {
			$this->messages->notValidParam();  
			redirect('pages/employee');
		}else{
			$pas=array(
				'old_pass'=>hash("sha512", $this->input->post('old_pass')),
				'password'=>hash("sha512", $this->input->post('password2')),
			);
			$ck=$this->model_karyawan->emp_nik($nik);
			if ($ck['password'] == $pas['password']) {
				$this->session->set_flashdata('pass_same','<label><i class="fa fa-times-circle"></i> Update Password Gagal</label><hr class="message-inner-separator">Password baru yang anda masukkan sama dengan password lama'); 
				redirect('pages/view_employee/'.$nik);
			}else{
				if ($ck['password'] == $pas['old_pass']) {
					$data=array('password'=>$pas['password'],'update_date'=>$this->date);
					$this->db->where('nik',$nik); 
					$in=$this->db->update('karyawan',$data);
					if ($in) {
						$this->session->set_flashdata('pass_sc','<label><i class="fa fa-check-circle"></i> Update Password Berhasil</label><hr class="message-inner-separator">Password Berhasil diupdate ke password yang baru'); 
					}else{
						$this->session->set_flashdata('pass_err','<label><i class="fa fa-times-circle"></i> Update Password Gagal</label><hr class="message-inner-separator">Password Gagal diupdate ke password yang baru'); 
					}
					redirect('pages/view_employee/'.$nik);
				}else{
					$this->session->set_flashdata('pass_nsame','<label><i class="fa fa-times-circle"></i> Update Password Gagal</label><hr class="message-inner-separator">Password Lama Tidak Cocok'); 
					redirect('pages/view_employee/'.$nik);
				}
			}
		}
	}
	function del_log(){
		$nik=$this->input->post('nik');
		$nik1=$this->model_karyawan->emp($nik);
		if ($nik == "") {
			$this->messages->notValidParam();  
			redirect('pages/employee');
		}else{
			$this->db->where('id_karyawan',$nik);
			$in=$this->db->delete('log_login_karyawan');
			if ($in) {
				$this->session->set_flashdata('dlog_sc','<label><i class="fa fa-check-circle"></i> Hapus Riwayat Login Berhasil</label><hr class="message-inner-separator">Semua Riwayat Login Karyawan ini berhasil dihapus'); 
			}else{
				$this->session->set_flashdata('dlog_err','<label><i class="fa fa-times-circle"></i> Hapus Riwayat Login Gagal</label><hr class="message-inner-separator">Semua Riwayat Login Karyawan ini Gagal dihapus'); 
			}
			redirect('pages/view_employee/'.$nik1['nik']);
		}
	}
	function del_employee(){
		$kode=$this->input->post('id');
		if ($kode != "") {
			$this->db->where('id_karyawan',$kode);
			$in=$this->db->delete('karyawan');
			if ($in) {
				$this->messages->delGood(); 
			}else{
				$this->messages->delFailure();
			}
		}else{
			$this->messages->notValidParam();  
		}
		redirect('pages/employee');
	}
//            ____  ____    __  __                ||
//           / _  |/ _  \  / / / /                ||
//=======   / /_/ / /_) / / / / /      ===========||
//         / __  / /__) \/ /_/ /                  ||
//        /_/ /_/_______/\____/                   ||
 
	public function employee()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getEmployeeAllActive();
				$access=$this->codegenerator->decryptChar($this->input->post('access'));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					unset($access['l_ac']['stt']);
					$var=[
						'id'=>$d->id_karyawan,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_karyawan,
						$d->nik,
						$d->nama,
						($d->nama_jabatan != null) ? $d->nama_jabatan : $this->otherfunctions->getMark(),
						($d->nama_loker != null) ? $d->nama_loker : $this->otherfunctions->getMark(),
						($d->nama_grade != null) ? $d->nama_grade.' ('.$d->nama_loker_grade.')': $this->otherfunctions->getMark(),
						($d->email != null) ? $d->email : $this->otherfunctions->getMark(),
						($d->tgl_masuk != null) ? $this->formatter->getDateMonthFormatUser($d->tgl_masuk) : $this->otherfunctions->getMark(),
						$properties['tanggal'],
						($d->status != null && $d->status == 1) ? '<i class="fa fa-circle scc" title="Online"></i>' : '<i class="fa fa-circle text-muted" title="Offline"></i>',
						$properties['aksi'],
						$this->codegenerator->encryptChar($d->nik),
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_karyawan');
				$d=$this->model_karyawan->getEmployeeId($id);
				$datax=[
					'id'=>$d['id_karyawan'],
					'nama'=>$d['nama'],
					'nik'=>$d['nik'],
					'loker'=>$d['loker'],
					'foto'=>base_url($d['foto']),
					'nama_loker'=>$d['nama_loker'],
					'grade'=>(isset($d['grade'])) ? $d['nama_grade'].' ('.$d['nama_loker_grade'].')':$this->otherfunctions->getMark(),
					'tgl_masuk'=>$d['tgl_masuk'],
					'gettgl_masuk'=>$this->formatter->getDateMonthFormatUser($d['tgl_masuk']),
					'jabatan'=>$d['jabatan'],
					'nama_jabatan'=>(isset($d['nama_jabatan'])) ? $d['nama_jabatan']:$this->otherfunctions->getMark(),
					'status'=>$d['status'],
					'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d['create_date']),
					'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d['update_date']),
					'create_by'=>$d['create_by'],
					'update_by'=>$d['update_by'],
					'nama_buat'=>(!empty($d['nama_buat'])) ? $d['nama_buat']:$this->otherfunctions->getMark($d['nama_buat']),
					'nama_update'=>(!empty($d['nama_update']))?$d['nama_update']:$this->otherfunctions->getMark($d['nama_update'])
				];
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	} 
	//**VIEW info pribadi **//
	public function emp_part_info()
	{
			$nik=$this->uri->segment(3);
			$kar=$this->model_karyawan->getEmployeeNik($nik);
			$jum_anak = $this->db->get_where('karyawan_anak',array('nik'=>$nik))->num_rows(); 
			$jum_pen = $this->model_karyawan->countPendidikan($nik);
			$max=$this->model_karyawan->pendidikan_max($nik);
			$data=array(
				'profile'=>$kar,
				'jumlah_anak'=>(!empty($jum_anak) || $jum_anak != 0) ? $jum_anak: $this->otherfunctions->getCustomMark($jum_anak,'<label class="label label-danger">Belum Punya Anak</label>'),
				'jml_pend'=>$jum_pen,
				'pendidikan_max'=>$max,
			);
		$datax = $this->load->view('_partial/_view_employee_info',$data);
		echo json_encode($datax);
	}
	//** partial employee **//
	public function emppribadi()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
		$nik = $this->input->post('nik');
		$data=$this->model_karyawan->getEmployeeOneNik($nik);
		foreach ($data as $d) {
			$jum_anak = $this->db->get_where('karyawan_anak',array('nik'=>$nik))->num_rows();
			$datax=[
				'id_karyawan'=>$d->id_karyawan,
				'nik'=>$d->nik,
				'finger_code'=>$d->id_finger,
				'no_ktp'=>$d->no_ktp,
				'nama'=>$d->nama,
				'grade'=>$d->nama_grade.' ('.$d->nama_loker_grade.')',
				'no_hp'=>$d->no_hp,
				'foto'=>$d->foto,
				'email'=>$d->email,
				'kodekelamin'=>$d->kelamin,
				// 'kelamin'=>$this->otherfunctions->getGender($d->kelamin),
				// 'darah'=>$this->otherfunctions->getBlood($d->gol_darah),
				// 'agama'=>$this->otherfunctions->getReligion($d->agama),
				'status_karyawan'=>$d->nama_status,
				'tempat_lahir'=>$d->tempat_lahir,
				'tgl_lahir'=>$this->formatter->getDateMonthFormatUser($d->tgl_lahir),
				'update_date'=>$this->formatter->getDateTimeFormatUser($d->update_date),
				'nama_loker'=>$d->nama_loker,
				'nama_jbt'=>$d->nama_jabatan,
				'nama_bgn'=>$d->nama_bagian,
				'nama_lvl'=>$d->nama_level_jabatan,
				// 'alamat_asal_jalan'=>$d->alamat_asal_jalan,
				// 'alamat_asal_desa'=>$d->alamat_asal_desa,
				// 'alamat_asal_kecamatan'=>$d->alamat_asal_kecamatan,
				// 'alamat_asal_kabupaten'=>$d->alamat_asal_kabupaten,
				// 'alamat_asal_provinsi'=>$d->alamat_asal_provinsi,
				// 'alamat_asal_pos'=>$d->alamat_asal_pos,
				// 'alamat_sekarang_jalan'=>$d->alamat_sekarang_jalan,
				// 'alamat_sekarang_desa'=>$d->alamat_sekarang_desa,
				// 'alamat_sekarang_kecamatan'=>$d->alamat_sekarang_kecamatan,
				// 'alamat_sekarang_kabupaten'=>$d->alamat_sekarang_kabupaten,
				// 'alamat_sekarang_provinsi'=>$d->alamat_sekarang_provinsi,
				// 'alamat_sekarang_pos'=>$d->alamat_sekarang_pos,
				// 'gol_darah'=>$this->otherfunctions->getBlood($d->gol_darah),
				// 'tinggi_badan'=>$d->tinggi_badan,
				// 'berat_badan'=>$d->berat_badan,
				// 'npwp'=>$d->npwp,
				// 'bpjstk'=>$d->bpjstk,
				// 'bpjskes'=>$d->bpjskes,
				// 'rekening'=>$d->rekening,
				// 'status_nikah'=>$this->otherfunctions->getStatusNikah($d->status_nikah),
				// 'agama'=>$this->otherfunctions->getReligion($d->agama),
				// 'hp_ayah'=>(!empty($d->no_hp_ayah)) ? $d->no_hp_ayah:$this->otherfunctions->getCustomMark($d->no_hp_ayah,'<label class="label label-danger">Nomor Ponsel Ayah Tidak Ada</label>'),
				// 'nama_ayah'=>(!empty($d->nama_ayah)) ? $d->nama_ayah:$this->otherfunctions->getCustomMark($d->nama_ayah,'<label class="label label-danger">Data Ayah Kandung Tidak Ada</label>'),
				// 'hp_ibu'=>(!empty($d->no_hp_ibu)) ? $d->no_hp_ibu:$this->otherfunctions->getCustomMark($d->no_hp_ibu,'<label class="label label-danger">Nomor Ponsel Ibu Tidak Ada</label>'),
				// 'nama_ibu'=>(!empty($d->nama_ibu)) ? $d->nama_ibu:$this->otherfunctions->getCustomMark($d->nama_ibu,'<label class="label label-danger">Data Ibu Kandung Tidak Ada</label>'),
				// 'hp_pasangan'=>(!empty($d->no_hp_pasangan)) ? $d->no_hp_pasangan:$this->otherfunctions->getCustomMark($d->no_hp_pasangan,'<label class="label label-danger">Nomor Ponsel Pasangan Tidak Ada</label>'),
				// 'nama_pasangan'=>(!empty($d->nama_pasangan)) ? $d->nama_pasangan:$this->otherfunctions->getCustomMark($d->nama_pasangan,'<label class="label label-danger">Data Pasangan Tidak Ada</label>'),
				'jumlah_anak'=>(!empty($jum_anak) || $jum_anak != 0) ? $jum_anak: $this->otherfunctions->getCustomMark($jum_anak,'<label class="label label-danger">Belum Punya Anak</label>'),
			];
		}
		echo json_encode($datax);
	}
	public function emp_part_update()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			//'status'=>$this->model_master->getListStatusKaryawan(),
			'kelamin'=> $this->otherfunctions->getGenderList(),
			'nikah'	=> $this->otherfunctions->getStatusNikahList(),
			'darah'=> $this->otherfunctions->getBloodList(),
			'agama'=> $this->otherfunctions->getReligionList(),
			'pendidikan'=> $this->otherfunctions->getEducateList(),
			'bahasa'=>$this->otherfunctions->getBahasaList(),
			'status_pajak'=> $this->otherfunctions->getStatusPajakList(),
			'profile'=>$kar,
			'access'=>$this->access,
		);
		$this->load->view('_partial/_view_employee_update',$data);
	}

	//------------------------------------------------------------------------------------------------------//
	//**Update info pribadi **//
	public function emp_part_pribadi()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			//'status'=>$this->model_master->getListStatusKaryawan(),
			'kelamin'=> $this->otherfunctions->getGenderList(),
			'darah'=> $this->otherfunctions->getBloodList(),
			'nikah'	=> $this->otherfunctions->getStatusNikahList(),
			'agama'=> $this->otherfunctions->getReligionList(),
			'status_pajak'=> $this->otherfunctions->getStatusPajakList(),
			'profile'=>$kar,
		);
		$this->load->view('_partial/_view_employee_pribadi',$data);
	}
	public function edit_pribadi()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
	   	$id=$this->input->post('id_karyawan');
	   	if($id!=''){
	   		$data=array(
				'id_finger'=>$this->input->post('finger_code'),
				'no_ktp'=>$this->input->post('no_ktp'),
				'nama'=>strtoupper($this->input->post('nama')),
				'alamat_asal_jalan'=>$this->input->post('alamat_asal_jalan'),
				'alamat_asal_desa'=>$this->input->post('alamat_asal_desa'),
				'alamat_asal_kecamatan'=>$this->input->post('alamat_asal_kecamatan'),
				'alamat_asal_kabupaten'=>$this->input->post('alamat_asal_kabupaten'),
				'alamat_asal_provinsi'=>$this->input->post('alamat_asal_provinsi'),
				'alamat_asal_pos'=>$this->input->post('alamat_asal_pos'),
				'alamat_sekarang_jalan'=>$this->input->post('alamat_sekarang_jalan'),
				'alamat_sekarang_desa'=>$this->input->post('alamat_sekarang_desa'),
				'alamat_sekarang_kecamatan'=>$this->input->post('alamat_sekarang_kecamatan'),
				'alamat_sekarang_kabupaten'=>$this->input->post('alamat_sekarang_kabupaten'),
				'alamat_sekarang_provinsi'=>$this->input->post('alamat_sekarang_provinsi'),
				'alamat_sekarang_pos'=>$this->input->post('alamat_sekarang_pos'),
				'berat_badan'=>$this->input->post('berat'),
				'tinggi_badan'=>$this->input->post('tinggi'),
				'no_hp'=>$this->input->post('no_hp'),
				'npwp'=>$this->input->post('npwp'),
				'bpjstk'=>$this->input->post('bpjstk'),
				'bpjskes'=>$this->input->post('bpjskes'),
				'rekening'=>$this->input->post('rekening'),
				//'nama_bank'=>$this->input->post('nama_bank'),
				'email'=>$this->input->post('email'),
				//'status_karyawan'=>$this->input->post('status_karyawan'),
				'kelamin'=>$this->input->post('kelamin'),
				'agama'=>$this->input->post('agama'),
				'gol_darah'=>$this->input->post('gol_darah'),
				'status_nikah'=>$this->input->post('nikah'),
				'status_pajak'=>$this->input->post('status_pajak'),
				'tempat_lahir'=>$this->input->post('tempat_lahir'),
				'tgl_lahir'=>$this->formatter->getDateFormatDb($this->input->post('tgl_lahir')),
			);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'karyawan',['id_karyawan'=>$id]);
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
	  echo json_encode($datax);
	}
	public function emp_part_keluarga()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'access'=>$this->access,
			'profile'=>$kar,
			'pendidikan'=> $this->otherfunctions->getEducateList(),
		);
		$this->load->view('_partial/_view_employee_keluarga',$data);
	}
	public function emp_part_ayah()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'profile'=>$kar,
			'pendidikan'=> $this->otherfunctions->getEducateList(),
		);
		$this->load->view('_partial/_view_employee_ayah',$data);
	}
	public function edit_ayah()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
	   	$id=$this->input->post('id_karyawan');
	   	if($id!=''){
	   		$data=array(
				'nama_ayah'=>strtoupper($this->input->post('nama_ayah')),
				'tempat_lahir_ayah'=>$this->input->post('tempat_lahir_ayah'),
				'tanggal_lahir_ayah'=>$this->formatter->getDateFormatDb($this->input->post('tanggal_lahir_ayah')),
				'no_hp_ayah'=>$this->input->post('no_hp_ayah'),
				'pendidikan_terakhir_ayah'=>$this->input->post('pendidikan_terakhir_ayah'),
				'alamat_ayah'=>$this->input->post('alamat_ayah'),
				'desa_ayah'=>$this->input->post('desa_ayah'),
				'kecamatan_ayah'=>$this->input->post('kecamatan_ayah'),
				'kabupaten_ayah'=>$this->input->post('kabupaten_ayah'),
				'provinsi_ayah'=>$this->input->post('provinsi_ayah'),
				'kode_pos_ayah'=>$this->input->post('kode_pos_ayah'),
			);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'karyawan',['id_karyawan'=>$id]);
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
	  echo json_encode($datax);
	}
	public function emp_part_ibu()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'profile'=>$kar,
			'pendidikan'=> $this->otherfunctions->getEducateList(),
		);
		$this->load->view('_partial/_view_employee_ibu',$data);
	}
	public function edit_ibu()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
	   	$id=$this->input->post('id_karyawan');
	   	if($id!=''){
	   		$data=array(
				'nama_ibu'=>strtoupper($this->input->post('nama_ibu')),
				'tempat_lahir_ibu'=>$this->input->post('tempat_lahir_ibu'),
				'tanggal_lahir_ibu'=>$this->formatter->getDateFormatDb($this->input->post('tanggal_lahir_ibu')),
				'no_hp_ibu'=>$this->input->post('no_hp_ibu'),
				'pendidikan_terakhir_ibu'=>$this->input->post('pendidikan_terakhir_ibu'),
				'alamat_ibu'=>$this->input->post('alamat_ibu'),
				'desa_ibu'=>$this->input->post('desa_ibu'),
				'kecamatan_ibu'=>$this->input->post('kecamatan_ibu'),
				'kabupaten_ibu'=>$this->input->post('kabupaten_ibu'),
				'provinsi_ibu'=>$this->input->post('provinsi_ibu'),
				'kode_pos_ibu'=>$this->input->post('kode_pos_ibu'),
			);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'karyawan',['id_karyawan'=>$id]);
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
	  echo json_encode($datax);
	}
	public function emp_part_anak()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'access'=>$this->access,
			'profile'=>$kar,
			'pendidikan'=> $this->otherfunctions->getEducateList(),
			'kelamin'=> $this->otherfunctions->getGenderList(),
		);
		$this->load->view('_partial/_view_employee_anak',$data);
	}
	//--EMP ANAK--//
	public function emp_anak(){
	// if (!$this->input->is_ajax_request()) 
	// 	   redirect('not_found');
		$nik=$this->uri->segment(4);
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getListAnak($nik);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_anak,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];

					$properties=$this->otherfunctions->getPropertiesTable($var);
					$ttl = $d->tempat_lahir_anak.', '.$this->formatter->getDateMonthFormatUser($d->tanggal_lahir_anak);
					$gender = $this->otherfunctions->getGender($d->kelamin_anak);
					$educa = $this->otherfunctions->getEducate($d->pendidikan_anak);
					$aksi = '
						<button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_anak('.$d->id_anak.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button>
						<button type="button" class="btn btn-danger btn-sm"  href="javascript:void(0)" onclick="delete_anak('.$d->id_anak.')"><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i></button>';
					$datax['data'][]=[
						$d->id_anak,
						(!empty($d->nama_anak)) ? $d->nama_anak:$this->otherfunctions->getMark($d->nama_anak),
						(!empty($ttl)) ? $ttl:$this->otherfunctions->getMark($ttl),
						(!empty($gender)) ? $gender:$this->otherfunctions->getMark($gender),
						(!empty($educa)) ? $educa:$this->otherfunctions->getMark($educa),
						(!empty($d->no_telp)) ? $d->no_telp:$this->otherfunctions->getMark($d->no_telp),
						$aksi,
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_anak');
				$data=$this->model_karyawan->getAnak($id,$nik);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_anak,
						'nama_anak'=>$d->nama_anak,
						'nik'=>$d->nik,
						'kelamin_anak'=>$d->kelamin_anak,
						'getkelamin_anak'=>$this->otherfunctions->getGender($d->kelamin_anak),
						'tempat_lahir_anak'=>$d->tempat_lahir_anak,
						'tanggal_lahir_anak'=>$this->formatter->getDateFormatUser($d->tanggal_lahir_anak),
						'getTTL'=>$d->tempat_lahir_anak.', '.$this->formatter->getDateMonthFormatUser($d->tanggal_lahir_anak),
						'pendidikan_anak'=>$d->pendidikan_anak,
						'getPendidikan'=>$this->otherfunctions->getEducate($d->pendidikan_anak),
						'no_telp'=>$d->no_telp,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update)
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function add_anak()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
   	$nik=$this->input->post('nik');
   	if($nik!=''){
   		$data=array(
				'nik'=>$nik,
				'nama_anak'=>$this->input->post('nama_anak'),
				'kelamin_anak'=>$this->input->post('kelamin_anak'),
				'tempat_lahir_anak'=>$this->input->post('tempat_lahir_anak'),
				'tanggal_lahir_anak'=>$this->formatter->getDateFormatDb($this->input->post('tanggal_lahir_anak')),
				'pendidikan_anak'=>$this->input->post('pendidikan_anak'),
				'no_telp'=>$this->input->post('no_telp'),
   		);
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			$datax = $this->model_global->insertQuery($data,'karyawan_anak');
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
		echo json_encode($datax);
	}

	public function edit_anak()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
   	$nik=$this->input->post('nik');
   	$id=$this->input->post('id_anak');
   	if($nik!='' || $id!=''){
   		$data=array(
				'nama_anak'=>$this->input->post('nama_anak'),
				'kelamin_anak'=>$this->input->post('kelamin_anak'),
				'tempat_lahir_anak'=>$this->input->post('tempat_lahir_anak'),
				'tanggal_lahir_anak'=>$this->formatter->getDateFormatDb($this->input->post('tanggal_lahir_anak')),
				'pendidikan_anak'=>$this->input->post('pendidikan_anak'),
				'no_telp'=>$this->input->post('no_telp'),
   		);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$where = array('id_anak' => $id, 'nik' => $nik );
			$datax = $this->model_global->updateQuery($data,'karyawan_anak',$where);
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
		echo json_encode($datax);
	}
	public function emp_part_saudara()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'access'=>$this->access,
			'profile'=>$kar,
			'pendidikan'=> $this->otherfunctions->getEducateList(),
			'kelamin'=> $this->otherfunctions->getGenderList(),
		);
		$this->load->view('_partial/_view_employee_saudara',$data);
	}
	//** saudara **//
	public function empsaudara()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$nik=$this->uri->segment(4);
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->saudara($nik);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_saudara,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];

					$properties=$this->otherfunctions->getPropertiesTable($var);
					$ttl = $d->tempat_lahir_saudara.', '.$this->formatter->getDateMonthFormatUser($d->tanggal_lahir_saudara);
					$gender = $this->otherfunctions->getGender($d->jenis_kelamin_saudara);
					$educa = $this->otherfunctions->getEducate($d->pendidikan_saudara);
					$aksi = '
						<button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_saudara('.$d->id_saudara.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button>
						<button type="button" class="btn btn-danger btn-sm"  href="javascript:void(0)" onclick="delete_saudara('.$d->id_saudara.')"><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i></button>';
					$datax['data'][]=[
						$d->id_saudara,
						(!empty($d->nama_saudara)) ? $d->nama_saudara:$this->otherfunctions->getMark($d->nama_saudara),
						(!empty($ttl)) ? $ttl:$this->otherfunctions->getMark($ttl),
						(!empty($gender)) ? $gender:$this->otherfunctions->getMark($gender),
						(!empty($educa)) ? $educa:$this->otherfunctions->getMark($educa),
						(!empty($d->no_telp_saudara)) ? $d->no_telp_saudara:$this->otherfunctions->getMark($d->no_telp_saudara),
						$aksi
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_saudara');
				$data=$this->model_karyawan->getSaudara($id,$nik);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_saudara,
						'nama_saudara'=>$d->nama_saudara,
						'nik'=>$d->nik,
						'jenis_kelamin_saudara'=>$d->jenis_kelamin_saudara,
						'getkelamin_saudara'=>$this->otherfunctions->getGender($d->jenis_kelamin_saudara),
						'tempat_lahir_saudara'=>$d->tempat_lahir_saudara,
						'tanggal_lahir_saudara'=>$this->formatter->getDateFormatUser($d->tanggal_lahir_saudara),
						'getTTL'=>$d->tempat_lahir_saudara.', '.$this->formatter->getDateMonthFormatUser($d->tanggal_lahir_saudara),
						'pendidikan_saudara'=>$d->pendidikan_saudara,
						'getPendidikan'=>$this->otherfunctions->getEducate($d->pendidikan_saudara),
						'no_telp_saudara'=>$d->no_telp_saudara,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update)
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function add_saudara()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
   	$nik=$this->input->post('nik');
   	if($nik!=''){
   		$data=array(
				'nik'=>$nik,
				'nama_saudara'=>$this->input->post('nama_saudara'),
				'jenis_kelamin_saudara'=>$this->input->post('jenis_kelamin_saudara'),
				'tempat_lahir_saudara'=>$this->input->post('tempat_lahir_saudara'),
				'tanggal_lahir_saudara'=>$this->formatter->getDateFormatDb($this->input->post('tanggal_lahir_saudara')),
				'pendidikan_saudara'=>$this->input->post('pendidikan_saudara'),
				'no_telp_saudara'=>$this->input->post('no_telp_saudara'),
   		);
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			$datax = $this->model_global->insertQuery($data,'karyawan_saudara');
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
		echo json_encode($datax);
	}

	public function edit_saudara()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
   	$nik=$this->input->post('nik');
   	$id=$this->input->post('id_saudara');
   	if($nik!='' || $id!=''){
   		$data=array(
				'nama_saudara'=>$this->input->post('nama_saudara'),
				'jenis_kelamin_saudara'=>$this->input->post('kelamin_saudara'),
				'tempat_lahir_saudara'=>$this->input->post('tempat_lahir_saudara'),
				'tanggal_lahir_saudara'=>$this->formatter->getDateFormatDb($this->input->post('tanggal_lahir_saudara')),
				'pendidikan_saudara'=>$this->input->post('pendidikan_saudara'),
				'no_telp_saudara'=>$this->input->post('no_telp'),
   		);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$where = array('id_saudara' => $id, 'nik' => $nik );
			$datax = $this->model_global->updateQuery($data,'karyawan_saudara',$where);
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
		echo json_encode($datax);
	}
	public function emp_part_pasangan()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'profile'=>$kar,
			'pendidikan'=> $this->otherfunctions->getEducateList(),
		);
		$this->load->view('_partial/_view_employee_pasangan',$data);
	}
	public function edit_pasangan()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
	   	$id=$this->input->post('id_karyawan');
	   	if($id!=''){
	   		$data=array(
				'nama_pasangan'=>strtoupper($this->input->post('nama_pasangan')),
				'tempat_lahir_pasangan'=>$this->input->post('tempat_lahir_pasangan'),
				'tanggal_lahir_pasangan'=>$this->formatter->getDateFormatDb($this->input->post('tanggal_lahir_pasangan')),
				'no_hp_pasangan'=>$this->input->post('no_hp_pasangan'),
				'pendidikan_terakhir_pasangan'=>$this->input->post('pendidikan_terakhir_pasangan'),
				'alamat_pasangan'=>$this->input->post('alamat_pasangan'),
				'desa_pasangan'=>$this->input->post('desa_pasangan'),
				'kecamatan_pasangan'=>$this->input->post('kecamatan_pasangan'),
				'kabupaten_pasangan'=>$this->input->post('kabupaten_pasangan'),
				'provinsi_pasangan'=>$this->input->post('provinsi_pasangan'),
				'kode_pos_pasangan'=>$this->input->post('kode_pos_pasangan'),
			);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'karyawan',['id_karyawan'=>$id]);
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
	  echo json_encode($datax);
	}
	public function emp_part_pendidikan()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'access'=>$this->access,
			'profile'=>$kar,
			'pendidikan'=> $this->otherfunctions->getEducateList(),
		);
		$this->load->view('_partial/_view_employee_pendidikan',$data);
	}
	public function emp_part_formal()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'access'=>$this->access,
			'profile'=>$kar,
			'pendidikan'=> $this->otherfunctions->getEducateList(),
		);
		$this->load->view('_partial/_view_employee_formal',$data);
	}
	//** pendidikan formal **//
	public function emppendidikan()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$nik=$this->uri->segment(4);
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->pendidikan($nik);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_k_pendidikan,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];

					$properties=$this->otherfunctions->getPropertiesTable($var);
					$tgl_masuk = $this->formatter->getDateMonthFormatUser($d->tahun_masuk);
					$tgl_keluar = $this->formatter->getDateMonthFormatUser($d->tahun_keluar);
					$educa = $this->otherfunctions->getEducate($d->jenjang_pendidikan);
					$aksi = '
						<button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_formal('.$d->id_k_pendidikan.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button>
						<button type="button" class="btn btn-danger btn-sm"  href="javascript:void(0)" onclick="delete_formal('.$d->id_k_pendidikan.')"><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i></button>';
					$datax['data'][]=[
						$d->id_k_pendidikan,
						$educa,
						(!empty($d->nama_sekolah)) ? $d->nama_sekolah:$this->otherfunctions->getMark($d->nama_sekolah),
						(!empty($d->jurusan)) ? $d->jurusan:$this->otherfunctions->getMark($d->jurusan),
						$aksi,
						(!empty($d->fakultas)) ? $d->fakultas:$this->otherfunctions->getMark($d->fakultas),
						$tgl_masuk,
						$tgl_keluar,
						$d->alamat_sekolah
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_k_pendidikan');
				$data=$this->model_karyawan->getPendidikan($id,$nik);
				$maxJenjanng = $this->model_karyawan->pendidikan_max($nik);
				$mjjang = $this->otherfunctions->getEducate($maxJenjanng['jenjang_pendidikan']);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_k_pendidikan,
						'jenjang_pendidikan'=>$d->jenjang_pendidikan,
						'getjenjang_pendidikan'=>$this->otherfunctions->getEducate($d->jenjang_pendidikan),
						'nama_sekolah'=>$d->nama_sekolah,
						'jurusan'=>$d->jurusan,
						'fakultas'=>$d->fakultas,
						'tahun_masuk'=>$d->tahun_masuk,
						'gettahun_masuk'=>$this->formatter->getDateFormatUser($d->tahun_masuk),
						'getvtahun_masuk'=>$this->formatter->getDateMonthFormatUser($d->tahun_masuk),
						'tahun_keluar'=>$d->tahun_keluar,
						'gettahun_keluar'=>$this->formatter->getDateFormatUser($d->tahun_keluar),
						'getvtahun_keluar'=>$this->formatter->getDateMonthFormatUser($d->tahun_keluar),
						'alamat_sekolah'=>$d->alamat_sekolah,
						'nik'=>$d->nik,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'maxJenjang'=> (!empty($mjjang)) ? $mjjang: $this->otherfunctions->getCustomMark($mjjang,'<label class="label label-danger">Jenjang Tidak Ada</label>'),
						'maxSekolah'=> (!empty($maxJenjanng['nama_sekolah'])) ? $maxJenjanng['nama_sekolah']: $this->otherfunctions->getCustomMark($maxJenjanng['nama_sekolah'],'<label class="label label-danger">Universitas Tidak Ada</label>'),
						'MaxJurusan'=> (!empty($maxJenjanng['jurusan'])) ? $maxJenjanng['jurusan'] : $this->otherfunctions->getCustomMark($maxJenjanng['jurusan'],'<label class="label label-danger">Jurusan Tidak Ada</label>'),
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function add_formal()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
   	$nik=$this->input->post('nik');
   	if($nik!=''){
   		$data=array(
				'nik'=>$nik,
				'jenjang_pendidikan'=>$this->input->post('jenjang_pendidikan'),
				'nama_sekolah'=>$this->input->post('nama_sekolah'),
				'jurusan'=>$this->input->post('jurusan'),
				'fakultas'=>$this->input->post('fakultas'),
				'tahun_masuk'=>$this->formatter->getDateFormatDb($this->input->post('tahun_masuk')),
				'tahun_keluar'=>$this->formatter->getDateFormatDb($this->input->post('tahun_keluar')),
				'alamat_sekolah'=>$this->input->post('alamat_sekolah'),
   		);
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			$datax = $this->model_global->insertQuery($data,'karyawan_pendidikan');
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
		echo json_encode($datax);
	}
	public function edit_formal()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
   	$nik=$this->input->post('nik');
   	$id=$this->input->post('id_k_pendidikan');
   	if($nik!='' || $id!=''){
   		$data=array(
				'jenjang_pendidikan'=>$this->input->post('jenjang_pendidikan'),
				'nama_sekolah'=>$this->input->post('nama_sekolah'),
				'jurusan'=>$this->input->post('jurusan'),
				'fakultas'=>$this->input->post('fakultas'),
				'tahun_masuk'=>$this->formatter->getDateFormatDb($this->input->post('tahun_masuk')),
				'tahun_keluar'=>$this->formatter->getDateFormatDb($this->input->post('tahun_keluar')),
				'alamat_sekolah'=>$this->input->post('alamat_sekolah'),
   		);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$where = array('id_k_pendidikan' => $id, 'nik' => $nik );
			$datax = $this->model_global->updateQuery($data,'karyawan_pendidikan',$where);
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
		echo json_encode($datax);
	}

	//** pendidikan non formal **//
	public function emp_part_n_formal()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'profile'=>$kar,
			'access'=>$this->access,
		);
		$this->load->view('_partial/_view_employee_n_formal',$data);
	}
	public function emp_nonformal()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$nik=$this->uri->segment(4);
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->pnf($nik);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_k_pnf,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];

					$properties=$this->otherfunctions->getPropertiesTable($var);
					$tgl_masuk = $this->formatter->getDateMonthFormatUser($d->tanggal_masuk_pnf);
					$aksi = '
						<button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_nformal('.$d->id_k_pnf.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button>
						<button type="button" class="btn btn-danger btn-sm"  href="javascript:void(0)" onclick="delete_nformal('.$d->id_k_pnf.')"><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i></button>';
					$datax['data'][]=[
						$d->id_k_pnf,
						$d->nama_pnf,
						$d->sertifikat_pnf,
						$d->nama_lembaga_pnf,
						$aksi
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_k_pnf');
				$data=$this->model_karyawan->getPnf($id,$nik);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_k_pnf,
						'nama_pnf'=>$d->nama_pnf,
						'tanggal_masuk_pnf'=>$d->tanggal_masuk_pnf,
						'gettanggal_masuk_pnf'=>$this->formatter->getDateFormatUser($d->tanggal_masuk_pnf),
						'getvtanggal_masuk_pnf'=>$this->formatter->getDateMonthFormatUser($d->tanggal_masuk_pnf),
						'sertifikat_pnf'=>$d->sertifikat_pnf,
						'nama_lembaga_pnf'=>$d->nama_lembaga_pnf,
						'alamat_pnf'=>$d->alamat_pnf,
						'keterangan_pnf'=>$d->keterangan_pnf,
						'nik'=>$d->nik,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update)
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function add_nformal()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
   	$nik=$this->input->post('nik');
   	if($nik!=''){
   		$data=array(
				'nik'=>$nik,
				'nama_pnf'=>$this->input->post('nama_pnf'),
				'tanggal_masuk_pnf'=>$this->formatter->getDateFormatDb($this->input->post('tanggal_masuk_pnf')),
				'sertifikat_pnf'=>$this->input->post('sertifikat_pnf'),
				'nama_lembaga_pnf'=>$this->input->post('nama_lembaga_pnf'),
				'alamat_pnf'=>$this->input->post('alamat_pnf'),
				'keterangan_pnf'=>$this->input->post('keterangan_pnf'),
   		);
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			$datax = $this->model_global->insertQuery($data,'karyawan_pnf');
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
		echo json_encode($datax);
	}
	public function edit_nformal()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
   	$nik=$this->input->post('nik');
   	$id=$this->input->post('id_k_pnf');
   	if($nik!='' || $id!=''){
   		$data=array(
				'nama_pnf'=>$this->input->post('nama_pnf'),
				'tanggal_masuk_pnf'=>$this->formatter->getDateFormatDb($this->input->post('tanggal_masuk_pnf')),
				'sertifikat_pnf'=>$this->input->post('sertifikat_pnf'),
				'nama_lembaga_pnf'=>$this->input->post('nama_lembaga_pnf'),
				'alamat_pnf'=>$this->input->post('alamat_pnf'),
				'keterangan_pnf'=>$this->input->post('keterangan_pnf'),
   		);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$where = array('id_k_pnf' => $id, 'nik' => $nik );
			$datax = $this->model_global->updateQuery($data,'karyawan_pnf',$where);
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
		echo json_encode($datax);
	}
	public function emp_part_organisasi()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'profile'=>$kar,
			'access'=>$this->access,
		);
		$this->load->view('_partial/_view_employee_organisasi',$data);
	}
	//------------------------------------------------------------------------------------------------------//
	//** organisasi **//
	public function emp_org()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$nik=$this->uri->segment(4);
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->organisasi($nik);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_k_organisasi,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];

					$properties=$this->otherfunctions->getPropertiesTable($var);
					$tahun_masuk = $this->formatter->getDateMonthFormatUser($d->tahun_masuk);
					$tahun_keluar = $this->formatter->getDateMonthFormatUser($d->tahun_keluar);
					$aksi = '
						<button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_org('.$d->id_k_organisasi.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button>
						<button type="button" class="btn btn-danger btn-sm"  href="javascript:void(0)" onclick="delete_org('.$d->id_k_organisasi.')"><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i></button>';
					$datax['data'][]=[
						$d->id_k_organisasi,
						$d->nama_organisasi,
						$tahun_masuk,
						$tahun_keluar,
						$d->jabatan_org,
						$aksi
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_k_organisasi');
				$data=$this->model_karyawan->getOrganisasi($id,$nik);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_k_organisasi,
						'nama_organisasi'=>$d->nama_organisasi,
						'tahun_masuk'=>$d->tahun_masuk,
						'gettahun_masuk'=>$this->formatter->getDateFormatUser($d->tahun_masuk),
						'getvtahun_masuk'=>$this->formatter->getDateMonthFormatUser($d->tahun_masuk),
						'tahun_keluar'=>$d->tahun_keluar,
						'gettahun_keluar'=>$this->formatter->getDateFormatUser($d->tahun_keluar),
						'getvtahun_keluar'=>$this->formatter->getDateMonthFormatUser($d->tahun_keluar),
						'jabatan_org'=>$d->jabatan_org,
						'nik'=>$d->nik,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update)
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function add_org()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
   	$nik=$this->input->post('nik');
   	if($nik!=''){
   		$data=array(
				'nik'=>$nik,
				'nama_organisasi'=>$this->input->post('nama_organisasi'),
				'tahun_masuk'=>$this->formatter->getDateFormatDb($this->input->post('tahun_masuk')),
				'tahun_keluar'=>$this->formatter->getDateFormatDb($this->input->post('tahun_keluar')),
				'jabatan_org'=>$this->input->post('jabatan_org'),
   		);
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			$datax = $this->model_global->insertQuery($data,'karyawan_organisasi');
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
		echo json_encode($datax);
	}
	public function edit_org()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
   	$nik=$this->input->post('nik');
   	$id=$this->input->post('idorg');
   	if($nik!='' || $id!=''){
   		$data=array(
				'nama_organisasi'=>$this->input->post('nama_organisasi'),
				'tahun_masuk'=>$this->formatter->getDateFormatDb($this->input->post('tahun_masuk')),
				'tahun_keluar'=>$this->formatter->getDateFormatDb($this->input->post('tahun_keluar')),
				'jabatan_org'=>$this->input->post('jabatan_org'),
   		);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$where = array('id_k_organisasi' => $id, 'nik' => $nik );
			$datax = $this->model_global->updateQuery($data,'karyawan_organisasi',$where);
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
		echo json_encode($datax);
	}

//** Penghargaan **//
	public function emp_part_penghargaan()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'profile'=>$kar,
			'access'=>$this->access,
		);
		$this->load->view('_partial/_view_employee_penghargaan',$data);
	}
	public function emp_hrg()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$nik=$this->uri->segment(4);
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->penghargaan($nik);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_k_penghargaan,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];

					$properties=$this->otherfunctions->getPropertiesTable($var);
					$tanggal = $this->formatter->getDateMonthFormatUser($d->tahun);
					$aksi = '
						<button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_hrg('.$d->id_k_penghargaan.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button>
						<button type="button" class="btn btn-danger btn-sm"  href="javascript:void(0)" onclick="delete_hrg('.$d->id_k_penghargaan.')"><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i></button>';
					$datax['data'][]=[
						$d->id_k_penghargaan,
						$d->nama_penghargaan,
						$tanggal,
						$d->peringkat,
						$d->penyelenggara,
						$aksi
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_k_penghargaan');
				$data=$this->model_karyawan->getPenghargaan($id,$nik);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_k_penghargaan,
						'nama_penghargaan'=>$d->nama_penghargaan,
						'tanggalv'=>$this->formatter->getDateMonthFormatUser($d->tahun),
						'peringkat'=>$d->peringkat,
						'yg_menetapkan'=>$d->yg_menetapkan,
						'penyelenggara'=>$d->penyelenggara,
						'keterangan'=>$d->keterangan,
						'tanggal'=>$this->formatter->getDateFormatUser($d->tahun),
						'nik'=>$d->nik,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function add_hrg()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
	   	$nik=$this->input->post('nik');
	   	if($nik!=''){
	   		$data=array(
					'nik'=>$nik,
					'nama_penghargaan'=>$this->input->post('nama_penghargaan'),
					'tahun'=>$this->formatter->getDateFormatDb($this->input->post('tanggal')),
					'peringkat'=>$this->input->post('peringkat'),
					'yg_menetapkan'=>$this->input->post('yg_menetapkan'),
					'penyelenggara'=>$this->input->post('penyelenggara'),
					'keterangan'=>$this->input->post('keterangan'),
	   		);
				$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
				$datax = $this->model_global->insertQuery($data,'karyawan_penghargaan');
	   	}else{
				$datax = $this->messages->notValidParam(); 
			}
			echo json_encode($datax);
	}
	public function edit_hrg()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
	   	$nik=$this->input->post('nik');
	   	$id=$this->input->post('idhrg');
	   	if($nik!='' || $id!=''){
	   		$data=array(
					'nama_penghargaan'=>$this->input->post('nama_penghargaan'),
					'tahun'=>$this->formatter->getDateFormatDb($this->input->post('tanggal')),
					'peringkat'=>$this->input->post('peringkat'),
					'yg_menetapkan'=>$this->input->post('yg_menetapkan'),
					'penyelenggara'=>$this->input->post('penyelenggara'),
					'keterangan'=>$this->input->post('keterangan'),
	   		);
				$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
				$where = array('id_k_penghargaan' => $id, 'nik' => $nik );
				$datax = $this->model_global->updateQuery($data,'karyawan_penghargaan',$where);
	   	}else{
				$datax = $this->messages->notValidParam(); 
			}
			echo json_encode($datax);
	}
	public function emp_part_bahasa()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'profile'=>$kar,
			'access'=>$this->access,
			'bahasa'=>$this->otherfunctions->getBahasaList(),
			'radio'=>$this->otherfunctions->getRadioList(),
		);
		$this->load->view('_partial/_view_employee_bahasa',$data);
	}
	public function empbahasa()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$nik=$this->uri->segment(4);
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->bahasa($nik);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_k_bahasa,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];

					$properties=$this->otherfunctions->getPropertiesTable($var);
					$bahasa = $this->otherfunctions->getBahasa($d->bahasa);
					$membaca = $this->otherfunctions->getRadio($d->membaca);
					$menulis = $this->otherfunctions->getRadio($d->menulis);
					$berbicara = $this->otherfunctions->getRadio($d->berbicara);
					$mendengar = $this->otherfunctions->getRadio($d->mendengar);
					$aksi = '
						<button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_bahasa('.$d->id_k_bahasa.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button>
						<button type="button" class="btn btn-danger btn-sm"  href="javascript:void(0)" onclick="delete_bahasa('.$d->id_k_bahasa.')"><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i></button>';
					$datax['data'][]=[
						$d->id_k_bahasa,
						$bahasa,
						$membaca,
						$menulis,
						$berbicara,
						$mendengar,
						$aksi,
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_k_bahasa');
				$data=$this->model_karyawan->getBahasa($id,$nik);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_k_bahasa,
						'bahasa'=>$d->bahasa,
						'bahasav'=>$this->otherfunctions->getBahasa($d->bahasa),
						'membacav'=>$this->otherfunctions->getRadio($d->membaca),
						'menulisv'=>$this->otherfunctions->getRadio($d->menulis),
						'berbicarav'=>$this->otherfunctions->getRadio($d->berbicara),
						'mendengarv'=>$this->otherfunctions->getRadio($d->mendengar),
						'membaca'=>$d->membaca,
						'menulis'=>$d->menulis,
						'berbicara'=>$d->berbicara,
						'mendengar'=>$d->mendengar,
						'nik'=>$d->nik,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function add_bahasa()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
	   	$nik=$this->input->post('nik');
	   	if($nik!=''){
	   		$data=array(
					'nik'=>$nik,
					'bahasa'=>$this->input->post('bahasa'),
					'membaca'=>$this->input->post('membaca'),
					'menulis'=>$this->input->post('menulis'),
					'berbicara'=>$this->input->post('berbicara'),
					'mendengar'=>$this->input->post('mendengar'),
	   		);
				$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
				$datax = $this->model_global->insertQuery($data,'karyawan_bahasa');
	   	}else{
				$datax = $this->messages->notValidParam(); 
			}
			echo json_encode($datax);
	}
	public function edit_bahasa()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
	   	$nik=$this->input->post('nik');
	   	$id=$this->input->post('id_k_bahasa');
	   	if($nik!='' || $id!=''){
	   		$data=array(
					'bahasa'=>$this->input->post('bahasa2'),
					'membaca'=>$this->input->post('membaca2'),
					'menulis'=>$this->input->post('menulis2'),
					'berbicara'=>$this->input->post('berbicara2'),
					'mendengar'=>$this->input->post('mendengar2'),
	   		);
				$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
				$where = array('id_k_bahasa' => $id, 'nik' => $nik );
				$datax = $this->model_global->updateQuery($data,'karyawan_bahasa',$where);
	   	}else{
				$datax = $this->messages->notValidParam(); 
			}
			echo json_encode($datax);
	}
	//------------------------------------------------------------------------------------------------------//
	//** employee jabatan **//
	public function emp_part_jabatan()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'profile'=>$kar,
		);
		$this->load->view('_partial/_view_employee_jabatan',$data);
	}
	public function emp_part_jabatan_grade()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'grade') {
				$data = $this->model_master->getMasterGradeForSelect2();
        		echo json_encode($data);
        	}
        }
	}
	
	public function edit_jabatan()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
	   	$id=$this->input->post('id_karyawan');
		if($id!=''){
	   		$data=array(
					'jabatan'=>$this->input->post('jabatan'),
					'loker'=>$this->input->post('loker'),
					'grade'=>$this->input->post('grade'),
					'status_karyawan'=>$this->input->post('status_karyawan'),
	   		);
				$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
				$datax = $this->model_global->updateQuery($data,'karyawan',['id_karyawan'=>$id]);
	   	}else{
				$datax = $this->messages->notValidParam(); 
			}
		echo json_encode($datax);
	}

	//------------------------------------------------------------------------------------------------------//
	//** employee foto **//
	public function emp_part_foto()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'profile'=>$kar,
		);
		$this->load->view('_partial/_view_employee_foto',$data);
	}

	//------------------------------------------------------------------------------------------------------//
	//** employee password **//
	public function emp_part_pass()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'profile'=>$kar,
		);
		$this->load->view('_partial/_view_employee_pass',$data);
	}
	
	//------------------------------------------------------------------------------------------------------//
	//** employee riwayat pekerjaan **//
	public function emp_part_job()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'profile'=>$kar,
		);
		$this->load->view('_partial/_view_employee_job',$data);
	}
	
	//------------------------------------------------------------------------------------------------------//
	//** employee riwayat login **//
	public function emp_part_log()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$jum_log = $this->db->get_where('log_login_karyawan',array('id_karyawan'=>$kar['id_karyawan']))->num_rows(); 
		$data=array(
			'profile'=>$kar,
			'jumlah_log'=>$jum_log,
		);
		$this->load->view('_partial/_view_employee_log',$data);
	}

	public function emp_log()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$nik=$this->uri->segment(4);
		$usage=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);

		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->db->get_where('log_login_karyawan',array('id_karyawan'=>$kar['id_karyawan']))->result(); 
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_karyawan,
					];

					$lastlog = date("l, d F Y", strtotime($d->tgl_login)).' <i style="color:red;" class="fa fa-clock-o"></i> '.date("H:i:s", strtotime($d->tgl_login));
					$datax['data'][]=[
						$d->id_karyawan,
						$lastlog
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$jum_log = $this->db->get_where('log_login_karyawan',array('id_karyawan'=>$kar['id_karyawan']))->num_rows(); 
				$datax=[
						'jumlah_log'=>$jum_log,
					];
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	// function del_log(){

	// 	if (!$this->input->is_ajax_request()) 
	// 	   redirect('not_found');
	// 	$id=$this->input->post('id');
	// 	if (empty($id))
	// 		echo json_encode($this->messages->notValidParam());
	// 	$where=['id_karyawan'=>$id];
	// 	$datax=$this->model_global->deleteQuery('log_login_karyawan',$where);
	// 	echo json_encode($datax);

	// 	// $nik=$this->input->post('nik');
	// 	// $nik1=$this->model_karyawan->emp($nik);
	// 	// if ($nik == "") {
	// 	// 	$this->messages->notValidParam();  
	// 	// 	redirect('pages/employee');
	// 	// }else{
	// 	// 	$this->db->where('id_karyawan',$nik);
	// 	// 	$in=$this->db->delete('log_login_karyawan');
	// 	// 	if ($in) {
	// 	// 		$this->session->set_flashdata('dlog_sc','<label><i class="fa fa-check-circle"></i> Hapus Riwayat Login Berhasil</label><hr class="message-inner-separator">Semua Riwayat Login Karyawan ini berhasil dihapus'); 
	// 	// 	}else{
	// 	// 		$this->session->set_flashdata('dlog_err','<label><i class="fa fa-times-circle"></i> Hapus Riwayat Login Gagal</label><hr class="message-inner-separator">Semua Riwayat Login Karyawan ini Gagal dihapus'); 
	// 	// 	}
	// 	// 	redirect('pages/view_employee/'.$nik1['nik']);
	// 	// }
	// }
	//------------------------------------------------------------------------------------------------------//
	//** employee grade **//
	public function emp_part_grade()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$grade=$this->model_karyawan->emp_grade($kar['grade']);
		$data=array(
			'profile'=>$kar,
			'grade'=>$grade,
		);
		$this->load->view('_partial/_view_employee_grade',$data);
	}
	public function edt_grade()
	{
		if (!$this->input->is_ajax_request()) 
   		redirect('not_found');
   	$id=$this->input->post('id_karyawan');
   	if($id!=''){
   		$data=array(
				'grade'=>$this->input->post('grade'),
   		);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'karyawan',['id_karyawan'=>$id]);
   	}else{
			$datax = $this->messages->notValidParam(); 
		}
		echo json_encode($datax);
	}
	//------------------------------------------------------------------------------------------------------//
	//** employee riwayat pekerjaan non_aktif**//
	public function emp_part_work()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'profile'=>$kar,
			'access'=>$this->access,
		);
		$this->load->view('_partial/_view_employee_work',$data);
	}
	//------------------------------------------------------------------------------------------------------//
	//** employee data tambahan non_aktif**//
	public function emp_part_other()
	{
		$nik=$this->uri->segment(3);
		$kar=$this->model_karyawan->getEmployeeNik($nik);
		$data=array(
			'profile'=>$kar,
			'access'=>$this->access,
		);
		$this->load->view('_partial/_view_employee_other',$data);
	}

//MUTASI
	public function pilih_k_mutasi()
	{
		if (!$this->input->is_ajax_request())
			redirect('not_found');
		$data=$this->model_karyawan->getPilihKaryawanMutasi();
		$datax['data']=[];
		foreach ($data as $d) {
			$datax['data'][]=[
				$d->id_karyawan,
				'<a class="pilih" style="cursor:pointer" 
				data-nik			="'.$d->nik.'" 
				data-id_karyawan	="'.$d->id_karyawan.'" 
				data-nama			="'.$d->nama.'" 
				data-jabatan		="'.$d->jabatan.'" 
				data-nama_jabatan	="'.$d->nama_jabatan.'" 
				data-kode_lokasi	="'.$d->loker.'" 
				data-nama_lokasi	="'.$d->nama_loker.'">'.
				$d->nik.'</a>',
				$d->nama,
				$d->nama_jabatan,
				$d->nama_loker,
			];
		}
		echo json_encode($datax);		
	}
	public function mutasi_jabatan()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getListMutasi();
				$access=$this->codegenerator->decryptChar($this->input->post('access'));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_mutasi,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_mutasi,
						$d->nik_karyawan,
						$d->nama_karyawan,
						$d->no_sk,
						$this->formatter->getDateMonthFormatUser($d->tgl_sk),
						$d->nama_status,
						$d->nama_jabatan_baru,
						$d->nama_loker_baru,
						$d->jum,
						$properties['aksi'],
						$this->codegenerator->encryptChar($d->nik_karyawan),
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_mutasi');
				$data=$this->model_karyawan->getMutasi($id);
				foreach ($data as $d) {
					$tabel='';
					$tabel.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-blue">
          							<th>No.</th>
          							<th>Nama</th>
          							<th>NO SK</th>
          							<th>Tanggal SK</th>
          							<th>Status</th>
          							<th>Jabatan Baru</th>
          							<th>Lokasi Baru</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_mutasi=$this->model_karyawan->getListMutasiNik($d->nik_karyawan);
          						$no=1;
          						foreach ($data_mutasi as $d_m) {
          							$tabel.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_m->nama_karyawan.'</td>
          							<td>'.$d_m->no_sk.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_m->tgl_sk).'</td>
          							<td>'.$d_m->nama_status.'</td>
          							<td>'.$d_m->nama_jabatan_baru.'</td>
          							<td>'.$d_m->nama_loker_baru.'</td>
          						</tr>';
          						$no++;
          					}
	          				$tabel.='</tbody>
	          			</table>';
					$jbt_mengetahui=($d->jbt_mengetahui != null) ? ' - '.$d->jbt_mengetahui : null;
					$jbt_menyetujui=($d->jbt_menyetujui != null) ? ' - '.$d->jbt_menyetujui : null;
					$datax=[
						'tabel'=>$tabel,
						'loker'=>$d->nama_loker_baru,
						'jabatan'=>$d->nama_jabatan_baru,
						'id'=>$d->id_mutasi,
						'no_sk'=>$d->no_sk,
						'tgl_sk'=>$this->formatter->getDateMonthFormatUser($d->tgl_sk),
						'tgl_berlaku'=>$this->formatter->getDateMonthFormatUser($d->tgl_berlaku),
						'nik'=>$d->nik_karyawan,
						'nama'=>$d->nama_karyawan,
						'lokasi_asal'=>(!empty($d->lokasi_asal)) ? $d->nama_loker:$this->otherfunctions->getMark($d->lokasi_asal),
						'lokasi_baru'=>$d->lokasi_baru,
						'status_mutasi'=>$d->status_mutasi,
						'jabatan_lama'=>(!empty($d->jabatan_asal)) ? $d->nama_jabatan:$this->otherfunctions->getMark($d->jabatan_asal),
						'jabatan_baru'=>$d->jabatan_baru,
						'mengetahui'=>$this->model_karyawan->getEmployeeForSelect2(),
						'menyetujui'=>$this->model_karyawan->getEmployeeForSelect2(),
						'keterangan'=>$d->keterangan,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'tgl_sk_e'=>$this->formatter->getDateFormatUser($d->tgl_sk),
						'tgl_berlaku_e'=>$this->formatter->getDateFormatUser($d->tgl_berlaku),
						'vjabatan_lama'=>(!empty($d->jabatan_asal)) ? $d->nama_jabatan:null,
						'vlokasi_asal'=>(!empty($d->lokasi_asal)) ? $d->nama_loker:null,
						'vlokasi_baru'=>$d->nama_loker_baru,
						'vstatus_mutasi'=>$d->nama_status,
						'vjabatan_baru'=>$d->nama_jabatan_baru,
						'vmengetahui'=>(!empty($d->nama_mengetahui)) ? $d->nama_mengetahui.$jbt_mengetahui:$this->otherfunctions->getMark(),
						'vmenyetujui'=>(!empty($d->nama_menyetujui)) ? $d->nama_menyetujui.$jbt_menyetujui:$this->otherfunctions->getMark(),
						'vketerangan'=>(!empty($d->keterangan)) ? $d->keterangan:$this->otherfunctions->getMark($d->keterangan),
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'kode') {
				$data = $this->codegenerator->kodeSkMutasi();
        		echo json_encode($data);
			}elseif ($usage == 'employee') {
				$data = $this->model_karyawan->getEmployeeForSelect2();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function view_mutasi_jabatan()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getListMutasiNik($this->codegenerator->decryptChar($this->uri->segment(4)));
				$access=$this->codegenerator->decryptChar($this->input->post('access'));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_mutasi,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_mutasi,
						$d->nama_karyawan,
						$d->no_sk,
						$this->formatter->getDateMonthFormatUser($d->tgl_sk),
						$d->nama_status,
						$d->nama_jabatan_baru,
						$d->nama_loker_baru,
						$properties['tanggal'],
						$properties['status'],
						$properties['aksi']
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_mutasi');
				$data=$this->model_karyawan->getMutasi($id);
				foreach ($data as $d) {
					$jbt_mengetahui=($d->jbt_mengetahui != null) ? ' - '.$d->jbt_mengetahui : null;
					$jbt_menyetujui=($d->jbt_menyetujui != null) ? ' - '.$d->jbt_menyetujui : null;
					$datax=[
						'id'=>$d->id_mutasi,
						'id_karyawan'=>$d->id_karyawan,
						'no_sk'=>$d->no_sk,
						'tgl_sk'=>$this->formatter->getDateMonthFormatUser($d->tgl_sk),
						'tgl_berlaku'=>$this->formatter->getDateMonthFormatUser($d->tgl_berlaku),
						'nik'=>$d->nik_karyawan,
						'nama'=>$d->nama_karyawan,
						'lokasi_asal'=>(!empty($d->lokasi_asal)) ? $d->nama_loker:$this->otherfunctions->getMark(),
						'lokasi_baru'=>$d->lokasi_baru,
						'status_mutasi'=>$d->status_mutasi,
						'jabatan_lama'=>(!empty($d->jabatan_asal)) ? $d->nama_jabatan:$this->otherfunctions->getMark(),
						'jabatan_baru'=>$d->jabatan_baru,
						'percobaan'=>$d->lama_percobaan,
						'mengetahui'=>$d->mengetahui,
						'menyetujui'=>$d->menyetujui,
						'keterangan'=>$d->keterangan,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark(),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark(),
						'tgl_sk_e'=>$this->formatter->getDateFormatUser($d->tgl_sk),
						'tgl_berlaku_e'=>$this->formatter->getDateFormatUser($d->tgl_berlaku),
						'vjabatan_lama'=>(!empty($d->jabatan_asal)) ? $d->nama_jabatan:$this->otherfunctions->getMark(),
						'vlokasi_asal'=>(!empty($d->lokasi_asal)) ? $d->nama_loker:$this->otherfunctions->getMark(),
						'vlokasi_baru'=>$d->nama_loker_baru,
						'vstatus_mutasi'=>$d->nama_status,
						'vpercobaan'=>(isset($d->lama_percobaan)) ? $d->lama_percobaan.' Bulan' : $this->otherfunctions->getMark(),
						'vjabatan_baru'=>$d->nama_jabatan_baru,
						'vmengetahui'=>(!empty($d->nama_mengetahui)) ? $d->nama_mengetahui.$jbt_mengetahui:$this->otherfunctions->getMark(),
						'vmenyetujui'=>(!empty($d->nama_menyetujui)) ? $d->nama_menyetujui.$jbt_menyetujui:$this->otherfunctions->getMark(),
						'vketerangan'=>(!empty($d->keterangan)) ? $d->keterangan:$this->otherfunctions->getMark($d->keterangan),
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'employee') {
				$data = $this->model_karyawan->getEmployeeForSelect2();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function edit_mutasi(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$id_karyawan=$this->input->post('id_karyawan');
		if ($id != "") {
			$data=array(
				'no_sk'=>strtoupper($this->input->post('no_sk')),
				'tgl_sk'=>$this->formatter->getDateFormatDb($this->input->post('tgl_sk')),
				'tgl_berlaku'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku')),
				'status_mutasi'=>$this->input->post('status_mutasi'),
				'jabatan_baru'=>$this->input->post('jabatan_baru'),
				'lokasi_baru'=>$this->input->post('lokasi_baru'),
				'mengetahui'=>$this->input->post('mengetahui'),
				'menyetujui'=>$this->input->post('menyetujui'),
				'lama_percobaan'=>$this->input->post('lama_percobaan'),
				'keterangan'=>$this->input->post('keterangan'),
			);
				$data_kar=array('loker'=>$this->input->post('lokasi_baru'),'jabatan'=>$this->input->post('jabatan_baru'),);
				$where = array('id_karyawan' => $id_karyawan );
				$this->model_global->updateQuery($data_kar,'karyawan',$where);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'mutasi_jabatan',['id_mutasi'=>$id]);
		}else{
			$datax = $this->messages->notValidParam(); 
		}
	   	echo json_encode($datax);
	}
	function add_mutasi(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$kode=$this->input->post('no_sk');
		if ($kode != "") {
			$data=array(
				'id_karyawan'=>$this->input->post('id_karyawan'),
				'no_sk'=>strtoupper($this->input->post('no_sk')),
				'tgl_sk'=>$this->formatter->getDateFormatDb($this->input->post('tgl_sk')),
				'tgl_berlaku'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku')),
				'status_mutasi'=>$this->input->post('status_mutasi'),
				'jabatan_asal'=>$this->input->post('jabatan'),
				'lokasi_asal'=>$this->input->post('lokasi_asal'),
				'jabatan_baru'=>$this->input->post('jabatan_baru'),
				'lokasi_baru'=>$this->input->post('lokasi_baru'),
				'mengetahui'=>$this->input->post('mengetahui'),
				'menyetujui'=>$this->input->post('menyetujui'),
				'lama_percobaan'=>$this->input->post('lama_percobaan'),
				'keterangan'=>$this->input->post('keterangan'),
			);
				$data_kar=array('loker'=>$this->input->post('lokasi_baru'),'jabatan'=>$this->input->post('jabatan_baru'),);
				$where = array('id_karyawan' => $this->input->post('id_karyawan') );
				$this->model_global->updateQuery($data_kar,'karyawan',$where);
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));

			$datax = $this->model_global->insertQueryCC($data,'mutasi_jabatan',$this->model_karyawan->checkMutasiCode($data['no_sk']));
		}else{
			$datax = $this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
//Perjanjian KErja
	public function pilih_k_perjanjian()
	{
		if (!$this->input->is_ajax_request())
			redirect('not_found');
		$data=$this->model_karyawan->getPilihKaryawanPerjanjian();
		$datax['data']=[];
		foreach ($data as $d) {
			$datax['data'][]=[
				$d->id_karyawan,
				'<a class="pilih" style="cursor:pointer" 
				data-nik				="'.$d->knik.'" 
				data-nama				="'.$d->nama.'" 
				data-status_perjanjian	="'.$d->status_baru.'" 
				data-nama_status_perjanjian	="'.$d->nama_status.'" 
				data-no_sk_lama			="'.$d->no_sk_baru.'" 
				data-tgl_sk_lama		="'.$this->formatter->getDateFormatUser($d->tgl_sk_baru).'" 
				data-tgl_berlaku_lama			="'.$this->formatter->getDateFormatUser($d->tgl_berlaku_baru).'" 
				data-tgl_berlaku_sampai_lama	="'.$this->formatter->getDateFormatUser($d->berlaku_sampai_baru).'">'.
				$d->knik.'</a>',
				$d->nama,
				$d->nama_status,
			];
		}
		echo json_encode($datax);		
	}
	public function pilih_k_perjanjian_add()
	{
		if (!$this->input->is_ajax_request())
			redirect('not_found');
		$nik=$this->input->post('nik');
		$data=$this->model_karyawan->getPilihKaryawanPerjanjianNIK($nik);
		//$datax=[];
		foreach ($data as $d) {
			$datax=[
				'no_sk_lama'=>(!empty($d->no_sk_baru)) ? $d->no_sk_baru : null,
				'tgl_sk_lama'=>(!empty($d->tgl_sk_baru)) ? $this->formatter->getDateFormatUser($d->tgl_sk_baru) : null,
				'tgl_berlaku_lama'=>(!empty($d->tgl_berlaku_baru)) ? $this->formatter->getDateFormatUser($d->tgl_berlaku_baru) : null,
				'berlaku_sampai_lama'=>(!empty($d->berlaku_sampai_baru)) ? $this->formatter->getDateFormatUser($d->berlaku_sampai_baru) : null,
				'status_lama'=>$d->status_baru
			];
		}
		echo json_encode($datax);		
	}
	public function perjanjian_kerja()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getListPerjanjianKerja();
				$access=$this->codegenerator->decryptChar($this->input->post('access'));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_p_kerja,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_p_kerja,
						$d->nik,
						$d->nama_karyawan,
						$d->no_sk_baru,
						$this->formatter->getDateMonthFormatUser($d->tgl_sk_baru),
						$d->nama_status_baru,
						$this->formatter->getDateMonthFormatUser($d->berlaku_sampai_baru),
						$d->jum,
						$properties['aksi'],
						$this->codegenerator->encryptChar($d->nik),
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_p_kerja');
				$data=$this->model_karyawan->getPerjanjianKerja($id);
				foreach ($data as $d) {
					$tabel='';
					$tabel.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-blue">
          							<th>No.</th>
          							<th>NO SK</th>
          							<th>Tanggal SK</th>
          							<th>Status</th>
          							<th>Tanggal Berlaku</th>
          							<th>Berlaku Sampai</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_perjanjian=$this->model_karyawan->getListPerjanjianKerjaNik($d->nik);
          						$no=1;
          						foreach ($data_perjanjian as $d_p) {
          							$tabel.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_p->no_sk_baru.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl_sk_baru).'</td>
          							<td>'.$d_p->nama_status_baru.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl_berlaku_baru).'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->berlaku_sampai_baru).'</td>
          						</tr>';
          						$no++;
          					}
	          				$tabel.='</tbody>
	          			</table>';
					$jbt_mengetahui=($d->jbt_mengetahui != null) ? ' - '.$d->jbt_mengetahui : null;
					$jbt_menyetujui=($d->jbt_menyetujui != null) ? ' - '.$d->jbt_menyetujui : null;
					$datax=[
						'tabel'=>$tabel,
						'loker'=>$d->nama_loker,
						'jabatan'=>$d->nama_jabatan,
						'id'=>$d->id_p_kerja,
						'no_sk_lama'=>(!empty($d->no_sk_lama)) ? $d->no_sk_lama : $this->otherfunctions->getMark($d->no_sk_lama),
						'tgl_sk_lama'=>(!empty($d->tgl_sk_lama)) ? $this->formatter->getDateMonthFormatUser($d->tgl_sk_lama) : $this->otherfunctions->getMark($d->tgl_sk_lama),
						'tgl_berlaku_lama'=>(!empty($d->tgl_berlaku_lama)) ? $this->formatter->getDateMonthFormatUser($d->tgl_berlaku_lama) : $this->otherfunctions->getMark($d->tgl_berlaku_lama),
						'berlaku_sampai_lama'=>(!empty($d->berlaku_sampai_lama)) ? $this->formatter->getDateMonthFormatUser($d->berlaku_sampai_lama) : $this->otherfunctions->getMark($d->berlaku_sampai_lama),
						'nik'=>$d->nik,
						'nama'=>$d->nama_karyawan,
						'status_lama'=>(!empty($d->nama_status_lama)) ? $d->nama_status_lama : $this->otherfunctions->getMark($d->nama_status_lama),
						'status_baru'=>$d->nama_status_baru,
						'no_sk_baru'=>$d->no_sk_baru,
						'tgl_sk_baru'=>$this->formatter->getDateMonthFormatUser($d->tgl_sk_baru),
						'tgl_berlaku_baru'=>$this->formatter->getDateMonthFormatUser($d->tgl_berlaku_baru),
						'berlaku_sampai_baru'=>$this->formatter->getDateMonthFormatUser($d->berlaku_sampai_baru),
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'mengetahui'=>$d->nama_mengetahui.$jbt_mengetahui,
						'menyetujui'=>$d->nama_menyetujui.$jbt_menyetujui,
						'keterangan'=>(!empty($d->keterangan)) ? $d->keterangan:$this->otherfunctions->getMark($d->keterangan),
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'kode') {
				$data = $this->codegenerator->kodePerjanjianKerja();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function view_perjanjian_kerja()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getListPerjanjianKerjaNik($this->codegenerator->decryptChar($this->uri->segment(4)));
				$access=$this->codegenerator->decryptChar($this->input->post('access'));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_p_kerja,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_p_kerja,
						$d->nama_karyawan,
						$d->no_sk_baru,
						$this->formatter->getDateMonthFormatUser($d->tgl_sk_baru),
						$d->nama_status_baru,
						(!empty($d->tgl_berlaku_baru)) ? $this->formatter->getDateMonthFormatUser($d->tgl_berlaku_baru):$this->otherfunctions->getMark($d->tgl_berlaku_baru),
						(!empty($d->berlaku_sampai_baru)) ? $this->formatter->getDateMonthFormatUser($d->berlaku_sampai_baru):$this->otherfunctions->getMark($d->berlaku_sampai_baru),
						$properties['tanggal'],
						$properties['status'],
						$properties['aksi']
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_p_kerja');
				$data=$this->model_karyawan->getPerjanjianKerja($id);
				foreach ($data as $d) {
					$jbt_mengetahui=($d->jbt_mengetahui != null) ? ' - '.$d->jbt_mengetahui : null;
					$jbt_menyetujui=($d->jbt_menyetujui != null) ? ' - '.$d->jbt_menyetujui : null;
					$datax=[
						'id'=>$d->id_p_kerja,
						'no_sk_lama'=>(!empty($d->no_sk_lama)) ? $d->no_sk_lama : $this->otherfunctions->getMark($d->no_sk_lama),
						'tgl_sk_lama'=>(!empty($d->tgl_sk_lama)) ? $this->formatter->getDateMonthFormatUser($d->tgl_sk_lama) : $this->otherfunctions->getMark($d->tgl_sk_lama),
						'tgl_berlaku_lama'=>(!empty($d->tgl_berlaku_lama)) ? $this->formatter->getDateMonthFormatUser($d->tgl_berlaku_lama) : $this->otherfunctions->getMark($d->tgl_berlaku_lama),
						'berlaku_sampai_lama'=>(!empty($d->berlaku_sampai_lama)) ? $this->formatter->getDateMonthFormatUser($d->berlaku_sampai_lama) : $this->otherfunctions->getMark($d->berlaku_sampai_lama),
						'nik'=>$d->nik,
						'nama'=>$d->nama_karyawan,
						'status_lama'=>(!empty($d->nama_status_lama)) ? $d->nama_status_lama : $this->otherfunctions->getMark($d->nama_status_lama),
						'status_baru'=>$d->nama_status_baru,
						'no_sk_baru'=>$d->no_sk_baru,
						'tgl_sk_baru'=>$this->formatter->getDateMonthFormatUser($d->tgl_sk_baru),
						'tgl_berlaku_baru'=>$this->formatter->getDateMonthFormatUser($d->tgl_berlaku_baru),
						'berlaku_sampai_baru'=>$this->formatter->getDateMonthFormatUser($d->berlaku_sampai_baru),
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'mengetahui'=>$d->mengetahui,
						'menyetujui'=>$d->menyetujui,
						'keterangan'=>(!empty($d->keterangan)) ? $d->keterangan:$this->otherfunctions->getMark($d->keterangan),
						'no_sk_lamav'=>(!empty($d->no_sk_lama)) ? $d->no_sk_lama : null,
						'tgl_sk_lamav'=>(!empty($d->tgl_sk_lama)) ? $this->formatter->getDateFormatUser($d->tgl_sk_lama) : null,
						'tgl_berlaku_lamav'=>(!empty($d->tgl_berlaku_lama)) ? $this->formatter->getDateFormatUser($d->tgl_berlaku_lama) : null,
						'berlaku_sampai_lamav'=>(!empty($d->berlaku_sampai_lama)) ? $this->formatter->getDateFormatUser($d->berlaku_sampai_lama) : null,
						'perjanjian_lama'=>$d->nama_status_lama,
						'perjanjian_baru'=>$d->status_baru,
						'status_baruv'=>$d->nama_status_baru,
						'tgl_sk_baruv'=>$this->formatter->getDateFormatUser($d->tgl_sk_baru),
						'tgl_berlaku_baruv'=>$this->formatter->getDateFormatUser($d->tgl_berlaku_baru),
						'berlaku_sampai_baruv'=>$this->formatter->getDateFormatUser($d->berlaku_sampai_baru),
						'mengetahuiv'=>(!empty($d->nama_mengetahui)) ? $d->nama_mengetahui.$jbt_mengetahui:$this->otherfunctions->getMark(),
						'menyetujuiv'=>(!empty($d->nama_menyetujui)) ? $d->nama_menyetujui.$jbt_menyetujui:$this->otherfunctions->getMark(),
						'keteranganv'=>$d->keterangan,
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function edit_perjanjian_kerja(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		if ($id != "") {
			$data=array(
				'nik'=>$this->input->post('nik'),
				'no_sk_lama'=>strtoupper($this->input->post('no_sk_lama')),
				'tgl_sk_lama'=>$this->formatter->getDateFormatDb($this->input->post('tgl_sk_lama')),
				'tgl_berlaku_lama'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku_lama')),
				'berlaku_sampai_lama'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku_sampai_lama')),
				'status_lama'=>$this->input->post('perjanjian_lama'),
				'no_sk_baru'=>strtoupper($this->input->post('no_sk_baru')),
				'tgl_sk_baru'=>$this->formatter->getDateFormatDb($this->input->post('tgl_sk_baru')),
				'tgl_berlaku_baru'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku_baru')),
				'berlaku_sampai_baru'=>$this->formatter->getDateFormatDb($this->input->post('berlaku_sampai_baru')),
				'status_baru'=>$this->input->post('perjanjian_baru'),
				'mengetahui'=>$this->input->post('mengetahui'),
				'menyetujui'=>$this->input->post('menyetujui'),
				'keterangan'=>$this->input->post('keterangan'),
			);
				$data_kar=array('status_perjanjian'=>$this->input->post('no_sk_baru'));
				$where = array('nik' => $this->input->post('nik') );
				$this->model_global->updateQuery($data_kar,'karyawan',$where);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'perjanjian_kerja',['id_p_kerja'=>$id]);
		}else{
			$datax = $this->messages->notValidParam(); 
		}
	   	echo json_encode($datax);
	}
	function add_perjanjian_kerja(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$kode=$this->input->post('no_sk_baru');
		$nik=$this->input->post('nik');
		$tgl_sk=$this->formatter->getDateFormatDb($this->input->post('tgl_sk_baru'));
		if ($kode != "" && $nik != null && $tgl_sk != null) {
			$data=array(
				'nik'=>$nik,
				'no_sk_lama'=>$this->input->post('no_sk_lama'),
				'tgl_sk_lama'=>$this->formatter->getDateFormatDb($this->input->post('tgl_sk_lama')),
				'tgl_berlaku_lama'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku_lama')),
				'berlaku_sampai_lama'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku_sampai_lama')),
				'status_lama'=>$this->input->post('status_lama'),
				'no_sk_baru'=>$this->input->post('no_sk_baru'),
				'tgl_sk_baru'=>$tgl_sk,
				'tgl_berlaku_baru'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku_baru')),
				'berlaku_sampai_baru'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku_sampai_baru')),
				'status_baru'=>$this->input->post('perjanjian_baru'),
				'mengetahui'=>$this->input->post('mengetahui'),
				'menyetujui'=>$this->input->post('menyetujui'),
				'keterangan'=>$this->input->post('keterangan'),
			);
				$data_kar=array('status_perjanjian'=>$this->input->post('no_sk_baru'));
				$where = array('nik' => $this->input->post('nik') );
				$this->model_global->updateQuery($data_kar,'karyawan',$where);
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			//$datax = $this->model_global->insertQuery($data,'perjanjian_kerja');
			$datax = $this->model_global->insertQueryCC($data,'perjanjian_kerja',$this->model_karyawan->checkPerjanjianCode($data['no_sk_baru']));
		}else{
			$datax = $this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
	function tanggal_janji(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$kode = $this->input->post('status');
		$status = $this->db->query("SELECT berlaku FROM master_surat_perjanjian WHERE kode_perjanjian = '$kode'")->result();
		foreach ($status as $s) {
			if(substr($s->berlaku,2,1) == 'H') {
				$d = substr($s->berlaku,0,1);
			}elseif(substr($s->berlaku,3,1) == 'H'){
				$d = substr($s->berlaku,0,2);
			}else{
				$d = null;
			}
			if(substr($s->berlaku,2,1) == 'M') {
				$g = substr($s->berlaku,0,1);
			}elseif(substr($s->berlaku,3,1) == 'M'){
				$g = substr($s->berlaku,0,2);
			}else{
				$g = null;
			}
			if(substr($s->berlaku,2,1) == 'B') {
				$e = substr($s->berlaku,0,1);
			}elseif(substr($s->berlaku,3,1) == 'B'){
				$e = substr($s->berlaku,0,2);
			}else{
				$e = null;
			}
			if(substr($s->berlaku,2,1) == 'T'){
				$f = substr($s->berlaku,0,1);
			}elseif(substr($s->berlaku,3,1) == 'T'){
				$f = substr($s->berlaku,0,2);
			}else{
				$f = null;
			}
			$gettanggal = $this->input->post('tgl_berlaku');
			$tanggal = substr($gettanggal,0,2);
			$bulan = substr($gettanggal,3,2);
			$tahun = substr($gettanggal,6,4);
			$tgl = mktime(0, 0, 0, date($bulan)+$e, date($tanggal)+$d+($g*7), date($tahun)+$f);
			$data = date("d/m/Y", $tgl);
			echo json_encode($data);
		}
	}
//Peringatan KErja
	public function pilih_k_peringatan()
	{
		if (!$this->input->is_ajax_request())
			redirect('not_found');
		$data=$this->model_karyawan->getPilihKaryawanPeringatan();
		$datax['data']=[];
		foreach ($data as $d) {
			$datax['data'][]=[
				$d->id_kar,
				'<a class="pilih" style="cursor:pointer" 
				data-nik			="'.$d->nik.'" 
				data-id_kar	="'.$d->id_kar.'" 
				data-nama			="'.$d->nama.'" 
				data-kode_disiplin	="'.$d->kode_disiplin.'" 
				data-nama_disiplin	="'.$d->nama_disiplin.'">'.
				$d->nik.'</a>',
				$d->nama,
				$d->nama_disiplin,
			];
		}
		echo json_encode($datax);		
	}
	public function peringatan_karyawan()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getListPeringatan();
				$access=$this->codegenerator->decryptChar($this->input->post('access'));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_peringatan,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_peringatan,
						$d->nik_karyawan,
						$d->nama_karyawan,
						$d->no_sk,
						$this->formatter->getDateMonthFormatUser($d->tgl_sk),
						$d->nama_status_baru,
						$d->jum,
						$properties['aksi'],
						$this->codegenerator->encryptChar($d->nik_karyawan),
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_peringatan');
				$data=$this->model_karyawan->getPeringatanKerja($id);
				foreach ($data as $d) {
					$tabel='';
					$tabel.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-blue">
          							<th>No.</th>
          							<th>NO SK</th>
          							<th>Tanggal SK</th>
          							<th>Peringatan</th>
          							<th>Tanggal Berlaku</th>
          							<th>Berlaku Sampai</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_peringatan=$this->model_karyawan->getListPeringatanNik($d->nik_karyawan);
          						$no=1;
          						foreach ($data_peringatan as $d_p) {
          							$tabel.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_p->no_sk.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl_sk).'</td>
          							<td>'.$d_p->nama_status_baru.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl_berlaku).'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->berlaku_sampai).'</td>
          						</tr>';
          						$no++;
          					}
	          				$tabel.='</tbody>
	          			</table>';
					$jbt_mengetahui=($d->jbt_mengetahui != null) ? ' - '.$d->jbt_mengetahui : null;
					$jbt_menyetujui=($d->jbt_menyetujui != null) ? ' - '.$d->jbt_menyetujui : null;
					$datax=[
						'tabel'=>$tabel,
						'jabatan'=>$d->nama_jabatan,
						'loker'=>$d->nama_loker,
						'id'=>$d->id_peringatan,
						'no_sk'=>(!empty($d->no_sk)) ? $d->no_sk : $this->otherfunctions->getMark($d->no_sk),
						'tgl_sk'=>(!empty($d->tgl_sk)) ? $this->formatter->getDateMonthFormatUser($d->tgl_sk) : $this->otherfunctions->getMark($d->tgl_sk),
						'tgl_berlaku'=>(!empty($d->tgl_berlaku)) ? $this->formatter->getDateMonthFormatUser($d->tgl_berlaku) : $this->otherfunctions->getMark($d->tgl_berlaku),
						'berlaku_sampai'=>(!empty($d->berlaku_sampai)) ? $this->formatter->getDateMonthFormatUser($d->berlaku_sampai) : $this->otherfunctions->getMark($d->berlaku_sampai),
						'nik'=>$d->nik_karyawan,
						'nama'=>$d->nama_karyawan,
						'status_lama'=>(!empty($d->nama_status_lama)) ? $d->nama_status_lama : $this->otherfunctions->getMark($d->nama_status_lama),
						'status_baru'=>$d->nama_status_baru,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'mengetahui'=>(!empty($d->nama_mengetahui)) ? $d->nama_mengetahui.$jbt_mengetahui:$this->otherfunctions->getMark(),
						'menyetujui'=>(!empty($d->nama_menyetujui)) ? $d->nama_mengetahui.$jbt_mengetahui:$this->otherfunctions->getMark(),
						'keterangan'=>(!empty($d->keterangan)) ? $d->keterangan:$this->otherfunctions->getMark($d->keterangan),
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'kode') {
				$data = $this->codegenerator->kodePeringatanKerja();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function view_peringatan_karyawan()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getListPeringatanNik($this->codegenerator->decryptChar($this->uri->segment(4)));
				$access=$this->codegenerator->decryptChar($this->input->post('access'));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_peringatan,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_peringatan,
						$d->nama_karyawan,
						$d->no_sk,
						$this->formatter->getDateMonthFormatUser($d->tgl_sk),
						$d->nama_status_baru,
						(!empty($d->berlaku_sampai)) ? $this->formatter->getDateMonthFormatUser($d->berlaku_sampai):$this->otherfunctions->getMark($d->berlaku_sampai),
						$properties['tanggal'],
						$properties['status'],
						$properties['aksi']
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_peringatan');
				$data=$this->model_karyawan->getPeringatanKerja($id);
				foreach ($data as $d) {
					$jbt_mengetahui=($d->jbt_mengetahui != null) ? ' - '.$d->jbt_mengetahui : null;
					$jbt_menyetujui=($d->jbt_menyetujui != null) ? ' - '.$d->jbt_menyetujui : null;
					$datax=[
						'id'=>$d->id_peringatan,
						'id_karyawan'=>$d->id_karyawan,
						'no_sk'=>(!empty($d->no_sk)) ? $d->no_sk : $this->otherfunctions->getMark($d->no_sk),
						'tgl_sk'=>(!empty($d->tgl_sk)) ? $this->formatter->getDateMonthFormatUser($d->tgl_sk) : $this->otherfunctions->getMark($d->tgl_sk),
						'tgl_berlaku'=>(!empty($d->tgl_berlaku)) ? $this->formatter->getDateMonthFormatUser($d->tgl_berlaku) : $this->otherfunctions->getMark($d->tgl_berlaku),
						'berlaku_sampai'=>(!empty($d->berlaku_sampai)) ? $this->formatter->getDateMonthFormatUser($d->berlaku_sampai) : $this->otherfunctions->getMark($d->berlaku_sampai),
						'etgl_sk'=>$this->formatter->getDateFormatUser($d->tgl_sk),
						'etgl_berlaku'=>$this->formatter->getDateFormatUser($d->tgl_berlaku),
						'eberlaku_sampai'=>$this->formatter->getDateFormatUser($d->berlaku_sampai),
						'estatus_baru'=>$d->status_baru,
						'emengetahui'=>$d->mengetahui,
						'emenyetujui'=>$d->menyetujui,
						'nik'=>$d->nik_karyawan,
						'nama'=>$d->nama_karyawan,
						'status_lama'=>(!empty($d->nama_status_lama)) ? $d->nama_status_lama : $this->otherfunctions->getMark($d->nama_status_lama),
						'estatus_lama'=>$d->nama_status_lama,
						'status_baru'=>$d->nama_status_baru,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'mengetahui'=>(!empty($d->nama_mengetahui)) ? $d->nama_mengetahui.$jbt_mengetahui:$this->otherfunctions->getMark(),
						'menyetujui'=>(!empty($d->nama_menyetujui)) ? $d->nama_menyetujui.$jbt_menyetujui:$this->otherfunctions->getMark(),
						'keterangan'=>(!empty($d->keterangan)) ? $d->keterangan:$this->otherfunctions->getMark($d->keterangan),
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'kode') {
				$data = $this->codegenerator->kodePeringatanKerja();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function edit_peringatan_karyawan(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		if ($id != "") {
			$data=array(
				'id_karyawan'=>$this->input->post('id_karyawan'),
				'no_sk'=>strtoupper($this->input->post('no_sk')),
				'tgl_sk'=>$this->formatter->getDateFormatDb($this->input->post('tgl_sk')),
				'tgl_berlaku'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku')),
				'berlaku_sampai'=>$this->formatter->getDateFormatDb($this->input->post('berlaku_sampai')),
				'status_baru'=>$this->input->post('peringatan_baru'),
				'mengetahui'=>$this->input->post('mengetahui'),
				'menyetujui'=>$this->input->post('menyetujui'),
				'keterangan'=>$this->input->post('keterangan'),
			);
				$data_kar=array('status_disiplin'=>$this->input->post('peringatan_baru'));
				$where = array('id_karyawan' => $this->input->post('id_karyawan') );
				$this->model_global->updateQuery($data_kar,'karyawan',$where);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'peringatan_karyawan',['id_peringatan'=>$id]);
		}else{
			$datax = $this->messages->notValidParam(); 
		}
	   	echo json_encode($datax);
	}
	function add_peringatan_karyawan(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$kode=$this->input->post('no_sk');
		$id_k=$this->input->post('id_karyawan');
		$tgl_sk=$this->formatter->getDateFormatDb($this->input->post('tgl_sk'));
		if ($kode != "" && $id_k != null && $tgl_sk != null) {
			$data=array(
				'id_karyawan'=>$id_k,
				'no_sk'=>$this->input->post('no_sk'),
				'tgl_sk'=>$tgl_sk,
				'tgl_berlaku'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku')),
				'berlaku_sampai'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku_sampai')),
				'status_asal'=>$this->input->post('status_asal'),
				'status_baru'=>$this->input->post('peringatan_baru'),
				'mengetahui'=>$this->input->post('mengetahui'),
				'menyetujui'=>$this->input->post('menyetujui'),
				'keterangan'=>$this->input->post('keterangan'),
			);
				$data_kar=array('status_disiplin'=>$this->input->post('peringatan_baru'));
				$where = array('id_karyawan' => $this->input->post('id_karyawan') );
				$this->model_global->updateQuery($data_kar,'karyawan',$where);
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			$datax = $this->model_global->insertQueryCC($data,'peringatan_karyawan',$this->model_karyawan->checkPeringatanCode($kode));
		}else{
			$datax = $this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
	function tanggal_peringatan(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$kode = $this->input->post('status');
		$status = $this->db->query("SELECT berlaku FROM master_surat_peringatan WHERE kode_sp = '$kode'")->result();
		foreach ($status as $s) {
			if(substr($s->berlaku,2,1) == 'H') {
				$d = substr($s->berlaku,0,1);
			}elseif(substr($s->berlaku,3,1) == 'H'){
				$d = substr($s->berlaku,0,2);
			}else{
				$d = null;
			}
			if(substr($s->berlaku,2,1) == 'M') {
				$g = substr($s->berlaku,0,1);
			}elseif(substr($s->berlaku,3,1) == 'M'){
				$g = substr($s->berlaku,0,2);
			}else{
				$g = null;
			}
			if(substr($s->berlaku,2,1) == 'B') {
				$e = substr($s->berlaku,0,1);
			}elseif(substr($s->berlaku,3,1) == 'B'){
				$e = substr($s->berlaku,0,2);
			}else{
				$e = null;
			}
			if(substr($s->berlaku,2,1) == 'T'){
				$f = substr($s->berlaku,0,1);
			}elseif(substr($s->berlaku,3,1) == 'T'){
				$f = substr($s->berlaku,0,2);
			}else{
				$f = null;
			}
			$gettanggal = $this->input->post('tgl_berlaku');
			$tanggal = substr($gettanggal,0,2);
			$bulan = substr($gettanggal,3,2);
			$tahun = substr($gettanggal,6,4);
			$tgl = mktime(0, 0, 0, date($bulan)+$e, date($tanggal)+$d+($g*7), date($tahun)+$f);
			$data = date("d/m/Y", $tgl);
			echo json_encode($data);
		}
	}
//GRADE KARYAWAN
	public function pilih_k_grade()
	{
		if (!$this->input->is_ajax_request())
			redirect('not_found');
		$data=$this->model_karyawan->getPilihKaryawanGrade();
		$datax['data']=[];
		foreach ($data as $d) {
			$datax['data'][]=[
				$d->id_kar,
				'<a class="pilih" style="cursor:pointer" 
				data-nik		="'.$d->nik.'" 
				data-id_kar		="'.$d->id_kar.'" 
				data-nama		="'.$d->nama.'" 
				data-kode_grade	="'.$d->kode_grade.'" 
				data-nama_grade	="'.$d->nama_grade.'  ('.$d->nama_loker.')">'.
				$d->nik.'</a>',
				$d->nama,
				(empty($d->nama_grade)?$d->nama_grade : $d->nama_grade.' ('.$d->nama_loker.')'),
			];
		}
		echo json_encode($datax);		
	}
	public function grade_karyawan()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getListGrade();
				$access=$this->codegenerator->decryptChar($this->input->post('access'));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_grade,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_grade,
						$d->nik_karyawan,
						$d->nama_karyawan,
						$d->no_sk,
						$this->formatter->getDateMonthFormatUser($d->tgl_sk),
						$d->nama_grade_baru.' ('.$d->nama_loker_grade.')',
						$d->jum,
						$properties['aksi'],
						$this->codegenerator->encryptChar($d->nik_karyawan),
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_grade');
				$data=$this->model_karyawan->getGradeKaryawan($id);
				foreach ($data as $d) {
					$tabel='';
					$tabel.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-blue">
          							<th>No.</th>
          							<th>NO SK</th>
          							<th>Tanggal SK</th>
          							<th>Grade</th>
          							<th>Tanggal Berlaku</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_grade=$this->model_karyawan->getListGradeNik($d->nik_karyawan);
          						$no=1;
          						foreach ($data_grade as $d_p) {
          							$tabel.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_p->no_sk.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl_sk).'</td>
          							<td>'.$d_p->nama_grade_baru.' ('.$d_p->nama_loker_grade.')</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl_berlaku).'</td>
          							</tr>';
          						$no++;
          					}
	          				$tabel.='</tbody>
	          			</table>';
					$jbt_mengetahui=($d->jbt_mengetahui != null) ? ' - '.$d->jbt_mengetahui : null;
					$jbt_menyetujui=($d->jbt_menyetujui != null) ? ' - '.$d->jbt_menyetujui : null;
					$datax=[
						'tabel'=>$tabel,
						'jabatan'=>$d->nama_jabatan,
						'loker'=>$d->nama_loker,
						'id'=>$d->id_karyawan,
						'no_sk'=>(!empty($d->no_sk)) ? $d->no_sk : $this->otherfunctions->getMark($d->no_sk),
						'tgl_sk'=>(!empty($d->tgl_sk)) ? $this->formatter->getDateMonthFormatUser($d->tgl_sk) : $this->otherfunctions->getMark($d->tgl_sk),
						'tgl_berlaku'=>(!empty($d->tgl_berlaku)) ? $this->formatter->getDateMonthFormatUser($d->tgl_berlaku) : $this->otherfunctions->getMark($d->tgl_berlaku),
						'nik'=>$d->nik_karyawan,
						'nama'=>$d->nama_karyawan,
						'grade_lama'=>(!empty($d->nama_grade_lama)) ? $d->nama_grade_lama : $this->otherfunctions->getMark($d->nama_grade_lama),
						'grade_baru'=>$d->nama_grade_baru.$d->nama_loker_grade,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'mengetahui'=>(!empty($d->nama_mengetahui)) ? $d->nama_mengetahui.$jbt_mengetahui:$this->otherfunctions->getMark(),
						'menyetujui'=>(!empty($d->nama_menyetujui)) ? $d->nama_menyetujui.$jbt_menyetujui:$this->otherfunctions->getMark(),
						'keterangan'=>(!empty($d->keterangan)) ? $d->keterangan:$this->otherfunctions->getMark($d->keterangan),
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'kode') {
				$data = $this->codegenerator->kodeGradeKaryawan();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function view_grade_karyawan()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getListGradeNik($this->codegenerator->decryptChar($this->uri->segment(4)));
				$access=$this->codegenerator->decryptChar($this->input->post('access'));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_grade,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_grade,
						$d->nama_karyawan,
						$d->no_sk,
						$this->formatter->getDateMonthFormatUser($d->tgl_sk),
						$d->nama_grade_baru.' ('.$d->nama_loker_grade.')',
						(!empty($d->tgl_berlaku)) ? $this->formatter->getDateMonthFormatUser($d->tgl_berlaku):$this->otherfunctions->getMark($d->tgl_berlaku),
						$properties['tanggal'],
						$properties['status'],
						$properties['aksi']
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_grade');
				$data=$this->model_karyawan->getGradeKaryawan($id);
				foreach ($data as $d) {
					$jbt_mengetahui=($d->jbt_mengetahui != null) ? ' - '.$d->jbt_mengetahui : null;
					$jbt_menyetujui=($d->jbt_menyetujui != null) ? ' - '.$d->jbt_menyetujui : null;
					$datax=[
						'id'=>$d->id_grade,
						'id_karyawan'=>$d->id_karyawan,
						'no_sk'=>(!empty($d->no_sk)) ? $d->no_sk : $this->otherfunctions->getMark($d->no_sk),
						'tgl_sk'=>(!empty($d->tgl_sk)) ? $this->formatter->getDateMonthFormatUser($d->tgl_sk) : $this->otherfunctions->getMark($d->tgl_sk),
						'tgl_berlaku'=>(!empty($d->tgl_berlaku)) ? $this->formatter->getDateMonthFormatUser($d->tgl_berlaku) : $this->otherfunctions->getMark($d->tgl_berlaku),
						'etgl_sk'=>$this->formatter->getDateFormatUser($d->tgl_sk),
						'etgl_berlaku'=>$this->formatter->getDateFormatUser($d->tgl_berlaku),
						'egrade_baru'=>$d->grade_baru,
						'emengetahui'=>$d->mengetahui,
						'emenyetujui'=>$d->menyetujui,
						'eketerangan'=>$d->keterangan,
						'nik'=>$d->nik_karyawan,
						'nama'=>$d->nama_karyawan,
						'grade_lama'=>(!empty($d->nama_grade_lama)) ? $d->nama_grade_lama.' ('.$d->nama_loker_grade_lama.')': $this->otherfunctions->getMark($d->nama_grade_lama),
						'egrade_lama'=>$d->nama_grade_lama.' ('.$d->nama_loker_grade_lama.')',
						'grade_baru'=>$d->nama_grade_baru.' ('.$d->nama_loker_grade.')',
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'mengetahui'=>(!empty($d->nama_mengetahui)) ? $d->nama_mengetahui.$jbt_mengetahui:$this->otherfunctions->getMark(),
						'menyetujui'=>(!empty($d->nama_menyetujui)) ? $d->nama_menyetujui.$jbt_menyetujui:$this->otherfunctions->getMark(),
						'keterangan'=>(!empty($d->keterangan)) ? $d->keterangan:$this->otherfunctions->getMark(),
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'kode') {
				$data = $this->codegenerator->kodeGradeKerja();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function edit_grade_karyawan(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		if ($id != "") {
			$data=array(
				'id_karyawan'=>$this->input->post('id_karyawan'),
				'no_sk'=>strtoupper($this->input->post('no_sk')),
				'tgl_sk'=>$this->formatter->getDateFormatDb($this->input->post('tgl_sk')),
				'tgl_berlaku'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku')),
				'grade_baru'=>$this->input->post('grade_baru'),
				'mengetahui'=>$this->input->post('mengetahui'),
				'menyetujui'=>$this->input->post('menyetujui'),
				'keterangan'=>$this->input->post('keterangan'),
			);
				$data_kar=array('grade'=>$this->input->post('grade_baru'));
				$where = array('id_karyawan' => $this->input->post('id_karyawan') );
				$this->model_global->updateQuery($data_kar,'karyawan',$where);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'grade_karyawan',['id_grade'=>$id]);
		}else{
			$datax = $this->messages->notValidParam(); 
		}
	   	echo json_encode($datax);
	}
	function add_grade_karyawan(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$kode=$this->input->post('no_sk');
		$id_k=$this->input->post('id_karyawan');
		$tgl_sk=$this->formatter->getDateFormatDb($this->input->post('tgl_sk'));
		if ($kode != "" && $id_k != null && $tgl_sk != null) {
			$data=array(
				'id_karyawan'=>$id_k,
				'no_sk'=>$this->input->post('no_sk'),
				'tgl_sk'=>$tgl_sk,
				'tgl_berlaku'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku')),
				'grade_asal'=>$this->input->post('grade_asal'),
				'grade_baru'=>$this->input->post('grade_baru'),
				'mengetahui'=>$this->input->post('mengetahui'),
				'menyetujui'=>$this->input->post('menyetujui'),
				'keterangan'=>$this->input->post('keterangan'),
			);
				$data_kar=array('grade'=>$this->input->post('grade_baru'));
				$where = array('id_karyawan' => $this->input->post('id_karyawan') );
				$this->model_global->updateQuery($data_kar,'karyawan',$where);
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			$datax = $this->model_global->insertQueryCC($data,'grade_karyawan',$this->model_karyawan->checkGradeCode($kode));
		}else{
			$datax = $this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
//KECELAKAAN KERJA
	public function pilih_k_kecelakaankerja()
	{
		if (!$this->input->is_ajax_request())
			redirect('not_found');
		$data=$this->model_karyawan->getPilihKaryawanKecelakaanKerja();
		$datax['data']=[];
		foreach ($data as $d) {
			$datax['data'][]=[
				$d->id_kar,
				'<a class="pilih" style="cursor:pointer" 
				data-nik		="'.$d->nik.'" 
				data-id_kar		="'.$d->id_kar.'" 
				data-nama		="'.$d->nama.'"
				data-nama_jabatan	="'.$d->nama_jabatan.'">'.
				$d->nik.'</a>',
				$d->nama,
				$d->nama_jabatan,
			];
		}
		echo json_encode($datax);		
	}
	public function kecelakaan_kerja()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getListKecelakaanKerja();
				$access=$this->codegenerator->decryptChar($this->input->post('access'));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_kecelakaan,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_kecelakaan,
						$d->nik_karyawan,
						$d->nama_karyawan,
						$d->no_sk,
						$this->formatter->getDateMonthFormatUser($d->tgl),
						$d->nama_kategori_kecelakaan,
						$d->jum,
						$properties['aksi'],
						$this->codegenerator->encryptChar($d->nik_karyawan),
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_kecelakaan');
				$data=$this->model_karyawan->getKecelakaanKerjaKaryawan($id);
				foreach ($data as $d) {
					$tabel='';
					$tabel.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-blue">
          							<th>No.</th>
          							<th>No Kecelakaan</th>
          							<th>Tanggal</th>
          							<th>Kecelakaan Kerja</th>
          							<th>Rumah Sakit</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_grade=$this->model_karyawan->getListKecelakaanKerjaNik($d->nik_karyawan);
          						$no=1;
          						foreach ($data_grade as $d_p) {
          							$tabel.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_p->no_sk.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl).'</td>
          							<td>'.$d_p->nama_kategori_kecelakaan.'</td>
          							<td>'.$d_p->nama_rs.'</td>
          							</tr>';
          						$no++;
          					}
	          				$tabel.='</tbody>
	          			</table>';
					$jbt_mengetahui=($d->jbt_mengetahui != null) ? ' - '.$d->jbt_mengetahui : null;
					$datax=[
						'tabel'=>$tabel,
						'jabatan'=>$d->nama_jabatan,
						'loker'=>$d->nama_loker,
						'id'=>$d->id_karyawan,
						'no_sk'=>(!empty($d->no_sk)) ? $d->no_sk : $this->otherfunctions->getMark($d->no_sk),
						'tgl'=>(!empty($d->tgl)) ? $this->formatter->getDateMonthFormatUser($d->tgl) : $this->otherfunctions->getMark($d->tgl),
						'nik'=>$d->nik_karyawan,
						'nama'=>$d->nama_karyawan,
						'kategori'=>$d->nama_kategori_kecelakaan,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'mengetahui'=>$d->nama_mengetahui.$jbt_mengetahui,
						'keterangan'=>(!empty($d->keterangan)) ? $d->keterangan:$this->otherfunctions->getMark($d->keterangan),
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'kode') {
				$data = $this->codegenerator->kodeKecelakaanKerja();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function view_kecelakaan_kerja()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getListKecelakaanKerjaNik($this->codegenerator->decryptChar($this->uri->segment(4)));
				$access=$this->codegenerator->decryptChar($this->input->post('access'));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_kecelakaan,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_kecelakaan,
						$d->nama_karyawan,
						$d->no_sk,
						$this->formatter->getDateMonthFormatUser($d->tgl),
						$d->nama_kategori_kecelakaan,
						$d->nama_loker,
						$d->nama_rs,
						$properties['tanggal'],
						$properties['status'],
						$properties['aksi']
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_kecelakaan');
				$data=$this->model_karyawan->getKecelakaanKerjaKaryawan($id);
				foreach ($data as $d) {
					$jbt_menget=($d->jbt_mengetahui != null) ? ' - '.$d->jbt_mengetahui : null;
					$jbt_menyat=($d->jbt_menyatakan != null) ? ' - '.$d->jbt_menyatakan : null;
					$jbt_sak1=($d->jbt_saksi_1 != null) ? ' - '.$d->jbt_saksi_1 : null;
					$jbt_sak2=($d->jbt_saksi_2 != null) ? ' - '.$d->jbt_saksi_2 : null;
					$jbt_tggjwb=($d->jbt_penanggungjawab != null) ? ' - '.$d->jbt_penanggungjawab : null;
					$datax=[
						'id'=>$d->id_kecelakaan,
						'id_karyawan'=>$d->id_karyawan,
						'nik'=>$d->nik_karyawan,
						'nama'=>$d->nama_karyawan,
						'nama_jabatan'=>$d->nama_jabatan,
						'nama_loker'=>$d->nama_loker,
						'no_sk'=>(!empty($d->no_sk)) ? $d->no_sk : $this->otherfunctions->getMark($d->no_sk),
						'tgl'=>(!empty($d->tgl)) ? $this->formatter->getDateMonthFormatUser($d->tgl) : $this->otherfunctions->getMark($d->tgl),
						'jam'=>(!empty($d->jam)) ? $this->formatter->timeFormatUser($d->jam) : $this->otherfunctions->getMark(),
						'lokasi'=>(!empty($d->nama_loker_kejadian)) ? $d->nama_loker_kejadian : $this->otherfunctions->getMark($d->nama_loker_kejadian),
						'kategori'=>(!empty($d->nama_kategori_kecelakaan)) ? $d->nama_kategori_kecelakaan : $this->otherfunctions->getMark($d->nama_kategori_kecelakaan),
						'rumahsakit'=>(!empty($d->nama_rs)) ? $d->nama_rs : $this->otherfunctions->getMark($d->nama_rs),
						'mengetahui'=>(!empty($d->nama_mengetahui))?$d->nama_mengetahui.$jbt_menget : $this->otherfunctions->getMark(),
						'menyatakan'=>(!empty($d->nama_menyatakan))?$d->nama_menyatakan.$jbt_menyat : $this->otherfunctions->getMark(),
						'saksi_1'=>(!empty($d->nama_saksi_1))?$d->nama_saksi_1.$jbt_sak1 : $this->otherfunctions->getMark(),
						'saksi_2'=>(!empty($d->nama_saksi_2))?$d->nama_saksi_2.$jbt_sak2 : $this->otherfunctions->getMark(),
						'penanggungjawab'=>(!empty($d->nama_penanggungjawab))?$d->nama_penanggungjawab.$jbt_tggjwb : $this->otherfunctions->getMark(),
						'keterangan'=>(!empty($d->keterangan)) ? $d->keterangan:$this->otherfunctions->getMark($d->keterangan),
						'kejadian'=>(!empty($d->kejadian)) ? $d->kejadian:$this->otherfunctions->getMark($d->kejadian),
						'alat'=>(!empty($d->alat)) ? $d->alat:$this->otherfunctions->getMark($d->alat),
						'bagiantubuh'=>(!empty($d->bagian_tubuh)) ? $d->bagian_tubuh:$this->otherfunctions->getMark($d->bagian_tubuh),
						'etgl'=>$this->formatter->getDateFormatUser($d->tgl),
						'ejam'=>$this->formatter->timeFormatUser($d->jam),
						'elokasi'=>$d->kode_loker,
						'ekategori'=>$d->kode_kategori_kecelakaan,
						'erumahsakit'=>$d->kode_master_rs,
						'emengetahui'=>$d->mengetahui,
						'emenyatakan'=>$d->menyatakan,
						'esaksi_1'=>$d->saksi_1,
						'esaksi_2'=>$d->saksi_2,
						'epenanggungjawab'=>$d->penanggungjawab,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'kode') {
				$data = $this->codegenerator->kodeKecelakaanKerja();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function edit_kecelakaan_kerja(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		if ($id != "") {
			$data=array(
				'id_karyawan'=>$this->input->post('id_karyawan'),
				'no_sk'=>$this->input->post('no_kecelakaan'),
				'tgl'=>$this->formatter->getDateFormatDb($this->input->post('tgl_kecelakaan')),
				'jam'=>$this->formatter->timeFormatDb($this->input->post('jam_terjadi')),
				'kode_kategori_kecelakaan'=>$this->input->post('kategori_kecelakaan'),
				'kode_master_rs'=>$this->input->post('tempat_kejadian'),
				'mengetahui'=>$this->input->post('mengetahui'),
				'menyatakan'=>$this->input->post('menyatakan'),
				'saksi_1'=>$this->input->post('saksi1'),
				'saksi_2'=>$this->input->post('saksi2'),
				'penanggungjawab'=>$this->input->post('penanggungjawab'),
				'kode_master_rs'=>$this->input->post('rumahsakit'),
				'kode_loker'=>$this->input->post('tempat_kejadian'),
				'keterangan'=>$this->input->post('keterangan'),
				'kejadian'=>$this->input->post('kejadian'),
				'alat'=>$this->input->post('alat'),
				'bagian_tubuh'=>$this->input->post('bagian_tubuh'),
			);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'kecelakaan_kerja',['id_kecelakaan'=>$id]);
		}else{
			$datax = $this->messages->notValidParam(); 
		}
	   	echo json_encode($datax);
	}
	function add_kecelakaan_kerja(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$kode=$this->input->post('no_kecelakaan');
		$id_k=$this->input->post('id_karyawan');
		$tgl=$this->formatter->getDateFormatDb($this->input->post('tgl_kecelakaan'));
		if ($kode != "" && $id_k != null && $tgl != null) {
			$data=array(
				'id_karyawan'=>$id_k,
				'no_sk'=>$kode,
				'tgl'=>$tgl,
				'jam'=>$this->formatter->timeFormatDb($this->input->post('jam_terjadi')),
				'kode_kategori_kecelakaan'=>$this->input->post('kategori_kecelakaan'),
				'kode_master_rs'=>$this->input->post('tempat_kejadian'),
				'mengetahui'=>$this->input->post('mengetahui'),
				'menyatakan'=>$this->input->post('menyatakan'),
				'saksi_1'=>$this->input->post('saksi1'),
				'saksi_2'=>$this->input->post('saksi2'),
				'penanggungjawab'=>$this->input->post('penanggungjawab'),
				'kode_master_rs'=>$this->input->post('rumahsakit'),
				'kode_loker'=>$this->input->post('tempat_kejadian'),
				'keterangan'=>$this->input->post('keterangan'),
				'kejadian'=>$this->input->post('kejadian'),
				'alat'=>$this->input->post('alat'),
				'bagian_tubuh'=>$this->input->post('bagian_tubuh'),
			);
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			$datax = $this->model_global->insertQueryCC($data,'kecelakaan_kerja',$this->model_karyawan->checkKecelakaanKerjaCode($kode));
		}else{
			$datax = $this->messages->notValidParam();
		}
		echo json_encode($datax);
	}

//KARYAWAN TIDAK AKTIF
	public function pilih_k_nonaktif()
	{
		if (!$this->input->is_ajax_request())
			redirect('not_found');
		$data=$this->model_karyawan->getPilihKaryawanNonAktif();
		$datax['data']=[];
		foreach ($data as $d) {
			$datax['data'][]=[
				$d->id_karyawan,
				'<a class="pilih" style="cursor:pointer" 
				data-nik			="'.$d->nik.'" 
				data-id_karyawan	="'.$d->id_karyawan.'" 
				data-nama			="'.$d->nama.'" 
				data-jabatan		="'.$d->jabatan.'" 
				data-nama_jabatan	="'.$d->nama_jabatan.'" 
				data-kode_lokasi	="'.$d->loker.'" 
				data-nama_lokasi	="'.$d->nama_loker.'"
				data-tgl_masuk		="'.$this->formatter->getDateFormatUser($d->tgl_masuk).'">'.
				$d->nik.'</a>',
				$d->nama,
				$d->nama_jabatan,
				$d->nama_loker,
			];
		}
		echo json_encode($datax);		
	}
	public function karyawan_tidak_aktif()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getListKaryawanNonAktif();
				$access=$this->codegenerator->decryptChar($this->input->post('access'));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_kta,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_kta,
						$d->nik_karyawan,
						$d->nama_karyawan,
						$d->nama_jabatan,
						$d->nama_loker,
						$this->formatter->getDateMonthFormatUser($d->tgl_masuk),
						$this->formatter->getDateMonthFormatUser($d->tgl_keluar),
						$properties['aksi'],
						$this->codegenerator->encryptChar($d->nik_karyawan),
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_kta');
				$data=$this->model_karyawan->getKaryawanNonAktif($id);
				foreach ($data as $d) {
					$jbt_mengetahui=($d->jbt_mengetahui != null) ? ' - '.$d->jbt_mengetahui : null;
					$jbt_menyetujui=($d->jbt_menyetujui != null) ? ' - '.$d->jbt_menyetujui : null;
					$datax=[
						'id'=>$d->id_kta,
						'no_sk'=>$d->no_sk,
						'tgl_sk'=>$this->formatter->getDateMonthFormatUser($d->tgl_sk),
						'tgl_berlaku'=>$this->formatter->getDateMonthFormatUser($d->tgl_berlaku),
						'tgl_masuk'=>$this->formatter->getDateMonthFormatUser($d->tgl_masuk),
						'tgl_keluar'=>$this->formatter->getDateMonthFormatUser($d->tgl_keluar),
						'id_karyawan'=>$d->id_karyawan,
						'nik'=>$d->nik,
						'nama'=>$d->nama_karyawan,
						'loker'=>$d->nama_loker,
						'jabatan'=>$d->nama_jabatan,
						'alasan'=>$this->otherfunctions->getAlasanKeluar($d->alasan),
						'mengetahui'=>(!empty($d->nama_mengetahui)) ? $d->nama_mengetahui.$jbt_mengetahui:$this->otherfunctions->getMark(),
						'menyetujui'=>(!empty($d->nama_menyetujui)) ? $d->nama_menyetujui.$jbt_menyetujui:$this->otherfunctions->getMark(),
						'keterangan'=>$d->keterangan,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'etgl_sk'=>$this->formatter->getDateFormatUser($d->tgl_sk),
						'etgl_berlaku'=>$this->formatter->getDateFormatUser($d->tgl_berlaku),
						'etgl_masuk'=>$this->formatter->getDateFormatUser($d->tgl_masuk),
						'etgl_keluar'=>$this->formatter->getDateFormatUser($d->tgl_keluar),
						'emengetahui'=>$d->mengetahui,
						'emenyetujui'=>$d->menyetujui,
						'ealasan'=>$d->alasan,
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'kode') {
				$data = $this->codegenerator->kodeKaryawanNonAktif();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function view_karyawan_tidak_aktif()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if($usage == 'view_anak'){
				$id = $this->input->post('id_karyawan');
				$data=$this->model_karyawan->getListAnak($id);
				foreach ($data as $d) {
					$tabel_anak='';
					$tabel_anak.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-green">
          							<th>No.</th>
          							<th>Nama</th>
          							<th>TTL</th>
          							<th>L/P</th>
          							<th>Pendidikan</th>
          							<th>No. HP</th>
          							<th>Aksi</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_anak=$this->model_karyawan->getListAnak($id);
          						$no=1;
          						foreach ($data_anak as $d_m) {
          							$tabel_anak.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_m->nama_anak.'</td>
          							<td>'.$this->otherfunctions->getGender($d_m->kelamin_anak).'</td>
          							<td>'.$d_m->tempat_lahir_anak.', '.$this->formatter->getDateMonthFormatUser($d_m->tanggal_lahir_anak).'</td>
          							<td>'.$d_m->pendidikan_anak.'</td>
          							<td>'.$d_m->no_telp.'</td>
          							<td><center><button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_anak('.$d->id_anak.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button></center></td>
          						</tr>';
          						$no++;
          					}
	          				$tabel_anak.='</tbody>
	          			</table>';
					$datax=[
						'tabel_anak'=>$tabel_anak,
					];
				}
				echo json_encode($datax);
			}elseif($usage == 'view_saudara'){
				$id = $this->input->post('id_karyawan');
					$tabel_saudara='';
					$tabel_saudara.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-green">
          							<th>No.</th>
          							<th>Nama</th>
          							<th>TTL</th>
          							<th>L/P</th>
          							<th>Pendidikan</th>
          							<th>No. HP</th>
          							<th>Aksi</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_saudara=$this->model_karyawan->saudara($id);
          						$no=1;
          						foreach ($data_saudara as $d_m) {
          							$tabel_saudara.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_m->nama_saudara.'</td>
          							<td>'.$this->otherfunctions->getGender($d_m->jenis_kelamin_saudara).'</td>
          							<td>'.$d_m->tempat_lahir_saudara.', '.$this->formatter->getDateMonthFormatUser($d_m->tanggal_lahir_saudara).'</td>
          							<td>'.$d_m->pendidikan_saudara.'</td>
          							<td>'.$d_m->no_telp_saudara.'</td>
          							<td><center><button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_saudara('.$d_m->id_saudara.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button></center></td>
          						</tr>';
          						$no++;
          					}
	          				$tabel_saudara.='</tbody>
	          			</table>';
					$datax=[
						'tabel_saudara'=>$tabel_saudara,
					];
				echo json_encode($datax);
			}elseif($usage == 'view_formal'){
				$id = $this->input->post('id_karyawan');
					$tabel_formal='';
					$tabel_formal.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-green">
          							<th>No.</th>
          							<th>Jenjang</th>
          							<th>Nama Sekolah</th>
          							<th>Jurusan</th>
          							<th>Fakultas</th>
          							<th>Tahun Keluar</th>
          							<th>Aksi</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_formal=$this->model_karyawan->pendidikan($id);
          						$no=1;
          						foreach ($data_formal as $d_m) {
          							$tabel_formal.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_m->jenjang_pendidikan.'</td>
          							<td>'.$d_m->nama_sekolah.'</td>
          							<td>'.$d_m->jurusan.'</td>
          							<td>'.$d_m->fakultas.'</td>
          							<td>'.$d_m->tahun_keluar.'</td>
          							<td><center><button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_formal('.$d_m->id_k_pendidikan.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button></center></td>
          						</tr>';
          						$no++;
          					}
	          				$tabel_formal.='</tbody>
	          			</table>';
					$datax=[
						'tabel_formal'=>$tabel_formal,
					];
				echo json_encode($datax);
			}elseif($usage == 'view_nformal'){
				$id = $this->input->post('id_karyawan');
					$tabel_nformal='';
					$tabel_nformal.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-green">
          							<th>No.</th>
          							<th>Nama</th>
          							<th>Tanggal Masuk</th>
          							<th>Sertifikat</th>
          							<th>Nama Lembaga</th>
          							<th>Alamat</th>
          							<th>Aksi</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_nformal=$this->model_karyawan->pnf($id);
          						$no=1;
          						foreach ($data_nformal as $d_m) {
          							$tabel_nformal.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_m->nama_pnf.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_m->tanggal_masuk_pnf).'</td>
          							<td>'.$d_m->sertifikat_pnf.'</td>
          							<td>'.$d_m->nama_lembaga_pnf.'</td>
          							<td>'.$d_m->alamat_pnf.'</td>
          							<td><center><button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_nformal('.$d_m->id_k_pnf.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button></center></td>
          						</tr>';
          						$no++;
          					}
	          				$tabel_nformal.='</tbody>
	          			</table>';
					$datax=[
						'tabel_nformal'=>$tabel_nformal,
					];
				echo json_encode($datax);
			}elseif($usage == 'view_org'){
				$id = $this->input->post('id_karyawan');
					$tabel_org='';
					$tabel_org.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-green">
          							<th>No.</th>
          							<th>Nama Organisasi</th>
          							<th>Tanggal Masuk</th>
          							<th>Tanggal Keluar</th>
          							<th>Jabatan</th>
          							<th>Aksi</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_org=$this->model_karyawan->organisasi($id);
          						$no=1;
          						foreach ($data_org as $d_m) {
          							$tabel_org.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_m->nama_organisasi.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_m->tahun_masuk).'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_m->tahun_keluar).'</td>
          							<td>'.$d_m->jabatan_org.'</td>
          							<td><center><button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_org('.$d_m->id_k_organisasi.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button></center></td>
          						</tr>';
          						$no++;
          					}
	          				$tabel_org.='</tbody>
	          			</table>';
					$datax=[
						'tabel_org'=>$tabel_org,
					];
				echo json_encode($datax);
			}elseif($usage == 'view_penghargaan'){
				$id = $this->input->post('id_karyawan');
					$tabel_penghargaan='';
					$tabel_penghargaan.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-green">
          							<th>No.</th>
          							<th>Nama Penghargaan</th>
          							<th>Tanggal</th>
          							<th>Peringatan</th>
          							<th>Menetapkan</th>
          							<th>Penyelenggara</th>
          							<th>Aksi</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_penghargaan=$this->model_karyawan->penghargaan($id);
          						$no=1;
          						foreach ($data_penghargaan as $d_m) {
          							$tabel_penghargaan.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_m->nama_penghargaan.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_m->tahun).'</td>
          							<td>'.$d_m->peringkat.'</td>
          							<td>'.$d_m->yg_menetapkan.'</td>
          							<td>'.$d_m->penyelenggara.'</td>
          							<td><center><button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_penghargaan('.$d_m->id_k_penghargaan.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button></center></td>
          						</tr>';
          						$no++;
          					}
	          				$tabel_penghargaan.='</tbody>
	          			</table>';
					$datax=[
						'tabel_penghargaan'=>$tabel_penghargaan,
					];
				echo json_encode($datax);
			}elseif($usage == 'view_bahasa'){
				$id = $this->input->post('id_karyawan');
					$tabel_bahasa='';
					$tabel_bahasa.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-green">
          							<th>No.</th>
          							<th>Bahasa</th>
          							<th>Membaca</th>
          							<th>Menulis</th>
          							<th>Berbicara</th>
          							<th>Mendengar</th>
          							<th>Aksi</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_bahasa=$this->model_karyawan->bahasa($id);
          						$no=1;
          						foreach ($data_bahasa as $d) {
          							$tabel_bahasa.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$this->otherfunctions->getBahasa($d->bahasa).'</td>
          							<td>'.$this->otherfunctions->getRadio($d->membaca).'</td>
          							<td>'.$this->otherfunctions->getRadio($d->menulis).'</td>
          							<td>'.$this->otherfunctions->getRadio($d->berbicara).'</td>
          							<td>'.$this->otherfunctions->getRadio($d->mendengar).'</td>
          							<td><center><button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_bahasa('.$d->id_k_bahasa.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button></center></td>
          						</tr>';
          						$no++;
          					}
	          				$tabel_bahasa.='</tbody>
	          			</table>';
					$datax=[
						'tabel_bahasa'=>$tabel_bahasa,
					];
				echo json_encode($datax);
			}elseif($usage == 'view_mutasi'){
				$id = $this->input->post('id_karyawan');
					$tabel_mutasi='';
					$tabel_mutasi.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-green">
          							<th>No.</th>
          							<th>Nama</th>
          							<th>NO SK</th>
          							<th>Tanggal SK</th>
          							<th>Status</th>
          							<th>Jabatan Baru</th>
          							<th>Lokasi Baru</th>
          							<th>Aksi</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_mutasi=$this->model_karyawan->getListMutasiNik($id);
          						$no=1;
          						foreach ($data_mutasi as $d_m) {
          							$tabel_mutasi.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_m->nama_karyawan.'</td>
          							<td>'.$d_m->no_sk.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_m->tgl_sk).'</td>
          							<td>'.$d_m->nama_status.'</td>
          							<td>'.$d_m->nama_jabatan_baru.'</td>
          							<td>'.$d_m->nama_loker_baru.'</td>
          							<td><center><button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_mutasi('.$d_m->id_mutasi.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button></center></td>
          						</tr>';
          						$no++;
          					}
	          				$tabel_mutasi.='</tbody>
	          			</table>';
					$datax=[
						'tabel_mutasi'=>$tabel_mutasi,
					];
				echo json_encode($datax);
			}elseif($usage == 'view_perjanjian'){
				$id = $this->input->post('id_karyawan');
					$tabel_perjanjian='';
					$tabel_perjanjian.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-green">
          							<th>No.</th>
          							<th>NO SK</th>
          							<th>Tanggal SK</th>
          							<th>Status</th>
          							<th>Tanggal Berlaku</th>
          							<th>Berlaku Sampai</th>
          							<th>Aksi</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_perjanjian=$this->model_karyawan->getListPerjanjianKerjaNik($id);
          						$no=1;
          						foreach ($data_perjanjian as $d_p) {
          							$tabel_perjanjian.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_p->no_sk_baru.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl_sk_baru).'</td>
          							<td>'.$d_p->nama_status_baru.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl_berlaku_baru).'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->berlaku_sampai_baru).'</td>
          							<td><center><button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_perjanjian('.$d_p->id_p_kerja.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button></center></td>
          						</tr>';
          						$no++;
          					}
	          				$tabel_perjanjian.='</tbody>
	          			</table>';
					$datax=[
						'tabel_perjanjian'=>$tabel_perjanjian,
					];
				echo json_encode($datax);
			}elseif($usage == 'view_peringatan'){
				$id = $this->input->post('id_karyawan');
					$tabel_peringatan='';
					$tabel_peringatan.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-green">
          							<th>No.</th>
          							<th>NO SK</th>
          							<th>Tanggal SK</th>
          							<th>Peringatan</th>
          							<th>Tanggal Berlaku</th>
          							<th>Berlaku Sampai</th>
          							<th>Aksi</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_peringatan=$this->model_karyawan->getListPeringatanNik($id);
          						$no=1;
          						foreach ($data_peringatan as $d_p) {
          							$tabel_peringatan.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_p->no_sk.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl_sk).'</td>
          							<td>'.$d_p->nama_status_baru.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl_berlaku).'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->berlaku_sampai).'</td>
          							<td><center><button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_peringatan('.$d_p->id_peringatan.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button></center></td>
          						</tr>';
          						$no++;
          					}
	          				$tabel_peringatan.='</tbody>
	          			</table>';
					$datax=[
						'tabel_peringatan'=>$tabel_peringatan,
					];
				echo json_encode($datax);
			}elseif($usage == 'view_grade'){
				$id = $this->input->post('id_karyawan');
					$tabel_grade='';
					$tabel_grade.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-green">
          							<th>No.</th>
          							<th>NO SK</th>
          							<th>Tanggal SK</th>
          							<th>Grade</th>
          							<th>Tanggal Berlaku</th>
          							<th>Aksi</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_grade=$this->model_karyawan->getListGradeNik($id);
          						$no=1;
          						foreach ($data_grade as $d_p) {
          							$tabel_grade.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_p->no_sk.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl_sk).'</td>
          							<td>'.$d_p->nama_grade_baru.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl_berlaku).'</td>
          							<td><center><button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_grade('.$d_p->id_grade.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button></center></td>
          							</tr>';
          						$no++;
          					}
	          				$tabel_grade.='</tbody>
	          			</table>';
					$datax=[
						'tabel_grade'=>$tabel_grade,
					];
				echo json_encode($datax);
			}elseif($usage == 'view_kecelakaan'){
				$id = $this->input->post('id_karyawan');
					$tabel_kecelakaan='';
					$tabel_kecelakaan.='
          				<table class="table table-bordered table-striped data-table">
          					<thead>
          						<tr class="bg-green">
          							<th>No.</th>
          							<th>No Kecelakaan</th>
          							<th>Tanggal</th>
          							<th>Kecelakaan Kerja</th>
          							<th>Rumah Sakit</th>
          							<th>Aksi</th>
          						</tr>
          					</thead>
          					<tbody>';
          					$data_grade=$this->model_karyawan->getListKecelakaanKerjaNik($id);
          						$no=1;
          						foreach ($data_grade as $d_p) {
          							$tabel_kecelakaan.='<tr>
          							<td>'.$no.'</td>
          							<td>'.$d_p->no_sk.'</td>
          							<td>'.$this->formatter->getDateMonthFormatUser($d_p->tgl).'</td>
          							<td>'.$d_p->nama_kategori_kecelakaan.'</td>
          							<td>'.$d_p->nama_rs.'</td>
          							<td><center><button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_kecelakaan('.$d_p->id_kecelakaan.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button></center></td>
          							</tr>';
          						$no++;
          					}
	          				$tabel_kecelakaan.='</tbody>
	          			</table>';
					$datax=[
						'tabel_kecelakaan'=>$tabel_kecelakaan,
					];
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function edit_karyawan_tidak_aktif(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$id_karyawan=$this->input->post('id_karyawan');
		if ($id != "" && $id_karyawan != "") {
			$data=array(
				'id_karyawan'=>$id_karyawan,
				'no_sk'=>strtoupper($this->input->post('no_sk')),
				'tgl_sk'=>$this->formatter->getDateFormatDb($this->input->post('tgl_sk')),
				'tgl_berlaku'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku')),
				'tgl_keluar'=>$this->formatter->getDateFormatDb($this->input->post('tgl_keluar')),
				'alasan'=>$this->input->post('alasan'),
				'mengetahui'=>$this->input->post('mengetahui'),
				'menyetujui'=>$this->input->post('menyetujui'),
				'keterangan'=>$this->input->post('keterangan'),
			);
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'karyawan_tidak_aktif',['id_kta'=>$id]);
		}else{
			$datax = $this->messages->notValidParam(); 
		}
	   	echo json_encode($datax);
	}
	function add_karyawan_tidak_aktif(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$kode=$this->input->post('no_sk');
		$id=$this->input->post('id_karyawan');
		if ($kode != "" && $id != "") {
			$data=array(
				'id_karyawan'=>$id,
				'no_sk'=>strtoupper($this->input->post('no_sk')),
				'tgl_sk'=>$this->formatter->getDateFormatDb($this->input->post('tgl_sk')),
				'tgl_berlaku'=>$this->formatter->getDateFormatDb($this->input->post('tgl_berlaku')),
				'tgl_keluar'=>$this->formatter->getDateFormatDb($this->input->post('tgl_keluar')),
				'alasan'=>$this->input->post('alasan'),
				'mengetahui'=>$this->input->post('mengetahui'),
				'menyetujui'=>$this->input->post('menyetujui'),
				'keterangan'=>$this->input->post('keterangan'),
			);
				$data_kar=array('status_emp'=>0);
				$where = array('id_karyawan' => $this->input->post('id_karyawan') );
				$this->model_global->updateQuery($data_kar,'karyawan',$where);
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));

			$datax = $this->model_global->insertQueryCC($data,'karyawan_tidak_aktif',$this->model_karyawan->checkMutasiCode($data['no_sk']));
		}else{
			$datax = $this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
	//--EMP ANAK--//
	public function emp_anak_non(){
		$nik=$this->uri->segment(4);
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_karyawan->getListAnak($nik);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_anak,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];

					$properties=$this->otherfunctions->getPropertiesTable($var);
					$ttl = $d->tempat_lahir_anak.', '.$this->formatter->getDateMonthFormatUser($d->tanggal_lahir_anak);
					$gender = $this->otherfunctions->getGender($d->kelamin_anak);
					$educa = $this->otherfunctions->getEducate($d->pendidikan_anak);
					$aksi = '
						<button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick="view_modal_anak('.$d->id_anak.')"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button>';
					$datax['data'][]=[
						$d->id_anak,
						(!empty($d->nama_anak)) ? $d->nama_anak:$this->otherfunctions->getMark($d->nama_anak),
						(!empty($ttl)) ? $ttl:$this->otherfunctions->getMark($ttl),
						(!empty($gender)) ? $gender:$this->otherfunctions->getMark($gender),
						(!empty($educa)) ? $educa:$this->otherfunctions->getMark($educa),
						(!empty($d->no_telp)) ? $d->no_telp:$this->otherfunctions->getMark($d->no_telp),
						$aksi,
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_anak');
				$data=$this->model_karyawan->getAnak($id,$nik);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_anak,
						'nama_anak'=>$d->nama_anak,
						'nik'=>$d->nik,
						'kelamin_anak'=>$d->kelamin_anak,
						'getkelamin_anak'=>$this->otherfunctions->getGender($d->kelamin_anak),
						'tempat_lahir_anak'=>$d->tempat_lahir_anak,
						'tanggal_lahir_anak'=>$this->formatter->getDateFormatUser($d->tanggal_lahir_anak),
						'getTTL'=>$d->tempat_lahir_anak.', '.$this->formatter->getDateMonthFormatUser($d->tanggal_lahir_anak),
						'pendidikan_anak'=>$d->pendidikan_anak,
						'getPendidikan'=>$this->otherfunctions->getEducate($d->pendidikan_anak),
						'no_telp'=>$d->no_telp,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update)
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
//IZIN CUTI
	public function pilih_k_izin()
	{
		if (!$this->input->is_ajax_request())
			redirect('not_found');
		$data=$this->model_karyawan->getPilihKaryawanMutasi();
		$datax['data']=[];
		foreach ($data as $d) {
			$datax['data'][]=[
				$d->id_karyawan,
				'<a class="pilih" style="cursor:pointer" 
				data-nik			="'.$d->nik.'" 
				data-id_karyawan	="'.$d->id_karyawan.'" 
				data-nama			="'.$d->nama.'" 
				data-jabatan		="'.$d->jabatan.'" 
				data-nama_jabatan	="'.$d->nama_jabatan.'" 
				data-kode_lokasi	="'.$d->loker.'" 
				data-nama_lokasi	="'.$d->nama_loker.'">'.
				$d->nik.'</a>',
				$d->nama,
				$d->nama_jabatan,
				$d->nama_loker,
			];
		}
		echo json_encode($datax);		
	}
//IZIN CUTI
	public function pilih_k_cuti()
	{
		if (!$this->input->is_ajax_request())
			redirect('not_found');
		$data=$this->model_karyawan->getPilihKaryawanMutasi();
		$datax['data']=[];
		foreach ($data as $d) {
			$datax['data'][]=[
				$d->id_karyawan,
				'<a class="pilih_cuti" style="cursor:pointer" 
				data-nik_cuti			="'.$d->nik.'" 
				data-id_karyawan_cuti	="'.$d->id_karyawan.'" 
				data-nama_cuti			="'.$d->nama.'" 
				data-jabatan_cuti		="'.$d->jabatan.'" 
				data-nama_jabatan_cuti	="'.$d->nama_jabatan.'" 
				data-kode_lokasi_cuti	="'.$d->loker.'" 
				data-nama_lokasi_cuti	="'.$d->nama_loker.'">'.
				$d->nik.'</a>',
				$d->nama,
				$d->nama_jabatan,
				$d->nama_loker,
			];
		}
		echo json_encode($datax);		
	}
}