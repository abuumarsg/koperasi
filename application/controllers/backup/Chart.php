<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
     * Code From GFEACORP.
     * Web Developer
     * @author      Galeh Fatma Eko Ardiansa
     * @package     Otherfunctions
     * @copyright   Copyright (c) 2018 GFEACORP
     * @version     1.0, 1 September 2018
     * Email        galeh.fatma@gmail.com
     * Phone        (+62) 85852924304
     */

class Chart extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->date = $this->otherfunctions->getDateNow();

		if ($this->session->has_userdata('adm')) {
			$this->admin = $this->session->userdata('adm')['id'];	 
		}else{ 
			redirect('auth');
		}
	    $this->rando = $this->codegenerator->getPin(6,'number');		
		$dtroot['admin']=$this->model_admin->adm($this->admin);
		$datax['adm'] = array(
				'nama'=>$dtroot['admin']['nama'],
				'email'=>$dtroot['admin']['email'],
				'kelamin'=>$dtroot['admin']['kelamin'],
				'foto'=>$dtroot['admin']['foto'],
				'create'=>$dtroot['admin']['create_date'],
				'update'=>$dtroot['admin']['update_date'],
				'login'=>$dtroot['admin']['last_login'],
				'level'=>$dtroot['admin']['level'],
			);
		$this->dtroot=$datax;
	}
	function index(){
		redirect('pages/dashboard');
	}
	public function chart_datein_employee()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$data_label=$this->model_karyawan->getEmployeeDateInChart()['tahun'];
		$data_fill=$this->model_karyawan->getEmployeeDateInChart()['jumlah'];
		$data_prop[]=[
			'label'=>$this->model_karyawan->getEmployeeDateInChart()['label'],
			'fill'=>'false',
			'borderColor'=>$this->otherfunctions->getRandomColor(),
			'backgroundColor'=>$this->otherfunctions->getRandomColor(),
			'data'=>$data_fill,
		];
		$data=[
			'labels'=>$data_label,
			'datasets'=>$data_prop,
		];
		echo json_encode($data);
	}
	public function chart_gender_employee()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$data_label=$this->model_karyawan->getEmployeeGenderChart()['kelamin'];
		$data_fill=$this->model_karyawan->getEmployeeGenderChart()['jumlah'];
		$data_prop[]=[
			'label'=>$this->model_karyawan->getEmployeeGenderChart()['label'],
			'fill'=>'false',
			'borderColor'=>$this->otherfunctions->getRandomColor(),
			'backgroundColor'=>$this->otherfunctions->getRandomColor(),
			'data'=>$data_fill,
		];
		$data=[
			'labels'=>$data_label,
			'datasets'=>$data_prop,
		];
		echo json_encode($data);
	}
}