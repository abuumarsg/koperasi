<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Publik
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

class Publik extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->link=[];
		$menu=$this->model_master->getListMenuUserActive();
		foreach ($menu as $menux) {
			$this->link[]=$menux->url;
		}
		$dtroot=$this->model_data->getCompanyProfile();
		$datax['data'] = [ 
				'nama'=>$dtroot['name'],
				'nama_kantor'=>$dtroot['name_office'],
				'alamat'=>$dtroot['alamat'],
				'email'=>$dtroot['email'],
				'no_hp'=>$dtroot['no_hp'],
				'maps'=>$dtroot['maps'],
				'visi'=>$dtroot['visi'],
				'misi'=>$dtroot['misi'],
				'moto'=>$dtroot['moto'],
				'sejarah'=>$dtroot['sejarah'],
				'about'=>$dtroot['about'],
				'logo'=>$dtroot['logo'],
				'kontak'=>$this->model_data->getListKontakFO(),
				'gambar_footer'=>$this->model_data->getListGaleriGambar(),
		];
		$this->dtroot=$datax;
		$this_url=$this->uri->segment(3);
	}
	function index(){
		redirect('publik/beranda');
		// $this->load->view('publik/coming_soon');
	}
	function not_found(){
		$this->load->view('user_tem/header');
		$this->load->view('not_found');
		$this->load->view('user_tem/footer');
	}
	public function custom($this_url)
    {
        $row = $this->model_master->customMenuUrl($this_url);
        if(!empty($row)) {
            $data = [
                'id_menu' => $row['id_menu'],
                'nama'=> $row['nama'],
                'url' => $row['url'],
                'icon'=> $row['icon'],
                'isi' => $row['isi'],
            ];
			$this->load->view('user_tem/head',$this->dtroot);
			$this->load->view('user_tem/header',$this->dtroot);
            $this->template->load('publik/custom','publik/custom', decode_spesial($data));
			$this->load->view('user_tem/footer',$this->dtroot);
        }else{
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect('publik');
        }
    }
	function add_message(){
		$data=array(
			'nama'=>$this->input->post('nama'),
			'email'=>$this->input->post('email'),
			'website'=>$this->input->post('website'),
			'subject'=>$this->input->post('subject'),
			'pesan'=>$this->input->post('pesan'),
			'tgl_kirim'=>$this->otherfunctions->getDateNow(),
			'create_date'=>$this->otherfunctions->getDateNow(),
			'update_date'=>$this->otherfunctions->getDateNow(),
			'create_by'=>'2',
			'update_by'=>'2',
		);
		$datax=$this->model_global->insertQuery($data,'inbox');
		if($datax){
			$this->session->set_flashdata('sukses', '<i class="fa fa-check-circle"></i> Pesan Anda Berhasil dikirim, terima kasih');
		}else{
			$this->session->set_flashdata('gagal', '<i class="fa fa-times-circle"></i> Pesan Anda Tidak dikirim, silahkan ulangi lagi');
		}
		redirect('publik/kontak');
	}
	function beranda(){
		$nama_menu="beranda";
		if (in_array($nama_menu, $this->link)) {
			$banner=$this->model_data->getListBannerFO();
			$berita=$this->model_data->getListBeritaFO();
			$data=['banner'=>$banner,
					'berita'=>$berita,
				];
			$this->load->view('user_tem/head',$this->dtroot);
			$this->load->view('user_tem/header',$this->dtroot);
			$this->load->view('publik/beranda',$data);
			$this->load->view('user_tem/footer',$this->dtroot);
		}else{
			redirect('publik/not_found');
		}
	}
	function berita(){
		$nama_menu="berita";
		if (in_array($nama_menu, $this->link)) {
			$berita=$this->model_data->getListBeritaFO();
			$tgl_berita=$this->model_data->getListTanggalBeritaFO();
			$this->load->library('pagination');
			$config	= ['base_url'=>base_url('publik/berita/'),
						'total_rows'=> count($this->model_data->getListBeritaFO()),
						'per_page'=>6,
						'uri_segment'=>3,
					];
			$limit = $config['per_page'];
			$start = ($this->uri->segment(3)) ? ($this->uri->segment(3)) : 0;
			$this->pagination->initialize($config); 
			$berita_pg = $this->model_data->dataBeritaPagination($limit, $start);
			$data=[
					'berita'=>$berita_pg,
					'tgl_berita'=>$tgl_berita,
					'paginasi' 	=> $this->pagination->create_links(),
					'limit'		=> $limit,
					'total'		=> $config['total_rows'],
				];
			$this->load->view('user_tem/head',$this->dtroot);
			$this->load->view('user_tem/header',$this->dtroot);
			$this->load->view('publik/berita',$data);
			$this->load->view('user_tem/footer',$this->dtroot);
		}else{
			redirect('publik/not_found');
		}
	}
	function read_berita(){
		$id=$this->codegenerator->decryptChar($this->uri->segment(3));
		$count=$this->model_data->getListBeritaFOID($id)['counting_reader']+1;
		$this->model_global->updateQuery(['counting_reader'=>$count],'data_berita',['id_berita'=>$id]);
		$berita=$this->model_data->getListBeritaFOID($id);
		$tgl_berita=$this->model_data->getListTanggalBeritaFO();
		$data=[
				'berita'=>$berita,
				'tgl_berita'=>$tgl_berita,
			];
		$this->load->view('user_tem/head',$this->dtroot);
		$this->load->view('user_tem/header',$this->dtroot);
		$this->load->view('publik/berita_detail',$data);
		$this->load->view('user_tem/footer',$this->dtroot);
	}
	function prototipe(){
		$this->load->view('user_tem/head');
		$this->load->view('user_tem/header');
		$this->load->view('publik/contoh1');
		$this->load->view('user_tem/footer');
	}
	function visimisi(){
		$nama_menu="visimisi";
		if (in_array($nama_menu, $this->link)) {
			$data=[
					'comp'=>$this->model_data->getCompanyProfile(),
				];
			$this->load->view('user_tem/head',$this->dtroot);
			$this->load->view('user_tem/header',$this->dtroot);
			$this->load->view('publik/visi_misi',$data);
			$this->load->view('user_tem/footer',$this->dtroot);
		}else{
			redirect('publik/not_found');
		}
	}
	function sejarah(){
		$nama_menu="sejarah";
		if (in_array($nama_menu, $this->link)) {
			$data=[
					'comp'=>$this->model_data->getCompanyProfile(),
				];
			$this->load->view('user_tem/head',$this->dtroot);
			$this->load->view('user_tem/header',$this->dtroot);
			$this->load->view('publik/sejarah',$data);
			$this->load->view('user_tem/footer',$this->dtroot);
		}else{
			redirect('publik/not_found');
		}
	}
	function unduhan(){
		$nama_menu="unduhan";
		if (in_array($nama_menu, $this->link)) {
			$dokumen=$this->model_data->getListDokumenFO();
			$data=[
					'dokumen'=>$dokumen,
					'comp'=>$this->model_data->getCompanyProfile(),
				];
			$this->load->view('user_tem/head',$this->dtroot);
			$this->load->view('user_tem/header',$this->dtroot);
			$this->load->view('publik/download',$data);
			$this->load->view('user_tem/footer',$this->dtroot);
		}else{
			redirect('publik/not_found');
		}
	}
	function kontak(){
		$nama_menu="kontak";
		if (in_array($nama_menu, $this->link)) {
			$data=[
					'kontak'=>$this->model_data->getListKontakFO(),
				];
			$this->load->view('user_tem/head',$this->dtroot);
			$this->load->view('user_tem/header',$this->dtroot);
			$this->load->view('publik/kontak',$data);
			$this->load->view('user_tem/footer',$this->dtroot);
		}else{
			redirect('publik/not_found');
		}
	}
	function gambar(){
		$nama_menu="gambar";
		if (in_array($nama_menu, $this->link)) {
			$this->load->library('pagination');
			$config	= ['base_url'=>base_url('publik/gambar/'),
						'total_rows'=> count($this->model_data->getGambarKategoriFO()),
						'per_page'=>6,
						'uri_segment'=>3,
					];
			$limit = $config['per_page'];
			$start = ($this->uri->segment(3)) ? ($this->uri->segment(3)) : 0;
			$this->pagination->initialize($config); 
			$gambar_pg = $this->model_data->dataGambarPagination($limit, $start);
			$data=[
					'gambar'	=> $gambar_pg,
					'paginasi' 	=> $this->pagination->create_links(),
					'limit'		=> $limit,
					'total'		=> $config['total_rows'],
				];
			// $data=[
			// 		'gambar'=>$this->model_data->getGambarKategoriFO(),
			// 	];
			$this->load->view('user_tem/head',$this->dtroot);
			$this->load->view('user_tem/header',$this->dtroot);
			$this->load->view('publik/gambar',$data);
			$this->load->view('user_tem/footer',$this->dtroot);
		}else{
			redirect('publik/not_found');
		}
	}
	function detail_galeri(){
		$id_kategori=$this->codegenerator->decryptChar($this->uri->segment(3));
		$this->load->library('pagination');
		$config	= ['base_url'=>base_url('publik/detail_galeri/'.$this->uri->segment(3)),
					'total_rows'=> count($this->model_data->getListGambarFOKategori($id_kategori)),
					'per_page'=>6,
					'uri_segment'=>4,
				];
		$limit = $config['per_page'];
		$start = ($this->uri->segment(4)) ? ($this->uri->segment(4)) : 0;
		$this->pagination->initialize($config); 
		$gambar_pg = $this->model_data->dataGambarDetailPagination($id_kategori, $limit, $start);
		$data=[
				'gambar'	=> $gambar_pg,
				'paginasi' 	=> $this->pagination->create_links(),
				'limit'		=> $limit,
				'total'		=> $config['total_rows'],
				'kategori'=>$this->model_master->getKategoriGambarRow($id_kategori)['nama'],
			];
		// $id_kategori=$this->codegenerator->decryptChar($this->uri->segment(3));
		// $gambar=$this->model_data->getListGambarFOKategori($id_kategori);
		// $data=[
		// 		'gambar'=>$gambar,
		// 		'kategori'=>$this->model_master->getKategoriGambarRow($id_kategori)['nama'],
		// 	];
		$this->load->view('user_tem/head',$this->dtroot);
		$this->load->view('user_tem/header',$this->dtroot);
		$this->load->view('publik/gambar_detail',$data);
		$this->load->view('user_tem/footer',$this->dtroot);
	}
	function video(){
		$nama_menu="video";
		if (in_array($nama_menu, $this->link)) {
			$this->load->library('pagination');
			$config	= ['base_url'=>base_url('publik/video/'),
						'total_rows'=> count($this->model_data->getVideoFO()),
						'per_page'=>6,
						'uri_segment'=>3,
					];
			$limit = $config['per_page'];
			$start = ($this->uri->segment(3)) ? ($this->uri->segment(3)) : 0;
			$this->pagination->initialize($config); 
			$video_pg = $this->model_data->dataVideoPagination($limit, $start);
			$data=[
					'video'		=> $video_pg,
					'paginasi' 	=> $this->pagination->create_links(),
					'limit'		=> $limit,
					'total'		=> $config['total_rows'],
				];
			$this->load->view('user_tem/head',$this->dtroot);
			$this->load->view('user_tem/header',$this->dtroot);
			$this->load->view('publik/video',$data);
			$this->load->view('user_tem/footer',$this->dtroot);
		}else{
			redirect('publik/not_found');
		}
	}

}