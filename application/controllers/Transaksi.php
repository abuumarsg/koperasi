<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Transaksi
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

class Transaksi extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->date = $this->otherfunctions->getDateNow();
		if ($this->session->has_userdata('adm')) {
			$this->admin = $this->session->userdata('adm')['id'];	 
		}else{ 
			redirect('auth');
		}	
		$dtroot['admin']=$this->model_admin->adm($this->admin);
		$nm=explode(" ", $dtroot['admin']['nama']);
		$datax['adm'] = [
				'nama'=>$nm[0],
				'namax'=>$dtroot['admin']['nama'],
				'email'=>$dtroot['admin']['email'],
				'kelamin'=>$dtroot['admin']['kelamin'],
				'foto'=>$dtroot['admin']['foto'],
				'create'=>$dtroot['admin']['create_date'],
				'update'=>$dtroot['admin']['update_date'],
				'login'=>$dtroot['admin']['last_login'],
		];
		$this->dtroot=$datax;
	}
	public function index(){
		redirect('pages/dashboard');
	}
//========================================================== PENGELOLAAH ========================================================================
	public function data_anggota()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_data->getListDataAnggota();
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_anggota,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_anggota,
						$d->kode,
						$d->nama,
						$d->tempat_lahir.', '.$this->formatter->getDateFormatUser($d->tanggal_lahir),
						$d->pekerjaan,
						$this->formatter->getDateMonthFormatUser($d->tanggal_masuk),
						$properties['tanggal'],
						$properties['status'],
						$properties['aksi'],
						$d->foto,
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_anggota');
				$data=$this->model_data->getListDataAnggota(['a.id_anggota'=>$id]);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_anggota,
						'kode'=>$d->kode,
						'nama'=>$d->nama,
						'ttl'=>$d->tempat_lahir.', '.$this->formatter->getDateMonthFormatUser($d->tanggal_lahir),
						'ktp'=>$d->no_ktp,
						'no_hp'=>$d->no_hp,
						'kelamin'=>$this->otherfunctions->getGender($d->kelamin),
						'pekerjaan'=>$d->pekerjaan,
						'alamat'=>$d->alamat,
						'tempat_lahir'=>$d->tempat_lahir,
						'tanggal_lahir'=>$this->formatter->getDateFormatUser($d->tanggal_lahir),
						'kelamin_edit'=>$d->kelamin,
						'simpanan_pokok'=>$d->simpanan_pokok,
						'tanggal_masuk_edit'=>$this->formatter->getDateFormatUser($d->tanggal_masuk),
						'foto'=>base_url($d->foto),
						'foto_db'=>$d->foto,
						'tanggal_masuk'=>$this->formatter->getDateMonthFormatUser($d->tanggal_masuk),
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update)
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'kode') {
				$data = $this->codegenerator->kodeDataAnggota();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function add_anggota()
	{
		if (!$this->input->is_ajax_request()) 
			redirect('not_found');
		$no_ktp=$this->input->post('no_ktp');
		if(!empty($no_ktp)){
			$foto=$this->input->post('foto');
			$kode=$this->input->post('kode');
			$nama=$this->input->post('nama');
			$tempat_lahir=$this->input->post('tempat_lahir');
			$tgl_lahir=$this->input->post('tgl_lahir');
			$kelamin=$this->input->post('kelamin');
			$no_hp=$this->input->post('no_hp');
			$simpanan_pokok=$this->input->post('simpanan_pokok');
			$tgl_masuk=$this->input->post('tgl_masuk');
			$alamat=$this->input->post('alamat');
			$other = [
				'kode'=>$kode,
				'nama'=>$nama,
				'no_ktp'=>$no_ktp,
				'tempat_lahir'=>$tempat_lahir,
				'tanggal_lahir'=>$this->formatter->getDateFormatDb($tgl_lahir),
				'kelamin'=>$kelamin,
				'no_hp'=>$no_hp,
				'simpanan_pokok'=>$this->formatter->getFormatMoneyDb($simpanan_pokok),
				'tanggal_masuk'=>$this->formatter->getDateFormatDb($tgl_masuk),
				'alamat'=>$alamat,
				'pekerjaan'=>$this->input->post('pekerjaan'),
			];
			$other2=array_merge($other,$this->model_global->getCreateProperties($this->admin));
			$foto_gender=($other['kelamin'] == 'p')?'userf.png':'user.png';
			$data=[
				'post'=>'foto',
				'data_post'=>$this->input->post('foto', TRUE),
				'table'=>'data_anggota',
				'column'=>'foto',
				'usage'=>'insert',
				'allow_null'=>TRUE,
				'default_dir'=>'asset/img/user-photo/'.$foto_gender,
				'otherdata'=>$other2,
			];
			if(!empty($simpanan_pokok)){
				$kode_tabungan = $this->codegenerator->kodeDataTabungan();
				$dataTab = [
					'kode_tabungan'=>$kode_tabungan,
					'kode_anggota'=>$kode,
					'jenis_tabungan'=>null,
					'tanggal'=>$this->formatter->getDateFormatDb($tgl_masuk),
					'besar_tabungan'=>$this->formatter->getFormatMoneyDb($simpanan_pokok),
				];
				$dataTab=array_merge($dataTab,$this->model_global->getCreateProperties($this->admin));
				$this->model_global->insertQueryNoMsg($dataTab,'data_tabungan');
				$datacc = [
					'kode'=>$this->codegenerator->kodeTransaksiTabungan(),
					'jenis'=>'SIM202104100001',
					'kode_tabungan'=>$kode_tabungan,
					'nominal'=>$this->formatter->getFormatMoneyDb($simpanan_pokok),
					'tanggal'=>date('Y-m-d'),
					'flag'=>'penambah',
					'keterangan'=>'pokok',
				];
				$datacc=array_merge($datacc,$this->model_global->getCreateProperties($this->admin));
				$this->model_global->insertQueryNoMsg($datacc,'transaksi_tabungan');
			}
			$datax=$this->filehandler->doUpload($data,'image','anggota');
		}else{
			$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
	public function edit_anggota()
	{
		if (!$this->input->is_ajax_request()) 
			redirect('not_found');
		$no_ktp=$this->input->post('no_ktp');
		if(!empty($no_ktp)){
			$id_anggota=$this->input->post('id');
			$foto=$this->input->post('foto');
			$kode=$this->input->post('kode');
			$nama=$this->input->post('nama');
			$tempat_lahir=$this->input->post('tempat_lahir');
			$tgl_lahir=$this->input->post('tgl_lahir');
			$kelamin=$this->input->post('kelamin');
			$no_hp=$this->input->post('no_hp');
			$simpanan_pokok=$this->input->post('simpanan_pokok');
			$tgl_masuk=$this->input->post('tgl_masuk');
			$alamat=$this->input->post('alamat');
			$other = [
				'kode'=>$kode,
				'nama'=>$nama,
				'no_ktp'=>$no_ktp,
				'tempat_lahir'=>$tempat_lahir,
				'tanggal_lahir'=>$this->formatter->getDateFormatDb($tgl_lahir),
				'kelamin'=>$kelamin,
				'no_hp'=>$no_hp,
				'simpanan_pokok'=>$simpanan_pokok,
				'tanggal_masuk'=>$this->formatter->getDateFormatDb($tgl_masuk),
				'alamat'=>$alamat,
				'pekerjaan'=>$this->input->post('pekerjaan'),
			];
			$other2=array_merge($other,$this->model_global->getCreateProperties($this->admin));
			$foto_gender=($other['kelamin'] == 'p')?'userf.png':'user.png';
			$data=[
				'post'=>'foto',
				'data_post'=>$this->input->post('foto', TRUE),
				'table'=>'data_anggota',
				'column'=>'foto',
				'usage'=>'update',
				'allow_null'=>TRUE,
				'default_dir'=>$this->input->post('foto_old'),
				'otherdata'=>$other2,
				// 'unlink'=>'yes',
		        'where'=>['id_anggota'=>$id_anggota],
			];
			$datax=$this->filehandler->doUpload($data,'image','anggota');
		}else{
			$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
// =============================== TABUNGAN ==========================================//
	public function data_tabungan()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_data->getListDataTabungan();
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_tabungan,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$aksi = '<a type="button" class="btn btn-danger btn-md" href="'.base_url('pages/data_ambil_uang/'.$d->kode_tabungan).'"><i class="fas fa-donate" data-toggle="tooltip" title="Detail Data"></i>  Ambil Uang</a>';
					$datax['data'][]=[
						$d->id_tabungan,
						$d->kode_tabungan,
						$d->kode_anggota,
						$d->nama_anggota,
						$this->formatter->getDateFormatUser($d->tanggal),
						$this->formatter->getFormatMoneyUser($d->besar_tabungan),
						$properties['tanggal'],
						$aksi,
					];
					$no++;
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
// =============== AMBIL UANG ===================
	public function ambil_uang()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if($usage == 'view_all'){
				$kode_tabungan=$this->input->post('kode_tabungan');
				$data=$this->model_data->getListTransaksiTabungan(['a.kode_tabungan'=>$kode_tabungan,'a.flag'=>'pengurang']);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id,
						$d->kode,
						$d->kode_tabungan,
						$d->kode_anggota,
						$d->nama_anggota,
						$this->formatter->getDateFormatUser($d->tanggal),
						$this->formatter->getFormatMoneyUser($d->nominal),
						$properties['tanggal'],
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif($usage == 'cek_total'){
				$ambil = $this->formatter->getFormatMoneyDb($this->input->post('ambil'));
				$total = $this->formatter->getFormatMoneyDb($this->input->post('total'));
				$sisa = $total-$ambil;
				$data = $this->formatter->getFormatMoneyUser($sisa);
				echo json_encode($data);
			}elseif($usage == 'cek_total_update'){
				$kode_tabungan=$this->input->post('kode_tabungan');
				$tabungan = $this->model_data->getListDataTabungan(['a.kode_tabungan'=>$kode_tabungan],true)['besar_tabungan'];
				$data = $this->formatter->getFormatMoneyUser($tabungan);
				echo json_encode($data);
			}elseif($usage == 'kode'){
				$data = $this->codegenerator->kodeTransaksiTabungan();
				echo json_encode($data);
			}elseif($usage == 'do_add'){
				$kode_tabungan=$this->input->post('kode_tabungan');
				$total=$this->formatter->getFormatMoneyDb($this->input->post('total'));
				$pengambilan=$this->formatter->getFormatMoneyDb($this->input->post('pengambilan'));
				$sisa=$this->formatter->getFormatMoneyDb($this->input->post('sisa'));
				$dataTrans = [
					'kode'=>$this->codegenerator->kodeTransaksiTabungan(),
					'kode_tabungan'=>$kode_tabungan,
					'nominal'=>$pengambilan,
					'tanggal'=>$this->otherfunctions->getDateNow('Y-m-d'),
					'flag'=>'pengurang',
				];
				$dataTrans=array_merge($dataTrans,$this->model_global->getCreateProperties($this->admin));
				$ss = $this->model_global->insertQueryNoMsg($dataTrans,'transaksi_tabungan');
				if($ss){
					$dataTab = [
						'besar_tabungan'=>$sisa,
					];
					$dataTab=array_merge($dataTab,$this->model_global->getUpdateProperties($this->admin));
					$data = $this->model_global->updateQuery($dataTab,'data_tabungan',['kode_tabungan'=>$kode_tabungan]);
				}
				echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
// =============================== DATA PENGAJUAN ==========================================//
	public function data_pengajuan()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_data->getListDataPengajuan();
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					if (isset($access['l_ac']['del'])) {
						if($d->validasi != 1 || $d->status_pinjaman == 1){
							$delete = (in_array($access['l_ac']['del'], $access['access'])) ? '<button type="button" class="btn btn-danger btn-xs"  href="javascript:void(0)" onclick=delete_modal("'.$d->id.'")><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i> Hapus</button> ' : null;
						}else{
							$delete = null;
						}
					}else{
						$delete = null;
					}
					$info = '<button class="btn btn-info btn-xs" href="javascript:void(0)" onclick=view_modal("'.$d->id.'")><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i> View</button> ';
					$disable = ($d->status_pinjaman != 0)?'disabled':null;
					if($d->validasi == 1){
						$angsur = '<a type="button" class="btn btn-success btn-xs" href="'.base_url('pages/data_angsuran/'.$d->id).'" '.$disable.'><i class="fas fa-donate" data-toggle="tooltip" title="Angsur"></i> Angsur</a>';
					}else{
						$angsur = null;
					}
					if (isset($access['l_ac']['apr'])) {
						if(in_array($access['l_ac']['apr'], $access['access'])){
							if($d->validasi==2){
								$validasi = '<button type="button" class="btn btn-warning btn-sm" href="javascript:void(0)" onclick=modal_need("'.$d->id.'")><i class="fa fa-warning"></i> Perlu Validasi</button> ';
							}elseif($d->validasi==1){
								$validasi = '<button type="button" class="btn btn-success btn-sm" href="javascript:void(0)" onclick=modal_yes("'.$d->id.'")><i class="fa fa-check-circle"></i> Disetujui</button> ';
							}elseif($d->validasi==0){
								$validasi = '<button type="button" class="btn btn-danger btn-sm" href="javascript:void(0)" onclick=modal_no("'.$d->id.'")><i class="fa fa-times-circle"></i> Ditolak</button> ';
							}
						}else{
							$validasi = $this->otherfunctions->getStatusPengajuan($d->validasi);
						}
					}else{
						$validasi = null;
					}
					$jumAng=0;
					$ang = $this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$d->kode]);
					if(!empty($ang)){
						$jumAng = count($ang);
					}
					$datax['data'][]=[
						$d->id,
						$d->kode,
						$d->nama_anggota,
						$this->formatter->getDateFormatUser($d->tgl_pengajuan),
						$d->nama_pinjaman,
						$this->formatter->getFormatMoneyUser($d->besar_pinjam),
						$jumAng.' Dari '.$d->lama_pinjam.' Bulan',
						($d->status_pinjaman == 0)?'<label class="label label-warning" style="font-size:12px;"> Belum Lunas</label>':'<label class="label label-success" style="font-size:12px;"> Lunas</label>',
						$validasi,
						// $properties['tanggal'],
						$info.$angsur.$delete,
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif($usage == 'view_one'){
				$id = $this->input->post('id');
				$data=$this->model_data->getListDataPengajuan(['a.id'=>$id]);
				foreach ($data as $d) {
					$banyakAngsuran=$this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$d->kode]);
					if(!empty($banyakAngsuran)){
						$tabel='';
						$tabel.='<h4 align="center"><b>Data Angsuran '.$d->nama.'</b></h4>
						<div style="max-height: 400px; overflow: auto;">
							<table class="table table-bordered table-striped data-table">
	          					<thead>
	          						<tr class="bg-blue">
	          							<th>No.</th>
	          							<th>Kode Angsuran</th>
	          							<th>Angsuran Ke</th>
	          							<th>Besar Angsuran</th>
	          							<th>Tanggal</th>
	          						</tr>
	          					</thead>
	          					<tbody>';
								  if(!empty($banyakAngsuran)){
										$no=1;
										foreach ($banyakAngsuran as $d_p) {
											$tabel.='<tr>
												<td>'.$no.'</td>
												<td>'.$d_p->kode.'</td>
												<td>'.$d_p->angsuran_ke.'</td>
												<td>'.$this->formatter->getFormatMoneyUser($d_p->nominal).'</td>
												<td>'.$this->formatter->getDateFormatUser($d_p->tanggal).'</td>
											</tr>';
											$no++;
										}
									}else{
										$tabel.='<tr>
											<td colspan="4>Tidak Ada Data</td>
										</tr>';
									}
		          				$tabel.='</tbody>
		          			</table>
						</div>';
					}
					$table_kurang = '<table class="table" border="3">
					<tr>
					<th>Kurang Bayar</th>
					<th>'.$this->formatter->getFormatMoneyUser($d->sisa_pinjam).'</th>
					</tr>';
					$table_kurang .= '</table>';
					$datax=[
						'id'=>$d->id,
						'kode'=>$d->kode,
						'nama'=>$d->nama_anggota,
						'tanggal'=>$this->formatter->getDateFormatUser($d->tgl_pengajuan),
						'jenis'=>$d->nama_pinjaman,
						'besar_pinjaman'=>$this->formatter->getFormatMoneyUser($d->besar_pinjam),
						'lama_pinjam'=>$d->lama_pinjam,
						'maksimal'=>$this->formatter->getFormatMoneyUser($d->maksimal),
						'bunga'=>$d->bunga,
						'keterangan'=>$d->keterangan,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'jenis_e'=>$d->jenis_pinjam,
						'sudah_angsur'=>$d->sudah_angsur,
						'angsuran'=>$this->formatter->getFormatMoneyUser($d->angsuran),
						'banyakAngsuran'=>count($banyakAngsuran),
						'tabel'=>$tabel,
						'table_kurang'=>$table_kurang,
					];
				}
				echo json_encode($datax);
			}elseif($usage == 'view_select'){
				$kode_pinjaman = $this->input->post('kode_pinjaman');
				$nominal = $this->formatter->getFormatMoneyDb($this->input->post('nominal'));
				$data = $this->model_master->getMasterPinjamanKode($kode_pinjaman);
				$angsuran_pokok = $nominal/$data['lama_pinjam'];
				$angsuran_bunga = (($data['bunga']/100)*$nominal)/$data['lama_pinjam'];
				$tanggal = $this->input->post('tanggal');
				$tempo = null;
				if(!empty($tanggal)){
					$bulan = $data['lama_pinjam'];
					$tgl = $this->formatter->getDateFormatDb($tanggal);
					$tglx = explode('-', $tgl);
					$tgl = mktime(0, 0, 0, date($tglx[1])+$bulan, date($tglx[2]), date($tglx[0]));
					$tempo = date("d/m/Y", $tgl);
				}
				$datax = [
					'lama_pinjam'=>$data['lama_pinjam'].' Bulan',
					'maksimal'=>$this->formatter->getFormatMoneyUser($data['maksimal']),
					'bunga'=>$data['bunga'].' %',
					'angsuran'=>$this->formatter->getFormatMoneyUser(($angsuran_pokok+$angsuran_bunga)),
					'tempo'=>$tempo,
				];
				// print_r($nominal);
				echo json_encode($datax);
			}elseif($usage == 'kode'){
				$data = $this->codegenerator->kodeTransaksiPengajuan();
				echo json_encode($data);
			}elseif($usage == 'do_add'){
				$kode=$this->input->post('kode');
				$anggota=$this->input->post('anggota');
				$tanggal=$this->input->post('tanggal');
				$tanggal_tempo=$this->input->post('tanggal_tempo');
				$nominal=$this->formatter->getFormatMoneyDb($this->input->post('nominal'));
				$jenis=$this->input->post('jenis');
				$lama_angsur=$this->input->post('lama_angsur');
				// $max_pinjam=$this->input->post('max_pinjam');
				// $bunga=$this->input->post('bunga');
				$angsuran=$this->formatter->getFormatMoneyDb($this->input->post('angsuran'));
				$keterangan=$this->input->post('keterangan');
				$data = [
					'kode'=>$kode,
					'kode_anggota'=>$anggota,
					'besar_pinjam'=>$nominal,
					'jenis_pinjam'=>$jenis,
					'angsuran'=>$angsuran,
					'sisa_pinjam'=>$angsuran*$lama_angsur,
					'tgl_pengajuan'=>$this->formatter->getDateFormatDb($tanggal),
					'tgl_tempo'=>$this->formatter->getDateFormatDb($tanggal_tempo),
					'validasi'=>2,
					'status_pinjaman'=>0,
					'keterangan'=>$keterangan,
				];
				$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
				$datax = $this->model_global->insertQuery($data,'data_pengajuan');
				echo json_encode($datax);
			}elseif($usage == 'do_edit'){
				$id=$this->input->post('id');
				$tanggal=$this->input->post('tanggal');
				$jenis=$this->input->post('jenis');
				$keterangan=$this->input->post('keterangan');
				$nominal=$this->formatter->getFormatMoneyDb($this->input->post('nominal'));
				$angsuran=$this->formatter->getFormatMoneyDb($this->input->post('angsuran'));
				$keterangan=$this->input->post('keterangan');
				$data = [
					'besar_pinjam'=>$nominal,
					'jenis_pinjam'=>$jenis,
					'angsuran'=>$angsuran,
					'tgl_pengajuan'=>$this->formatter->getDateFormatDb($tanggal),
					'validasi'=>2,
					'status_pinjaman'=>0,
					'keterangan'=>$keterangan,
				];
				$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
				$datax = $this->model_global->updateQuery($data,'data_pengajuan',['id'=>$id]);
				echo json_encode($datax);
			}elseif($usage == 'validasi_pengajuan'){
				$id=$this->input->post('id');
				$val_db=$this->input->post('validasi_db');
				$vali=$this->input->post('validasi');
				//Jika ada kondisi Lain
				if(($val_db==2 && $vali==1) || ($val_db==0 && $vali==1)){
					$data=['validasi'=>$vali];
					$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
					$where=['id'=>$id,'validasi'=>$val_db];
					$datax=$this->model_global->updateQuery($data,'data_pengajuan',$where);
				}elseif($val_db==1 && $vali==0){
					$data=['validasi'=>$vali];
					$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
					$where=['id'=>$id,'validasi'=>$val_db];
					$datax=$this->model_global->updateQuery($data,'data_pengajuan',$where);
				}else{
					$data=['validasi'=>$vali];
					$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
					$where=['id'=>$id,'validasi'=>$val_db];
					$datax=$this->model_global->updateQuery($data,'data_pengajuan',$where);
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
// =============================== DATA ANGSURAN ==========================================//
	public function data_angsuran()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$kode_pinjaman = $this->input->post('kode_pinjaman');
				$data=$this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$kode_pinjaman]);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					if (isset($access['l_ac']['del'])) {
						$delete = (in_array($access['l_ac']['del'], $access['access'])) ? '<button type="button" class="btn btn-danger btn-sm"  href="javascript:void(0)" onclick=delete_modal("'.$d->id.'")><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i></button> ' : null;
					}else{
						$delete = null;
					}
					$info = '<button class="btn btn-info btn-xs" href="javascript:void(0)" onclick=view_modal("'.$d->id.'")><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i> View</button> ';
					$datax['data'][]=[
						$d->id,
						$d->kode,
						$d->nama_anggota,
						$d->kode_pengajuan,
						$this->formatter->getFormatMoneyUser($d->nominal),
						$d->angsuran_ke,
						$this->formatter->getDateFormatUser($d->tanggal),
						$info,//.$angsur,//.$delete,
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif($usage == 'view_one'){
				$id = $this->input->post('id');
				$data=$this->model_data->getListTransaksiAngsuran(['a.id'=>$id]);
				foreach ($data as $d) {
					$datax = [
						'kode'=>$d->kode,
						'nama'=>$d->nama_anggota,
						'kode_pinjaman'=>$d->kode_pengajuan,
						'nominal'=>$this->formatter->getFormatMoneyUser($d->nominal),
						'angsuran_ke'=>$d->angsuran_ke,
						'tanggal'=>$this->formatter->getDateFormatUser($d->tanggal),
						'denda'=>$this->formatter->getFormatMoneyUser($d->denda),
						'keterangan'=>$d->keterangan,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
					];
				}
				echo json_encode($datax);
			}elseif($usage == 'kode'){
				$data = $this->codegenerator->kodeTransaksiAngsuran();
				echo json_encode($data);
			}elseif($usage == 'cekAngsuran'){
				$kode_pinjaman = $this->input->post('kode');
				$banyakAngsuran=$this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$kode_pinjaman]);
				$data = [
					'jumlahAngsuran'=>count($banyakAngsuran),
					'angsuran_ke'=>count($banyakAngsuran)+1,
				];
				echo json_encode($data);
			}elseif($usage == 'do_add'){
				$kode=$this->input->post('kode');
				$kode_pinjaman=$this->input->post('kode_pinjaman');
				$tanggal=$this->formatter->getFormatMoneyDb($this->input->post('tanggal'));
				$nominal_angsuran=$this->formatter->getFormatMoneyDb($this->input->post('nominal_angsuran'));
				$angsuran_ke=$this->input->post('angsuran_ke');
				$denda=$this->formatter->getFormatMoneyDb($this->input->post('denda'));
				$keterangan=$this->input->post('keterangan');
				$sisa_saldo=$this->input->post('sisa_saldo');
				$data = [
					'kode'=>$kode,
					'kode_pengajuan'=>$kode_pinjaman,
					'nominal'=>$nominal_angsuran,
					'angsuran_ke'=>$angsuran_ke,
					'tanggal'=>$this->formatter->getDateFormatDb($tanggal),
					'denda'=>$denda,
					'keterangan'=>$keterangan,
				];
				$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
				$datax = $this->model_global->insertQuery($data,'transaksi_angsuran');
				if($datax){
					// $Angsuran=$this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$kode_pinjaman]);
					$Pinjaman =$this->model_data->getListDataPengajuan(['a.kode'=>$kode_pinjaman],true);
					$status_pinjaman = ($angsuran_ke==$Pinjaman['lama_pinjam'])?'1':'0';
					$sisaSaldo = $sisa_saldo-$nominal_angsuran;
					$data = [
						'sisa_pinjam'=>$sisaSaldo,
						'status_pinjaman'=>$status_pinjaman,
					];
					$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
					$datax = $this->model_global->updateQuery($data,'data_pengajuan',['kode'=>$kode_pinjaman]);
				}
				echo json_encode($datax);
			}elseif($usage == 'do_edit'){
				$id=$this->input->post('id');
				$tanggal=$this->input->post('tanggal');
				$jenis=$this->input->post('jenis');
				$keterangan=$this->input->post('keterangan');
				$nominal=$this->formatter->getFormatMoneyDb($this->input->post('nominal'));
				$angsuran=$this->formatter->getFormatMoneyDb($this->input->post('angsuran'));
				$keterangan=$this->input->post('keterangan');
				$data = [
					'besar_pinjam'=>$nominal,
					'jenis_pinjam'=>$jenis,
					'angsuran'=>$angsuran,
					'tgl_pengajuan'=>$this->formatter->getDateFormatDb($tanggal),
					'validasi'=>2,
					'status_pinjaman'=>0,
					'keterangan'=>$keterangan,
				];
				$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
				$datax = $this->model_global->updateQuery($data,'data_pengajuan',['id'=>$id]);
				echo json_encode($datax);
			}else{

			}
		}
	}
// =============================== DATA TRANSAKSI ==========================================//
	public function data_transaksi()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_data->getListDataAnggota(['a.status'=>1]);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_anggota,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$info = '<a type="button" class="btn btn-info btn-sm" href="'.base_url('pages/data_simpanan/'.$d->kode).'"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i> Simpanan</a> ';
					$angsur = '<a type="button" class="btn btn-success btn-sm" href="'.base_url('pages/data_pinjaman/'.$d->kode).'"><i class="fas fa-donate" data-toggle="tooltip" title="Angsur"></i> Pinjam | <i class="fas fa-donate" data-toggle="tooltip" title="Angsur"></i> Angsur</a>';
					$datax['data'][]=[
						$d->id_anggota,
						$d->kode,
						$d->nama,
						$d->pekerjaan,
						$this->formatter->getDateFormatUser($d->tanggal_masuk),
						$info.$angsur,
						$d->foto,
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif($usage == 'view_one'){
				// $id = $this->input->post('id');
				// $data=$this->model_data->getListDataPengajuan(['a.id'=>$id]);
				// foreach ($data as $d) {
				// 	$banyakAngsuran=$this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$d->kode]);
				// 	if(!empty($banyakAngsuran)){
				// 		$tabel='';
				// 		$tabel.='<h4 align="center"><b>Data Angsuran '.$d->nama.'</b></h4>
				// 		<div style="max-height: 400px; overflow: auto;">
				// 			<table class="table table-bordered table-striped data-table">
	          	// 				<thead>
	          	// 					<tr class="bg-blue">
	          	// 						<th>No.</th>
	          	// 						<th>Kode Angsuran</th>
	          	// 						<th>Angsuran Ke</th>
	          	// 						<th>Besar Angsuran</th>
	          	// 						<th>Tanggal</th>
	          	// 					</tr>
	          	// 				</thead>
	          	// 				<tbody>';
				// 				  if(!empty($banyakAngsuran)){
				// 						$no=1;
				// 						foreach ($banyakAngsuran as $d_p) {
				// 							$tabel.='<tr>
				// 								<td>'.$no.'</td>
				// 								<td>'.$d_p->kode.'</td>
				// 								<td>'.$d_p->angsuran_ke.'</td>
				// 								<td>'.$this->formatter->getFormatMoneyUser($d_p->nominal).'</td>
				// 								<td>'.$this->formatter->getDateFormatUser($d_p->tanggal).'</td>
				// 							</tr>';
				// 							$no++;
				// 						}
				// 					}else{
				// 						$tabel.='<tr>
				// 							<td colspan="4>Tidak Ada Data</td>
				// 						</tr>';
				// 					}
		        //   				$tabel.='</tbody>
		        //   			</table>
				// 		</div>';
				// 	}
				// 	$table_kurang = '<table class="table" border="3">
				// 	<tr>
				// 	<th>Kurang Bayar</th>
				// 	<th>'.$this->formatter->getFormatMoneyUser($d->sisa_pinjam).'</th>
				// 	</tr>';
				// 	$table_kurang .= '</table>';
				// 	$datax=[
				// 		'id'=>$d->id,
				// 		'kode'=>$d->kode,
				// 		'nama'=>$d->nama_anggota,
				// 		'tanggal'=>$this->formatter->getDateFormatUser($d->tgl_pengajuan),
				// 		'jenis'=>$d->nama_pinjaman,
				// 		'besar_pinjaman'=>$this->formatter->getFormatMoneyUser($d->besar_pinjam),
				// 		'lama_pinjam'=>$d->lama_pinjam,
				// 		'maksimal'=>$this->formatter->getFormatMoneyUser($d->maksimal),
				// 		'bunga'=>$d->bunga,
				// 		'keterangan'=>$d->keterangan,
				// 		'status'=>$d->status,
				// 		'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
				// 		'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
				// 		'create_by'=>$d->create_by,
				// 		'update_by'=>$d->update_by,
				// 		'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
				// 		'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
				// 		'jenis_e'=>$d->jenis_pinjam,
				// 		'sudah_angsur'=>$d->sudah_angsur,
				// 		'angsuran'=>$this->formatter->getFormatMoneyUser($d->angsuran),
				// 		'banyakAngsuran'=>count($banyakAngsuran),
				// 		'tabel'=>$tabel,
				// 		'table_kurang'=>$table_kurang,
				// 	];
				// }
				// echo json_encode($datax);
				$data=$this->model_data->getListDataAnggota(['a.status'=>1]);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_anggota,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$info = '<a type="button" class="btn btn-info btn-sm" href="'.base_url('pages/data_simpanan/'.$d->kode).'"><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i> Simpanan</a> ';
					$angsur = '<a type="button" class="btn btn-success btn-sm" href="'.base_url('pages/data_pinjaman/'.$d->kode).'"><i class="fas fa-donate" data-toggle="tooltip" title="Angsur"></i> Pinjam | <i class="fas fa-donate" data-toggle="tooltip" title="Angsur"></i> Angsur</a>';
					$datax['data'][]=[
						$d->id_anggota,
						$d->kode,
						$d->nama,
						$d->pekerjaan,
						$this->formatter->getDateFormatUser($d->tanggal_masuk),
						$info.$angsur,
						$d->foto,
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif($usage == 'simpanan_all'){
				$kode_tabungan=$this->input->post('kode_tabungan');
				$data=$this->model_data->getListTransaksiTabungan(['a.kode_tabungan'=>$kode_tabungan]);
				// $data=$this->model_data->getListTransaksiTabungan(['a.kode_tabungan'=>$kode_tabungan,'a.flag'=>'penambah']);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$jenis = (!empty($d->jenis))?$d->nama_jenis:$this->otherfunctions->getMark();
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id,
						($d->flag == 'pengurang')?'<span style="color:red;">'.$d->kode.'</span>':$d->kode,
						($d->flag == 'pengurang')?'<span style="color:red;">'.$this->formatter->getDateFormatUser($d->tanggal).'</span>':$this->formatter->getDateFormatUser($d->tanggal),
						$jenis,
						($d->flag == 'pengurang')?'<span style="color:red;">Pengurang</span>':'Penambah',
						($d->flag == 'pengurang')?'<span style="color:red;">'.$this->formatter->getFormatMoneyUser($d->nominal).'</span>':$this->formatter->getFormatMoneyUser($d->nominal),
						$properties['tanggal'],
						$properties['info'],
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif($usage == 'simpanan_one'){
				$id = $this->input->post('id');
				$data=$this->model_data->getListTransaksiTabungan(['a.id'=>$id]);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id,
						'kode'=>($d->flag == 'pengurang')?'<span style="color:red;">'.$d->kode.'</span>':$d->kode,
						'nama'=>($d->flag == 'pengurang')?'<span style="color:red;">'.$d->nama_anggota.'</span>':$d->nama_anggota,
						'tanggal'=>($d->flag == 'pengurang')?'<span style="color:red;">'.$this->formatter->getDateFormatUser($d->tanggal).'</span>':$this->formatter->getDateFormatUser($d->tanggal),
						'kode_simpanan'=>($d->flag == 'pengurang')?'<span style="color:red;">'.$d->kode_tabungan.'</span>':$d->kode_tabungan,
						'nominal'=>($d->flag == 'pengurang')?'<span style="color:red;">'.$this->formatter->getFormatMoneyUser($d->nominal).'</span>':$this->formatter->getFormatMoneyUser($d->nominal),
						'jenis_simpanan'=>(!empty($d->jenis))?$d->nama_jenis:$this->otherfunctions->getMark(),
						'plus'=>($d->flag == 'pengurang')?'<span style="color:red;">Pengurang</span>':'Penambah',
						'e_kode'=>$d->kode,
						'e_nama'=>$d->nama_anggota,
						'e_tanggal'=>$this->formatter->getDateFormatUser($d->tanggal),
						'e_kode_simpanan'=>$d->kode_tabungan,
						'e_nominal'=>$this->formatter->getFormatMoneyUser($d->nominal),
						'e_jenis_simpanan'=>$d->jenis,
						'flag'=>$d->flag,
						'keterangan'=>$d->keterangan,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'total_simpanan'=>$d->besar_tabungan,
					];
				}
				echo json_encode($datax);
			}elseif($usage == 'view_select'){
				$kode = $this->input->post('kode');
				$data = $this->model_master->getMasterSimpananKode($kode);
				$datax = [
					'besar_simpanan'=>$this->formatter->getFormatMoneyUser($data['besar_simpanan']),
				];
				echo json_encode($datax);
			}elseif($usage == 'do_add'){
				$kode_tabungan = $this->input->post('kode_tabungan');
				$jenis = $this->input->post('jenis');
				$besar_simpanan = $this->formatter->getFormatMoneyDb($this->input->post('besar_simpanan'));
				$tanggal = $this->input->post('tanggal');
				$keterangan=$this->input->post('keterangan');
				$data = [
					'kode'=>$this->codegenerator->kodeTransaksiTabungan(),
					'jenis'=>$jenis,
					'kode_tabungan'=>$kode_tabungan,
					'nominal'=>$besar_simpanan,
					'tanggal'=>$this->formatter->getDateFormatDb($tanggal),
					'flag'=>'penambah',
					'keterangan'=>$keterangan,
				];
				$tab = $this->model_data->getListDataTabungan(['kode_tabungan'=>$kode_tabungan],true);
				$dataTab['besar_tabungan']=$tab['besar_tabungan']+$besar_simpanan;
				$dataTab=array_merge($dataTab,$this->model_global->getUpdateProperties($this->admin));
				$this->model_global->updateQueryNoMsg($dataTab,'data_tabungan',['kode_tabungan'=>$kode_tabungan]);
				$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
				$datax = $this->model_global->insertQuery($data,'transaksi_tabungan');
				echo json_encode($datax);
			}elseif($usage == 'do_edit'){
				$id = $this->input->post('id');
				$tanggal = $this->formatter->getDateFormatUser($this->input->post('tanggal'));
				$nominal = $this->formatter->getFormatMoneyDb($this->input->post('nominal'));
				$jenis = $this->input->post('jenis');
				$keterangan=$this->input->post('keterangan');
				$besar_simpanan=$this->formatter->getFormatMoneyDb($this->input->post('besar_simpanan'));
				$total_simpanan=$this->formatter->getFormatMoneyDb($this->input->post('total_simpanan'));
				$nominal_old=$this->formatter->getFormatMoneyDb($this->input->post('nominal_old'));
				$data = [
					'kode'=>$this->codegenerator->kodeTransaksiTabungan(),
					'jenis'=>$jenis,
					'kode_tabungan'=>$kode_tabungan,
					'nominal'=>$besar_simpanan,
					'tanggal'=>$this->formatter->getDateFormatDb($tanggal),
					'flag'=>'penambah',
					'keterangan'=>$keterangan,
				];
				$tab = $this->model_data->getListDataTabungan(['kode_tabungan'=>$kode_tabungan],true);
				$dataTab['besar_tabungan']=$tab['besar_tabungan']+$besar_simpanan;
				$dataTab=array_merge($dataTab,$this->model_global->getUpdateProperties($this->admin));
				$this->model_global->updateQueryNoMsg($dataTab,'data_tabungan',['kode_tabungan'=>$kode_tabungan]);
				$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
				$datax = $this->model_global->insertQuery($data,'transaksi_tabungan');
				echo json_encode($datax);
			}elseif($usage == 'view_total_simpanan'){
				$kode_tabungan = $this->input->post('kode');
				$tab = $this->model_data->getListDataTabungan(['kode_tabungan'=>$kode_tabungan],true);
				$datax = [
					'besar_simpanan'=>$this->formatter->getFormatMoneyUser($tab['besar_tabungan']),
				];
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
//=================================================== DATA PINJAMAN ========================================================//
	public function data_pinjaman()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$kode_anggota = $this->input->post('kode_anggota');
				$data=$this->model_data->getListDataPengajuan(['a.kode_anggota'=>$kode_anggota]);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					if (isset($access['l_ac']['del'])) {
						if($d->validasi != 1 || $d->status_pinjaman == 1){
							$delete = (in_array($access['l_ac']['del'], $access['access'])) ? '<button type="button" class="btn btn-danger btn-xs"  href="javascript:void(0)" onclick=delete_modal("'.$d->id.'")><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i> Hapus</button> ' : null;
						}else{
							$delete = null;
						}
					}else{
						$delete = null;
					}
					$info = '<button class="btn btn-info btn-xs" href="javascript:void(0)" onclick=view_modal("'.$d->id.'")><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i> View</button> ';
					$disable = ($d->status_pinjaman != 0)?'disabled':null;
					if($d->validasi == 1){
						$angsur = '<a type="button" class="btn btn-success btn-xs" href="'.base_url('pages/data_angsur/'.$d->id).'" '.$disable.'><i class="fas fa-donate" data-toggle="tooltip" title="Angsur"></i> Angsur</a>';
					}else{
						$angsur = null;
					}
					if (isset($access['l_ac']['apr'])) {
						if(in_array($access['l_ac']['apr'], $access['access'])){
							if($d->validasi==2){
								$validasi = '<button type="button" class="btn btn-warning btn-sm" href="javascript:void(0)" onclick=modal_need("'.$d->id.'")><i class="fa fa-warning"></i> Perlu Validasi</button> ';
							}elseif($d->validasi==1){
								$validasi = '<button type="button" class="btn btn-success btn-sm" href="javascript:void(0)" onclick=modal_yes("'.$d->id.'")><i class="fa fa-check-circle"></i> Disetujui</button> ';
							}elseif($d->validasi==0){
								$validasi = '<button type="button" class="btn btn-danger btn-sm" href="javascript:void(0)" onclick=modal_no("'.$d->id.'")><i class="fa fa-times-circle"></i> Ditolak</button> ';
							}
						}else{
							$validasi = $this->otherfunctions->getStatusPengajuan($d->validasi);
						}
					}else{
						$validasi = null;
					}
					$kode = $this->formatter->encryptChar($d->kode);
					$print = '<a type="button" class="btn btn-warning btn-xs" href="'.base_url('pages/print_data_angsuran/'.$kode).'"><i class="fas fa-print" data-toggle="tooltip" title="Print"></i> Print</a>';
					$jumAng=0;
					$ang = $this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$d->kode]);
					if(!empty($ang)){
						$jumAng = count($ang);
					}
					$datax['data'][]=[
						$d->id,
						$d->kode,
						$d->nama_anggota,
						$this->formatter->getDateFormatUser($d->tgl_pengajuan),
						$d->nama_pinjaman,
						$this->formatter->getFormatMoneyUser($d->besar_pinjam),
						$jumAng.' Dari '.$d->lama_pinjam.' Bulan',
						($d->status_pinjaman == 0)?'<label class="label label-warning" style="font-size:12px;"> Belum Lunas</label>':'<label class="label label-success" style="font-size:12px;"> Lunas</label>',
						// $validasi,
						// $properties['tanggal'],
						$info.$print.$angsur.$delete,
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif($usage == 'view_one'){
				$id = $this->input->post('id');
				$data=$this->model_data->getListDataPengajuan(['a.id'=>$id]);
				foreach ($data as $d) {
					$banyakAngsuran=$this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$d->kode]);
					if(!empty($banyakAngsuran)){
						$tabel='';
						$tabel.='<h4 align="center"><b>Data Angsuran '.$d->nama.'</b></h4>
						<div style="max-height: 400px; overflow: auto;">
							<table class="table table-bordered table-striped data-table">
	          					<thead>
	          						<tr class="bg-blue">
	          							<th>No.</th>
	          							<th>Kode Angsuran</th>
	          							<th>Angsuran Ke</th>
	          							<th>Besar Angsuran</th>
	          							<th>Tanggal</th>
	          						</tr>
	          					</thead>
	          					<tbody>';
								  if(!empty($banyakAngsuran)){
										$no=1;
										foreach ($banyakAngsuran as $d_p) {
											$tabel.='<tr>
												<td>'.$no.'</td>
												<td>'.$d_p->kode.'</td>
												<td>'.$d_p->angsuran_ke.'</td>
												<td>'.$this->formatter->getFormatMoneyUser($d_p->nominal).'</td>
												<td>'.$this->formatter->getDateFormatUser($d_p->tanggal).'</td>
											</tr>';
											$no++;
										}
									}else{
										$tabel.='<tr>
											<td colspan="4>Tidak Ada Data</td>
										</tr>';
									}
		          				$tabel.='</tbody>
		          			</table>
						</div>';
					}
					$table_kurang = '<table class="table" border="3">
					<tr>
					<th>Kurang Bayar</th>
					<th>'.$this->formatter->getFormatMoneyUser($d->sisa_pinjam).'</th>
					</tr>';
					$table_kurang .= '</table>';
					$datax=[
						'id'=>$d->id,
						'kode'=>$d->kode,
						'nama'=>$d->nama_anggota,
						'tanggal'=>$this->formatter->getDateFormatUser($d->tgl_pengajuan),
						'jenis'=>$d->nama_pinjaman,
						'besar_pinjaman'=>$this->formatter->getFormatMoneyUser($d->besar_pinjam),
						'lama_pinjam'=>$d->lama_pinjam,
						'maksimal'=>$this->formatter->getFormatMoneyUser($d->maksimal),
						'bunga'=>$d->bunga,
						'keterangan'=>$d->keterangan,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
						'jenis_e'=>$d->jenis_pinjam,
						'sudah_angsur'=>$d->sudah_angsur,
						'angsuran'=>$this->formatter->getFormatMoneyUser($d->angsuran),
						'banyakAngsuran'=>count($banyakAngsuran),
						'tabel'=>$tabel,
						'table_kurang'=>$table_kurang,
					];
				}
				echo json_encode($datax);
			}elseif($usage == 'view_select'){
				$kode_pinjaman = $this->input->post('kode_pinjaman');
				$nominal = $this->formatter->getFormatMoneyDb($this->input->post('nominal'));
				$data = $this->model_master->getMasterPinjamanKode($kode_pinjaman);
				$angsuran_pokok = $nominal/$data['lama_pinjam'];
				$angsuran_bunga = (($data['bunga']/100)*$nominal)/$data['lama_pinjam'];
				$datax = [
					'lama_pinjam'=>$data['lama_pinjam'].' Bulan',
					'maksimal'=>$this->formatter->getFormatMoneyUser($data['maksimal']),
					'bunga'=>$data['bunga'].' %',
					'angsuran'=>$this->formatter->getFormatMoneyUser(($angsuran_pokok+$angsuran_bunga)),
				];
				// print_r($nominal);
				echo json_encode($datax);
			}elseif($usage == 'kode'){
				$data = $this->codegenerator->kodeTransaksiPengajuan();
				echo json_encode($data);
			}elseif($usage == 'do_add'){
				$kode=$this->input->post('kode');
				$anggota=$this->input->post('anggota');
				$tanggal=$this->input->post('tanggal');
				$nominal=$this->formatter->getFormatMoneyDb($this->input->post('nominal'));
				$jenis=$this->input->post('jenis');
				$lama_angsur=$this->input->post('lama_angsur');
				// $max_pinjam=$this->input->post('max_pinjam');
				// $bunga=$this->input->post('bunga');
				$angsuran=$this->formatter->getFormatMoneyDb($this->input->post('angsuran'));
				$keterangan=$this->input->post('keterangan');
				$data = [
					'kode'=>$kode,
					'kode_anggota'=>$anggota,
					'besar_pinjam'=>$nominal,
					'jenis_pinjam'=>$jenis,
					'angsuran'=>$angsuran,
					'sisa_pinjam'=>$angsuran*$lama_angsur,
					'tgl_pengajuan'=>$this->formatter->getDateFormatDb($tanggal),
					'validasi'=>2,
					'status_pinjaman'=>0,
					'keterangan'=>$keterangan,
				];
				$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
				$datax = $this->model_global->insertQuery($data,'data_pengajuan');
				echo json_encode($datax);
			}elseif($usage == 'do_edit'){
				$id=$this->input->post('id');
				$tanggal=$this->input->post('tanggal');
				$jenis=$this->input->post('jenis');
				$keterangan=$this->input->post('keterangan');
				$nominal=$this->formatter->getFormatMoneyDb($this->input->post('nominal'));
				$angsuran=$this->formatter->getFormatMoneyDb($this->input->post('angsuran'));
				$keterangan=$this->input->post('keterangan');
				$data = [
					'besar_pinjam'=>$nominal,
					'jenis_pinjam'=>$jenis,
					'angsuran'=>$angsuran,
					'tgl_pengajuan'=>$this->formatter->getDateFormatDb($tanggal),
					'validasi'=>2,
					'status_pinjaman'=>0,
					'keterangan'=>$keterangan,
				];
				$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
				$datax = $this->model_global->updateQuery($data,'data_pengajuan',['id'=>$id]);
				echo json_encode($datax);
			}elseif($usage == 'validasi_pengajuan'){
				$id=$this->input->post('id');
				$val_db=$this->input->post('validasi_db');
				$vali=$this->input->post('validasi');
				//Jika ada kondisi Lain
				if(($val_db==2 && $vali==1) || ($val_db==0 && $vali==1)){
					$data=['validasi'=>$vali];
					$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
					$where=['id'=>$id,'validasi'=>$val_db];
					$datax=$this->model_global->updateQuery($data,'data_pengajuan',$where);
				}elseif($val_db==1 && $vali==0){
					$data=['validasi'=>$vali];
					$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
					$where=['id'=>$id,'validasi'=>$val_db];
					$datax=$this->model_global->updateQuery($data,'data_pengajuan',$where);
				}else{
					$data=['validasi'=>$vali];
					$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
					$where=['id'=>$id,'validasi'=>$val_db];
					$datax=$this->model_global->updateQuery($data,'data_pengajuan',$where);
				}
				echo json_encode($datax);
			}elseif($usage == 'view_total_angsuran'){
				$kode_anggota = $this->input->post('kode');
				$pinjaman = $this->model_data->getListDataPengajuan(['a.kode_anggota'=>$kode_anggota]);
				$besar_angsuran = 0;
				foreach ($pinjaman as $p) {
					$besar_angsuran += $p->sisa_pinjam;
				}
				$datax = [
					'besar_angsuran'=>$this->formatter->getFormatMoneyUser($besar_angsuran),
				];
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function data_angsur()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$kode_pinjaman = $this->input->post('kode_pinjaman');
				$data=$this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$kode_pinjaman]);
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					if (isset($access['l_ac']['del'])) {
						$delete = (in_array($access['l_ac']['del'], $access['access'])) ? '<button type="button" class="btn btn-danger btn-sm"  href="javascript:void(0)" onclick=delete_modal("'.$d->id.'")><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i></button> ' : null;
					}else{
						$delete = null;
					}
					$info = '<button class="btn btn-info btn-xs" href="javascript:void(0)" onclick=view_modal("'.$d->id.'")><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i> View</button> ';
					$edit = '<button class="btn btn-primary btn-xs" href="javascript:void(0)" onclick=edit_modal("'.$d->id.'")><i class="fa fa-edit" data-toggle="tooltip" title="Edit Data"></i> Edit</button> ';
					$datax['data'][]=[
						$d->id,
						$d->kode,
						$d->nama_anggota,
						$d->kode_pengajuan,
						$this->formatter->getFormatMoneyUser($d->nominal),
						$d->angsuran_ke,
						$this->formatter->getDateFormatUser($d->tanggal),
						$info.$edit,//.$angsur,//.$delete,
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif($usage == 'view_one'){
				$id = $this->input->post('id');
				$data=$this->model_data->getListTransaksiAngsuran(['a.id'=>$id]);
				foreach ($data as $d) {
					$datax = [
						'id'=>$d->id,
						'kode'=>$d->kode,
						'nama'=>$d->nama_anggota,
						'kode_pinjaman'=>$d->kode_pengajuan,
						'nominal'=>$this->formatter->getFormatMoneyUser($d->nominal),
						'angsuran_ke'=>$d->angsuran_ke,
						'tanggal'=>$this->formatter->getDateFormatUser($d->tanggal),
						'denda'=>$this->formatter->getFormatMoneyUser($d->denda),
						'keterangan'=>$d->keterangan,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),
					];
				}
				echo json_encode($datax);
			}elseif($usage == 'kode'){
				$data = $this->codegenerator->kodeTransaksiAngsuran();
				echo json_encode($data);
			}elseif($usage == 'cekAngsuran'){
				$kode_pinjaman = $this->input->post('kode');
				$banyakAngsuran=$this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$kode_pinjaman]);
				$data = [
					'jumlahAngsuran'=>count($banyakAngsuran),
					'angsuran_ke'=>count($banyakAngsuran)+1,
				];
				echo json_encode($data);
			}elseif($usage == 'do_add'){
				$kode=$this->input->post('kode');
				$kode_pinjaman=$this->input->post('kode_pinjaman');
				$tanggal=$this->formatter->getFormatMoneyDb($this->input->post('tanggal'));
				$nominal_angsuran=$this->formatter->getFormatMoneyDb($this->input->post('nominal_angsuran'));
				$angsuran_ke=$this->input->post('angsuran_ke');
				$denda=$this->formatter->getFormatMoneyDb($this->input->post('denda'));
				$keterangan=$this->input->post('keterangan');
				$sisa_saldo=$this->input->post('sisa_saldo');
				$data = [
					'kode'=>$kode,
					'kode_pengajuan'=>$kode_pinjaman,
					'nominal'=>$nominal_angsuran,
					'angsuran_ke'=>$angsuran_ke,
					'tanggal'=>$this->formatter->getDateFormatDb($tanggal),
					'denda'=>$denda,
					'keterangan'=>$keterangan,
				];
				$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
				$datax = $this->model_global->insertQuery($data,'transaksi_angsuran');
				if($datax){
					// $Angsuran=$this->model_data->getListTransaksiAngsuran(['a.kode_pengajuan'=>$kode_pinjaman]);
					$Pinjaman =$this->model_data->getListDataPengajuan(['a.kode'=>$kode_pinjaman],true);
					$status_pinjaman = ($angsuran_ke==$Pinjaman['lama_pinjam'])?'1':'0';
					$sisaSaldo = $sisa_saldo-$nominal_angsuran;
					$data = [
						'sisa_pinjam'=>$sisaSaldo,
						'status_pinjaman'=>$status_pinjaman,
					];
					$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
					$datax = $this->model_global->updateQuery($data,'data_pengajuan',['kode'=>$kode_pinjaman]);
				}
				echo json_encode($datax);
			}elseif($usage == 'do_edit'){
				$id=$this->input->post('id');
				$kode_transaksi=$this->input->post('kode_transaksi');
				$kode_pengajuan=$this->input->post('kode');
				$tanggal=$this->formatter->getDateFormatDb($this->input->post('tanggal'));
				$nominal_angsur=$this->formatter->getFormatMoneyDb($this->input->post('nominal_angsur'));
				$denda=$this->formatter->getFormatMoneyDb($this->input->post('denda'));
				$angsuran_ke=$this->input->post('angsuran_ke');
				$keterangan=$this->input->post('keterangan');
				$data = [
					'tanggal'=>$tanggal,
					'denda'=>$denda,
					'angsuran_ke'=>$angsuran_ke,
					'keterangan'=>$keterangan,
				];
				$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
				$datax = $this->model_global->updateQuery($data,'transaksi_angsuran',['id'=>$id]);
				echo json_encode($datax);
			}else{

			}
		}
	}
//========================================================= DATA LAPORAN ===========================================================
public function laporan_anggota()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_data->getListDataAnggota();
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_anggota,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_anggota,
						$d->kode,
						$d->nama,
						$d->tempat_lahir.', '.$this->formatter->getDateFormatUser($d->tanggal_lahir),
						$d->alamat,
						$this->otherfunctions->getGender($d->kelamin),
						$d->pekerjaan,
						$this->formatter->getDateMonthFormatUser($d->tanggal_masuk),
						($d->status==1)?'Aktif':'Keluar',
						$d->no_hp,
					];
					$no++;
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function laporan_simpanan()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_data->getListDataAnggota();
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_anggota,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$info = '<button class="btn btn-info btn-xs" href="javascript:void(0)" onclick=view_modal("'.$d->id_anggota.'")><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i> View</button> ';
					$trans=$this->model_data->getListTransaksiTabungan(['e.kode'=>$d->kode]);
					$wajib = [];
					$sukarela = [];
					$pokok = [];
					$pengurang = [];
					foreach ($trans as $t) {
						if($t->jenis == 'SIM202103090001'){
							$wajib[]=$t->nominal;
						}
						if($t->jenis == 'SIM202103090002'){
							$sukarela[]=$t->nominal;
						}
						if($t->jenis == 'SIM202104100001'){
							$pokok[]=$t->nominal;
						}
						if($t->flag == 'pengurang'){
							$pengurang[]=$t->nominal;
						}
					};
					$total = array_sum($pokok)+array_sum($wajib)+array_sum($sukarela)-array_sum($pengurang);
					$data_1 = [
						$d->id_anggota,
						$d->kode,
						$d->nama,
					];
					$dataTab = [
						$this->formatter->getFormatMoneyUser(array_sum($pokok)),
						$this->formatter->getFormatMoneyUser(array_sum($wajib)),
						$this->formatter->getFormatMoneyUser(array_sum($sukarela)),
						$this->formatter->getFormatMoneyUser(array_sum($pengurang)),
					];
					$data_3 = [
						$this->formatter->getFormatMoneyUser($total),
						$info,
					];
					$data_view = array_merge($data_1, $dataTab, $data_3);
					$datax['data'][]=$data_view;
					$no++;
				}
				echo json_encode($datax);
			}elseif($usage == 'view_one'){
				$id = $this->input->post('id_anggota');
				$data=$this->model_data->getListDataAnggota(['id_anggota'=>$id]);
				foreach ($data as $d) {
					$trans=$this->model_data->getListTransaksiTabungan(['e.kode'=>$d->kode]);
					$wajib = [];
					$sukarela = [];
					$pokok = [];
					$pengurang = [];
					foreach ($trans as $t) {
						if($t->jenis == 'SIM202103090001'){
							$wajib[]=$t->nominal;
						}
						if($t->jenis == 'SIM202103090002'){
							$sukarela[]=$t->nominal;
						}
						if($t->jenis == 'SIM202104100001'){
							$pokok[]=$t->nominal;
						}
						if($t->flag == 'pengurang'){
							$pengurang[]=$t->nominal;
						}
					};
					$total = array_sum($pokok)+array_sum($wajib)+array_sum($sukarela)-array_sum($pengurang);
					$datax=[
						'id_anggota'=>$d->id_anggota,
						'kode'=>$d->kode,
						'nama'=>$d->nama,
						'pokok'=>$this->formatter->getFormatMoneyUser(array_sum($pokok)),
						'wajib'=>$this->formatter->getFormatMoneyUser(array_sum($wajib)),
						'sukarela'=>$this->formatter->getFormatMoneyUser(array_sum($sukarela)),
						'pengurang'=>$this->formatter->getFormatMoneyUser(array_sum($pengurang)),
						'total'=>$this->formatter->getFormatMoneyUser($total),
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update),	
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	public function laporan_pinjaman()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_data->getListDataPengajuan();
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$info = '<button class="btn btn-info btn-xs" href="javascript:void(0)" onclick=view_modal("'.$d->id.'")><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i> View</button> ';
					$datax['data'][]=[
						$d->id,
						$d->kode,
						$d->nama_anggota,
						$this->formatter->getDateFormatUser($d->tgl_pengajuan),
						$this->formatter->getDateFormatUser($d->tgl_tempo),
						$d->nama_pinjaman,
						$this->formatter->getFormatMoneyUser($d->besar_pinjam),
						$d->lama_pinjam.' Bulan',
						($d->status_pinjaman == 0)?'<label class="label label-warning" style="font-size:12px;"> Belum Lunas</label>':'<label class="label label-success" style="font-size:12px;"> Lunas</label>',
						$info,
					];
					$no++;
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
































// ================ DATA KONTAK ====================
	public function data_kontak()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_data->getListKontak();
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_kontak,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];

					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
						$d->id_kontak,
						$d->nama,
						'<i class="'.$d->icon.'"></i> ',
						$d->deskripsi,
						$d->link,
						$properties['tanggal'],
						$properties['status'],
						$properties['aksi']
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_kontak');
				$data=$this->model_data->getKontak($id);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_kontak,
						'nama'=>$d->nama,
						'icon'=>$d->icon,
						'link'=>$d->link,
						'deskripsi'=>$d->deskripsi,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update)
					];
				}
				echo json_encode($datax);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function add_kontak(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$nama=$this->input->post('nama');
		if ($nama != "") {
			$data=[
				'nama'=>ucwords($this->input->post('nama')),
				'icon'=>$this->input->post('icon'),
				'link'=>$this->input->post('link'),
				'deskripsi'=>$this->input->post('deskripsi'),
			];
			$data=array_merge($data,$this->model_global->getCreateProperties($this->admin));
			$datax = $this->model_global->insertQuery($data,'data_kontak');
		}else{
			$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
	function edit_kontak(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$nama=$this->input->post('nama');
		if ($nama != "") {
			$data=[
				'nama'=>ucwords($this->input->post('nama')),
				'icon'=>$this->input->post('icon'),
				'link'=>$this->input->post('link'),
				'deskripsi'=>$this->input->post('deskripsi'),
			];
			$data=array_merge($data,$this->model_global->getUpdateProperties($this->admin));
			$datax = $this->model_global->updateQuery($data,'data_kontak',['id_kontak'=>$id]);
		}else{
        	$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
	function update_company(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$nama=$this->input->post('nama');
		if ($nama != "") {
			$other=[
				'name'=>$this->input->post('nama'),
				'name_office'=>$this->input->post('nama_kantor'),
				'alamat'=>$this->input->post('alamat'),
				'email'=>$this->input->post('email'),
				'no_hp'=>$this->input->post('no_hp'),
				'maps'=>$this->input->post('maps'),
				'visi'=>$this->input->post('visi'),
				'misi'=>$this->input->post('misi'),
				'moto'=>$this->input->post('moto'),
				'sejarah'=>$this->input->post('sejarah'),
				'about'=>$this->input->post('about'),
			];
			$data=[
				'post'=>'file',
		        'data_post'=>$this->input->post('file', TRUE),
		        'table'=>'data_company',
		        'column'=>'logo',
		        'where'=>['id'=>$id],
		        'usage'=>'update',
                'otherdata'=>$other,
                'unlink'=>'yes',
		    ];
		    $datax=$this->filehandler->doUpload($data,'image','logo');
		}else{
        	$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
	function update_foto_sejarah(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$other=['id'=>1,];
		$data=[
			'post'=>'file',
			'data_post'=>$this->input->post('file', TRUE),
			'table'=>'data_company',
			'column'=>'gambar_sejarah',
			'where'=>['id'=>$id],
			'usage'=>'update',
			'otherdata'=>$other,
			'unlink'=>'yes',
		];
		$datax=$this->filehandler->doUpload($data,'image','logo');
		echo json_encode($datax);
	}
	function update_foto_visi(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$other=['id'=>1,];
		$data=[
			'post'=>'file',
			'data_post'=>$this->input->post('file', TRUE),
			'table'=>'data_company',
			'column'=>'gambar_visi',
			'where'=>['id'=>$id],
			'usage'=>'update',
			'otherdata'=>$other,
			'unlink'=>'yes',
		];
		$datax=$this->filehandler->doUpload($data,'image','logo');
		echo json_encode($datax);
	}
	function update_z_company(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$nama=$this->input->post('nama');
		if ($nama != "") {
			$other=[
				'name'=>$this->input->post('nama'),
				'name_office'=>$this->input->post('nama_kantor'),
				'alamat'=>$this->input->post('alamat'),
				'email'=>$this->input->post('email'),
				'no_hp'=>$this->input->post('no_hp'),
				'maps'=>$this->input->post('maps'),
				'visi'=>$this->input->post('visi'),
				'misi'=>$this->input->post('misi'),
				'moto'=>$this->input->post('moto'),
				'sejarah'=>$this->input->post('sejarah'),
				'about'=>$this->input->post('about'),
				'visi_real'=>$this->input->post('visi_real'),
				'misi_real'=>$this->input->post('misi_real'),
				'sejarah_real'=>$this->input->post('sejarah_real'),
			];
			$data=[
				'post'=>'file',
		        'data_post'=>$this->input->post('file', TRUE),
		        'table'=>'z_data_company',
		        'column'=>'logo',
		        'where'=>['id'=>$id],
		        'usage'=>'update',
                'otherdata'=>$other,
				'unlink'=>'yes',
		    ];
		    $datax=$this->filehandler->doUpload($data,'belajar');
		}else{
        	$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
	function update_gambar(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$other=['id'=>1,'sejarah'=>$this->input->post('keterangan'),];
		$data=[
			'post'=>'file',
			'data_post'=>$this->input->post('file', TRUE),
			'table'=>'z_data_company',
			'column'=>'gambar_sejarah',
			'where'=>['id'=>$id],
			'usage'=>'update',
			'otherdata'=>$other,
			'unlink'=>'yes',
		];
		$datax=$this->filehandler->doUpload($data,'belajar');
		echo json_encode($datax);
	}
	function update_gambar_1(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$other=['id'=>1,'about'=>$this->input->post('keterangan'),];
		$data=[
			'post'=>'file',
			'data_post'=>$this->input->post('file', TRUE),
			'table'=>'z_data_company',
			'column'=>'gambar_visi',
			'where'=>['id'=>$id],
			'usage'=>'update',
			'otherdata'=>$other,
			'unlink'=>'yes',
		];
		$datax=$this->filehandler->doUpload($data,'belajar');
		echo json_encode($datax);
	}
	function update_gambar_2(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$other=['id'=>1,'about_3'=>$this->input->post('keterangan'),];
		$data=[
			'post'=>'file',
			'data_post'=>$this->input->post('file', TRUE),
			'table'=>'z_data_company',
			'column'=>'gambar_3',
			'where'=>['id'=>$id],
			'usage'=>'update',
			'otherdata'=>$other,
			'unlink'=>'yes',
		];
		$datax=$this->filehandler->doUpload($data,'belajar');
		echo json_encode($datax);
	}
	function update_gambar_3(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$other=['id'=>1,'about_4'=>$this->input->post('keterangan'),];
		$data=[
			'post'=>'file',
			'data_post'=>$this->input->post('file', TRUE),
			'table'=>'z_data_company',
			'column'=>'gambar_4',
			'where'=>['id'=>$id],
			'usage'=>'update',
			'otherdata'=>$other,
			'unlink'=>'yes',
		];
		$datax=$this->filehandler->doUpload($data,'belajar');
		echo json_encode($datax);
	}
	function update_gambar_testi(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$other=['id'=>1];
		$data=[
			'post'=>'file',
			'data_post'=>$this->input->post('file', TRUE),
			'table'=>'z_data_company',
			'column'=>'gambar_testi',
			'where'=>['id'=>$id],
			'usage'=>'update',
			'otherdata'=>$other,
			'unlink'=>'yes',
		];
		$datax=$this->filehandler->doUpload($data,'belajar');
		echo json_encode($datax);
	}
	//================= DATA BANNER ==========================
	public function z_data_banner()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$usage=$this->uri->segment(3);
		if ($usage == null) {
		   echo json_encode($this->messages->notValidParam());
		}else{
			if ($usage == 'view_all') {
				$data=$this->model_data->getListBannerZ();
				$access=unserialize(base64_decode($this->input->post('access')));
				$no=1;
				$datax['data']=[];
				foreach ($data as $d) {
					$var=[
						'id'=>$d->id_banner,
						'create'=>$d->create_date,
						'update'=>$d->update_date,
						'access'=>$access,
						'status'=>$d->status,
					];
					$properties=$this->otherfunctions->getPropertiesTable($var);
					$datax['data'][]=[
                        $d->id_banner,
                        '<img src="'.base_url($d->gambar).'" width="200px">',
						$d->nama,
						$d->keterangan,
						$properties['tanggal'],
						$properties['status'],
						$properties['aksi']
					];
					$no++;
				}
				echo json_encode($datax);
			}elseif ($usage == 'view_one') {
				$id = $this->input->post('id_banner');
				$data=$this->model_data->getListBannerZ(['a.id_banner'=>$id]);
				foreach ($data as $d) {
					$datax=[
						'id'=>$d->id_banner,
						'nama'=>$d->nama,
						'gambar'=>'<img src="'.base_url($d->gambar).'" width="400px" style="align:center;">',
						'e_gambar'=>$d->gambar,
						'keterangan'=>$d->keterangan,
						'status'=>$d->status,
						'create_date'=>$this->formatter->getDateTimeMonthFormatUser($d->create_date),
						'update_date'=>$this->formatter->getDateTimeMonthFormatUser($d->update_date),
						'create_by'=>$d->create_by,
						'update_by'=>$d->update_by,
						'nama_buat'=>(!empty($d->nama_buat)) ? $d->nama_buat:$this->otherfunctions->getMark($d->nama_buat),
						'nama_update'=>(!empty($d->nama_update))?$d->nama_update:$this->otherfunctions->getMark($d->nama_update)
					];
				}
				echo json_encode($datax);
			}elseif ($usage == 'kode') {
				$data = $this->codegenerator->kodeDokumen();
        		echo json_encode($data);
			}else{
				echo json_encode($this->messages->notValidParam());
			}
		}
	}
	function add_z_banner(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');        
		$nama=$this->input->post('nama');
		if ($nama != "") {
			$other=[
				'nama'=>ucwords($nama),
				'keterangan'=>$this->input->post('keterangan'),
			];
			$other=array_merge($other,$this->model_global->getCreateProperties($this->admin));
			$data=[
				'post'=>'file',
		        'data_post'=>$this->input->post('file', TRUE),
		        'table'=>'z_data_banner',
		        'column'=>'gambar', 
		        'usage'=>'insert',
		        'otherdata'=>$other,
		    ];
		    $datax=$this->filehandler->doUpload($data,'belajar');
		}else{
			$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
	function edit_z_banner(){
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$id=$this->input->post('id');
		$nama=$this->input->post('nama');
		if ($nama != "") {
			$other=[
				'nama'=>ucwords($nama),
				'keterangan'=>$this->input->post('keterangan'),
			];
			$other=array_merge($other,$this->model_global->getUpdateProperties($this->admin));
			$data=[
				'post'=>'file',
		        'data_post'=>$this->input->post('file', TRUE),
		        'table'=>'z_data_banner',
		        'column'=>'gambar',
		        'where'=>['id_banner'=>$id],
		        'usage'=>'update',
                'otherdata'=>$other,
                'unlink'=>'yes',
		    ];
		    $datax=$this->filehandler->doUpload($data,'belajar');
		}else{
        	$datax=$this->messages->notValidParam();
		}
		echo json_encode($datax);
	}
}	