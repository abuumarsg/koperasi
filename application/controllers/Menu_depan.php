<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Menu_depan
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */


class Menu_depan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    public function publik($url)
    {
        $row = $this->model_master->customMenuUrl($url);
        if(!empty($row)) {
            $data = [
                'id_menu' => $row['id_menu'],
                'nama'=> $row['nama'],
                'url' => $row['url'],
                'icon'=> $row['icon'],
                'isi' => $row['isi'],
            ];
            $this->template->load('frontend','frontend/publik', decode_spesial($data));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect('publik');
        }
    }

}
?>
