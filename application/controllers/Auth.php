<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Auth
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

class Auth extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->date = $this->otherfunctions->getDateNow();
	    $this->rando = $this->codegenerator->getPin(50,'full');
	} 
	public function index(){
		if ($this->session->has_userdata('adm')) {
			redirect('pages'); 
		}
		$this->load->view('admin_tem/header');
		$this->load->view('utama/login');
		$this->load->view('admin_tem/footer');
	}
	public function logout()
	{
		if ($this->session->has_userdata('adm')) {
			$data=['status'=>0];
			$this->db->where('id_admin',$this->session->userdata('adm')['id']);
			$this->db->update('admin',$data);
		}
		if ($this->session->has_userdata('emp')) {
			$data=['status'=>0];
			$this->db->where('id_karyawan',$this->session->userdata('emp')['id']);
			$this->db->update('karyawan',$data);
		}
		$this->session->sess_destroy();
		redirect('auth');
	}
	public function do_login()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$username=$this->input->post('username');
		$password=$this->codegenerator->genPassword($this->input->post('password'));
		if (empty($username) || empty($password)) {
			echo json_encode($this->messages->unfillForm());
		}else{
			echo json_encode($this->model_global->authSecure($username,$password));
		}
	}
	function history_url()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$url=$this->input->post('ur');
		$this->session->set_flashdata('url', $url);

	}
}