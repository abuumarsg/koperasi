<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Not_found
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

class Not_found extends CI_Controller
{
	public function __construct() 
	{ 
		parent::__construct();
	}
	function not_found(){
		$this->load->view('admin_tem/header');
		$this->load->view('not_found');
		$this->load->view('admin_tem/footer');
	}
}