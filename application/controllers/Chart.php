<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Chart extends CI_Controller
{	
	function __construct()
	{
		parent::__construct();
		$this->date = $this->otherfunctions->getDateNow();

		if ($this->session->has_userdata('adm')) {
			$this->admin = $this->session->userdata('adm')['id'];	 
		}else{ 
			redirect('auth');
		}	
		$dtroot['admin']=$this->model_admin->adm($this->admin);
		$datax['adm'] = array(
				'nama'=>$dtroot['admin']['nama'],
				'email'=>$dtroot['admin']['email'],
				'kelamin'=>$dtroot['admin']['kelamin'],
				'foto'=>$dtroot['admin']['foto'],
				'create'=>$dtroot['admin']['create_date'],
				'update'=>$dtroot['admin']['update_date'],
				'login'=>$dtroot['admin']['last_login'],
				'level'=>$dtroot['admin']['level'],
			);
		$this->dtroot=$datax;
	}
	function index(){
		redirect('pages/dashboard');
	}
	public function chart_peminjaman()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		parse_str($this->input->post('param'),$search);
		$bulan=$search['bulan_pinjam'];
		$tahun=(!empty($search['tahun_pinjam'])?$search['tahun_pinjam']:date('Y'));
		$getBulan = $this->formatter->getMonth();
			// echo '<pre>';
			// print_r($tahun);
		$data_label = [];
		foreach ($getBulan as $gb =>$bulan) {
			$where = ['MONTH(a.tgl_pengajuan)'=>$gb,'YEAR(a.tgl_pengajuan)'=>$tahun,'a.status_pinjaman'=>'0'];
			$dtx = $this->model_data->getListDataPengajuan($where);
			$aa = 0;
			foreach ($dtx as $d) {
				$aa+=$d->besar_pinjam;
			}
			array_push($data_label,$bulan);
			$data_fill['Besar Peminjaman'][]=$aa;
			// $data_fill['Besar Peminjaman'][]=$this->formatter->getFormatMoneyUser($aa);
		}
		$color=[$this->otherfunctions->getRandomColor(),$this->otherfunctions->getRandomColor(),$this->otherfunctions->getRandomColor(),$this->otherfunctions->getRandomColor()];
		if (isset($data_fill)) {
			$n=0;
			foreach ($data_fill as $k=>$df) {
				$data_prop[$n]=[
					'label'=>$k,
					'fill'=>'false',
					'borderColor'=>$color[$n],
					'backgroundColor'=>$color[$n],
					'data'=>$df,
				];
				$n++;
			}
		}
		$data=[
			'labels'=>$data_label,
			'datasets'=>$data_prop,
		];
		echo json_encode($data);
	}
	public function chart_simpanan()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		parse_str($this->input->post('param'),$search);
		$bulan=$search['bulan_pinjam'];
		$tahun=(!empty($search['tahun_pinjam'])?$search['tahun_pinjam']:date('Y'));
		$getBulan = $this->formatter->getMonth();
		$data_label = [];
		foreach ($getBulan as $gb =>$bulan) {
			$where = ['MONTH(a.tanggal)'=>$gb,'YEAR(a.tanggal)'=>$tahun, 'a.flag'=>'penambah'];
			$dtx = $this->model_data->getListTransaksiTabungan($where);
			$aa = 0;
			foreach ($dtx as $d) {
				$aa+=$d->nominal;
			}
			array_push($data_label,$bulan);
			$data_fill['Besar Simpanan'][]=$aa;
		}
		$color=[$this->otherfunctions->getRandomColor(),$this->otherfunctions->getRandomColor(),$this->otherfunctions->getRandomColor(),$this->otherfunctions->getRandomColor()];
		if (isset($data_fill)) {
			$n=0;
			foreach ($data_fill as $k=>$df) {
				$data_prop[$n]=[
					'label'=>$k,
					'fill'=>'false',
					'borderColor'=>$color[$n],
					'backgroundColor'=>$color[$n],
					'data'=>$df,
				];
				$n++;
			}
		}
		$data=[
			'labels'=>$data_label,
			'datasets'=>$data_prop,
		];
		echo json_encode($data);
	}
	public function chart_angsuran()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		parse_str($this->input->post('param'),$search);
		$bulan=$search['bulan_pinjam'];
		$tahun=(!empty($search['tahun_pinjam'])?$search['tahun_pinjam']:date('Y'));
		$getBulan = $this->formatter->getMonth();
		$data_label = [];
		foreach ($getBulan as $gb =>$bulan) {
			$where = ['MONTH(a.tanggal)'=>$gb,'YEAR(a.tanggal)'=>$tahun,'d.status_pinjaman'=>'0'];
			$dtx = $this->model_data->getListTransaksiAngsuran($where);
			$aa = 0;
			foreach ($dtx as $d) {
				$aa+=$d->nominal;
			}
			array_push($data_label,$bulan);
			$data_fill['Besar Angsuran'][]=$aa;
		}
		$color=[$this->otherfunctions->getRandomColor(),$this->otherfunctions->getRandomColor(),$this->otherfunctions->getRandomColor(),$this->otherfunctions->getRandomColor()];
		if (isset($data_fill)) {
			$n=0;
			foreach ($data_fill as $k=>$df) {
				$data_prop[$n]=[
					'label'=>$k,
					'fill'=>'false',
					'borderColor'=>$color[$n],
					'backgroundColor'=>$color[$n],
					'data'=>$df,
				];
				$n++;
			}
		}
		$data=[
			'labels'=>$data_label,
			'datasets'=>$data_prop,
		];
		echo json_encode($data);
	}
	public function chart_ambil()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		parse_str($this->input->post('param'),$search);
		$bulan=$search['bulan_pinjam'];
		$tahun=(!empty($search['tahun_pinjam'])?$search['tahun_pinjam']:date('Y'));
		$getBulan = $this->formatter->getMonth();
		$data_label = [];
		foreach ($getBulan as $gb =>$bulan) {
			$where = ['MONTH(a.tanggal)'=>$gb,'YEAR(a.tanggal)'=>$tahun, 'a.flag'=>'pengurang'];
			$dtx = $this->model_data->getListTransaksiTabungan($where);
			$aa = 0;
			foreach ($dtx as $d) {
				$aa+=$d->nominal;
			}
			array_push($data_label,$bulan);
			$data_fill['Besar Pengambilan Uang'][]=$aa;
		}
		$color=[$this->otherfunctions->getRandomColor(),$this->otherfunctions->getRandomColor(),$this->otherfunctions->getRandomColor(),$this->otherfunctions->getRandomColor()];
		if (isset($data_fill)) {
			$n=0;
			foreach ($data_fill as $k=>$df) {
				$data_prop[$n]=[
					'label'=>$k,
					'fill'=>'false',
					'borderColor'=>$color[$n],
					'backgroundColor'=>$color[$n],
					'data'=>$df,
				];
				$n++;
			}
		}
		$data=[
			'labels'=>$data_label,
			'datasets'=>$data_prop,
		];
		echo json_encode($data);
	}
}