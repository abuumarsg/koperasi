<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Global_Control
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

class Global_control extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->date = $this->otherfunctions->getDateNow();
		if (!$this->session->has_userdata('adm') && !$this->session->has_userdata('emp')) {
			redirect('not_found'); 
		}
	}
	public function index(){
		redirect('not_found');
	}
	//update select option
	public function select2_global()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$table=$this->input->post('table');
		$column=$this->input->post('column');
		$name=$this->input->post('name');
		if (empty($table) || empty($column) || empty($name))
			echo json_encode($this->messages->notValidParam());
		$datax=$this->model_global->listActiveRecord($table,$column,$name);
		echo json_encode($datax);
	}
	public function select2_global_2()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$table=$this->input->post('table');
		$column=$this->input->post('column');
		$name=$this->input->post('name');
		if (empty($table) || empty($column) || empty($name))
			echo json_encode($this->messages->notValidParam());
		$datax=$this->model_global->listActiveRecord_2($table,$column,$name);
		echo json_encode($datax);
	}
	public function change_status()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$table=$this->input->post('table');
		$data=$this->input->post('data');
		$where=$this->input->post('where');
		if (empty($table) || empty($data) || empty($where))
			echo json_encode($this->messages->notValidParam());
		$datax=$this->model_global->updateQuery($data,$table,$where);
		echo json_encode($datax);
	}
	public function delete()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$table=$this->input->post('table');
		$column=$this->input->post('column');
		$id=$this->input->post('id');
		$column_file=$this->input->post('col_file');
		$value_file=$this->input->post('val_file');
		if (empty($table) || empty($column) || empty($id))
			echo json_encode($this->messages->notValidParam());
		if(!empty($value_file)){
			unlink($value_file);
		}
		$where=[$column=>$id];
		$datax=$this->model_global->deleteQuery($table,$where);
		echo json_encode($datax);
	}
	public function delete_2()
	{
		if (!$this->input->is_ajax_request()) 
		   redirect('not_found');
		$table=$this->input->post('table');
		$column=$this->input->post('column');
		$id=$this->input->post('id');
		$column_file=$this->input->post('col_file');
		$value_file=$this->input->post('val_file');
		if (empty($table) || empty($column) || empty($id))
			echo json_encode($this->messages->notValidParam());
		if(!empty($value_file)){
			unlink($value_file);
		}
		$where=[$column=>$id];
		$data = ['delete_at'=>$this->date];
		$datax=$this->model_global->updateQuery($data,$table,$where);
		echo json_encode($datax);
	}
	public function file_download()
	{
		if (empty($this->uri->segment(3))) {
			redirect('not_found');
		}
		$file=$this->codegenerator->decryptChar($this->uri->segment(3));
		$do=$this->filehandler->doDownload($file);
		if (!$do) {
			redirect('not_found');
		}
	}
}