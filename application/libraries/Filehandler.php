<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Filehandler
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

class Filehandler{
    
    protected $CI;
    public function __construct()
    {
        $this->CI =& get_instance();
        $this->date=$this->CI->otherfunctions->getDateNow();
    }

    public function index()
    {
        $this->redirect('not_found');
    }
    public function ruleFile($usage='image',$val='banner')
    {
        if ($usage == 'image') {
            if ($val == 'banner') {
                $properties['upload_path']          = './asset/img/banner';
            }
            if ($val == 'berita') {
                $properties['upload_path']          = './asset/img/berita';
            }
            if ($val == 'gambar') {
                $properties['upload_path']          = './asset/img/gambar';
            }
            if ($val == 'logo') {
                $properties['upload_path']          = './asset/img';
            }
            if ($val == 'anggota') {
                $properties['upload_path']          = './asset/img/anggota';
            }
            $properties['allowed_types']        = 'gif|jpg|png|jpeg|JPEG';
            $properties['max_size']             = 10000;
            $properties['max_width']            = 10240;
            $properties['max_height']           = 7680;
        }
        if ($usage == 'doc') {
            $properties['upload_path']          = './asset/upload/document';
            $properties['allowed_types']        = 'doc|docx';
            $properties['max_size']             = 3000;
        }
        if ($usage == 'pdf') {
            // $new_name = $this->date.$_FILES["userfiles"]['name'];
            // $properties['file_name']            = $new_name;
            $properties['upload_path']          = './asset/upload/pdf';
            $properties['allowed_types']        = 'pdf';
            $properties['max_size']             = 3000;
            $properties['encrypt_name']         = TRUE;
        }
        if ($usage == 'file') {
            $properties['upload_path']          = './asset/upload/file';
            $properties['allowed_types']        = 'pdf|doc|docx|xls|xlsx';
            $properties['max_size']             = 10000;
        }
        if ($usage == 'belajar') {
            $properties['upload_path']          = './belajar/asset/upload/img';
            $properties['allowed_types']        = 'gif|jpg|png|jpeg|JPEG';
            $properties['max_size']             = 10000;
            $properties['max_width']            = 10240;
            $properties['max_height']           = 7680;
        }
        return $properties;
    } 
    public function doDb($data,$usage ='insert',$table)
    {
        $ret=$this->CI->messages->allFailure();
        if (!isset($data)){
            $ret=$this->CI->messages->customFailure('Fatal Error!, Harap Hubungi Administrator');
        }
        if ($usage == 'insert') {
            $ret=$this->CI->model_global->insertQuery($data['data'],$table);
        }else{
            $ret=$this->CI->model_global->updateQuery($data['data'],$table,$data['where']);
        }
        return $ret;
    }
    public function doUpload($data,$usage='image',$val=null)
    {
        //data is array
        $ret=$this->CI->messages->allFailure();
        if (!isset($data)){
            $ret=$this->CI->messages->customFailure('Fatal Error!, Harap Hubungi Administrator');
        }
        $config=$this->ruleFile($usage,$val);
        $this->CI->load->library('upload', $config);
        $upload=$this->CI->upload->do_upload($data['post']);
        $dt=$this->CI->upload->data();
        if ($usage == 'image') {
            if ($val == 'banner') {
                $dir='asset/img/banner/';
            }
            if ($val == 'berita') {
                $dir='asset/img/berita/';
            }
            if ($val == 'gambar') {
                $dir='asset/img/gambar/';
            }
            if ($val == 'logo') {
                $dir='asset/img/';
            }
            if ($val == 'anggota') {
                $dir='asset/img/anggota/';
            }
        }elseif ($usage == 'doc') {
            $dir='asset/upload/document/';
        }elseif ($usage == 'pdf') {
            $dir='asset/upload/pdf/';
        }elseif ($usage == 'file') {
            $dir='asset/upload/file/';
        }elseif ($usage == 'belajar') {
            $dir='belajar/asset/upload/img/';
        }
        $file=$dir.$dt['file_name'];
        $db=[$data['column']=>$file];
        if (!$upload){
            if ($usage == 'image') {
                if ($dt['is_image'] == false) {
                    $ret = $this->CI->messages->customFailure('Type File harus *.gif, *.jpg, *.jpeg, *.png');
                }elseif ($dt['file_size'] > $config['max_size'] ) {
                    $ret = $this->CI->messages->customFailure('Ukuran Foto harus berukuran KURANG DARI 5 MB');
                }else{
                    $ret = $this->CI->messages->customFailure('Foto tidak bisa diupload, silahkan ganti file foto yang lain');
                }
            }elseif ($usage == 'doc') {
                $ext=['.doc','docx'];
                if (!in_array($dt['file_ext'],$ext)) {
                    $ret = $this->CI->messages->customFailure('Type File harus *.doc, *.docx');
                }elseif ($dt['file_size'] > $config['max_size'] ) {
                    $ret = $this->CI->messages->customFailure('Ukuran File harus berukuran KURANG DARI 3 MB');
                }else{
                    $ret = $this->CI->messages->customFailure('File tidak bisa diupload, silahkan ganti file dokumen yang lain');
                }
            }elseif ($usage == 'pdf') {
                $ext=['.pdf'];
                if (!in_array($dt['file_ext'],$ext)) {
                    $ret = $this->CI->messages->customFailure('Type File harus *.pdf');
                }elseif ($dt['file_size'] > $config['max_size'] ) {
                    $ret = $this->CI->messages->customFailure('Ukuran File harus berukuran KURANG DARI 3 MB');
                }else{
                    $ret = $this->CI->messages->customFailure('File tidak bisa diupload, silahkan ganti file dokumen yang lain');
                }
            }elseif ($usage == 'file') {
                $ext=['.pdf','.doc','docx','xls','xlsx'];
                if (!in_array($dt['file_ext'],$ext)) {
                    $ret = $this->CI->messages->customFailure('Type File harus *.pdf, *.doc, *.docx,*.xls, *.xlsx');
                }elseif ($dt['file_size'] > $config['max_size'] ) {
                    $ret = $this->CI->messages->customFailure('Ukuran File harus berukuran KURANG DARI 3 MB');
                }else{
                    $ret = $this->CI->messages->customFailure('File tidak bisa diupload, silahkan ganti file dokumen yang lain');
                }
            }
            if (isset($data['allow_null']) && ($dt['file_name'] == '' || $dt['file_type'] == '')) {
                if ($data['allow_null'] == TRUE && (isset($data['default_dir']))) {
                    $db=[$data['column']=>$data['default_dir']];
                    $data_db['data']=array_merge($data['otherdata'],$db);
                    if ($data['usage'] == 'update') {
                        $data_db['where']=$data['where'];
                    }
                    $ret=$this->doDb($data_db,$data['usage'],$data['table']);
                }
            }

            //bypass update
            if ($data['usage'] == 'update' && ($dt['file_name'] == '' || $dt['file_type'] == '') && !isset($data['allow_null'])) {
                $data_db=['data'=>$data['otherdata'],'where'=>$data['where']];
                $ret=$this->doDb($data_db,$data['usage'],$data['table']);
            }             
        }else{
            if ($dt['file_name'] == '' || $dt['file_type'] == '') {
                $data_db['data']=$data['otherdata'];
            }else{
                $data_db['data']=array_merge($data['otherdata'],$db);
            }
            if(isset($data['unlink']) && $data['unlink'] == 'yes'){
                $d_old=$this->CI->db->get_where($data['table'],$data['where'])->row_array();
                if($d_old[$data['column']]!=null && $d_old[$data['column']] != $file){
                    unlink($d_old[$data['column']]);
                }
            }
            if ($data['usage'] == 'update') {
                $data_db['where']=$data['where'];
            }
        // echo '<pre>';
        $ret=$this->doDb($data_db,$data['usage'],$data['table']);
		// print_r($data['usage']);
		// print_r($data['table']);
        }        
        return $ret;
    }
    public function doDownload($dir)
    {
        if (empty($dir))
            return false;
        $this->CI->load->helper('download');
        force_download($dir,NULL);
    }
}