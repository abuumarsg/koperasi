<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Template
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */


class Template {
    var $template_data = [];
    function set($name, $value)
    {
        $this->template_data[$name] = $value;
    }
    function load($template = '', $view = '' , $view_data = [], $return = FALSE)
    {
        $this->CI =& get_instance();
        $this->set('contents', $this->CI->load->view($view, $view_data, TRUE));
        return $this->CI->load->view($template, $this->template_data, $return);
    }
}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */
