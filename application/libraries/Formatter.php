<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Formatter
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */


class Formatter {
    
    protected $CI;
    public function __construct()
    {
        $this->CI =& get_instance();
    }
	//date format
    public function getMonth()
    {
        $month=[
            '01'=>'Januari',
            '02'=>'Februari',
            '03'=>'Maret',
            '04'=>'April',
            '05'=>'Mei',
            '06'=>'Juni',
            '07'=>'Juli',
            '08'=>'Agustus',
            '09'=>'September',
            '10'=>'Oktober',
            '11'=>'November',
            '12'=>'Desember',
        ];
        return $month;
    }
	public function getDateFormatDb($date)
	{
        if(empty($date)) 
            return null;
		$date=explode('/', $date);
		$new_date=$date[2].'-'.$date[1].'-'.$date[0];
		return $new_date;
	}
	public function getDateFormatUser($date)
	{
        if(empty($date)) 
            return null;
		$date=explode('-', date('Y-m-d',strtotime($date)));
		$new_date=$date[2].'/'.$date[1].'/'.$date[0];
		return $new_date;
	}
	public function getDateMonthFormatUser($date)
	{
        if(empty($date)) 
            return null;
		$date1=explode('-', date('Y-m-d',strtotime($date)));
		$new_date=$date1[2].' '.$this->getNameOfMonth($date1[1]).' '.$date1[0];
		return $new_date;
	}
	public function getDayDateFormatUserId($date)
	{
        if(empty($date)) 
            return null;
		$date1=explode('-', date('Y-m-d',strtotime($date)));
		$new_date=$this->getNameOfDay($date).', '.$date1[2].' '.$this->getNameOfMonth($date1[1]).' '.$date1[0];
		return $new_date;
	}
	public function getDayDateFormatUserIdTime($datex)
	{
        if(empty($datex)) 
            return null;
        $datetime=explode(' ', date('Y-m-d H:i:s',strtotime($datex)));
		$date1=explode('-', date('Y-m-d',strtotime($datetime[0])));
		$new_date=$this->getNameOfDay($datetime[0]).', '.$date1[2].' '.$this->getNameOfMonth($date1[1]).' '.$date1[0].' '.$datetime[1];
		return $new_date;
	}
	public function getDateTimeFormatDb($datetime)
	{
        if(empty($datetime)) 
            return null;
		$datetime=explode(' ', $datetime);
		$date=explode('/', $datetime[0]);
		if (isset($datetime[1])) {
			$time=$datetime[1];
		}else{
			$time='00:00:00';
		}
		$new_datetime=$date[2].'-'.$date[1].'-'.$date[0].' '.$time;
		return $new_datetime;
	}
	public function getDateTimeFormatUser($datetime)
	{
        if(empty($datetime)) 
            return null;
		$datetime=explode(' ', date('Y-m-d H:i:s',strtotime($datetime)));
		$date=explode('-', $datetime[0]);
		if (isset($datetime[1])) {
			$time=$datetime[1];
		}else{
			$time='00:00:00';
		}
		$new_datetime=$date[2].'/'.$date[1].'/'.$date[0].' '.$time;
		return $new_datetime;
	}
	public function getDateTimeMonthFormatUser($datetime)
	{
        if(empty($datetime)) 
            return null;
		$datetime=explode(' ', date('Y-m-d H:i:s',strtotime($datetime)));
		$date=explode('-', $datetime[0]);
		if (isset($datetime[1])) {
			$time=$datetime[1];
		}else{
			$time='00:00:00';
		}
		$new_datetime=$date[2].' '.$this->getNameOfMonth($date[1]).' '.$date[0].' '.$time;
		return $new_datetime;
	}
	public function getNameOfDay($date){
        if(empty($date)) 
            return null;
        $name_day = date('l',strtotime($date));
        $day = '';
        $day = ($name_day=='Sunday')?'Minggu':$day;
        $day = ($name_day=='Monday')?'Senin':$day;
        $day = ($name_day=='Tuesday')?'Selasa':$day;
        $day = ($name_day=='Wednesday')?'Rabu':$day;
        $day = ($name_day=='Thursday')?'Kamis':$day;
        $day = ($name_day=='Friday')?'Jumat':$day;
        $day = ($name_day=='Saturday')?'Sabtu':$day;
        return $day;
    }
    public function getNameOfMonth($inputmonth)
    {
        if(empty($inputmonth)) 
            return null;
        $return = null;
        $month = strtolower(trim($inputmonth));
        switch($month){
            case '1' : $return = 'Januari'; break;
            case '01' : $return = 'Januari'; break;
            case 'januari' : $return = 'Januari'; break;
            case 'january' : $return = 'Januari'; break;
            case '2' : $return = 'Februari'; break;
            case '02' : $return = 'Februari'; break;
            case 'februari' : $return = 'Februari'; break;
            case 'february' : $return = 'Februari'; break;
            case '3' : $return = 'Maret'; break;
            case '03' : $return = 'Maret'; break;
            case 'maret' : $return = 'Maret'; break;
            case 'march' : $return = 'Maret'; break;
            case '4' : $return = 'April'; break;
            case '04' : $return = 'April'; break;
            case 'april' : $return = 'April'; break;
            case '5' : $return = 'Mei'; break;
            case '05' : $return = 'Mei'; break;
            case 'may' : $return = 'Mei'; break;
            case '6' : $return = 'Juni'; break;
            case '06' : $return = 'Juni'; break;
            case 'juni' : $return = 'Juni'; break;
            case 'june' : $return = 'Juni'; break;
            case '7' : $return = 'Juli'; break;
            case '07' : $return = 'Juli'; break;
            case 'juli' : $return = 'Juli'; break;
            case 'july' : $return = 'Juli'; break;
            case '8' : $return = 'Agustus'; break;
            case '08' : $return = 'Agustus'; break;
            case 'agt' : $return = 'Agustus'; break;
            case 'agu' : $return = 'Agustus'; break;
            case 'aug' : $return = 'Agustus'; break;
            case 'agustus' : $return = 'Agustus'; break;
            case 'august' : $return = 'Agustus'; break;
            case '9' : $return = 'September'; break;
            case '09' : $return = 'September'; break;
            case 'september' : $return = 'September'; break;
            case '10' : $return = 'Oktober'; break;
            case 'oct' : $return = 'Oktober'; break;
            case 'oktober' : $return = 'Oktober'; break;
            case 'october' : $return = 'Oktober'; break;
            case '11' : $return = 'November'; break;
            case 'nov' : $return = 'November'; break;
            case 'nopember' : $return = 'November'; break;
            case 'november' : $return = 'November'; break;
            case '12' : $return = 'Desember'; break;
            case 'dec' : $return = 'Desember'; break;
            case 'desember' : $return = 'Desember'; break;
            case 'december' : $return = 'Desember'; break;
            default : $return = $inputmonth; break;
        }
        return $return;
    }
    public function timeFormatUser($var)
    {
        if(empty($var))
            return $this->CI->otherfunctions->getMark();
        $var=substr($var,0, -3);
        return $var;
    }
    public function timeFormatDb($var)
    {
        if(empty($var))
            return null;
        $var=$var.':00';
        return $var;
    }
    public function limit_words($string, $word_limit=10)
    {
        $words = explode(" ",$string);
        return implode(" ",array_splice($words,0,$word_limit));
    }
    public function getFormatMoneyUser($var,$par=0){
        if($var == '')
            return 'Rp. '.number_format(0,$par,',','.');
        $var='Rp. '.number_format($var,$par,',','.');
        return $var;
    }
    public function getFormatMoneyUserReq($var){
        if($var == '')
            return number_format(0,0,',','.');
        $varx=(is_numeric($var) ? number_format($var,0,',','.') : $var);
        return $varx;
    }
    public function getFormatMoneyDb($var){
        if(empty($var))
            return null;
        $var=strtoupper($var);
        $var= str_replace('RP. ', '', $var);
        $var= str_replace('.', '', $var);
        $var= str_replace(',', '.', $var);
        return $var;
    }
    public function encryptChar($plain)
    {
        if (empty($plain))
            return null;
        $new_val=base64_encode(serialize($plain));
        return $new_val;
    }
    public function decryptChar($plain)
    {
        if (empty($plain))
            return null;
        if (!@unserialize(base64_decode($plain))) {
			return null;
		}
        $new_val=unserialize(base64_decode($plain));
        return $new_val;
    }
    public function zeroPadding($val)
    {
        $new_val = sprintf("%02d", $val);
        return $new_val;
    }
    public function getYearAll()
    {
        $first=2020;
        $end=date('Y', strtotime(date('Y',strtotime($this->CI->otherfunctions->getDateNow())) . ' +2 year'));
        $date=range($first,$end);
        $year=[];
        foreach ($date as $d) {
            $year[$d]=$d;
        }
        return $year;
    }
}
?>