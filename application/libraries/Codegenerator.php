<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Codegenerator
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */

class Codegenerator{
    
    protected $CI;
    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function index()
    {
        $this->redirect('not_found');
    }
    public function encryptChar($plain)
    {
        if (empty($plain))
            return null;
        $new_val=base64_encode(serialize($plain));
        return $new_val;
    }
    public function decryptChar($plain)
    {
        if (empty($plain))
            return null;
        $new_val=unserialize(base64_decode($plain));
        return $new_val;
    }
    public function genPassword($plain)
    {
        if (empty($plain))
            return null;
        $new_val=hash('sha512', $plain);
        return $new_val;
    }
    public function genToken($length)
    {
        if (empty($length) || !is_numeric($length)) 
            return null;
        $new_val=bin2hex(random_bytes($length).uniqid());
        return $new_val;
    }
    public function matchAdminAuth($uname,$pass)
    {
        if (empty($uname) || empty($pass))
            redirect('auth/login');
        $pass=hash('sha512', $pass);
        $cek=$this->CI->model_admin->adm_cek($uname,$pass);
        if (isset($cek)) {
            $data=$cek;
        }else{
            $data=null;
        }
        return $data;
    }
    public function getPin($length,$use)
    {
        if (empty($length) || empty($use))
            return null;
        if ($use == 'full') {
            $string = '0123456789abcdefghijklmnopqrstuvwxyz';
            $string = $string.strtoupper($string);
        }elseif ($use == 'number') {
            $string = '0123456789';
        }elseif ($use == 'letter') {
            $string = 'abcdefghijklmnopqrstuvwxyz';
            $string = $string.strtoupper($string);
        }else{
            return null;
        }
        $panjang = strlen($string);
        $new_val = '';
        for ($i = 0; $i < $length; $i++) {
            $new_val .= $string[rand(0, $panjang - 1)];
        }
        return $new_val;
    }
    public function firstNum($val)
    {
        if (empty($val)) 
            return null;
        $zero='%0'.$val.'d';
        $depan=sprintf($zero, 1);
        return $depan;
    }
    public function magicNum($num)
    {
        if (empty($num)) 
            return null;
        $front=str_replace((int)$num, '', $num);
        $front=str_split($front);
        $n_new=$num+1;
        if (strlen($n_new) > strlen((int)$num)) {
          array_pop($front);
          $zero=implode('',$front);
          $new=$zero.$n_new;
        }else{
          $new=implode('',$front).$n_new;
        }
        return $new;
    }
    public function getDay($front,$val)
    {
        if (empty($front) || empty($val)) 
            return null;
        $n1=str_replace($front, '', $val);
        $n2=substr($n1, 0, -4);
        $dt=str_split($n2);
        if (!isset($dt[6]) || !isset($dt[7])) 
            return null;
        $day=$dt[6].$dt[7];
        return $day;
    }
    public function logicGenerator($table,$columncode,$columnid,$front,$usefor,$other_var=null)
    {
        if (empty($table) || empty($columncode) || empty($columnid) || empty($usefor)) 
            return null;
        //condition
        $where_penilaian=['kuisioner','aspek_sikap','c_aspek_sikap','c_output','c_assess','a_aspek_sikap','a_output','a_assess'];
        $other_list=['lembur','ijin_pribadi','ijin_dinas','perjanjian','kode_master'];

        $romawi=['01'=>'I','02'=>'II','03'=>'III','04'=>'IV','05'=>'V','06'=>'VI','07'=>'VII','08'=>'VIII','09'=>'IX','10'=>'X','11'=>'XI','12'=>'XII'];
        $query = "SELECT $columncode FROM $table WHERE $columnid = (SELECT MAX($columnid) FROM $table)";
        $qq = $this->CI->db->query($query)->row_array();
        $date=$this->CI->otherfunctions->getDateNow();
        $year_now=date("Y",strtotime($date));
        $month_now=date("m",strtotime($date));
        if ($usefor == 'kecelakaan_kerja') {
            $y=$this->firstNum(3);
            if ($qq == NULL) {
                $no=$y.'/'.$front.'/'.$romawi[$month_now].'/'.$year_now; 
            }else{
                $tt=explode('/',$qq[$columncode]);
                $bl1=$tt[2];
                $no1=$tt[0];
                $blny=array_search($bl1, $romawi);
                if ($blny != $month_now) {
                    $no=$y.'/'.$front.'/'.$romawi[$month_now].'/'.$year_now; 
                }else{
                    $nn=$this->magicNum($no1);
                    $no=$nn.'/'.$front.'/'.$romawi[$month_now].'/'.$year_now;
                }
            }
        }elseif (in_array($usefor, $where_penilaian)) {
            if ($qq == NULL) {
                $nox='1'.uniqid();
                $no=$front.md5($nox);
            }else{
                $nox=$qq[$columncode].uniqid();
                $no=$front.md5($nox); 
            }
        }elseif (in_array($usefor, $other_list)) {
            $y=$this->firstNum(4);
            $date_format=date('Ymd',strtotime($date));
            if ($qq == NULL) {
                $no=$front.$date_format.$y;
            }else{
                $d_new=date('d',strtotime($date));
                $d_old=$this->getDay($front,$qq[$columncode]);
                if (($d_new != $d_old) || empty($d_old)) {
                    $no=$front.$date_format.$y;
                }else{
                    $num=substr($qq[$columncode], -4);
                    $n1=str_replace($front, '', $qq[$columncode]);
                    $date_old=substr($n1, 0, -4);
                    $no=$front.$date_old.$this->magicNum($num);
                }
            }
        }elseif($usefor == 'anggota'){
            $y=$this->firstNum(4);
            if ($qq == NULL) {
                $no=$front.$y;
            }else{
                $num=substr($qq[$columncode], -4);
                $n1=str_replace($front, '', $qq[$columncode]);
                $no=$front.$this->magicNum($num);
            }
        }elseif($usefor == 'data_master'){
			$y=$this->firstNum(4);
			$date_format=date('Ymd',strtotime($date));
			if ($qq == NULL) {
				$no=$front.$date_format.$y;
			}else{
				$d_new=date('d',strtotime($date));
				$d_old=$this->getDay($front,$qq[$columncode]);
				if (($d_new != $d_old) || empty($d_old)) {
					$no=$front.$date_format.$y;
				}else{
					$num=substr($qq[$columncode], -4);
					$n1=str_replace($front, '', $qq[$columncode]);
					$date_old=substr($n1, 0, -4);
					$no=$front.$date_old.$this->magicNum($num);
				}
			}
        }
        return $no;
    }
    public function kodeMasterSimpan(){
        return $this->logicGenerator('master_simpanan', 'kode', 'id_simpanan', 'SIM','kode_master');
    }
    public function kodeMasterPinjam(){
        return $this->logicGenerator('master_pinjaman', 'kode', 'id_pinjaman', 'PNJ','kode_master');
    }
    public function kodeDataAnggota(){
        return $this->logicGenerator('data_anggota', 'kode', 'id_anggota', 'ANG','data_master');
    }
    public function kodeDataTabungan(){
        return $this->logicGenerator('data_tabungan', 'kode_tabungan', 'id_tabungan', 'TAB','data_master');
    }
    public function kodeTransaksiTabungan(){
        return $this->logicGenerator('transaksi_tabungan', 'kode', 'id', 'TT','data_master');
    }
    public function kodeTransaksiPengajuan(){
        return $this->logicGenerator('data_pengajuan', 'kode', 'id', 'PGJN','kecelakaan_kerja');
    }
    public function kodeTransaksiAngsuran(){
        return $this->logicGenerator('transaksi_angsuran', 'kode', 'id', 'TA','data_master');
    }
}