<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
     * Code From ASTAMA TECHNOLOGY.
     * Web Developer
     * @author      Abu Umar
     * @package     Otherfunctions
     * @copyright   Copyright (c) 2019 ASTAMA TECHNOLOGY
     * @version     1.0, 1 Juli 2019
     * Email        abuumarsg.com
     * Phone        (+62) 85725951044
     */


class Otherfunctions {
	
	protected $CI;
	public function __construct()
	{
		$this->CI =& get_instance();
	}
	public function index()
	{
		$this->redirect('not_found');
	}
    public function getDateNow($format = 'Y-m-d H:i:s') 
    {
        $date=gmdate($format, time() + 3600*(7));
        return $date;
    }
	public function companyProfile()
	{
		$data=[
			'name'=>'Intech Indonesia',
			'description'=>'Intech Indonesia Is a company that was established on July 20, 2020. This company is engaged in technology. One of our biggest visions is to create technology that can benefit society',
			'name_office'=>'CV. Intech Indonesia',
			'address'=>'Kota Semarang <br> Jawa Tengah',
			'call'=>'081 227 227 991',
			'email'=>'edwin@astamatechnology.com',
			'website'=>'http://intechindonesia.id/',
			'logo'=>base_url('asset/img/intech_indonesia.png'),
			'logo_only'=>base_url('asset/img/logo_koperasi.png'),
			'fav_icon'=>base_url('asset/img/logo_koperasi.png'),
			'title'=> 'Koperasi',
			'name_soft'=>'Astama Software',
			'version'=>'1.0',
			'meta_description'=>'ASTAMA SOFT, Astama Software, Company Profile, Company Profile System, CV. Intech Indonesia, '.$this->companyClientProfile()['meta_description'],
			'meta_author'=>'Abu Umar S.Kom',
			'maps'=>'<div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="300" id="gmap_canvas" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126715.84250581624!2d110.34702402183427!3d-7.024554222694183!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e708b4d3f0d024d%3A0x1e0432b9da5cb9f2!2sSemarang%2C+Kota+Semarang%2C+Jawa+Tengah!5e0!3m2!1sid!2sid!4v1563810497105!5m2!1sid!2sid" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net">embedgooglemap.net</a></div><style>.mapouter{text-align:right;height:300px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:300px;width:100%;}</style></div>',
			];
		return $data;
	}
	public function companyClientProfile()
	{
		$data=[
			'name'=>'Koperasi',
			'name_office'=>'Koperasi',
			'address'=>'Kota Semarang, Jawa Tengah 50256',
			'phone'=>'(024) 99999999',
			'email'=>'email@koperasi.co.id',
			'website'=>'koperasi.co.id',
			'logo'=>base_url('asset/publik/images/logo_ykks.png'),
			'gambar_ykks'=>base_url('asset/img/unika_bg.png'),
			'favicon'=>base_url('asset/publik/images/favicon_ykks.png'),
			'meta_description'=>'CV. Intech Indonesia, Astama Software, Company Profile, Company Profile System',
			'maps'=>'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15839.609712875234!2d110.4550034!3d-7.0207545!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xaad944273e6d081f!2sYayasan+Kesejahteraan+Keluarga+Soegijapranata!5e0!3m2!1sid!2sid!4v1563856779019!5m2!1sid!2sid',
		];
		return $data;
	}
	public function getLinkYouTube()
	{
		$data=[
			'embed'=>'https://www.youtube.com/embed/',
		];
		return $data;
	}
    public function getGenderList()
    {
        $pack=[
            'l'=>'Laki - Laki',
            'p'=>'Perempuan'
        ];
        return $pack;
    }
    public function getGender($key)
    {
        return $this->getVarFromArrayKey($key,$this->getGenderList());
    }
    public function getFotoValueAdmin($foto,$kelamin)
    {
        if(empty($foto)){
            if($kelamin=='p'){
                $val = 'styles/img/photo/user-photo/userf.png';
            }else{
                $val = 'styles/img/photo/user-photo/user.png';
            }
        }else{
            $val = $foto;
        }
        return $val;
    }
    public function convertResultToRowArray($valObj) {
        if (empty($valObj) || !isset($valObj))
          return null;
      $valObj=json_decode(json_encode($valObj), true);
      return $valObj[0];
    }
	public function getVarFromArrayKey($key,$pack)
	{
		if (!isset($pack[$key])) 
		return $this->getMark('danger');
		return $pack[$key];
	}
	public function titlePages($uri)
	{
		if (empty($uri))
		return null;
		$new_val=null;
		$ex=explode('_', $uri);
		if (count($ex) > 0) {
			$new_val=implode(' ', $ex);
		}else{
			$new_val=$uri;
		}
		return ucwords(strtolower($new_val)).' | ';
	}
	public function getParseVar($val)
	{
		if(empty($val)) 
		return null;
		$new_val = null;
		$bag=[];
		$ex=explode(';', $val);
		foreach ($ex as $e) {
			$var=explode(':',$e);
			$bag[$var[0]]=$var[1];
		}
		$new_val=$bag;
		return $new_val;
	}
	public function getParseOneLevelVar($val)
	{
		$bag=[];
		if(empty($val)) 
		return $bag;
		$bag=explode(';',$val);
		return $bag;
	}
	public function getAllAccess()
	{
		$access=$this->CI->model_master->getListAccess();
		$pack=[];
		foreach ($access as $a) {
			$pack[strtolower($a->kode_access)]=strtoupper($a->kode_access);
		}
		return $pack;
	}
	public function getYourAccess($id)
	{
		if (empty($id)) 
		return null;
		$pack=[];
		$admin=$this->CI->model_admin->getAdminById($id);
		if (!isset($admin)) 
		return null;
		$user_group=$this->CI->model_master->getUserGroup($admin['id_group']);
		if (!isset($user_group)) 
		return null;
		$ex=explode(';',$user_group['list_access']);
		if (!isset($ex)) 
		return null;
		foreach ($ex as $e) {
			$acc=$this->CI->model_master->getAccess($e);
			
			if (isset($acc)) {
				foreach ($acc as $d) {
					array_push($pack,$d->kode_access);
				}
			}
		}
		return $pack;
	}
	public function getYourMenu($id)
	{
		if (empty($id)) 
		return null;
		$pack=[];
		$admin=$this->CI->model_admin->getAdminById($id);
		if (!isset($admin)) 
		return null;
		$user_group=$this->CI->model_master->getUserGroup($admin['id_group']);
		if (!isset($user_group)) 
		return null;
		$ex=explode(';',$user_group['list_id_menu']);
		if (!isset($ex)) 
		return null;
		foreach ($ex as $e) {
			$menu=$this->CI->model_master->getMenu($e);
			if (isset($menu)) {
				$ex1=$this->getParseOneLevelVar($menu['sub_url']);
				if (isset($ex1)) {
					foreach ($ex1 as $e1) {
						array_push($pack,$e1);
					}
				}
				if ($menu['url'] != '#') {
					array_push($pack,$menu['url']);
				}
			}
		}
		return array_values(array_unique($pack));
	}
	public function getYourMenuId($id)
	{
		if (empty($id)) 
		return null;
		$pack=[];
		$admin=$this->CI->model_admin->getAdminById($id);
		if (!isset($admin)) 
		return null;
		$user_group=$this->CI->model_master->getUserGroup($admin['id_group']);
		if (!isset($user_group)) 
		return null;
		$ex=explode(';',$user_group['list_id_menu']);
		if (!isset($ex)) 
		return null;
		$pack=$ex;
		return $pack;
	}
	//list menu
	public function getDrawMenu($your_menu,$data,$parent,$url)
	{
		if (empty($data)) 
		return null;
		$new_val = null;
		foreach ($data as $d)
		{
			if ($d->parent == $parent){
				if (in_array($d->id_menu,$your_menu)) {
					
					if ($this->getChildren($data,$d->id_menu)) {
						$url_act=$this->getParseOneLevelVar($d->sub_url);
						if (in_array($url,$url_act)) {
							$class=' class="treeview active"';
						}else{
							$class=' class="treeview"';
						}
						$name='<i class="fa '.$d->icon.'"></i> <span>'.$d->nama.'</span>
						<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
						</span>';
						$urlx='#';
					}else{
						$url_act=$this->getParseOneLevelVar($d->sub_url);
						if (in_array($url,$url_act)) {
							$class=' class="active"';
						}else{
							if ($url == $d->url) {
								$class=' class="active"';
							}else{
								$class=null;
							}
						}
						$name='<i class="'.$d->icon.'"></i> <span>'.$d->nama.'</span>';
						$urlx=$d->url;
					}
					$new_val.= '<li '.$class.' style="white-space: normal; overflow-wrap: break-word;"><a href="'.base_url('pages/'.$urlx).'" title="'.$d->nama.'">'.$name.'</a>';
					if ($this->getChildren($data,$d->id_menu))                                   
					$new_val.= '<ul class="treeview-menu">'.$this->getDrawMenu($your_menu,$data,$d->id_menu,$url).'</ul>'; 
					$new_val.= "</li>";   
				}               
			}
		}
		return $new_val;
	}
	public function getChildren($data,$id)
	{
		foreach ($data as $d) {
			if ($d->parent == $id)
			return true;            
		}
		return false;
	}
	public function getDrawMenu2($data,$parent)
	{
		if (empty($data)) 
		return null;
		$new_val = null;
		foreach ($data as $d)
		{
			if ($d->parent == $parent){
				$new_val.= '<li data-jstree=\'{"icon":"'.$d->icon.'"}\' id="'.$d->id_menu.'"><a href="#">'.$d->nama.'</a>';
				if ($this->getChildren2($data,$d->id_menu))                                   
				$new_val.= '<ul class="sub_menu">'.$this->getDrawMenu2($data,$d->id_menu).'</ul>'; 
				$new_val.= "</li>";                  
			}
		}
		return $new_val;
	}
	public function getChildren2($data,$id)
	{
		foreach ($data as $d) {
			if ($d->parent == $id)
			return true;            
		}
		return false;
	}
	public function getMenuParent($data,$child)
	{
		if (empty($data)) 
		return null;
		$new_val = [];
		foreach ($data as $d){
			if ($this->getParent($data,$d->id_menu))
			$new_val[$d->id_menu][]=$d->parent;
		}
		return $new_val;
	}
	public function getParent($data,$id)
	{
		foreach ($data as $d) {
			if ($d->id_menu == $id)
			return true;            
		}
		return false;
	}
    public function addValueToArrayDb($arr,$val,$param)
    {
        if (empty($arr))
            return $val;
        if (empty($val) || empty($param))
            return null;
        $new_val=[];
        $new_val=explode($param,$arr);
        if (isset($new_val)) {
            array_push($new_val,$val);
        }
        $new_val=array_values(array_filter(array_unique($new_val)));
        return implode($param,$new_val);
    }
	//property table
	public function getPropertiesTable($val)
	{
		$data=[
			'tanggal'=>null,
			'status'=>null,
			'aksi'=>null
		];
		if (empty($val))
		return $data;
		$create=null;
		$update=null;
		$status=null;
		$delete=null;
		if (isset($val['create'])) {
			$create=$this->CI->formatter->getDateTimeMonthFormatUser($val['create']);
		}
		if (isset($val['update'])) {
			$update=$this->CI->formatter->getDateTimeMonthFormatUser($val['update']);
		}
		$tanggal='<label class="label label-warning" data-toggle="tooltip" title="Dibuat Tanggal"><i class="fas fa-pen fa-fw"></i>'.$create.' WIB</label><br><label class="label label-primary" data-toggle="tooltip" title="Diupdate Tanggal"><i class="fa fa-edit fa-fw"></i>'.$update.' WIB</label>';
		if (isset($val['status'])) {
			if ($val['status'] == 1) {
				$status='<button type="button" class="stat scc" href="javascript:void(0)" onclick=do_status('.$val['id'].',0)><i class="fa fa-toggle-on"></i></button>';
			}else{
				$status='<button type="button" class="stat err" href="javascript:void(0)" onclick=do_status('.$val['id'].',1)><i class="fa fa-toggle-off"></i></button>';
			}
			if (isset($val['access']['l_ac']['stt'])) {
				$var_st=($val['status'] == 1) ? '<i class="fa fa-toggle-on stat scc" title="Tidak Diijinkan"></i>':'<i class="fa fa-toggle-off stat err" title="Tidak Diijinkan"></i>';
				$status=(in_array($val['access']['l_ac']['stt'], $val['access']['access']) && isset($val['access']['l_ac']['stt']))  ? $status : $var_st;
			}else{
				$status=$this->CI->messages->not_allow();
			}
		}
		if (isset($val['access']['l_ac']['del'])) {
			$delete = (in_array($val['access']['l_ac']['del'], $val['access']['access'])) ? '<button type="button" class="btn btn-danger btn-sm"  href="javascript:void(0)" onclick=delete_modal('.$val['id'].')><i class="fa fa-trash" data-toggle="tooltip" title="Hapus Data"></i></button> ' : null;
		}else{
			$delete = null;
		}
		$info = '<button type="button" class="btn btn-info btn-sm" href="javascript:void(0)" onclick=view_modal('.$val['id'].')><i class="fa fa-info-circle" data-toggle="tooltip" title="Detail Data"></i></button> ';
		$aksi=$info.$delete;
		$data=[
			'tanggal'=>$tanggal,
			'status'=>$status,
			'info'=>$info,
			'aksi'=>$aksi,
		];
		return $data;
	}
	public function colorPiece() {
		return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
	}
	public function getRandomColor() {
		return '#'.$this->colorPiece().$this->colorPiece().$this->colorPiece();
	}
	public function getFotoValue($foto,$kelamin)
	{
		if(empty($foto)){
			if($kelamin=='p'){
				$val = 'asset/img/user-photo/userf.png';
			}else{
				$val = 'asset/img/user-photo/user.png';
			}
		}else{
			$val = $foto;
		}
		return $val;
	}
	// getMark
    public function getMark($usage = null)
    {
        $return='<i class="fa fa-times-circle" style="color:red" data-toggle="tooltip" title="Unknown"></i> ';
        switch($usage){
            case 'danger' : $return = '<i class="fa fa-times-circle" style="color:red"></i> '; break;
            case 'warning' : $return = '<i class="fa fa-warning" style="color:orange"></i> '; break;
            case 'success' : $return = '<i class="fa fa-check-circle scc" class=""></i> '; break;
            case 'info' : $return = '<i class="fa fa-info-circle" style="color:blue"></i> '; break;
        }
        return $return;
    }
	//list menu front end
	public function getDrawMenuFront($parent,$url)
	{
		$new_val = null;
		$data=$this->CI->model_master->getListMenuUserActive();
		foreach ($data as $d)
		{
			if ($d->parent == $parent){
				if ($this->getChildren($data,$d->id_menu)) {
					$class=' class="dropdown"';
					$att=' data-toggle="dropdown" class="dropdown-toggle"';
					$urlx='#';
				}else{
					$class=null;
					$att=null;
					$urlx=$d->url;
				}
				if($d->custom != 1){
					$new_val.= '<li'.$class.'><a href="'.base_url('publik/'.$urlx).'" '.$att.' title="'.$d->nama.'">'.$d->nama.'</a>';
				}
				if($d->custom == 1){
					$new_val.= '<li'.$class.'><a href="'.base_url('publik/custom/'.$urlx).'" '.$att.' title="'.$d->nama.'">'.$d->nama.'</a>';
				}
				if ($this->getChildren($data,$d->id_menu))                                   
				$new_val.= '<ul class="dropdown-menu" role="menu">'.$this->getDrawMenuFront($d->id_menu,$url).'</ul>'; 
				$new_val.= "</li>";
			}
		}
		return $new_val;
	}
    public function getYesNoList()
    {
        $pack=[
            '1'=>'Ya',
            '0'=>'Tidak'
        ];
        return $pack;
    }
    public function getYesNo($key)
    {
        return $this->getVarFromArrayKey($key,$this->getYesNoList());
    }
    public function getListKritikSaran()
    {
        $pack=[
            '0'=>'Kritik',
            '1'=>'Saran',
        ];
        return $pack;
    }
    public function getKritikSaran($key)
    {
        return $this->getVarFromArrayKey($key,$this->getListKritikSaran());
    }
    public function getListStatusValidasi()
    {
        $pack=[
            0=>'Ditolak',
            1=>'Disetujui',
            2=>'Diproses',
        ];
        return $pack;
    }
    public function getStatusValidasi($key)
    {
        return $this->getVarFromArrayKey($key,$this->getListStatusValidasi());
    }
    public function getStatusPengajuanList()
    {
        $pack=[
            0=>'<label class="label label-danger" style="font-size:14px;"><i class="fa fa-times-circle"></i> Ditolak</label>',
            1=>'<label class="label label-success" style="font-size:14px;"><i class="fa fa-check-circle"></i> Disetujui</label>',
            2=>'<label class="label label-warning" style="font-size:14px;"><i class="fa fa-warning"></i> Diproses</label>',
        ];
        return $pack;
    }
    public function getStatusPengajuan($key)
    {
        return $this->getVarFromArrayKey($key,$this->getStatusPengajuanList());
    }
}