var fail = '<span class="ec ec-construction"></span> ';

function kode_generator(urlx, idf) {
	$.ajax({
		url: urlx,
		type: 'ajax',
		dataType: 'json',
		async: false,
		method: "POST",
		success: function (data) {
			$('#' + idf).val(data);
		}
	})
}

function select_data(id_field, urlx, table, column, name, all_item = 'no') {
	/*all_item = yes or no*/
	var datax = {
		table: table,
		column: column,
		name: name
	};
	getSelect2(urlx, id_field, datax, all_item);
}

function getAjaxData(urlx, where) {
	Pace.restart();
	var viewx;
	$.ajax({
		url: urlx,
		method: "POST",
		data: where,
		async: false,
		dataType: 'json',
		success: function (data) {
			viewx = data;
			// console.clear();
		},
		error: function (data) {
			$("body").overhang({
				type: "error",
				message: fail + 'Invalid Parameter',
				html: true
			});
		}
	});
	return viewx;
}

function submitAjax(urlx, modalx, formx, url_kode, idf_kode, usage) {
	Pace.restart();
	if (usage == 'status' || usage == 'change_skin') {
		var data = formx;
	} else {
		var data = $('#' + formx).serialize();
	}
	$.ajax({
		url: urlx,
		method: "POST",
		data: data,
		async: false,
		dataType: 'json',
		success: function (data) {
			if (usage == 'auth') {
				if (data.status_data == 'wrong') {
					$('#forget_pass').html('<a href="auth/lupa">Lupa Password?</a>');
				}
				if (data.status_data == false) {}
			} else {
				if (data.status_data == true) {
					$('#' + modalx).modal('toggle');
				}
			}
			if (data.status_data == true) {
				$("body").overhang({
					type: "success",
					message: data.msg,
					html: true
				});
			} else if (data.status_data == 'warning') {
				$("body").overhang({
					type: "warn",
					message: data.msg,
					html: true
				});
			} else {
				if (data.msg != null) {
					$("body").overhang({
						type: "error",
						message: data.msg,
						html: true
					});
				} else {
					$("body").overhang({
						type: "error",
						message: fail + 'Invalid Parameter',
						html: true
					});
				}
			}
			if (data.linkx != null) {
				window.location.href = data.linkx;
			}
			// console.clear();
		},
		error: function (data) {
			$("body").overhang({
				type: "error",
				message: fail + 'Invalid Parameter',
				html: true
			});
		}
	});
}

function loadModalAjax(urlx, modalx, datax, usage) {
	Pace.restart();
	$.ajax({
		url: urlx,
		method: "POST",
		data: datax,
		async: false,
		dataType: 'json',
		success: function (data) {
			if (usage == 'delete') {
				$('#' + modalx).html(data.modal);
				$('#delete').modal('show');
				$('#data_name_delete').html(datax['nama']);
				$('#data_column_delete').val(datax['column']);
				$('#data_id_delete').val(datax['id']);
				$('#data_table_delete').val(datax['table']);
				$('#data_table_drop').val(datax['nama_tabel']);
				$('#data_link_table').val(datax['del_link_tb']);
				$('#data_link_col').val(datax['del_link_col']);
				$('#data_link_data_col').val(datax['del_link_data_col']);
				$('#data_table_ref').val(datax['table_ref']);
			} else {
				$("body").overhang({
					type: "error",
					message: fail + 'Invalid Parameter',
					html: true
				});
			}
			// console.clear();
		},
		error: function (data) {
			$("body").overhang({
				type: "error",
				message: fail + 'Invalid Parameter',
				html: true
			});
		}
	});
}

function getAjaxCount(urlx, datax, sendx) {
	// not with php
	var nomor = datax;
	var total = parseInt(0);
	for (i = 0; i < nomor.length; i++) {
		if (nomor[i] == '') {
			total += parseInt(0);
		} else {
			total += parseInt(nomor[i]);
		}
	}
	$('#' + sendx).val(total);
	// end
}

function getSelect2(urlx, formx, datax, all_item) {
	$.ajax({
		method: "POST",
		url: urlx,
		data: datax,
		async: false,
		dataType: 'json',
		success: function (data) {
			var html = '<option></option>';
			if (all_item == 'yes') {
				html += '<option value="semua_data">Pilih Semua</option>';
			}
			$.each(data, function (key, value) {
				html += '<option value="' + key + '">' + value + '</option>';
			});
			$('#' + formx).html(html);
		},
		error: function (data) {
			$("body").overhang({
				type: "error",
				message: fail + 'Invalid Parameter',
				html: true
			});
		}
	});
}

function realtimeAjax(urlx, id) {
	Pace.ignore(function () {
		setInterval(function () {
			$.ajax({
				type: 'ajax',
				url: urlx,
				async: false,
				dataType: 'json',
				success: function (data) {
					$('#show_notif').html(data);
				}

			});
		}, 5000);
	});
}

function submitAjaxFile(urlx, datax, modalx, progx, btnx, tblx = 'table_data') {
	$.ajax({
		url: urlx,
		type: "post",
		data: datax,
		processData: false,
		contentType: false,
		cache: false,
		async: false,
		dataType: 'json',
		success: function (data) {
			$(progx).hide();
			$(btnx).removeAttr('disabled');

			if (data.status_data == true) {
				$("body").overhang({
					type: "success",
					message: data.msg,
					html: true
				});
			} else if (data.status_data == 'warning') {
				$("body").overhang({
					type: "warn",
					message: data.msg,
					html: true
				});
			} else {
				if (data.msg != null) {
					$("body").overhang({
						type: "error",
						message: data.msg,
						html: true
					});
				} else {
					$("body").overhang({
						type: "error",
						message: fail + 'Invalid Parameter',
						html: true
					});
				}
			}
			setTimeout(function () {
				$(modalx).modal('toggle');
			}, 1000);
			$('#' + tblx).DataTable().ajax.reload(function () {
				Pace.restart();
			});
		},
		error: function (data) {
			$("body").overhang({
				type: "error",
				message: fail + 'Invalid Parameter',
				html: true
			});
		}
	});
}