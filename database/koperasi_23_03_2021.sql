/*
 Navicat Premium Data Transfer

 Source Server         : Localhost_Wamp
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : localhost:3308
 Source Schema         : koperasi

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : 65001

 Date: 23/03/2021 00:32:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id_admin` bigint(255) NOT NULL AUTO_INCREMENT,
  `nama` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `username` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `level` int(2) NOT NULL DEFAULT 2,
  `foto` varchar(2000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hp` varchar(14) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kelamin` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `last_login` datetime(0) NOT NULL,
  `id_jabatan` int(255) NOT NULL DEFAULT 0,
  `email_token` varchar(4000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_verified` int(1) NOT NULL DEFAULT 0,
  `reset_token` varchar(4000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_karyawan` bigint(255) NULL DEFAULT NULL,
  `id_group` int(100) NULL DEFAULT NULL,
  `status_adm` tinyint(1) NOT NULL DEFAULT 1,
  `skin` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'skin-blue',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime(0) NOT NULL,
  `update_date` datetime(0) NOT NULL,
  `create_by` bigint(255) NOT NULL,
  `update_by` bigint(255) NOT NULL,
  PRIMARY KEY (`id_admin`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'Administrator', 'Semarang Indonesia', 'abuumarsg@gmail.com', 'superuser', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', 0, 'asset/img/admin-photo/userm.png', '085852924304', 'l', '2021-03-22 21:52:09', 0, NULL, 0, 'VnL5lzc878G7osVMJeNeHSwyghJCpDlNqva2i8iOlKnOcjYkGb5b34a053098d5', NULL, 1, 1, 'skin-blue', 1, '2018-04-23 00:00:00', '2019-09-08 00:49:42', 2, 1);
INSERT INTO `admin` VALUES (2, 'Admin', 'Semarangsfasfas', 'a@gmail.com', 'adminocky', '3755faa35795d4fb1a4431c08117fdc6b15d755b83599353539d18a888972f695b0fd56e220281dae854772b4001dc31bc12629bfebb51d6b7c6f8caee8282ad', 1, 'asset/img/admin-photo/userm.png', '00', 'l', '2020-02-06 13:14:46', 0, NULL, 0, NULL, NULL, 2, 1, 'skin-blue', 1, '2019-03-20 10:22:45', '2019-08-19 18:55:53', 2, 2);
INSERT INTO `admin` VALUES (4, 'Dony Baskoro', 'updater@admin.com', 'donybaskoro@gmail.com', 'updater', '3755faa35795d4fb1a4431c08117fdc6b15d755b83599353539d18a888972f695b0fd56e220281dae854772b4001dc31bc12629bfebb51d6b7c6f8caee8282ad', 1, 'asset/img/admin-photo/userm.png', '-13', 'l', '2020-12-04 11:50:55', 0, NULL, 0, NULL, NULL, 3, 1, 'skin-blue', 1, '2020-07-17 11:18:56', '2020-07-17 11:18:56', 4, 4);
INSERT INTO `admin` VALUES (5, 'Admin Pembelajaran', 'semarang', 'pembelajaran@gmail.com', 'pembelajaran', '0dd3e512642c97ca3f747f9a76e374fbda73f9292823c0313be9d78add7cdd8f72235af0c553dd26797e78e1854edee0ae002f8aba074b066dfce1af114e32f8', 1, 'asset/img/admin-photo/userm.png', '0899999999', 'l', '2020-08-23 21:53:22', 0, NULL, 0, NULL, NULL, 4, 1, 'skin-blue', 0, '2020-08-21 14:40:08', '2020-08-21 14:40:08', 1, 1);

-- ----------------------------
-- Table structure for data_anggota
-- ----------------------------
DROP TABLE IF EXISTS `data_anggota`;
CREATE TABLE `data_anggota`  (
  `id_anggota` int(12) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_ktp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tempat_lahir` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal_lahir` date NULL DEFAULT NULL,
  `kelamin` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_hp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `simpanan_pokok` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pekerjaan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal_masuk` date NULL DEFAULT NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `foto` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` smallint(2) NULL DEFAULT NULL,
  `create_by` smallint(3) NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_by` smallint(3) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_anggota`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of data_anggota
-- ----------------------------
INSERT INTO `data_anggota` VALUES (2, 'ANG0002', 'FARA SAFITRI', '123124', 'demak', '2021-03-09', 'p', '2353463', '100000', NULL, '2021-03-09', '--', 'asset/img/anggota/images.png', 1, 1, '2021-03-10 12:04:23', 1, '2021-03-10 12:04:23');
INSERT INTO `data_anggota` VALUES (3, 'ANG0003', 'muttaqin', '332143083485783', 'Semarang', '2021-03-03', 'l', '089732649876', '100000', 'PNS', '2021-03-14', 'Semarang', 'asset/img/user-photo/user.png', 1, 1, '2021-03-15 22:13:34', 1, '2021-03-15 22:13:34');
INSERT INTO `data_anggota` VALUES (4, 'ANG0004', 'AGUNG TRI LAKSONO', '33124234234235', 'Kab Semarang', '2021-03-19', 'l', '0651236712312', '150000', 'wiraswasta', '2021-03-02', 'bergas', 'asset/img/user-photo/user.png', 1, 1, '2021-03-15 22:36:14', 1, '2021-03-15 22:36:14');
INSERT INTO `data_anggota` VALUES (5, 'ANG0005', 'SARWOTO', 'asd234234', 'asdwERD', '2021-03-17', 'l', '98', '2000', 'asd', '2021-03-09', 'asd', 'asset/img/user-photo/user.png', 1, 1, '2021-03-15 22:38:38', 1, '2021-03-15 22:38:38');
INSERT INTO `data_anggota` VALUES (7, 'ANG0006', 'MUHAMAD MURTADHO', 'asd234234', 'asdwERD', '2021-03-16', 'l', '0651236712312', '200000', '-', '2021-03-18', '-', 'asset/img/user-photo/user.png', 1, 1, '2021-03-15 22:51:08', 1, '2021-03-15 22:51:08');

-- ----------------------------
-- Table structure for data_company
-- ----------------------------
DROP TABLE IF EXISTS `data_company`;
CREATE TABLE `data_company`  (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name_office` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_hp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `maps` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `visi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `misi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `moto` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `sejarah` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `about` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `gambar_sejarah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gambar_visi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of data_company
-- ----------------------------
INSERT INTO `data_company` VALUES (1, 'Koperasi', 'Koperasi', 'Jl. Semarang 50256\r\n', 'ss@gmail.com', '024 6724865asd', 'asset/img/logo-koperasi-13099.png', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15839.609712875234!2d110.4550034!3d-7.0207545!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xaad944273e6d081f!2sYayasan+Kesejahteraan+Keluarga+Soegijapranata!5e0!3m2!1sid!2sid!4v1563856779019!5m2!1sid!2sid', '<p>Melayani Tuhan melalui sesama</p>', '<ol style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Trebuchet MS\', Arial, Helvetica, sans-serif; font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;\">\r\n<li>Pelayanan partisipatif untuk mencapai kemandirian dalam mengelola ekononomi rumah tangga untuk kesejahteraan anak dan keluarga, melalui kegiatan pendidikan, kesehatan, sosial ekonomi dan pemberdayaan masyarakat.</li>\r\n<li>Menguatkan kapasitas kelembagaan maupun masyarakat secara terus-menerus untuk meningkatkan pengetahuan manajerial dalam usaha-usaha produktif melalui pengembangan dan penguatan jaringan mitra kerja, menegakkan hak asasi manusia tanpa mengesampingkan pelestarian tradisi/budaya lokal dan sumber daya alam.</li>\r\n<li>Mendorong anak-anak dan remaja tampil menjadi agen perubahan dan pemimpin yang berdaya di komunitas mereka.</li>\r\n<li>Menjalin kerjasama kemitraan untuk mendorong terpenuhinya hak dan kesejahteraan anak.</li>\r\n</ol>', '<p><strong>Legalitas lembaga </strong></p>\r\n<p>Yayasan Kesejahteraan Keluarga Soegijapranata didirikan dengan akte notaris Subiyanto Putro, SH No 31 Tahun 2005 tanggal 25 Januari 2005, SK Menkum Ham RI No C-346.HT.01.02.TH. 2006 dan Surat Tanda Daftar di Dinas Sosial Kota Semarang No 446.3/3744 tahun 2014.</p>', '<p>Yayasan Kesejahteraan Keluarga Soegijapranata disingkat YKKS merupakan organisasi non pemerintah yang berdiri sejak tahun 1977 di Semarang. YKKS merupakan organisasi yang terbuka untuk bekerjasama dengan siapa saja yang peduli pada parsoalan perlindungan dan kesejahteraan anak. Sejak tahun 1977 &nbsp;sampai semarang yayasan ini bekerjasama dengan Christian Children Fund (CCF) kini ChildFund Indonesia&nbsp; untuk program sponsorship. Pernah bermitra dengan CCF Jerman untuk penanganan anak jalanan tahun 2005-2009. Bekerjasama dengan Fontera Australia untuk pengembangan anak usia dini di Kota dan Kabupaten Semarang. &nbsp;Yayasan ini didirikan untuk merespon permasalahan anak yang ada di Kota dan Kabupaten Semarang. Tahun 2010 Yayasan Sosial Sidomulyo yang mengelola GSM Sidomulyo yang melayani di wilayah Kabupaten Semarang&nbsp; bergabung dengan YKKS. &nbsp;Masalah yang dihadapi anak-anak Kota dan Kabupaten Semarang ialah kemiskinan dan kekerasan kepada anak. YKKS bekerja di Kalurahan Tandang dan Sendangguwo, Kecamatan Tembalang, Kota Semarang dan di Desa Gondoriyo, Kecamatan Bergas, Desa Wonorejo Kecamatan Pringapus dan Desa Tlompakan Kecamatan Tuntang Kabupaten Semarang.</p>', '<p>Yayasan Kesejahteraan Keluarga Soegijapranata disingkat YKKS merupakan organisasi non pemerintah yang berdiri sejak tahun 1977 di Semarang. YKKS merupakan organisasi yang terbuka untuk bekerjasama dengan siapa saja yang peduli pada parsoalan perlindungan dan kesejahteraan anak.&nbsp;</p>', 'asset/img/Kantor_YKKS.png', 'asset/img/YKKS_(1).png');

-- ----------------------------
-- Table structure for data_pengajuan
-- ----------------------------
DROP TABLE IF EXISTS `data_pengajuan`;
CREATE TABLE `data_pengajuan`  (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_anggota` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_pengajuan` date NULL DEFAULT NULL,
  `besar_pinjam` decimal(65, 0) NULL DEFAULT NULL,
  `jenis_pinjam` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `angsuran` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `validasi` tinyint(2) NULL DEFAULT NULL,
  `status_pinjaman` tinyint(2) NULL DEFAULT NULL,
  `status` tinyint(2) NULL DEFAULT NULL,
  `create_by` tinyint(2) NULL DEFAULT NULL,
  `update_by` tinyint(2) NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of data_pengajuan
-- ----------------------------
INSERT INTO `data_pengajuan` VALUES (1, 'PGJN0001', 'ANG0002', '2021-03-22', 1000000, 'PNJ202103080001', '125000', 2, 0, 1, 1, 1, '2021-03-22 22:58:56', '2021-03-22 22:59:02');
INSERT INTO `data_pengajuan` VALUES (3, '001/PGJN/III/2021', 'ANG0004', '2021-03-18', 1000000, 'PNJ202103060001', '255000', 2, 0, 1, 1, 1, '2021-03-23 00:23:42', '2021-03-23 00:23:42');

-- ----------------------------
-- Table structure for data_tabungan
-- ----------------------------
DROP TABLE IF EXISTS `data_tabungan`;
CREATE TABLE `data_tabungan`  (
  `id_tabungan` int(12) NOT NULL AUTO_INCREMENT,
  `kode_tabungan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_anggota` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `besar_tabungan` decimal(65, 2) NULL DEFAULT NULL,
  `status` tinyint(2) NULL DEFAULT NULL,
  `create_by` tinyint(2) NULL DEFAULT NULL,
  `update_by` tinyint(2) NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_tabungan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of data_tabungan
-- ----------------------------
INSERT INTO `data_tabungan` VALUES (1, 'TAB0001', 'ANG0006', '2021-03-18', 180000.00, 1, 1, 1, '2021-03-15 22:51:08', '2021-03-16 01:30:39');

-- ----------------------------
-- Table structure for karyawan2
-- ----------------------------
DROP TABLE IF EXISTS `karyawan2`;
CREATE TABLE `karyawan2`  (
  `id_karyawan` bigint(255) NOT NULL AUTO_INCREMENT,
  `nik` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_finger` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tempat_lahir` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_lahir` date NULL DEFAULT NULL,
  `no_hp` varchar(14) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_pajak` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `agama` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kelamin` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jabatan` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_sub` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `loker` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `grade` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_disiplin` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `npwp` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `foto` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'asset/img/user-photo/user.png',
  `foto_ttd` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gol_darah` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rekening` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_bank` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bpjskes` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bpjstk` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_ktp` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `berat_badan` int(3) NULL DEFAULT NULL,
  `tinggi_badan` int(3) NULL DEFAULT NULL,
  `pendidikan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `universitas` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jurusan` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_karyawan` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_perjanjian` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_masuk` date NULL DEFAULT NULL,
  `alamat_asal_jalan` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_asal_desa` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_asal_kecamatan` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_asal_kabupaten` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_asal_provinsi` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_asal_pos` int(100) NULL DEFAULT NULL,
  `alamat_sekarang_jalan` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_sekarang_desa` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_sekarang_kecamatan` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_sekarang_kabupaten` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_sekarang_provinsi` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_sekarang_pos` int(100) NULL DEFAULT NULL,
  `nama_ayah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tempat_lahir_ayah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal_lahir_ayah` date NULL DEFAULT NULL,
  `pendidikan_terakhir_ayah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_hp_ayah` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_ayah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `desa_ayah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kecamatan_ayah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kabupaten_ayah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `provinsi_ayah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_pos_ayah` int(12) NULL DEFAULT NULL,
  `nama_ibu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tempat_lahir_ibu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal_lahir_ibu` date NULL DEFAULT NULL,
  `no_hp_ibu` varchar(22) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pendidikan_terakhir_ibu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_ibu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `desa_ibu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kecamatan_ibu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kabupaten_ibu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `provinsi_ibu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_pos_ibu` int(22) NULL DEFAULT NULL,
  `nama_pasangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tempat_lahir_pasangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal_lahir_pasangan` date NULL DEFAULT NULL,
  `no_hp_pasangan` varchar(22) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pendidikan_terakhir_pasangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_pasangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `desa_pasangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kecamatan_pasangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kabupaten_pasangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `provinsi_pasangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_pos_pasangan` int(22) NULL DEFAULT NULL,
  `status_nikah` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jumlah_anak` int(3) NULL DEFAULT NULL,
  `last_login` datetime(0) NOT NULL,
  `email_token` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email_verified` int(1) NOT NULL DEFAULT 0,
  `reset_token` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jabatan_pa` bigint(255) NULL DEFAULT NULL,
  `loker_pa` bigint(255) NULL DEFAULT NULL,
  `gaji` float NOT NULL,
  `gaji_pokok` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sepatu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `baju` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `metode_pph` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_penggajian` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jabatan_sekunder` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `skin` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sisa_cuti` int(255) NOT NULL,
  `status_emp` tinyint(1) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime(0) NOT NULL,
  `update_date` datetime(0) NOT NULL,
  `create_by` bigint(255) NOT NULL,
  `update_by` bigint(255) NOT NULL,
  PRIMARY KEY (`id_karyawan`) USING BTREE,
  INDEX `nik`(`nik`) USING BTREE,
  INDEX `no_ktp`(`no_ktp`) USING BTREE,
  INDEX `id_finger`(`id_finger`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of karyawan2
-- ----------------------------
INSERT INTO `karyawan2` VALUES (1, '2205190519', '2222', 'WEWEEEDS', 'SEMARANG', '2019-05-22', '2432', 'TK/3', 'k_protestan', 'l', 'JBT201901090004', NULL, 'LOK201809260001', 'GRD201904120003', NULL, '42343', 'a@a.com', 'asset/img/user-photo/user.png', NULL, 'b', '24234234', 'BANK201901170003', '234234', '23423', '1244324', 56, 167, NULL, NULL, NULL, 'STK201901090002', '', '2019-05-30', 'SEMARANG', 'SEMARANG', 'SEMARANG', 'SEMARANG', 'SEMARANG', 34234, 'SEMARANG', 'SEMARANG', 'SEMARANG', 'SEMARANG', 'SEMARANG', 34234, 'DEDE', 'DEMAK', '2019-05-28', 'SLTP', '234234', 'SEMARANG', 'SEMARANG', 'SEMARANG', 'SEMARANG', 'SEMARANG', 34234, 'REWR', 'DEMAK', '2019-05-13', '324', 'SD', 'SEMARANG', 'SEMARANG', 'SEMARANG', 'SEMARANG', 'SEMARANG', 34234, 'EWRWRE', 'FEFE', '2019-05-21', '324', 'SLTA', 'SEMARANG', 'SEMARANG', 'SEMARANG', 'SEMARANG', 'SEMARANG', 34234, 'NIKAH', NULL, '2019-07-09 14:15:29', '', 0, NULL, 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', NULL, NULL, 0, NULL, '43', 's', 'nett', 'GAJI201903280003', 'JBT201901170001;JBT201901160004', 'dark-mode', 20, 1, 1, '2019-05-15 09:33:07', '2019-06-28 10:44:52', 2, 459);

-- ----------------------------
-- Table structure for log_login_admin
-- ----------------------------
DROP TABLE IF EXISTS `log_login_admin`;
CREATE TABLE `log_login_admin`  (
  `id_admin` int(255) NOT NULL,
  `tgl_login` datetime(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of log_login_admin
-- ----------------------------
INSERT INTO `log_login_admin` VALUES (1, '2018-10-10 07:00:45');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-10 07:33:26');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-10 08:13:44');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-10 08:41:06');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-10 08:49:19');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-10 08:49:49');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-22 20:43:37');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-24 14:50:54');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 08:12:55');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 08:12:27');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 08:51:25');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 09:33:59');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 09:34:19');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 09:39:11');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 10:21:26');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 11:47:50');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 13:09:48');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 13:29:44');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 13:37:52');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 14:22:21');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 16:28:00');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 18:10:34');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 19:29:59');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 19:30:31');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 21:45:02');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 21:45:16');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-29 22:37:23');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-30 08:27:15');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-30 09:48:03');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-30 10:19:09');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-30 10:20:35');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-30 10:46:40');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-30 11:37:56');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-30 13:08:24');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-30 13:17:08');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-31 08:48:13');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-31 09:59:45');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-31 11:00:33');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-31 13:43:44');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-31 14:21:12');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-31 15:46:42');
INSERT INTO `log_login_admin` VALUES (1, '2018-10-31 21:01:13');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-01 08:40:57');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-01 09:26:12');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-01 13:45:01');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-01 15:42:19');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-02 20:50:42');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-06 16:34:46');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-06 18:21:30');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-06 19:57:08');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-07 23:24:26');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-13 17:03:32');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-13 18:13:43');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-13 18:45:57');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-13 20:43:17');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-13 21:00:47');
INSERT INTO `log_login_admin` VALUES (1, '2018-11-13 21:45:03');
INSERT INTO `log_login_admin` VALUES (1, '2018-12-06 23:30:03');
INSERT INTO `log_login_admin` VALUES (1, '2018-12-11 10:09:26');
INSERT INTO `log_login_admin` VALUES (1, '2018-12-11 10:34:09');
INSERT INTO `log_login_admin` VALUES (1, '2018-12-11 12:07:05');
INSERT INTO `log_login_admin` VALUES (1, '2018-12-11 15:46:31');
INSERT INTO `log_login_admin` VALUES (1, '2018-12-12 11:49:21');
INSERT INTO `log_login_admin` VALUES (1, '2018-12-12 13:28:05');
INSERT INTO `log_login_admin` VALUES (1, '2018-12-13 15:04:35');
INSERT INTO `log_login_admin` VALUES (1, '2018-12-14 14:56:38');
INSERT INTO `log_login_admin` VALUES (1, '2018-12-14 15:45:20');
INSERT INTO `log_login_admin` VALUES (1, '2018-12-17 16:24:30');
INSERT INTO `log_login_admin` VALUES (1, '2018-12-17 19:13:52');
INSERT INTO `log_login_admin` VALUES (1, '2018-12-17 19:58:11');
INSERT INTO `log_login_admin` VALUES (3, '2018-12-27 22:27:28');
INSERT INTO `log_login_admin` VALUES (1, '2019-01-14 14:38:12');
INSERT INTO `log_login_admin` VALUES (1, '2019-01-16 15:14:08');
INSERT INTO `log_login_admin` VALUES (1, '2019-02-11 20:51:01');
INSERT INTO `log_login_admin` VALUES (1, '2019-03-18 11:20:08');
INSERT INTO `log_login_admin` VALUES (1, '2019-03-18 11:33:48');
INSERT INTO `log_login_admin` VALUES (3, '2019-04-01 12:19:51');
INSERT INTO `log_login_admin` VALUES (3, '2019-04-01 17:36:16');
INSERT INTO `log_login_admin` VALUES (3, '2019-04-01 17:38:20');
INSERT INTO `log_login_admin` VALUES (3, '2019-04-01 17:40:31');
INSERT INTO `log_login_admin` VALUES (3, '2019-04-01 17:42:44');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-02 08:48:17');
INSERT INTO `log_login_admin` VALUES (3, '2019-04-04 08:25:48');
INSERT INTO `log_login_admin` VALUES (3, '2019-04-04 08:26:57');
INSERT INTO `log_login_admin` VALUES (3, '2019-04-04 08:34:09');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-04 08:34:35');
INSERT INTO `log_login_admin` VALUES (3, '2019-04-04 08:46:09');
INSERT INTO `log_login_admin` VALUES (3, '2019-04-04 08:49:08');
INSERT INTO `log_login_admin` VALUES (3, '2019-04-04 11:08:10');
INSERT INTO `log_login_admin` VALUES (3, '2019-04-04 11:53:06');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-04 16:13:52');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-04 21:07:28');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-05 13:07:24');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-09 08:35:51');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-09 09:52:58');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-09 14:08:47');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-09 19:15:59');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-09 21:08:02');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-10 08:14:05');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-10 12:22:15');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-10 15:09:36');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-10 15:14:53');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-11 08:48:57');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-11 09:18:16');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-11 10:03:35');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-11 10:26:01');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-11 12:13:07');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-11 14:59:12');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-11 19:14:34');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-12 08:15:21');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-12 10:39:43');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-15 08:57:15');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-15 13:58:06');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-22 09:20:51');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-22 13:54:15');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-24 09:09:01');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-24 16:50:37');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-25 08:58:08');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-26 09:31:12');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-26 14:23:59');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-29 09:12:06');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-29 22:35:32');
INSERT INTO `log_login_admin` VALUES (1, '2019-04-30 09:08:40');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-02 09:34:20');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-02 13:39:31');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-02 13:53:24');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-02 21:38:03');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-03 08:36:54');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-03 14:39:00');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-06 09:18:07');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-06 09:29:06');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-06 17:07:26');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-07 08:36:27');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-07 10:03:32');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-07 14:29:02');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-07 16:56:00');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-08 08:53:39');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-08 15:44:03');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-09 09:04:45');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-09 14:25:07');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-09 14:53:55');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-10 09:21:27');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-10 13:47:28');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-13 08:50:20');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-13 14:05:16');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-14 14:12:53');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-15 11:25:39');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-15 11:26:40');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-15 12:52:12');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-15 16:41:50');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-16 08:58:31');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-17 09:24:31');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-20 08:38:29');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-20 15:31:37');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-20 16:35:08');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-20 16:35:10');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-21 08:57:43');
INSERT INTO `log_login_admin` VALUES (5, '2019-05-21 13:42:40');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-22 08:17:15');
INSERT INTO `log_login_admin` VALUES (5, '2019-05-22 08:20:33');
INSERT INTO `log_login_admin` VALUES (5, '2019-05-22 10:06:02');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-22 10:46:19');
INSERT INTO `log_login_admin` VALUES (5, '2019-05-27 08:20:29');
INSERT INTO `log_login_admin` VALUES (1, '2019-05-27 11:41:38');
INSERT INTO `log_login_admin` VALUES (5, '2019-05-27 21:22:00');
INSERT INTO `log_login_admin` VALUES (6, '2019-07-22 23:39:21');
INSERT INTO `log_login_admin` VALUES (2, '2019-07-22 23:39:49');
INSERT INTO `log_login_admin` VALUES (6, '2019-07-22 23:51:12');
INSERT INTO `log_login_admin` VALUES (6, '2019-07-22 23:54:55');
INSERT INTO `log_login_admin` VALUES (6, '2019-07-22 23:55:01');
INSERT INTO `log_login_admin` VALUES (6, '2019-07-22 23:55:05');
INSERT INTO `log_login_admin` VALUES (6, '2019-07-22 23:56:22');
INSERT INTO `log_login_admin` VALUES (6, '2019-07-23 00:17:00');
INSERT INTO `log_login_admin` VALUES (2, '2019-07-23 00:53:12');
INSERT INTO `log_login_admin` VALUES (2, '2019-07-23 07:57:43');
INSERT INTO `log_login_admin` VALUES (2, '2019-07-23 09:06:15');
INSERT INTO `log_login_admin` VALUES (2, '2019-07-25 23:00:02');
INSERT INTO `log_login_admin` VALUES (2, '2019-07-29 20:55:49');
INSERT INTO `log_login_admin` VALUES (2, '2019-07-30 20:55:23');
INSERT INTO `log_login_admin` VALUES (2, '2019-07-31 23:11:15');
INSERT INTO `log_login_admin` VALUES (2, '2019-08-01 08:00:03');
INSERT INTO `log_login_admin` VALUES (2, '2019-08-02 21:46:58');
INSERT INTO `log_login_admin` VALUES (2, '2019-08-03 01:11:57');
INSERT INTO `log_login_admin` VALUES (2, '2019-08-03 10:28:25');
INSERT INTO `log_login_admin` VALUES (2, '2019-08-12 22:36:56');
INSERT INTO `log_login_admin` VALUES (1, '2019-08-13 00:34:12');
INSERT INTO `log_login_admin` VALUES (2, '2019-08-13 00:47:19');
INSERT INTO `log_login_admin` VALUES (1, '2019-08-13 00:48:03');
INSERT INTO `log_login_admin` VALUES (2, '2019-08-13 22:09:49');
INSERT INTO `log_login_admin` VALUES (1, '2019-08-14 15:40:41');
INSERT INTO `log_login_admin` VALUES (1, '2019-08-14 21:15:23');
INSERT INTO `log_login_admin` VALUES (2, '2019-08-15 08:33:42');
INSERT INTO `log_login_admin` VALUES (1, '2019-08-15 15:31:22');
INSERT INTO `log_login_admin` VALUES (1, '2019-08-15 15:32:15');
INSERT INTO `log_login_admin` VALUES (1, '2019-08-15 21:37:58');
INSERT INTO `log_login_admin` VALUES (2, '2019-08-16 08:38:46');
INSERT INTO `log_login_admin` VALUES (2, '2019-08-18 21:36:22');
INSERT INTO `log_login_admin` VALUES (2, '2019-08-19 08:51:30');
INSERT INTO `log_login_admin` VALUES (1, '2019-08-19 08:52:37');
INSERT INTO `log_login_admin` VALUES (2, '2019-08-19 18:55:17');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:02');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:21');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:23');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:28');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:29');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:36');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:36');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:41');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:41');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:43');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:44');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:44');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:44');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:45');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-02 20:57:46');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-08 00:47:08');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-08 00:50:10');
INSERT INTO `log_login_admin` VALUES (1, '2019-09-09 14:11:07');
INSERT INTO `log_login_admin` VALUES (4, '2019-09-10 13:38:27');
INSERT INTO `log_login_admin` VALUES (4, '2019-09-11 13:04:48');
INSERT INTO `log_login_admin` VALUES (4, '2020-02-06 13:11:49');
INSERT INTO `log_login_admin` VALUES (2, '2020-02-06 13:14:46');
INSERT INTO `log_login_admin` VALUES (4, '2020-06-22 20:03:42');
INSERT INTO `log_login_admin` VALUES (4, '2020-06-25 12:38:03');
INSERT INTO `log_login_admin` VALUES (4, '2020-07-17 11:01:20');
INSERT INTO `log_login_admin` VALUES (4, '2020-07-17 11:21:10');
INSERT INTO `log_login_admin` VALUES (4, '2020-07-17 11:33:40');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-03 09:53:20');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-03 10:00:25');
INSERT INTO `log_login_admin` VALUES (4, '2020-08-04 13:44:25');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-04 13:50:19');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-04 13:53:57');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-04 14:00:43');
INSERT INTO `log_login_admin` VALUES (4, '2020-08-14 11:30:48');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-16 02:13:47');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-21 14:37:55');
INSERT INTO `log_login_admin` VALUES (5, '2020-08-21 14:40:29');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-21 14:41:31');
INSERT INTO `log_login_admin` VALUES (5, '2020-08-21 14:42:56');
INSERT INTO `log_login_admin` VALUES (5, '2020-08-21 22:58:25');
INSERT INTO `log_login_admin` VALUES (5, '2020-08-22 03:04:05');
INSERT INTO `log_login_admin` VALUES (5, '2020-08-23 21:53:22');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-24 02:46:26');
INSERT INTO `log_login_admin` VALUES (4, '2020-08-24 05:22:20');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-24 05:22:48');
INSERT INTO `log_login_admin` VALUES (4, '2020-08-24 05:24:30');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-24 05:25:23');
INSERT INTO `log_login_admin` VALUES (4, '2020-08-24 20:12:46');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-24 20:14:47');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-25 09:34:27');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-25 09:37:36');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-25 11:57:45');
INSERT INTO `log_login_admin` VALUES (4, '2020-08-25 13:01:34');
INSERT INTO `log_login_admin` VALUES (4, '2020-08-25 20:23:42');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-26 09:32:03');
INSERT INTO `log_login_admin` VALUES (4, '2020-08-26 19:36:00');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-27 08:53:39');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-28 09:06:42');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-28 09:09:53');
INSERT INTO `log_login_admin` VALUES (4, '2020-08-28 12:34:44');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-31 08:50:01');
INSERT INTO `log_login_admin` VALUES (1, '2020-08-31 08:50:07');
INSERT INTO `log_login_admin` VALUES (4, '2020-09-03 13:07:12');
INSERT INTO `log_login_admin` VALUES (4, '2020-09-23 14:00:48');
INSERT INTO `log_login_admin` VALUES (4, '2020-09-24 13:03:11');
INSERT INTO `log_login_admin` VALUES (4, '2020-09-26 11:05:27');
INSERT INTO `log_login_admin` VALUES (4, '2020-09-29 14:53:28');
INSERT INTO `log_login_admin` VALUES (4, '2020-12-04 11:50:55');
INSERT INTO `log_login_admin` VALUES (1, '2021-01-04 21:49:49');
INSERT INTO `log_login_admin` VALUES (1, '2021-01-17 22:29:02');
INSERT INTO `log_login_admin` VALUES (1, '2021-03-03 22:52:52');
INSERT INTO `log_login_admin` VALUES (1, '2021-03-03 23:13:01');
INSERT INTO `log_login_admin` VALUES (1, '2021-03-04 21:11:32');
INSERT INTO `log_login_admin` VALUES (1, '2021-03-05 23:01:32');
INSERT INTO `log_login_admin` VALUES (1, '2021-03-05 23:05:48');
INSERT INTO `log_login_admin` VALUES (1, '2021-03-08 22:15:15');
INSERT INTO `log_login_admin` VALUES (1, '2021-03-09 02:15:13');
INSERT INTO `log_login_admin` VALUES (1, '2021-03-10 08:49:54');
INSERT INTO `log_login_admin` VALUES (1, '2021-03-11 04:30:45');
INSERT INTO `log_login_admin` VALUES (1, '2021-03-12 23:53:00');
INSERT INTO `log_login_admin` VALUES (1, '2021-03-15 22:05:08');
INSERT INTO `log_login_admin` VALUES (1, '2021-03-20 04:36:06');
INSERT INTO `log_login_admin` VALUES (1, '2021-03-22 21:52:09');

-- ----------------------------
-- Table structure for log_login_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `log_login_karyawan`;
CREATE TABLE `log_login_karyawan`  (
  `id_karyawan` bigint(255) NOT NULL,
  `tgl_login` datetime(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of log_login_karyawan
-- ----------------------------
INSERT INTO `log_login_karyawan` VALUES (455, '2018-12-27 22:27:28');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-02-28 08:32:25');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-02-28 17:30:45');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-01 12:19:51');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-01 17:36:16');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-01 17:38:20');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-01 17:40:31');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-01 17:42:44');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-04 08:25:49');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-04 08:26:57');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-04 08:33:48');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-04 08:34:09');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-04 08:45:56');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-04 08:46:09');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-04 08:48:42');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-04 08:49:08');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-04 11:08:10');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-04-04 11:53:06');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-08 17:12:01');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-09 14:42:58');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-10 08:32:25');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-10 15:07:42');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-13 08:25:21');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-13 12:10:16');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-14 08:38:58');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-14 11:48:04');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-15 08:09:54');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-05-15 13:27:06');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-16 08:16:31');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-16 14:48:57');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-17 08:25:41');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-20 08:16:35');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-21 08:27:59');
INSERT INTO `log_login_karyawan` VALUES (452, '2019-05-21 11:39:37');
INSERT INTO `log_login_karyawan` VALUES (110, '2019-05-21 13:42:40');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-22 08:20:28');
INSERT INTO `log_login_karyawan` VALUES (110, '2019-05-22 08:20:33');
INSERT INTO `log_login_karyawan` VALUES (110, '2019-05-22 10:06:02');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-23 09:30:41');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-24 08:18:52');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-24 15:07:55');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-27 08:18:58');
INSERT INTO `log_login_karyawan` VALUES (110, '2019-05-27 08:20:29');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-27 13:12:09');
INSERT INTO `log_login_karyawan` VALUES (110, '2019-05-27 21:22:00');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-28 08:18:34');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-05-28 13:53:58');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-10 12:01:50');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-11 08:21:20');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-06-11 09:38:23');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-06-12 08:15:32');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-12 08:26:37');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-13 07:59:13');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-06-13 08:14:48');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-06-13 11:08:10');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-06-13 14:33:42');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-14 08:04:30');
INSERT INTO `log_login_karyawan` VALUES (455, '2019-06-14 08:27:38');
INSERT INTO `log_login_karyawan` VALUES (457, '2019-06-14 09:00:24');
INSERT INTO `log_login_karyawan` VALUES (457, '2019-06-14 13:27:45');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-06-14 14:27:11');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-17 08:26:50');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-18 08:20:46');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-19 08:28:21');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-19 12:20:58');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-20 08:57:15');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-24 08:15:14');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-06-24 08:19:55');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-06-24 10:31:50');
INSERT INTO `log_login_karyawan` VALUES (37, '2019-06-24 15:16:39');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-06-25 08:10:47');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-25 08:16:32');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-06-25 09:24:55');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-06-25 13:29:53');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-06-25 13:36:26');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-25 13:49:03');
INSERT INTO `log_login_karyawan` VALUES (449, '2019-06-26 10:26:13');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-06-27 10:29:57');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-06-27 10:30:34');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-06-27 13:51:32');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-06-28 08:34:16');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-07-01 08:19:22');
INSERT INTO `log_login_karyawan` VALUES (67, '2019-07-02 09:15:23');
INSERT INTO `log_login_karyawan` VALUES (67, '2019-07-02 13:02:06');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-07-05 08:48:51');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-07-05 13:46:32');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-07-08 08:25:37');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-07-08 10:28:59');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-07-08 16:00:11');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-07-08 16:02:24');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-07-08 16:17:29');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-07-09 08:24:11');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-07-09 11:40:39');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-07-09 14:15:29');
INSERT INTO `log_login_karyawan` VALUES (1, '2019-07-12 16:49:54');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-07-12 16:51:00');
INSERT INTO `log_login_karyawan` VALUES (459, '2019-07-12 16:53:42');

-- ----------------------------
-- Table structure for master_access
-- ----------------------------
DROP TABLE IF EXISTS `master_access`;
CREATE TABLE `master_access`  (
  `id_access` int(100) NOT NULL AUTO_INCREMENT,
  `kode_access` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime(0) NOT NULL,
  `update_date` datetime(0) NOT NULL,
  `create_by` bigint(255) NOT NULL,
  `update_by` bigint(255) NOT NULL,
  PRIMARY KEY (`id_access`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of master_access
-- ----------------------------
INSERT INTO `master_access` VALUES (1, 'ADD', 'Tambah', 1, '2018-04-27 09:33:57', '2019-07-29 23:22:45', 0, 2);
INSERT INTO `master_access` VALUES (2, 'EDT', 'Edit', 1, '2018-04-27 09:35:39', '2018-04-27 09:35:39', 0, 0);
INSERT INTO `master_access` VALUES (3, 'DEL', 'Hapus', 1, '2018-04-27 09:35:52', '2018-04-27 09:35:52', 0, 0);
INSERT INTO `master_access` VALUES (4, 'APR', 'Approve', 1, '2018-04-27 09:36:27', '2018-04-27 09:36:27', 0, 0);
INSERT INTO `master_access` VALUES (5, 'STT', 'Ganti Status', 1, '2018-04-27 09:37:11', '2018-04-27 09:37:11', 0, 0);
INSERT INTO `master_access` VALUES (6, 'EXP', 'Export', 1, '2018-05-08 08:43:46', '2018-05-08 08:43:46', 0, 0);
INSERT INTO `master_access` VALUES (7, 'IMP', 'Import', 1, '2018-05-08 08:44:04', '2018-05-08 08:44:04', 0, 0);
INSERT INTO `master_access` VALUES (8, 'RKP', 'Rekap', 1, '2018-05-20 16:31:18', '2018-05-20 16:31:18', 0, 0);
INSERT INTO `master_access` VALUES (9, 'PRN', 'Print', 1, '2018-05-20 16:31:32', '2018-05-20 16:31:32', 0, 0);

-- ----------------------------
-- Table structure for master_kategori_gambar
-- ----------------------------
DROP TABLE IF EXISTS `master_kategori_gambar`;
CREATE TABLE `master_kategori_gambar`  (
  `id_kategori` int(25) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `create_by` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `update_by` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_kategori`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_kategori_gambar
-- ----------------------------
INSERT INTO `master_kategori_gambar` VALUES (7, 'Hubungan Sponsor', '', '1', '4', '4', '2019-09-06 12:40:10', '2019-09-06 12:40:10');
INSERT INTO `master_kategori_gambar` VALUES (8, 'Perlindungan Anak', '', '1', '4', '4', '2019-09-06 12:40:36', '2019-09-06 12:40:36');
INSERT INTO `master_kategori_gambar` VALUES (9, 'Forum Anak', '', '1', '4', '4', '2019-09-06 12:40:48', '2019-09-06 12:40:48');
INSERT INTO `master_kategori_gambar` VALUES (10, 'PKHLK', '', '1', '4', '4', '2019-09-06 12:41:04', '2019-09-06 12:41:04');
INSERT INTO `master_kategori_gambar` VALUES (11, 'Positive Parenting', '', '1', '4', '4', '2019-09-06 12:41:18', '2019-09-06 12:41:18');
INSERT INTO `master_kategori_gambar` VALUES (12, 'Responsive Parenting', '', '1', '4', '4', '2019-09-06 12:41:35', '2019-09-06 12:41:35');

-- ----------------------------
-- Table structure for master_menu
-- ----------------------------
DROP TABLE IF EXISTS `master_menu`;
CREATE TABLE `master_menu`  (
  `id_menu` bigint(255) NOT NULL AUTO_INCREMENT,
  `nama` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `parent` bigint(255) NULL DEFAULT NULL,
  `sequence` int(100) NOT NULL,
  `url` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sub_url` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `icon` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'fa-chevron-circle-right',
  `status` tinyint(1) NOT NULL,
  `create_date` datetime(0) NOT NULL,
  `update_date` datetime(0) NOT NULL,
  `create_by` bigint(255) NOT NULL,
  `update_by` bigint(255) NOT NULL,
  PRIMARY KEY (`id_menu`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_menu
-- ----------------------------
INSERT INTO `master_menu` VALUES (0, 'Menu Utama', 0, 0, '#', '', 'fa-chevron-circle-right', 1, '2018-09-19 07:45:31', '2018-09-19 07:45:34', 1, 1);
INSERT INTO `master_menu` VALUES (2, 'Dashboard', 0, 1, 'dashboard', 'dashboard', 'fas fa-tachometer-alt', 1, '2018-09-28 13:30:43', '2018-09-28 13:30:43', 1, 1);
INSERT INTO `master_menu` VALUES (3, 'Pengaturan', 0, 8, 'setting', 'setting_user_group;setting_access;setting_admin;setting_menu', 'fas fa-cogs', 1, '2019-07-22 21:05:44', '2019-07-22 21:05:44', 2, 2);
INSERT INTO `master_menu` VALUES (4, 'Manajemen Admin', 3, 1, 'setting_admin', 'setting_admin', 'fas fa-user-secret', 1, '2019-07-22 21:07:23', '2019-07-22 21:07:23', 2, 2);
INSERT INTO `master_menu` VALUES (5, 'Manajemen Menu', 3, 2, 'setting_menu', 'skin-blue', 'fas fa-bars', 1, '2019-07-22 21:11:55', '2019-07-22 21:11:55', 2, 2);
INSERT INTO `master_menu` VALUES (6, 'Manajemen User Group', 3, 3, 'setting_user_group', 'setting_user_group', 'fas fa-users', 1, '2019-07-22 21:16:21', '2019-07-22 21:16:21', 2, 2);
INSERT INTO `master_menu` VALUES (7, 'Manajemen Hak Akses', 3, 4, 'setting_access', 'setting_access', 'fas fa-lock', 0, '2019-07-22 21:18:00', '2019-07-22 21:18:00', 2, 2);
INSERT INTO `master_menu` VALUES (8, 'Tentang Aplikasi', 0, 10, 'about_apps', 'about_apps', 'fas fa-info-circle', 1, '2019-07-22 21:43:56', '2019-07-22 21:43:56', 2, 2);
INSERT INTO `master_menu` VALUES (11, 'Master Data', 0, 2, 'master', 'master_simpanan;master_pinjaman', 'fas fa-database', 1, '2019-07-30 22:24:18', '2019-07-30 22:24:18', 2, 2);
INSERT INTO `master_menu` VALUES (12, 'Master Simpanan', 11, 1, 'master_simpanan', 'master_simpanan', 'fas fa-chevron-circle-right', 1, '2021-03-05 23:22:18', '2021-03-05 23:22:18', 1, 1);
INSERT INTO `master_menu` VALUES (14, 'Master Pinjaman', 11, 2, 'master_pinjaman', 'master_pinjaman', 'fas fa-chevron-circle-right', 1, '2021-03-05 23:23:00', '2021-03-05 23:23:00', 1, 1);
INSERT INTO `master_menu` VALUES (18, 'Setting Company', 0, 9, 'setting_company', 'setting_company;kontak;company_profile', 'fas fa-university', 1, '2019-08-03 11:52:12', '2019-08-03 11:52:12', 2, 2);
INSERT INTO `master_menu` VALUES (19, 'Kontak', 18, 1, 'kontak', 'kontak', 'fas fa-chevron-circle-right', 0, '2019-08-03 11:53:08', '2019-08-03 11:53:08', 2, 2);
INSERT INTO `master_menu` VALUES (20, 'Company Profile', 18, 1, 'company_profile', 'company_profile', 'fas fa-chevron-circle-right', 1, '2019-08-03 11:54:24', '2019-08-03 11:54:24', 2, 2);
INSERT INTO `master_menu` VALUES (24, 'Master Data', 23, 1, '#', '#;mata_pelajaran;tipe_soal', 'fas fa-database', 1, '2020-07-03 22:32:25', '2020-07-03 22:32:25', 1, 1);
INSERT INTO `master_menu` VALUES (37, 'Pengelolaan', 0, 2, '#', '#;data_anggota;data_tabungan;data_pinjaman;data_ambil_uang;data_pengajuan', 'fas fa-grip-horizontal', 1, '2021-03-05 23:47:29', '2021-03-05 23:47:29', 1, 1);
INSERT INTO `master_menu` VALUES (38, 'Data Anggota', 37, 1, 'data_anggota', 'data_anggota', 'fas fa-users', 1, '2021-03-05 23:48:43', '2021-03-05 23:48:43', 1, 1);
INSERT INTO `master_menu` VALUES (39, 'Data Tabungan', 37, 3, 'data_tabungan', 'data_tabungan;data_ambil_uang', 'far fa-credit-card', 1, '2021-03-06 00:13:25', '2021-03-06 00:13:25', 1, 1);
INSERT INTO `master_menu` VALUES (40, 'Transaksi', 0, 3, 'transaksi', 'transaksi', 'fas fa-dollar-sign', 1, '2021-03-05 23:57:49', '2021-03-05 23:57:49', 1, 1);
INSERT INTO `master_menu` VALUES (41, 'Data Pengajuan', 37, 3, 'data_pengajuan', 'data_pengajuan', 'fas fa-hand-holding-usd', 1, '2021-03-20 04:48:41', '2021-03-20 04:48:41', 1, 1);

-- ----------------------------
-- Table structure for master_menu_user
-- ----------------------------
DROP TABLE IF EXISTS `master_menu_user`;
CREATE TABLE `master_menu_user`  (
  `id_menu` bigint(255) NOT NULL AUTO_INCREMENT,
  `nama` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `parent` bigint(255) NOT NULL,
  `sequence` int(100) NOT NULL,
  `url` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sub_url` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `icon` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'fa-chevron-circle-right',
  `custom` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `isi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `atasan` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `create_date` datetime(0) NOT NULL,
  `update_date` datetime(0) NOT NULL,
  `create_by` bigint(255) NOT NULL,
  `update_by` bigint(255) NOT NULL,
  PRIMARY KEY (`id_menu`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for master_pinjaman
-- ----------------------------
DROP TABLE IF EXISTS `master_pinjaman`;
CREATE TABLE `master_pinjaman`  (
  `id_pinjaman` int(25) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lama_pinjam` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `maksimal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bunga` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `create_by` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `update_by` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_pinjaman`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_pinjaman
-- ----------------------------
INSERT INTO `master_pinjaman` VALUES (1, 'PNJ202103060001', 'Biasa', '4', '2000000', '2', '-', '1', '1', '1', '2021-03-06 02:35:03', '2021-03-06 02:35:03');
INSERT INTO `master_pinjaman` VALUES (2, 'PNJ202103080001', 'Menengah', '8', '3000000', '4', 'k-', '1', '1', '1', '2021-03-08 23:00:55', '2021-03-09 02:18:03');
INSERT INTO `master_pinjaman` VALUES (3, 'PNJ202103080002', 'Full', '12', '3000000', '10', 'k-', '1', '1', '1', '2021-03-08 23:00:55', '2021-03-09 02:18:03');

-- ----------------------------
-- Table structure for master_simpanan
-- ----------------------------
DROP TABLE IF EXISTS `master_simpanan`;
CREATE TABLE `master_simpanan`  (
  `id_simpanan` int(25) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `besar_simpanan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `create_by` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `update_by` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_simpanan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_simpanan
-- ----------------------------
INSERT INTO `master_simpanan` VALUES (3, 'SIM202103090001', 'tetapi', '20000000', '-a', '1', '1', '1', '2021-03-09 03:09:38', '2021-03-09 03:21:53');
INSERT INTO `master_simpanan` VALUES (4, 'SIM202103090002', 'jangka', '200000', '-\r\n', '1', '1', '1', '2021-03-09 04:17:38', '2021-03-09 04:17:38');

-- ----------------------------
-- Table structure for master_user_group
-- ----------------------------
DROP TABLE IF EXISTS `master_user_group`;
CREATE TABLE `master_user_group`  (
  `id_group` int(255) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `list_id_menu` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `list_access` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime(0) NOT NULL,
  `update_date` datetime(0) NOT NULL,
  `create_by` bigint(255) NOT NULL,
  `update_by` bigint(255) NOT NULL,
  PRIMARY KEY (`id_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_user_group
-- ----------------------------
INSERT INTO `master_user_group` VALUES (1, 'Super User', '0;2;3;4;5;6;8;11;12;14;18;19;20;37;38;39;40;41', '0;1;2;3;4;5;6;7;8;9', 1, '2018-09-11 11:43:09', '2021-03-06 00:13:57', 1, 1);
INSERT INTO `master_user_group` VALUES (2, 'Admin', '0;2;3;4;5;6;8;11;12;14;18;19;20;37;38;39', '0;1;2;3;4;5;6;7;8;9', 1, '2019-08-13 00:37:03', '2021-03-05 23:56:53', 1, 1);
INSERT INTO `master_user_group` VALUES (3, 'Updater', '2;9;11;12;13;14;15;16;17;18;19;20;21;22;23;24;25;26;27;28;29;30;31;32;33;34;35', '0;1;2;3;4;5;6;7;8;9', 1, '2019-08-19 18:56:33', '2020-08-24 05:24:21', 2, 1);
INSERT INTO `master_user_group` VALUES (4, 'Admin Pembelajaran', '2;23;24;25;26;27;28;29;30;31;32;33;34;35', '0;1;2;3;4;5;6;7;8;9', 1, '2020-08-21 14:38:56', '2020-08-21 14:42:41', 1, 1);

-- ----------------------------
-- Table structure for master_user_group_user
-- ----------------------------
DROP TABLE IF EXISTS `master_user_group_user`;
CREATE TABLE `master_user_group_user`  (
  `id_group` int(255) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `list_id_menu` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `list_access` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime(0) NOT NULL,
  `update_date` datetime(0) NOT NULL,
  `create_by` bigint(255) NOT NULL,
  `update_by` bigint(255) NOT NULL,
  PRIMARY KEY (`id_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_user_group_user
-- ----------------------------
INSERT INTO `master_user_group_user` VALUES (1, 'ATASAN', '0;1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20;21;22;23', '0;1;2;3;4;5;6;7;8;9', 1, '2019-03-04 11:38:40', '2019-07-02 09:17:39', 1, 5);
INSERT INTO `master_user_group_user` VALUES (2, 'BAWAHAN', '1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;21;23;20', '0;1;2;3;4;5;6;7;8;9', 1, '2019-03-04 11:56:58', '2019-07-09 11:44:24', 1, 2);
INSERT INTO `master_user_group_user` VALUES (3, 'ASSESSOR KOMPETENSI', '1;18;19', '0;1;2;3;4;5;6;7;8;9', 1, '2019-03-04 11:57:40', '2019-03-04 13:18:12', 1, 1);

-- ----------------------------
-- Table structure for notification
-- ----------------------------
DROP TABLE IF EXISTS `notification`;
CREATE TABLE `notification`  (
  `id_notif` bigint(255) NOT NULL AUTO_INCREMENT,
  `kode_notif` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `judul` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `isi` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `start` datetime(0) NOT NULL,
  `end_date` datetime(0) NOT NULL,
  `kode` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sifat` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipe` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `untuk` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_for` mediumtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_read` mediumtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_del` mediumtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `create_date` datetime(0) NOT NULL,
  `update_date` datetime(0) NOT NULL,
  `create_by` bigint(20) NULL DEFAULT NULL,
  `update_by` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id_notif`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for quote
-- ----------------------------
DROP TABLE IF EXISTS `quote`;
CREATE TABLE `quote`  (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `quote` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `person` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of quote
-- ----------------------------
INSERT INTO `quote` VALUES (1, 'Some beautiful paths can’t be discovered without getting lost.', 'Erol Ozan');
INSERT INTO `quote` VALUES (2, 'Yes I’m seeking for someone, to help me. So that some day I will be the someone to help some other one.', 'Vignesh Karthi');
INSERT INTO `quote` VALUES (3, 'You should be thankful for the journey of life. You only make this journey once in your life time.', 'Lailah Gifty Akita');
INSERT INTO `quote` VALUES (4, 'Stay focus and complete the journey.', 'Lailah Gifty Akita');
INSERT INTO `quote` VALUES (5, 'Though the road’s been rocky it sure feels good to me.', 'Bob Marley');
INSERT INTO `quote` VALUES (6, 'People who can use and save money is the most happy, because he has both a pleasure.', 'Samuel Johnson');
INSERT INTO `quote` VALUES (7, 'If there\'s a book that you want to read, but it hasn\'t been written yet, then you must write it.', 'Toni Morrison');
INSERT INTO `quote` VALUES (8, 'To succeed in life, you need three things: a wishbone, a backbone and a funny bone.', 'Reba McEntire');
INSERT INTO `quote` VALUES (9, 'Choosing to be positive and having a grateful attitude is going to determine how you\'re going to live your life.', 'Joel Osteen');
INSERT INTO `quote` VALUES (10, 'The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart.', 'Helen Keller');

-- ----------------------------
-- Table structure for root_password
-- ----------------------------
DROP TABLE IF EXISTS `root_password`;
CREATE TABLE `root_password`  (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `encrypt` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `plain` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of root_password
-- ----------------------------
INSERT INTO `root_password` VALUES (1, '1a6ee4787231898fa3aa814ad8899f45abf491deb2919152eac45e7cd60e06ad15ba48b8f4beeaf3ffa7218ed71626457c9b80cf6a109651796168d066c57dd9', 'zx2cvb3nm');

-- ----------------------------
-- Table structure for transaksi_tabungan
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_tabungan`;
CREATE TABLE `transaksi_tabungan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_tabungan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nominal` decimal(65, 2) NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `flag` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` tinyint(2) NULL DEFAULT NULL,
  `create_by` tinyint(2) NULL DEFAULT NULL,
  `update_by` tinyint(2) NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transaksi_tabungan
-- ----------------------------
INSERT INTO `transaksi_tabungan` VALUES (1, 'TRANSTAB0001', 'TAB0001', NULL, 10000.00, '2021-03-15', 'pengurang', 1, 1, 1, '2021-03-16 01:23:08', '2021-03-16 01:23:08');
INSERT INTO `transaksi_tabungan` VALUES (2, 'TRANSTAB0002', 'TAB0001', NULL, 10000.00, '2021-03-15', 'pengurang', 1, 1, 1, '2021-03-16 01:30:39', '2021-03-16 01:30:39');

SET FOREIGN_KEY_CHECKS = 1;
